#!/bin/python3 

import imaplib
import email
from email.header import decode_header
import webbrowser
import os, sys
import smtplib
from getpass import getpass, getuser
from argparse import ArgumentParser
import re
from oauth2_imap.config import load_config
from oauth2_imap.imap_client import ImapClient
import datetime

def parseEVMail(contents):
    source = ""
    if "RUN2-Bulk" in contents:
        source = "BLK"
    elif "RUN2-UPD4" in contents:
        source = "ES1"
    contents = contents.replace("=\r\n","")
    #contents = contents.replace("=\n","")   
    contents = contents.replace("=3D","=")   
    contents = contents.replace("^M","")   
    if "To run the update:" in contents:
        return source, contents.split("To run the update:")[0]

    else: return source, contents

def parseMissingEVMail(contents, runNumber):
    source = "BLK" #### is it?!?!?
    contents = "EVs in the local sqlite files which were not in the uploaded db\n"+contents
    contents = contents.split("EventVeto"+str(runNumber)+"_Missing.db")[1]
    contents = contents.split("veto length")[0]+"veto length"
    contents = contents.replace("=\r\n","")
    #contents = contents.replace("=\n","")
    contents = contents.replace("=","=")   
    contents = contents.replace("^M","")   
    return source, contents

def parseUPDMail(contents):   
    if "no UPD3 update is needed for run" in contents or "no UPD4 update is needed for run" in contents:
        return "No cells were flagged as noisy by the LAr DQ shifters for this run", None, None
    CLEANR = re.compile('<.*?>')                 
    contents = re.sub(CLEANR, '', contents).strip()
    contents = contents.replace("\r","")
    delim = "=3D"

    toread = {}
    contents = contents.replace("=\n","")
    contents = contents.replace("=\n","")
    contents = contents.replace("by \nLADIeS","by LADIeS")
    contents = contents.replace("-> \n","-> ")
    contents = contents.replace("by \nLADIeS", "by LADIeS")
    contents = contents.replace("by\nLADIeS", "by LADIeS")
    contents = contents.replace("\nby LADIeS", " by LADIeS")
    WDEstring = "WDE:WebDisplayExtractor"
    if "WDE:dev version" in contents:
        WDEstring = "WDE:dev version"
    try:
        contents = WDEstring+contents.split(WDEstring)[1]
    except Exception as e:
        print("Cannot parse contents")
        print(e)
        print(contents)
        return None, None, None
    #sys.exit()
        
        
    for line in contents.split("\n"):
        if line.startswith("Total number of"):
            line = line.replace("</div>","")
            thisline = line.split("Total number of ")[1].strip()
            category = thisline.split(":")[0]
            try:
                number = thisline.split(":")[1]
            except Exception as e:
                print("uh oh",thisline)
                print("\n****\n")
                print(repr(contents))
                sys.exit()
            if "channels flagged" in category:
                toread["Flagged cells"] = int(number.split("(")[0])
                toread["Flagged in PS"] = int(number.split("(")[1].split("in")[0])
            elif "unflagged" in category:
                toread["Unflagged by DQ shifters"] = int(number)
            elif "modified" in category:
                if "-> sporadicBurstNoise" in category:
                    toread["Changed to SBN"] = int(number)
                elif "-> highNoise" in category:
                    toread["Changed to HNHG"] = int(number)
            elif "sporadicBurstNoise" in category:
                toread["SBN"] = int(number.split("(")[0])
                toread["SBN in PS"] = int(number.split("(")[1].split("in")[0])
            elif "highNoiseHG" in category:
                toread["HNHG"] = int(number.split("(")[0])
                toread["HNHG in PS"] = int(number.split("(")[1].split("in")[0])
        elif line.startswith("Cluster matching"):
            toread["Cluster matching"] = line.split("Cluster matching")[1]
        elif line.startswith("WDE:Web"):
            toread["Tool version"] = line.split("WDE:")[1]
        else: continue


    cellList = None
    thesecontents = contents
    if "Sincerely yours" in thesecontents:
        thesecontents = thesecontents.split("Sincerely yours")[0]
    if "=3D=3D=3D=3D=3D=3D=3D=3DPense bete" in thesecontents:
        thesecontents = thesecontents.split("=3D=3D=3D=3D=3D=3D=3D=3DPense bete")[0]
    if delim not in thesecontents:
        delim = "="
        
    thesecontents = re.sub(CLEANR, '', thesecontents).strip()

    if delim in thesecontents:
        cellList = thesecontents.rsplit(delim,1)[1].strip().split("\n")
        if len(cellList) > 0:
            if 'Flagged cells' in toread.keys():
                if len(cellList) != toread['Flagged cells']:
                    unfl = [ c for c in cellList if "unflaggedByLADIeS" in c ]
                    if  len(cellList) - len(unfl) != toread['Flagged cells']:
                        print("WARNING: The reported number of flagged cells ("+str(toread["Flagged cells"])+") is not the same as the cell list size ("+str(len(cellList))+") ... please check")
                        print(len(unfl), "unflagged")
                        print("Total flagged:",toread["Flagged cells"])
    contents = contents.replace("=3D","=")   
    return toread, cellList, contents


def parseUPDMailNEW(contents):   
    if "no UPD3 update is needed for run" in contents or "no UPD4 update is needed for run" in contents:
        return "No cells were flagged as noisy by the LAr DQ shifters for this run", None, None
    toread = {}
    contents = contents.replace("\r","")
    chanline = None
    for line in contents.split("\n"):
        if "Following channels were added" in line: 
            chanline = line
        line = line.strip("\n")
        if line.startswith("Total number of"):
            thisline = line.split("Total number of ")[1].strip()
            category = thisline.split(":")[0]
            try:
                number = thisline.split(":")[1]
            except Exception as e:
                print("uh oh",thisline)
                print()
                print(repr(contents))
                sys.exit()
            if "channels flagged" in category:
                toread["Flagged cells"] = int(number.split("(")[0])
                toread["Flagged in PS"] = int(number.split("(")[1].split("in")[0])
            elif "unflagged" in category:
                toread["Unflagged by DQ shifters"] = int(number)
            elif "modified" in category:
                if "-> sporadicBurstNoise" in category:
                    toread["Changed to SBN"] = int(number)
                elif "-> highNoise" in category:
                    toread["Changed to HNHG"] = int(number)
            elif "sporadicBurstNoise" in category:
                toread["SBN"] = int(number.split("(")[0])
                toread["SBN in PS"] = int(number.split("(")[1].split("in")[0])
            elif "highNoiseHG" in category:
                toread["HNHG"] = int(number.split("(")[0])
                toread["HNHG in PS"] = int(number.split("(")[1].split("in")[0])
        elif line.startswith("Cluster matching"):
            toread["Cluster matching"] = line.split("Cluster matching")[1]
        elif line.startswith("WDE:Web"):
            toread["Tool version"] = line.split("WDE:")[1]

    cellList = None
    if chanline  is not None:
        delim = "="
        thesecontents = contents.split(chanline)[1]
        cellList = thesecontents.split(delim)[0].strip()
        #cellList = cellList.replace("\r","")
        cellList = cellList.split("\n")
        if 'Flagged cells' in toread.keys():
            if len(cellList) != toread['Flagged cells']:
                unfl = [ c for c in cellList if "unflaggedByLADIeS" in c ]
                if  len(cellList) - len(unfl) != toread['Flagged cells']:
                    print("WARNING: The reported number of flagged cells ("+str(toread["Flagged cells"])+") is not the same as the cell list size ("+str(len(cellList))+") ... please check")
                    print(len(unfl), "unflagged")
                    print("Total flagged:",toread["Flagged cells"])

    return toread, cellList, contents


def connectIMAP(): 
    conf = load_config()
    imap = ImapClient(conf)
    imap_conn = imaplib.IMAP4_SSL(conf.IMAP_SERVER) 
    access_token, username = imap.oauth.get_access_token()
    # Apparently 365 wants clear data, not base64, so set to False
    auth_string = imap.sasl_xoauth2(username, access_token, False)
    #print("username", username)
    imap_conn.authenticate('XOAUTH2', lambda x: auth_string)
    print(imap_conn)
    return imap_conn #, [username,password]


def getMessages(runNumberList=[999999], folder="inbox", local=False, mailmax=20000):
    #if returncred:
    #    imap, cred = connectIMAP(returncred=returncred)
    #else:
    imap = connectIMAP()
    folders =  imap.list()[1]
    folders = [ f.decode('utf-8').replace('"','').split('/ ')[1] for f in folders ]

    if folder not in folders:
        print("Requested folder",folder,"not in mail box... using inbox")
        folder = 'inbox'
    (status, response_text) = imap.select('"{}"'.format(folder))
    message_count = int(response_text[0].decode("ascii"))
    print("Found",message_count,"messages")

    #result, data = imap.uid('search', None, "ALL")
    today = datetime.datetime.now()
    delta = datetime.timedelta(360)
    firstdate = today - delta
    firstdate = firstdate.strftime(format="%d-%b-%Y")

    result, data = imap.uid('search', '(SINCE '+firstdate+')')
    if result != 'OK':
        print("Problem getting emails...")
        sys.exit()
    
    mails = []
    datalist = data[0].split()
    datalist.reverse()
    datalist = datalist[0:-1]  # [0:mailmax]
    print("Found",len(datalist),"messages")

    for num in datalist:
        n = datalist.index(num)
        result, data = imap.uid('fetch', num, '(RFC822)')

        if result != 'OK':
            print("Problem getting mail",num)
            continue
        try:
            email_message = email.message_from_bytes(data[0][1])
        except Exception as e:
            print("Cannot parse email_message")
            print(e)
            print(data)
            sys.exit()

        subject = email_message['Subject'] 

        if not any([str(r) in subject for r in runNumberList]):
            continue

        if n%100 == 0:
            print("Checked",n+1,"/",len(datalist))


        if isinstance(email_message.get_payload(), str):
            content = email_message.get_payload()
        else:
            content = str(email_message.get_payload()[0])

        if subject is None: continue
        if "RE:" in str(subject): continue
        if "Re:" in str(subject): continue


        details = ""
        details += "Subject: "+str(subject)+"\n"
        details += 'From: '+str(email_message['From'])+"\n"
        details += 'To: '+str(email_message['To'])+"\n"
        details += 'Date: '+str(email_message['Date'])+"\n"
        details += "Content: "+str(email_message.get_payload())

        
        printstr=""
        topic = ""
        source = "?"
        cells = None
        fullcontent = None
        parsed = ""
        runNumber = 999999
        
        if "Event Veto " in subject:
            for run in runNumberList:
                if "Event Veto "+str(run) in subject:
                    runNumber = run
                    topic = "Event Veto for run "+str(run)
                    printstr = "*"*10+" "+subject+" for run "+str(run)                       
                    source, parsed = parseEVMail(email_message.get_payload())
                else: continue
        elif "Missing EVs" in subject:
            for run in runNumberList:
                if "EventVeto"+str(run)+"_Missing.db" in content:
                    runNumber = run
                    topic = "Missing EVs for run "+str(run)
                    printstr = "*"*10+" "+subject+" for run "+str(run)   
                    source, parsed = parseMissingEVMail(content, runNumber)
                    if parsed != "":
                        mails.append( {"runNumber": runNumber, "type":topic, "fullcontent": fullcontent, "source":source, "content":parsed, "cells":cells, "date": email_message['Date'] } )
                else: continue
            parsed = ""
        elif "UPD" in subject and "proposal" in subject:           
            for run in runNumberList:
                if str(run) in content:
                    runNumber = run
                    topic = "Noisy cells update for run "+str(run)
                    if "UPD3" in subject:
                        printstr = "*"*10+" UPD3 proposal for run "+str(run)
                        source = "BLK"
                    elif "UPD4" in subject:
                        printstr = "*"*10+" UPD4 proposal for run "+str(run)
                        source = "ES1"
                    print(printstr)
                    parsed, cells, fullcontent = parseUPDMail(content)
                    if parsed is None: continue
                else: continue

        

        elif "UPD" in subject and "Bad channel upload" in subject:
            for run in runNumberList:
                if str(run) in content:
                    runNumber = run
                    topic = "Noisy cells update for run "+str(run)
                    if "UPD3" in subject:
                        printstr = "*"*10+" UPD3 proposal for run "+str(run)
                        source = "BLK"
                    elif "UPD4" in subject:
                        printstr = "*"*10+" UPD4 proposal for run "+str(run)
                        source = "ES1"
                    print(printstr)
                    parsed, cells, fullcontent = parseUPDMailNEW(content)
                else: continue
        else: continue
        
        if parsed == "": continue

        mails.append( {"runNumber": runNumber, "type":topic, "source":source, "fullcontent": fullcontent, "content":parsed, "cells":cells, "date": email_message['Date'] } )
        if local:
            print(printstr)
            if isinstance(parsed, dict):
                for k in parsed.keys():
                    print(k, ":", parsed[k])
            else:
                print(parsed)
            
            if cells is not None:
                print("*"*20)
                print("The flagged cells were:")
                print(("\n").join(cells))
        
    if not local:
        #if returncred:
        #    return mails, cred
        #else:
        return mails 

    imap.close()
    imap.logout()        


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-r', '--run', dest='runNumber', help='Run number(s) which you would like to get information for', type=int, nargs='+', required=True)
    parser.add_argument('-f', '--folder', dest='folder', default='inbox', help='Folder in your emails where the db expert update requests are found.')
    args = parser.parse_args()

    getMessages(runNumberList=args.runNumber, folder=args.folder, local=True)
