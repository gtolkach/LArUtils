Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1687

== Noisy cells update for run 463189 (Sat, 4 Nov 2023 20:32:51 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (f1408_h450)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:0
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:


