Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1690

== Missing EVs for run 463255 (Thu, 26 Oct 2023 12:15:46 +0200) ==

Found a total of 2 noisy periods, covering a total of 0.00 seconds
Found a total of 2 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.07 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.07 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.1993567200782126e-08 by 0.004155515 s of veto length


