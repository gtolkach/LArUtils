Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1686

== Noisy cells update for run 463185 (Sat, 4 Nov 2023 20:41:46 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (f1408_h450)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:0
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:


Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1686

== Missing EVs for run 463185 (Tue, 24 Oct 2023 11:16:24 +0200) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 2 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.2530631043575706e-08 by 0.00335278 s of veto length


