Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1699

== Missing EVs for run 463380 (Sun, 29 Oct 2023 02:15:24 +0100) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.06 nb-1 (0.00 per-mil)
Overall Lumi loss is 3.318713279440999e-09 by 0.001 s of veto length


