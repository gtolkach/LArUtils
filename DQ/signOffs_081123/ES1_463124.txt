Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1682

== Noisy cells update for run 463124 (Tue, 24 Oct 2023 09:07:39 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x797_h448)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:2
Flagged in PS:0
Unflagged by DQ shifters:5
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 6 12 115 0 unflaggedByLADIeS  # 0x3a35f300 -> 0x34b9a000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 1 6 1 124 0 unflaggedByLADIeS  # 0x3b307c00 -> 0x36662000
1 1 6 3 124 0 unflaggedByLADIeS  # 0x3b317c00 -> 0x3666a000
1 1 6 12 115 0 unflaggedByLADIeS  # 0x3b35f300 -> 0x36b94000
1 1 6 13 19 0 unflaggedByLADIeS  # 0x3b361300 -> 0x36b9a000

