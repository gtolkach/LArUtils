Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1699

== Noisy cells update for run 463380 (Sun, 29 Oct 2023 11:12:41 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x798_h449)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:1
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1699

== Event Veto for run 463380 (Sun, 29 Oct 2023 01:28:37 +0200) ==
Found Noise or data corruption in run 463380
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto463380.db

Found project tag data23_hi for run 463380
Found 62 Veto Ranges with 422 events
Found 41 isolated events
Reading event veto info from db sqlite://;schema=EventVeto463380.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-UPD4-13  Run 463380
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto463380.db;dbname=CONDBR2
Found a total of 62 corruption periods, covering a total of 87.50 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.06 nb-1 (1.15 per-mil)
Overall Lumi loss is 6.681494502822774e-05 by 87.49694818 s of veto length



