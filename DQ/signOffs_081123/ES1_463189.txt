Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1687

== Noisy cells update for run 463189 (Wed, 25 Oct 2023 10:22:33 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x798_h449)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:1
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024

