#!/bin/python3
import xmlrpc.client

dqmpassfile="/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"

with open(dqmpassfile, "r") as f:       
    cred = f.readline().strip()
if cred is None:
    print("Problem reading credentials from",dqmpassfile)
dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0'}
firstrun = 427000
lastrun = dqmapi.get_latest_run()

beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]
infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]
checked = {}

def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]

runs = list(divide_chunks( list(range(firstrun,lastrun+1)), 300))
print(len(runs),"chunks")
for runlist in runs:
    run_spec['run_list'] = runlist
    beam_info = dqmapi.get_run_beamluminfo(run_spec)
    newrunlist = [ r for r in runlist if str(r) in beam_info.keys() ]
    newrunlist = [ r for r in newrunlist if not (beam_info[str(r)][1] is False and beam_info[str(r)][2] is False) ]
    run_spec['run_list'] = newrunlist
    run_info = dqmapi.get_run_information(run_spec)
    print(runs.index(runlist)+1, len(runlist), "->", len(newrunlist))
    for run in newrunlist:
        if str(run) not in beam_info.keys(): continue
        this_beam_info = { ik:li for ik,li in zip(beamkeys,beam_info[str(run)]) }
        sb = this_beam_info["Stable beam flag enabled during run"]
        ar = this_beam_info["ATLAS ready flag enabled during run"]
        #print(run, sb, ar)
        #defects = dqmapi.get_defects_lb(run_spec,"", "HEAD", False, False, "Production", True)[str(runNumber)]
        this_run_info = { ik:li for ik,li in zip(infokeys,run_info[str(run)]) }
        checked[run] = this_beam_info
        checked[run].update(this_run_info)
print("*"*40)
print(len(list(checked.keys())),"runs")
sb = [ run for run in checked.keys() if checked[run]["Stable beam flag enabled during run"] is True ]
ar = [ run for run in checked.keys() if checked[run]["ATLAS ready flag enabled during run"] is True ] 
both = set(sb).intersection(ar)
both2 = set(ar).intersection(sb)

print(len(sb),"stable")
print(len(ar),"ready")
print(len(both),"both")
print(len(both2),"both2")


tags = list(set([ checked[run]["Project tag"] for run in checked.keys() ]))
types = list(set([ checked[run]["Run type"] for run in checked.keys() ]))

for tag in tags:
    instance = [ run for run in checked.keys() if checked[run]["Project tag"] == tag ]
    print(tag, ":", len(instance))
for typ in types:
    instance = [ run for run in checked.keys() if checked[run]["Run type"] == typ ]
    print(typ, ":", len(instance))
print()
print()
print()
print()
#print(sb)
#print(ar)
#print(both)
#print(both2)

#print("*"*30)
#print(checked)
