import sys
import pandas as pd
import numpy as np

import ROOT as R
from CaloMonitoring.LArCellBinning import lArCellBinningScheme

#from LArCellBinning import lArCellBinningScheme
R.gROOT.SetBatch(True)

verbose = False

PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"
sys.path.append(PrintMappingDir)
import printMapping as pm

pmdf1 = pd.read_csv("cellEtaPhi.txt", sep=" ", names=["ONL_ID", "ETA", "PHI"]) 
want = [ "ONL_ID", "DETNAME", "SAM" ]
print("Query printmapping")
pmout = pm.query(want = want, showdecimal = True)
pmdf2 = pd.DataFrame(pmout, columns = want)
pmdf1 = pmdf1.astype({"ONL_ID": int})
pmdf2 = pmdf2.astype({"ONL_ID": int})
pmdf2 = pmdf2.astype({"SAM": int})
pmdf = pd.merge(pmdf1, pmdf2, on="ONL_ID")

for col in [ "ETA", "PHI" ]:
    pmdf = pmdf.astype({col: float})

hists = {}
fillval = {}
count = {}
for layerName in lArCellBinningScheme.etaRange.keys():
    xbins = np.array(lArCellBinningScheme.etaRange[layerName], dtype='d')
    ybins = np.array(lArCellBinningScheme.phiRange[layerName], dtype='d')

    hists[layerName] = R.TH2F("h_"+layerName, "h_"+layerName, len(xbins)-1, xbins, len(ybins)-1, ybins)
    fillval[layerName] = {}
    count[layerName] = 0

refilled={}
toprint_full = {}
toprint_empty = {}


for onID in pmdf.ONL_ID.values:    
    thisRow = pmdf.loc[pmdf["ONL_ID"] == onID]

    if len(thisRow.index) != 1:
      print("NO... ", thisRow)
      sys.exit()
    eta = float(thisRow["ETA"].values[0])
    phi = float(thisRow["PHI"].values[0])
    sam = thisRow["SAM"].values[0]
    detname = thisRow["DETNAME"].values[0]
    # Do we need this?
    if "EMB" in detname and abs(eta) > 1.4:
        if sam > 0:
            sam = str(int(sam)+1)
    if int(sam) == 0 and "HEC" not in detname:
      sam = "P"
    detname = detname[:-1]+str(sam)+detname[-1]
    
    thisbin = hists[detname].FindBin(eta,phi)

    thisbin = hists[detname].FindBin(eta,phi)
    if thisbin not in fillval[detname].keys():
      fillval[detname][thisbin] = []
    fillval[detname][thisbin].append(onID)
    content = hists[detname].GetBinContent(thisbin)
    if content != 0:
        print(f"WARNING!! Bin {thisbin} already filled in {detname} with {content} - trying to fill with {onID}")
        if detname not in refilled.keys():
            refilled[detname] = {}
        if thisbin not in refilled[detname].keys():
            refilled[detname][thisbin] = 1
        else:
            refilled[detname][thisbin] += 1
    #hists[detname].Fill(eta,phi,onID)
    hists[detname].SetBinContent(thisbin, onID)
    
    if count[detname] < 10:
        if verbose: print("Fill",detname, thisbin, eta, phi, onID)
    count[detname] +=1

outfile = R.TFile("onIDs.root", "RECREATE")
for detname in hists.keys():
    hists[detname].SetDirectory(outfile)
    hists[detname].Write()

print(refilled)
