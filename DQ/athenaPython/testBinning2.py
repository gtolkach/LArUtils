import sys
import ROOT as R
import numpy as np
import pandas as pd
import ctypes
PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"

from CaloMonitoring.LArCellBinning import lArCellBinningScheme

sys.path.append(PrintMappingDir)
import printMapping as pm

pmdf1 = pd.read_csv("cellEtaPhi.txt", sep=" ", names=["ONL_ID", "ETA", "PHI"]) 

plots = {}
for subdet in ["EMB", "EMEC", "HEC", "FCAL" ]:
    plots[subdet] = {}
    for side in ["A", "C"]:
        plots[subdet][side] = {}
        for layer in [0, 1, 2, 3]:
            #plots[subdet][side][layer] = {}
            if layer == 0 and subdet.startswith("EM"):
                thislayer = "P"
            else:
                thislayer = str(layer)

            part = subdet+thislayer+side
            if part not in lArCellBinningScheme.etaRange.keys():
                plots[subdet][side][layer] = None
            else:
                xbins = np.array(lArCellBinningScheme.etaRange[part], dtype='d')
                ybins = np.array(lArCellBinningScheme.phiRange[part], dtype='d')
                plots[subdet][side][layer] = R.TH2D("h_"+part, "h_"+part, len(xbins)-1, xbins, len(ybins)-1, ybins)

isSC = False 

if isSC is False:
    onid = "ONL_ID"
    eta = "ETA"
    phi = "PHI"
else:
    onid = "SC_ONL_ID"
    eta = "SCETA"
    phi = "SCPHI"

want = [ onid, "DETNAME", "SAM" ]
print("Query printmapping")
pmout = pm.query(want = want, showdecimal = True)
pmdf2 = pd.DataFrame(pmout, columns = want)


pmdf1 = pmdf1.astype({onid: int})
pmdf2 = pmdf2.astype({onid: int})
pmdf2 = pmdf2.astype({"SAM": int})
pmdf = pd.merge(pmdf1, pmdf2, on=onid)

for col in [ eta, phi ]:
    pmdf = pmdf.astype({col: float})

#function returning Series
def findBin(detname, layer, eta, phi):
    #detname = x.DETNAME
    det = detname[:-1]
    side = detname[-1:]
    if "EMB" in detname and abs(eta) > 1.4:
      layer = int(layer)+1
    thisplot = plots[det][side][layer]
    try:
        nBin = thisplot.FindBin(eta, phi)
    except:
        print(type(thisplot), det, side,layer)
        nBin = None
    return nBin




pmdf['nBin'] = pmdf.apply(lambda row : findBin(row["DETNAME"], row["SAM"], row[eta], row[phi]), axis = 1)
for layerName in lArCellBinningScheme.etaRange.keys():
    det = layerName[:-2]
    layer = layerName[-2:][0]
    side = layerName[-2:][1]
    detname = det+side
    if layer == "P": layer = 0
    thisplot = plots[det][side][int(layer)]

    detLayRows = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer) ) ]
    if "EMB" in detname:
        if layer == 1:
            detLayRows = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer)  | ( ( pmdf["SAM"] == 0 ) & ( pmdf["ETA"].abs() > 1.4 ) ) ) ]
        elif layer == 0:
            detLayRows = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer) ) & ( pmdf["ETA"].abs() <= 1.4 ) ]
                                
    ncell = len(detLayRows.index.values)
    nbins = len(list(set(detLayRows['nBin'].values)))
    if ncell != nbins:
        print(layerName,detname, ncell, "cells with",nbins,"bins")
        counts = detLayRows.value_counts("nBin",sort=True)
        repeatBins = list(set(counts[counts > 1].index))

        for nb in repeatBins:
            def getLoHi(nb):
                x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
                thisplot.GetBinXYZ(nb, x, y, z)
                xlo = thisplot.GetXaxis().GetBinLowEdge(x.value)
                xhi = thisplot.GetXaxis().GetBinUpEdge(x.value)
                ylo = thisplot.GetYaxis().GetBinLowEdge(y.value)
                yhi = thisplot.GetYaxis().GetBinUpEdge(y.value)
                return xlo, xhi, ylo, yhi
            for ni in range(nb-4, nb+4):
                xlo, xhi, ylo, yhi = getLoHi(ni)
                print(layerName, xlo, xhi, ylo, yhi)
                theseCells = detLayRows[ (detLayRows[eta] >= xlo) & (detLayRows[eta] <= xhi) & (detLayRows[phi] >= ylo) & (detLayRows[phi] <= yhi) ]
                if len(theseCells.index.values) == 1:
                    thisonlid = theseCells[onid].values[0]
                    pmdf.loc[pmdf[onid] == thisonlid, "nBin"] = ni
                elif len(theseCells.index.values) > 1:
                    print("OIOIOIOI NO",detname, layer,len(theseCells.index.values))
                    print(theseCells)
                    sys.exit()

    print("*"*40)
    detLayRows = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer) ) ]
    
    ncell = len(detLayRows.index.values)
    nbins = len(list(set(detLayRows['nBin'].values)))

    if ncell != nbins:
        print(layerName, ncell, nbins)
        counts = detLayRows.value_counts("nBin",sort=True)
        repeatBins = list(set(counts[counts > 1].index))
        newtest = detLayRows[ detLayRows['nBin'].isin(repeatBins) ]
        print(newtest)
