import sys
from argparse import ArgumentParser
#import pickle as pkl
import xmlrpc.client
import ROOT as R
R.gROOT.SetBatch(True)


sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/LArPage1/makeJIRA")
#sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/UPDTools")
sys.path.append("/afs/cern.ch/user/e/ekay/LAr/UPDTools")
import detmask
import CoolTools
dqmsite = "atlasdqm.cern.ch"
dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
dqmpass = None
with open(dqmpassfile, "r") as f:
    dqmpass = f.readlines()[0].strip()
    if ":" not in dqmpass:
        print("Problem reading dqmpass")
        sys.exit()
    dqmapi = xmlrpc.client.ServerProxy("https://"+dqmpass+"@"+dqmsite)


infokeys = ["Run type", "Project tag", "Partition name", "Number of events passing Event Filter", "Run start", "Run end", "Number of luminosity blocks", "Data source", "Detector mask", "Recording enabled", "Number of events in physics streams" ]

beamkeys = ["Max beam energy during run", "Stable beam flag enabled during run", "ATLAS ready flag enabled during run", "Total integrated luminosity", "ATLAS ready luminosity (/nb)"]

magkeys = ["Soleniod actual current", "Toroid actual current", "Solenoid set current", "Toroid set current"]

def getRunList(runList, runFile, nruns, lowrun, highrun):
    run_spec = {}
    if runFile is not None:
        with open(runFile, "r") as f:
            run_spec['run_list'] = [ int(l.strip("\n").strip()) for l in f.readlines() ]
    else:
        if runList is None:
            if highrun is None:
                if lowrun is not None:
                    if nruns is not None:
                        highrun=lowrun+nruns
                    else:
                        highrun=dqmapi.get_latest_run()
                else:
                    highrun=dqmapi.get_latest_run()

            if lowrun is None:
                if nruns is not None:
                    lowrun = highrun-nruns
                else:
                    lowrun = highrun-1000
            #print("Low run:",lowrun, ".. High run:",highrun)
            run_spec['low_run'] = lowrun
            run_spec['high_run'] = highrun 
        else:
            run_spec['run_list'] = runList
    return run_spec


def getRunInfo(run_spec):
    nrun = 0
    if 'low_run' in run_spec.keys() and 'high_run' in run_spec.keys():
        nrun = run_spec['high_run'] - run_spec['low_run'] 
    elif 'run_list' in run_spec.keys():
        nrun = len(run_spec['run_list']) #max(run_spec['run_list']) - min(run_spec['run_list'])

    print(run_spec, nrun)
    if nrun < 30:
        defects = dqmapi.get_defects_lb(run_spec,"","HEAD",False,False,"Production",True)
        # Usage: get_defects_lb(run_spec, defects, tag, with_time, nonpresent, db, with_full_info, ignore)
        # CLdeadline = dqmapi.get_end_of_calibration_period(run_spec)
        run_info = dqmapi.get_run_information(run_spec)
        run_periods = dqmapi.get_data_periods(run_spec)
        # run_streams = dqmapi.get_runs_streams(run_spec)
        # run_ami = dqmapi.get_procpass_amitag_mapping(run_spec)
        run_mag = dqmapi.get_run_magfields(run_spec)
        beam_info = dqmapi.get_run_beamluminfo(run_spec)
    else:
        calls = []
        callName = []
        this_runspec = run_spec.copy()
        defects = {}
        run_periods = {}
        # CLdeadline = {}
        run_info = {}
        # run_streams = {}
        # run_ami = {}
        run_mag = {}
        beam_info = {}

        chunksize = 20
        if 'low_run' in run_spec.keys() and 'high_run' in run_spec.keys():
            del this_runspec['low_run']
            del this_runspec['high_run']
            theseRuns = [ (x,x+(chunksize-1)) for x in range(run_spec['low_run'], run_spec['high_run']+1,chunksize) ]
        else:
            theseRuns = [run_spec['run_list'][i:i+chunksize] for i in range(0,len(run_spec['run_list']),chunksize)]
            del this_runspec['run_list']
        for chunk in theseRuns:
            print(f"Chunk {theseRuns.index(chunk)+1} / {len(theseRuns)} ({chunk})")
            if isinstance(chunk,tuple):
                this_runspec['low_run'] = chunk[0]
                if chunk[1] <= run_spec['high_run']:
                    this_runspec['high_run'] = chunk[1]
                else:
                    this_runspec['high_run'] = run_spec['high_run']
            else:
                this_runspec['run_list'] = chunk
            defects.update(dqmapi.get_defects_lb(this_runspec,"","HEAD",False,False,"Production",True))
            run_periods.update(dqmapi.get_data_periods(this_runspec))
            # CLdeadline.update(dqmapi.get_end_of_calibration_period(this_runspec))
            run_info.update(dqmapi.get_run_information(this_runspec))
            # run_streams.update(dqmapi.get_runs_streams(this_runspec))
            # run_ami.update(dqmapi.get_procpass_amitag_mapping(this_runspec))
            run_mag.update(dqmapi.get_run_magfields(this_runspec))
            beam_info.update(dqmapi.get_run_beamluminfo(this_runspec))
    for ri in run_info.keys():
        run_info[ri] = { ik:li for ik,li in zip(infokeys,run_info[ri]) }
    #for ra in run_ami.keys():
    #    run_ami[ra] = [str(b)+" (pass "+str(a)+", ["+str(c)+"|http://ami.in2p3.fr:8080/?subapp=tagsShow&userdata="+str(c)+"])" for a,b,c in run_ami[ra]]
    for bi in beam_info.keys():
        beam_info[bi] = { ik:li for ik,li in zip(beamkeys,beam_info[bi]) }
    for mi in run_mag.keys():
        run_mag[mi] = { ik:li for ik,li in zip(magkeys,run_mag[mi]) }

    return run_info, beam_info, defects, run_periods, run_mag

def sort_runs(run_spec, tag, onlySB, onlyLAr):

    run_info, beam_info, defects, run_periods, run_mag = getRunInfo(run_spec)

    runs = run_info.keys()
    # HERE if run_list in run_spec, compare to this

    if 'low_run' in run_spec.keys() and 'high_run' in run_spec.keys():
        del run_spec['low_run']
        del run_spec['high_run']
    elif 'run_list' in run_spec.keys():
        del run_spec['run_list']

    run_list = []
    all_info = {}
    for run in runs:
        detectors = detmask.DecodeDetectorMaskToString(int(run_info[run]["Detector mask"]), False)
        larenabled = "lar" in detectors[0].lower()
        if onlyLAr and not larenabled:
            print(f"LAr was not enabled for run {run}, therefore skipping")
            continue
        if onlySB and beam_info[run]["Stable beam flag enabled during run"] == 0:
            print(f"Run {run} did not have stable beams, therefore skipping")
            continue
        if tag is not None and run_info[run]["Project tag"] != tag:
            print(f"Run {run} has tag {run_info[run]['Project tag']}, which does not match the requested {tag} - skipping")
            continue
        all_info[run] = {}
        all_info[run].update(run_info[run])
        if run in beam_info.keys():
            all_info[run].update(beam_info[run])
        if run in defects.keys():
            all_info[run]["Defects"] = defects[run]
        if run in run_periods.keys():
            all_info[run]["Period"] = run_periods[run]
        if run in run_mag.keys():
            all_info[run].update(run_mag[run])
        run_list.append(run)
        
    print(f"Left with {len(run_list)} runs: {run_list}")
    run_spec['run_list'] = run_list
    return all_info, run_spec

def plotDefects(all_info, outtag):
    intol = ["HVTRIP","SEVNOISEBURST","SEVCOVERAGE","HVNONNOMINAL","SEVNOISYCHANNEL","SEVMISCALIB","SEVUNKNOWN"] # LAR Prefix - LAR_[PART]_[NAME]
    col = [ R.kBlue, R.kRed, R.kOrange, R.kAzure-4, R.kGreen+2, R.kViolet-9, R.kMagenta ]
    prefix = "LAR"

    runs = list(all_info.keys())
    defectHist = R.TH1F("Defects","Defects", len(runs), 0, len(runs))
    defectHist.GetXaxis().SetTitle("Run Number")
    defectHist.GetYaxis().SetTitle("Lost Luminosity Due To Defects [%]")
    canv = R.TCanvas("","",1200,600)
    canv.objs = []
    canv.SetRightMargin(.25)
    canv.SetBottomMargin(.15)
    canv.SetTopMargin(.05)
    canv.SetGridy(1)
    leg = R.TLegend(0.76,0.3,0.98,0.9) 
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(0.037)
    R.gStyle.SetOptStat(0)

    ths1 = R.THStack ("test1","test1")
    hists = {}
    for i in intol:
        hists[i] = defectHist.Clone()
        hists[i].SetName(i)
        hists[i].SetTitle(i)
        hists[i].SetFillColor(col[intol.index(i)])
        leg.AddEntry(hists[i], i, "f")
    h_recov = defectHist.Clone()
    h_recov.SetName("Recoverable")
    h_recov.SetTitle("Recoverable")
    h_recov.SetMarkerStyle(20)
    h_recov.SetMarkerColor(R.kCyan)
    leg.AddEntry(h_recov, "Recoverable", "p")
    for run in runs:
        print(run, runs.index(run)+1, str(run))
        #defectHist.GetXaxis().SetBinLabel(runs.index(run)+1, str(run))
        h_recov.GetXaxis().SetBinLabel(runs.index(run)+1, str(run))
        defects = all_info[run]["Defects"]
        global_defects = [ g for g in defects.keys() if g.startswith("GLOBAL")]
        sb = CoolTools.GetStableBeams(int(run))
        ar = CoolTools.GoodLB(int(run))
        gl = []
        for gd in global_defects:
            this_gd = defects[gd]
            for tgd in this_gd:
                lbs = list(range(tgd[0], tgd[1]))
                gl.extend(lbs)
        goodLB = [ lb for lb in ar if sb[lb] == 1 ]
        print(f"{len(goodLB)} good LBs (SB & ATLAS ready) out of {all_info[run]['Number of luminosity blocks']}")
        torm = [ glb for glb in goodLB if glb in gl ]
        print(f"Will also ignore {len(torm)} LB(s) ({torm}) based on global defects")
        goodLB = [ glb for glb in goodLB if glb not in torm ]

        if len(goodLB) == 0 : continue 

        defects = { k:v for k,v in defects.items() if k.startswith(prefix) }
        
        toPlot = {i:[] for i in intol}
        toPlot_recov = []
        for de in defects.keys():
            if not any(i in de for i in intol): 
                print("skip",de)
                continue
            else:
                this_intol = [ i for i in intol if i in de ][0]
                for tde in defects[de]:
                    lbs = list(range(tde[0], tde[1]))
                    lbs = [ lb for lb in lbs if lb in goodLB ]
                    toPlot[this_intol].extend(lbs)
                    recov = tde[-2]
                    if recov:
                        toPlot_recov.extend(lbs)
        for de in toPlot.keys():
            toPlot[de] = 100* (len(list(set(toPlot[de]))) / len(goodLB))
            hists[de].SetBinContent(runs.index(run)+1, toPlot[de])           
        toPlot_recov = list(set(toPlot_recov))
        toPlot_recov = 100* (len(toPlot_recov) / len(goodLB))
        h_recov.SetBinContent(runs.index(run)+1, toPlot_recov)
        print("recov",toPlot_recov)
    for de in hists.keys():
        #hists[de].GetXaxis().SetBinLabel(runs.index(run)+1, str(run))
        ths1.Add(hists[de])

    h_recov.SetMaximum(ths1.GetMaximum())
    h_recov.SetMinimum(ths1.GetMinimum())
    #h_recov.Draw("hist,p")    
    #ths1.Draw("same,hist")
    ths1.Draw("hist")
    canv.objs.append(ths1)
    h_recov.Draw("p,same")    
    canv.objs.append(h_recov)
    for run in runs:
        ths1.GetXaxis().SetBinLabel(runs.index(run)+1, str(run))
    ths1.SetTitle("")
    ths1.GetXaxis().LabelsOption("v")
    ths1.GetXaxis().SetTitle("Run Number")
    ths1.GetXaxis().SetTitleOffset(2)
    ths1.GetYaxis().SetTitle("Lost Luminosity Due To Defects [%]")
    leg.Draw()
    canv.objs.append(leg)


    intruns = [int(r) for r in runs]
    lowrun = str(min(intruns))
    higrun = str(max(intruns))

    canv.Print(f"vetoed_{lowrun}-{higrun}_{outtag}.png")
        
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-f', '--runFile', dest='runFile', default=None, type=str, help='Read list of runs from a file')
    #parser.add_argument('-p', '--pickleDir', dest='pickleDir', default=None, type=str, help='Directory containing pickle files with queried data')
    parser.add_argument('-n', '--nruns', dest='nruns', default=None, type=int, help='If a run range is not defined - search for this number of runs above high run / below low run')
    parser.add_argument('-s', '--lowrun', dest='lowrun', default=None, type=int, help='Search for runs starting from this run number')
    parser.add_argument('-e', '--highrun', dest='highrun', default=None, type=int, help='Search for runs with a maximum of this run number')
    parser.add_argument('-r', '--runList', dest='runList', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
    parser.add_argument('--onlyLAr', dest='onlyLAr', action='store_true', help='Only show runs where LAr is enabled')
    parser.add_argument('--onlySB', dest='onlySB', action='store_true', help='Only look at stable beam runs')
    parser.add_argument('-t', '--tag', dest='tag', default=None, type=str, help='Only consider a specific data tag e.g. data23_13p6TeV')
    args = parser.parse_args()

    run_spec = getRunList(args.runList, args.runFile, args.nruns, args.lowrun, args.highrun)

    run_spec.update({'stream': 'physics_CosmicCalo', 'source': 'tier0'})
    

    all_info, run_spec = sort_runs(run_spec, args.tag, args.onlySB,  args.onlyLAr)

    outtag = ""
    if args.tag is not None:
        outtag = args.tag
    else:
        outtag = "anyTag"
    if args.onlyLAr:
        outtag += "_LArEnabled"
    if args.onlySB:
        outtag += "_SB"

    plotDefects(all_info, outtag)
