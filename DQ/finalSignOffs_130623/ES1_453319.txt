Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1177

== Noisy cells update for run 453319 (Thu, 1 Jun 2023 16:49:29 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:109
Flagged in PS:32
Unflagged by DQ shifters:31
Changed to SBN:26
Changed to HNHG:1
SBN:95
SBN in PS:26
HNHG:14
HNHG in PS:6

*****************
The treated cells were:
0 0 1 14 56 0 sporadicBurstNoise  # 0x380eb800 -> 0x2d406ce6
0 0 3 13 125 0 sporadicBurstNoise  # 0x381e7d00 -> 0x2d405ec4
0 0 5 1 0 0 sporadicBurstNoise  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 1 60 0 highNoiseHG  # 0x38283c00 -> 0x2d003828
0 0 7 10 41 0 sporadicBurstNoise  # 0x383ca900 -> 0x2d60348c
0 0 7 11 53 0 sporadicBurstNoise  # 0x383d3500 -> 0x2d401a8c
0 0 7 14 33 0 sporadicBurstNoise  # 0x383ea100 -> 0x2d40688c
0 0 7 14 44 0 sporadicBurstNoise  # 0x383eac00 -> 0x2d406e8e
0 0 7 14 45 0 sporadicBurstNoise  # 0x383ead00 -> 0x2d406e8c
0 0 7 14 50 0 sporadicBurstNoise  # 0x383eb200 -> 0x2d406882
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 9 10 80 0 sporadicBurstNoise  # 0x384cd000 -> 0x2d602866
0 0 9 10 81 0 sporadicBurstNoise  # 0x384cd100 -> 0x2d602864
0 0 9 11 64 0 sporadicBurstNoise  # 0x384d4000 -> 0x2d400066
0 0 9 13 96 0 highNoiseHG  # 0x384e6000 -> 0x2d405066
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 12 11 75 0 sporadicBurstNoise  # 0x38654b00 -> 0x2d400430
0 0 13 1 22 0 sporadicBurstNoise  # 0x38681600 -> 0x2d001408
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 13 2 68 0 sporadicBurstNoise  # 0x3868c400 -> 0x2d200808
0 0 13 2 69 0 sporadicBurstNoise  # 0x3868c500 -> 0x2d200a08
0 0 13 11 4 0 deadReadout highNoiseHG  # 0x386d0400 -> 0x2d40022e
0 0 15 5 60 0 unflaggedByLADIeS  # 0x387a3c00 -> 0x2d21f802
0 0 15 5 61 0 sporadicBurstNoise  # 0x387a3d00 -> 0x2d21fa02
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 12 72 0 unflaggedByLADIeS  # 0x387dc800 -> 0x2d402406
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 15 14 44 0 unflaggedByLADIeS  # 0x387eac00 -> 0x2d406e0e
0 0 16 13 0 0 unflaggedByLADIeS  # 0x38860000 -> 0x2d4041fe
0 0 17 2 12 0 sporadicBurstNoise  # 0x38888c00 -> 0x2d20187a
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 22 1 19 0 sporadicBurstNoise  # 0x38b01300 -> 0x2d001666
0 0 23 2 14 0 sporadicBurstNoise  # 0x38b88e00 -> 0x2d201c62
0 0 23 2 15 0 sporadicBurstNoise  # 0x38b88f00 -> 0x2d201e62
0 0 23 2 16 0 sporadicBurstNoise  # 0x38b89000 -> 0x2d202062
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 14 10 0 sporadicBurstNoise  # 0x38c68a00 -> 0x2d40657a
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 29 14 18 0 sporadicBurstNoise  # 0x38ee9200 -> 0x2d406122
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 0 9 58 0 unflaggedByLADIeS  # 0x39043a00 -> 0x2de01c04
0 1 0 11 23 0 unflaggedByLADIeS  # 0x39051700 -> 0x2dc00a06
0 1 0 11 27 0 sporadicBurstNoise  # 0x39051b00 -> 0x2dc00c06
0 1 0 13 0 0 unflaggedByLADIeS  # 0x39060000 -> 0x2dc04000
0 1 0 13 4 0 unflaggedByLADIeS  # 0x39060400 -> 0x2dc04200
0 1 0 13 8 0 unflaggedByLADIeS  # 0x39060800 -> 0x2dc04400
0 1 0 14 8 0 unflaggedByLADIeS  # 0x39068800 -> 0x2dc06400
0 1 0 14 12 0 unflaggedByLADIeS  # 0x39068c00 -> 0x2dc06600
0 1 0 14 32 0 unflaggedByLADIeS  # 0x3906a000 -> 0x2dc06800
0 1 0 14 36 0 unflaggedByLADIeS  # 0x3906a400 -> 0x2dc06a00
0 1 0 14 40 0 unflaggedByLADIeS  # 0x3906a800 -> 0x2dc06c00
0 1 2 11 46 0 sporadicBurstNoise  # 0x39152e00 -> 0x2dc01624
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 6 12 64 0 sporadicBurstNoise  # 0x3935c000 -> 0x2dc02068
0 1 8 1 70 0 sporadicBurstNoise  # 0x39404600 -> 0x2d804422
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 16 1 43 0 sporadicBurstNoise  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 16 11 125 0 sporadicBurstNoise  # 0x39857d00 -> 0x2dc01f0a
0 1 17 1 36 0 sporadicBurstNoise  # 0x39882400 -> 0x2d802046
0 1 17 1 40 0 sporadicBurstNoise  # 0x39882800 -> 0x2d802844
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 17 6 32 0 sporadicBurstNoise  # 0x398aa000 -> 0x2da24044
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 1 3 0 sporadicBurstNoise  # 0x39a80300 -> 0x2d800654
0 1 21 3 9 0 sporadicBurstNoise  # 0x39a90900 -> 0x2da09254
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 11 10 0 sporadicBurstNoise  # 0x39ad0a00 -> 0x2dc00554
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 23 2 3 0 sporadicBurstNoise  # 0x39b88300 -> 0x2da0065c
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 31 13 71 0 unflaggedByLADIeS  # 0x39fe4700 -> 0x2dc043fe
0 1 31 13 75 0 unflaggedByLADIeS  # 0x39fe4b00 -> 0x2dc045fe
0 1 31 13 79 0 unflaggedByLADIeS  # 0x39fe4f00 -> 0x2dc047fe
0 1 31 13 83 0 unflaggedByLADIeS  # 0x39fe5300 -> 0x2dc049fe
0 1 31 13 91 0 unflaggedByLADIeS  # 0x39fe5b00 -> 0x2dc04dfe
0 1 31 13 95 0 unflaggedByLADIeS  # 0x39fe5f00 -> 0x2dc04ffe
0 1 31 13 107 0 unflaggedByLADIeS  # 0x39fe6b00 -> 0x2dc055fe
0 1 31 13 111 0 unflaggedByLADIeS  # 0x39fe6f00 -> 0x2dc057fe
0 1 31 13 115 0 unflaggedByLADIeS  # 0x39fe7300 -> 0x2dc059fe
0 1 31 13 119 0 unflaggedByLADIeS  # 0x39fe7700 -> 0x2dc05bfe
0 1 31 13 123 0 unflaggedByLADIeS  # 0x39fe7b00 -> 0x2dc05dfe
0 1 31 13 126 0 deadPhys unflaggedByLADIeS  # 0x39fe7e00 -> 0x2dc05ffc
0 1 31 13 127 0 unflaggedByLADIeS  # 0x39fe7f00 -> 0x2dc05ffe
0 1 31 14 19 0 unflaggedByLADIeS  # 0x39fe9300 -> 0x2dc061fe
0 1 31 14 23 0 unflaggedByLADIeS  # 0x39fe9700 -> 0x2dc063fe
0 1 31 14 27 0 unflaggedByLADIeS  # 0x39fe9b00 -> 0x2dc065fe
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 101 0 sporadicBurstNoise  # 0x3a9ce500 -> 0x2cc4196c
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 1 5 10 33 0 sporadicBurstNoise  # 0x3b2ca100 -> 0x2e440e72
1 1 5 10 51 0 unflaggedByLADIeS  # 0x3b2cb300 -> 0x2e440e7e
1 1 9 4 76 0 sporadicBurstNoise  # 0x3b49cc00 -> 0x2e4404e0
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 12 4 14 0 sporadicBurstNoise  # 0x3b618e00 -> 0x2e2c1c38
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38
1 1 12 4 16 0 sporadicBurstNoise  # 0x3b619000 -> 0x2e2c2038
1 1 13 9 17 0 sporadicBurstNoise  # 0x3b6c1100 -> 0x2e601512
1 1 13 11 109 0 sporadicBurstNoise  # 0x3b6d6d00 -> 0x2e442d12
1 1 13 12 31 0 sporadicBurstNoise  # 0x3b6d9f00 -> 0x2e44350e
1 1 13 12 32 0 sporadicBurstNoise  # 0x3b6da000 -> 0x2e442f10
1 1 13 12 33 0 sporadicBurstNoise  # 0x3b6da100 -> 0x2e442f12
1 1 13 12 34 0 highNoiseHG  # 0x3b6da200 -> 0x2e442f14
1 1 13 12 35 0 sporadicBurstNoise  # 0x3b6da300 -> 0x2e442f16
1 1 13 12 37 0 sporadicBurstNoise  # 0x3b6da500 -> 0x2e443112
1 1 13 12 41 0 sporadicBurstNoise  # 0x3b6da900 -> 0x2e443312
1 1 22 2 88 0 highNoiseHG  # 0x3bb0d800 -> 0x2ec00c70

