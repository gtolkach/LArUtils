Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1142

== Noisy cells update for run 452163 (Sun, 28 May 2023 12:21:01 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:26
Flagged in PS:13
Unflagged by DQ shifters:0
Changed to SBN:13
Changed to HNHG:0
SBN:26
SBN in PS:13
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 14 2 120 0 sporadicBurstNoise  # 0x3870f800 -> 0x2d207004
0 0 16 13 19 0 sporadicBurstNoise  # 0x38861300 -> 0x2d4049f8
0 0 23 1 5 0 highNoiseHG sporadicBurstNoise  # 0x38b80500 -> 0x2d000260
0 0 26 1 80 0 sporadicBurstNoise  # 0x38d05000 -> 0x2d005056
0 0 30 1 12 0 sporadicBurstNoise  # 0x38f00c00 -> 0x2d000844
0 0 30 1 13 0 sporadicBurstNoise  # 0x38f00d00 -> 0x2d000a44
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 8 1 77 0 sporadicBurstNoise  # 0x39404d00 -> 0x2d804a22
0 1 8 1 78 0 sporadicBurstNoise  # 0x39404e00 -> 0x2d804c22
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 126 0 sporadicBurstNoise  # 0x3aae7e00 -> 0x2ce02702
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 118 0 sporadicBurstNoise  # 0x3aaf7600 -> 0x2cc45102
1 0 21 15 122 0 sporadicBurstNoise  # 0x3aaf7a00 -> 0x2cc45302
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1142

== Event Veto for run 452163 (Mon, 22 May 2023 10:58:19 +0200) ==
Found Noise or data corruption in run 452163
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452163_Main.db

Found project tag data23_13p6TeV for run 452163
Found 16 Veto Ranges with 10264 events
Found 49 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452163_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452163
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452163_Main.db;dbname=CONDBR2
Found a total of 16 corruption periods, covering a total of 14.57 seconds
Lumi loss due to corruption: 256.54 nb-1 out of 664069.72 nb-1 (0.39 per-mil)
Overall Lumi loss is 256.543620139045 by 14.573254325 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1142

== Event Veto for run 452163 (Mon, 22 May 2023 10:09:45 +0200) ==
Found Noise or data corruption in run 452163
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452163_Main.db

Found project tag data23_13p6TeV for run 452163
Found 16 Veto Ranges with 10264 events
Found 49 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452163_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452163
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452163_Main.db;dbname=CONDBR2
Found a total of 16 corruption periods, covering a total of 14.57 seconds
Lumi loss due to corruption: 256.54 nb-1 out of 664069.72 nb-1 (0.39 per-mil)
Overall Lumi loss is 256.543620139045 by 14.573254325 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1142

== Missing EVs for run 452163 (Mon, 15 May 2023 17:20:57 +0200) ==

Found a total of 244 noisy periods, covering a total of 0.31 seconds
Found a total of 910 Mini noise periods, covering a total of 0.95 seconds
Lumi loss due to noise-bursts: 4.82 nb-1 out of 664069.72 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 15.56 nb-1 out of 664069.72 nb-1 (0.02 per-mil)
Overall Lumi loss is 20.38834539958976 by 1.26262414 s of veto length


