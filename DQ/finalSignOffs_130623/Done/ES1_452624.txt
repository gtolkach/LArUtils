Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Noisy cells update for run 452624 (Mon, 22 May 2023 10:57:21 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x743_h421)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 150 events
Flagged cells:56
Flagged in PS:16
Unflagged by DQ shifters:1
SBN:47
SBN in PS:13
HNHG:9
HNHG in PS:3

*****************
The treated cells were:
1 0 10 8 94 0 highNoiseHG # 0x3a53de00
1 0 10 8 78 0 highNoiseHG # 0x3a53ce00
0 0 23 13 23 0 sporadicBurstNoise # 0x38be1700
0 1 22 13 123 0 sporadicBurstNoise # 0x39b67b00
0 0 0 7 69 0 sporadicBurstNoise # 0x38034500
0 0 0 7 70 0 sporadicBurstNoise # 0x38034600
0 0 0 7 71 0 sporadicBurstNoise # 0x38034700
0 0 0 7 68 0 sporadicBurstNoise # 0x38034400
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 0 4 13 37 0 sporadicBurstNoise # 0x38262500
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 0 6 13 62 0 sporadicBurstNoise # 0x38363e00
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 0 10 5 94 0 highNoiseHG # 0x3a525e00
1 0 10 8 122 0 unflaggedByLADIeS # 0x3a53fa00
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 25 2 5 0 sporadicBurstNoise # 0x38c88500
0 0 25 2 6 0 sporadicBurstNoise # 0x38c88600
0 0 8 6 31 0 sporadicBurstNoise # 0x38429f00
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 1 26 11 65 0 sporadicBurstNoise # 0x39d54100
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
1 0 2 2 84 0 highNoiseHG # 0x3a10d400
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise # 0x39802b00
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 0 23 2 6 0 sporadicBurstNoise # 0x38b88600
0 0 23 1 6 0 sporadicBurstNoise # 0x38b80600
0 0 23 2 5 0 sporadicBurstNoise # 0x38b88500
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
1 1 10 6 42 0 highNoiseHG # 0x3b52aa00
0 1 11 13 106 0 sporadicBurstNoise # 0x395e6a00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 22 6 117 0 sporadicBurstNoise # 0x38b2f500
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 16 13 19 0 sporadicBurstNoise # 0x38861300
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 4 1 14 0 sporadicBurstNoise # 0x38200e00
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00

