Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1109

== Noisy cells update for run 451295 (Fri, 12 May 2023 08:28:38 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (x739_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 150 events</p>
Flagged cells:145
Flagged in PS:24
Unflagged by DQ shifters:26
SBN:100
SBN in PS:12
HNHG:45
HNHG in PS:12

*****************
The treated cells were:
0 1 0 12 77 0 sporadicBurstNoise # 0x3905cd00
0 0 5 1 53 0 sporadicBurstNoise # 0x38283500
0 1 31 13 107 0 &nbsp;unflaggedByLADIeS # 0x39fe6b00
0 0 22 1 19 0 sporadicBurstNoise # 0x38b01300
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 0 15 5 61 0 sporadicBurstNoise # 0x387a3d00
0 0 15 5 60 0 sporadicBurstNoise # 0x387a3c00
0 0 15 5 57 0 sporadicBurstNoise # 0x387a3900
0 0 15 5 59 0 sporadicBurstNoise # 0x387a3b00
0 0 15 5 58 0 sporadicBurstNoise # 0x387a3a00
1 1 6 15 60 0 highNoiseHG # 0x3b373c00
1 1 0 10 4 0 highNoiseHG # 0x3b048400
1 0 15 7 105 0 sporadicBurstNoise # 0x3a7b6900
1 1 21 4 70 0 sporadicBurstNoise # 0x3ba9c600
1 1 21 2 80 0 sporadicBurstNoise # 0x3ba8d000
1 1 21 4 69 0 sporadicBurstNoise # 0x3ba9c500
0 1 31 1 11 0 sporadicBurstNoise # 0x39f80b00
0 0 15 14 18 0 sporadicBurstNoise # 0x387e9200
0 1 4 11 20 0 sporadicBurstNoise # 0x39251400
0 1 31 13 111 0 highNoiseHG # 0x39fe6f00
0 1 17 1 40 0 sporadicBurstNoise # 0x39882800
1 0 21 2 28 0 highNoiseHG # 0x3aa89c00
1 0 21 2 24 0 sporadicBurstNoise # 0x3aa89800
1 0 21 1 20 0 highNoiseHG # 0x3aa81400
1 0 21 1 24 0 &nbsp;unflaggedByLADIeS # 0x3aa81800
0 0 26 14 53 0 highNoiseHG # 0x38d6b500
0 0 26 10 100 0 sporadicBurstNoise # 0x38d4e400
0 0 26 14 52 0 sporadicBurstNoise # 0x38d6b400
0 0 26 14 57 0 &nbsp;unflaggedByLADIeS # 0x38d6b900
0 0 26 14 61 0 sporadicBurstNoise # 0x38d6bd00
0 0 26 14 62 0 sporadicBurstNoise # 0x38d6be00
0 0 26 14 63 0 sporadicBurstNoise # 0x38d6bf00
0 0 26 14 60 0 sporadicBurstNoise # 0x38d6bc00
0 0 26 14 56 0 sporadicBurstNoise # 0x38d6b800
0 0 26 14 59 0 sporadicBurstNoise # 0x38d6bb00
0 0 26 10 101 0 sporadicBurstNoise # 0x38d4e500
0 0 26 14 55 0 sporadicBurstNoise # 0x38d6b700
0 0 26 10 97 0 sporadicBurstNoise # 0x38d4e100
0 0 26 10 104 0 sporadicBurstNoise # 0x38d4e800
0 0 26 14 48 0 sporadicBurstNoise # 0x38d6b000
0 0 26 14 54 0 sporadicBurstNoise # 0x38d6b600
0 0 26 14 29 0 highNoiseHG # 0x38d69d00
0 0 26 14 24 0 sporadicBurstNoise # 0x38d69800
0 0 26 14 25 0 &nbsp;unflaggedByLADIeS # 0x38d69900
0 0 26 14 21 0 highNoiseHG # 0x38d69500
1 1 6 15 126 0 highNoiseHG # 0x3b377e00
0 0 19 12 93 0 sporadicBurstNoise # 0x389ddd00
0 1 2 11 46 0 sporadicBurstNoise # 0x39152e00
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 1 17 6 30 0 sporadicBurstNoise # 0x398a9e00
0 1 16 6 103 0 sporadicBurstNoise # 0x3982e700
0 1 16 13 83 0 sporadicBurstNoise # 0x39865300
1 1 9 14 62 0 sporadicBurstNoise # 0x3b4ebe00
1 1 9 13 27 0 sporadicBurstNoise # 0x3b4e1b00
1 1 9 14 55 0 sporadicBurstNoise # 0x3b4eb700
1 1 9 14 58 0 sporadicBurstNoise # 0x3b4eba00
1 1 9 13 26 0 sporadicBurstNoise # 0x3b4e1a00
1 1 9 14 54 0 sporadicBurstNoise # 0x3b4eb600
1 0 6 1 109 0 highNoiseHG # 0x3a306d00
1 0 6 1 107 0 highNoiseHG # 0x3a306b00
0 0 18 1 14 0 sporadicBurstNoise # 0x38900e00
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 18 1 7 0 sporadicBurstNoise # 0x38900700
0 0 18 1 15 0 highNoiseHG # 0x38900f00
0 1 29 11 65 0 sporadicBurstNoise # 0x39ed4100
0 0 15 3 82 0 sporadicBurstNoise # 0x38795200
0 0 15 3 81 0 sporadicBurstNoise # 0x38795100
0 1 1 8 64 0 sporadicBurstNoise # 0x390bc000
0 1 1 8 65 0 sporadicBurstNoise # 0x390bc100
1 1 9 3 81 0 sporadicBurstNoise # 0x3b495100
1 1 9 3 85 0 sporadicBurstNoise # 0x3b495500
1 1 9 3 80 0 highNoiseHG # 0x3b495000
1 1 9 3 71 0 sporadicBurstNoise # 0x3b494700
1 1 9 3 82 0 sporadicBurstNoise # 0x3b495200
1 1 9 3 66 0 sporadicBurstNoise # 0x3b494200
1 1 9 3 67 0 sporadicBurstNoise # 0x3b494300
1 1 9 3 86 0 sporadicBurstNoise # 0x3b495600
1 1 9 3 87 0 &nbsp;unflaggedByLADIeS # 0x3b495700
1 1 9 3 83 0 highNoiseHG # 0x3b495300
1 1 9 3 69 0 sporadicBurstNoise # 0x3b494500
1 1 9 3 65 0 sporadicBurstNoise # 0x3b494100
1 1 9 3 91 0 highNoiseHG # 0x3b495b00
1 1 9 3 70 0 sporadicBurstNoise # 0x3b494600
1 1 9 3 90 0 sporadicBurstNoise # 0x3b495a00
1 1 9 3 84 0 &nbsp;unflaggedByLADIeS # 0x3b495400
1 0 6 1 1 0 highNoiseHG # 0x3a300100
0 1 0 12 2 0 sporadicBurstNoise # 0x39058200
0 0 25 14 14 0 sporadicBurstNoise # 0x38ce8e00
0 0 27 1 78 0 sporadicBurstNoise # 0x38d84e00
1 0 21 13 122 0 sporadicBurstNoise # 0x3aae7a00
1 0 21 15 118 0 sporadicBurstNoise # 0x3aaf7600
1 0 21 13 126 0 sporadicBurstNoise # 0x3aae7e00
1 0 21 15 122 0 sporadicBurstNoise # 0x3aaf7a00
1 0 21 15 126 0 sporadicBurstNoise # 0x3aaf7e00
1 0 21 13 121 0 sporadicBurstNoise # 0x3aae7900
1 0 21 13 125 0 sporadicBurstNoise # 0x3aae7d00
0 1 31 13 75 0 &nbsp;unflaggedByLADIeS # 0x39fe4b00
0 1 31 13 83 0 &nbsp;unflaggedByLADIeS # 0x39fe5300
0 1 31 13 87 0 highNoiseHG # 0x39fe5700
0 1 31 13 79 0 highNoiseHG # 0x39fe4f00
0 1 0 14 32 0 highNoiseHG # 0x3906a000
0 1 31 13 127 0 highNoiseHG # 0x39fe7f00
0 1 0 14 12 0 &nbsp;unflaggedByLADIeS # 0x39068c00
0 1 0 14 36 0 &nbsp;unflaggedByLADIeS # 0x3906a400
0 1 31 13 123 0 &nbsp;unflaggedByLADIeS # 0x39fe7b00
0 1 0 14 4 0 highNoiseHG # 0x39068400
0 1 0 14 8 0 &nbsp;unflaggedByLADIeS # 0x39068800
0 1 0 14 40 0 highNoiseHG # 0x3906a800
0 0 14 11 91 0 sporadicBurstNoise # 0x38755b00
0 1 24 13 127 0 sporadicBurstNoise # 0x39c67f00
0 0 14 12 86 0 sporadicBurstNoise # 0x3875d600
0 0 15 12 86 0 sporadicBurstNoise # 0x387dd600
1 0 6 1 81 0 highNoiseHG # 0x3a305100
1 0 6 1 83 0 highNoiseHG # 0x3a305300
1 0 6 1 111 0 highNoiseHG # 0x3a306f00
1 0 6 1 103 0 highNoiseHG # 0x3a306700
0 1 31 13 115 0 &nbsp;unflaggedByLADIeS # 0x39fe7300
0 1 31 13 119 0 highNoiseHG # 0x39fe7700
0 1 31 13 103 0 highNoiseHG # 0x39fe6700
0 1 31 13 91 0 &nbsp;unflaggedByLADIeS # 0x39fe5b00
0 1 0 13 36 0 &nbsp;unflaggedByLADIeS # 0x39062400
0 1 31 13 99 0 &nbsp;unflaggedByLADIeS # 0x39fe6300
0 1 0 13 32 0 highNoiseHG # 0x39062000
0 1 0 13 28 0 &nbsp;unflaggedByLADIeS # 0x39061c00
0 1 0 13 40 0 highNoiseHG # 0x39062800
0 1 0 13 44 0 &nbsp;unflaggedByLADIeS # 0x39062c00
0 1 31 13 67 0 &nbsp;unflaggedByLADIeS # 0x39fe4300
0 1 0 13 24 0 highNoiseHG # 0x39061800
0 1 31 13 110 0 &nbsp;unflaggedByLADIeS # 0x39fe6e00
0 1 0 13 48 0 highNoiseHG # 0x39063000
0 0 23 13 95 0 sporadicBurstNoise # 0x38be5f00
0 1 9 11 50 0 sporadicBurstNoise # 0x394d3200
0 1 9 1 22 0 highNoiseHG # 0x39481600
0 1 9 1 23 0 sporadicBurstNoise # 0x39481700
1 0 20 1 6 0 highNoiseHG # 0x3aa00600
1 0 20 1 12 0 highNoiseHG # 0x3aa00c00
1 0 20 1 4 0 highNoiseHG # 0x3aa00400
1 0 20 1 19 0 sporadicBurstNoise # 0x3aa01300
1 0 20 1 17 0 highNoiseHG # 0x3aa01100
1 0 21 1 23 0 &nbsp;unflaggedByLADIeS # 0x3aa81700
1 0 20 1 25 0 sporadicBurstNoise # 0x3aa01900
1 0 21 1 27 0 highNoiseHG # 0x3aa81b00
1 0 21 1 26 0 &nbsp;unflaggedByLADIeS # 0x3aa81a00
1 0 21 1 25 0 highNoiseHG # 0x3aa81900
1 0 20 1 18 0 &nbsp;unflaggedByLADIeS # 0x3aa01200
1 0 20 1 16 0 &nbsp;unflaggedByLADIeS # 0x3aa01000
1 0 21 1 28 0 highNoiseHG # 0x3aa81c00
1 0 21 1 22 0 highNoiseHG # 0x3aa81600
1 0 21 3 127 0 sporadicBurstNoise # 0x3aa97f00
1 0 20 1 24 0 &nbsp;unflaggedByLADIeS # 0x3aa01800
0 0 30 12 67 0 sporadicBurstNoise # 0x38f5c300
0 0 15 13 5 0 sporadicBurstNoise # 0x387e0500
0 0 16 12 49 0 sporadicBurstNoise # 0x3885b100
0 1 7 11 75 0 sporadicBurstNoise # 0x393d4b00
0 1 17 13 113 0 sporadicBurstNoise # 0x398e7100
0 1 20 14 10 0 sporadicBurstNoise # 0x39a68a00
0 1 20 10 38 0 sporadicBurstNoise # 0x39a4a600
0 0 5 11 62 0 sporadicBurstNoise # 0x382d3e00
0 0 14 1 66 0 highNoiseHG # 0x38704200
1 0 6 1 74 0 highNoiseHG # 0x3a304a00
1 0 6 1 77 0 highNoiseHG # 0x3a304d00
0 1 31 12 30 0 sporadicBurstNoise # 0x39fd9e00
0 1 31 12 26 0 sporadicBurstNoise # 0x39fd9a00
0 1 23 13 33 0 sporadicBurstNoise # 0x39be2100
1 1 9 3 89 0 sporadicBurstNoise # 0x3b495900
1 1 9 3 73 0 sporadicBurstNoise # 0x3b494900
1 1 9 3 64 0 sporadicBurstNoise # 0x3b494000
1 1 9 3 68 0 sporadicBurstNoise # 0x3b494400
1 1 9 3 72 0 highNoiseHG # 0x3b494800
1 1 24 10 55 0 &nbsp;unflaggedByLADIeS # 0x3bc4b700

