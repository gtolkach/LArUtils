Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1132

== Noisy cells update for run 451896 (Sat, 13 May 2023 18:15:59 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x738_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 200 events</p>
Flagged cells:125
Flagged in PS:26
Unflagged by DQ shifters:2
SBN:108
SBN in PS:22
HNHG:17
HNHG in PS:4

*****************
The treated cells were:
1 0 10 8 94 0 highNoiseHG # 0x3a53de00
1 0 10 8 78 0 highNoiseHG # 0x3a53ce00
0 0 10 9 78 0 sporadicBurstNoise # 0x38544e00
0 0 11 1 15 0 highNoiseHG # 0x38580f00
0 0 11 1 21 0 sporadicBurstNoise # 0x38581500
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 0 23 13 23 0 sporadicBurstNoise # 0x38be1700
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 0 22 6 117 0 sporadicBurstNoise # 0x38b2f500
0 0 9 10 80 0 sporadicBurstNoise # 0x384cd000
0 0 9 13 96 0 highNoiseHG # 0x384e6000
0 0 9 13 100 0 sporadicBurstNoise # 0x384e6400
0 0 30 10 34 0 sporadicBurstNoise # 0x38f4a200
0 0 23 2 15 0 sporadicBurstNoise # 0x38b88f00
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 16 13 14 0 sporadicBurstNoise # 0x38860e00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
1 0 10 8 122 0 highNoiseHG # 0x3a53fa00
1 0 5 7 103 0 sporadicBurstNoise # 0x3a2b6700
1 0 10 5 94 0 highNoiseHG # 0x3a525e00
1 1 9 4 16 0 sporadicBurstNoise # 0x3b499000
0 1 8 10 48 0 sporadicBurstNoise # 0x3944b000
1 1 9 3 8 0 sporadicBurstNoise # 0x3b490800
1 1 9 2 6 0 sporadicBurstNoise reflaggedByLADIeS # 0x3b488600
1 1 9 2 3 0 sporadicBurstNoise # 0x3b488300
1 1 9 2 5 0 highNoiseHG # 0x3b488500
1 1 9 2 1 0 sporadicBurstNoise # 0x3b488100
1 1 9 2 0 0 sporadicBurstNoise # 0x3b488000
1 1 9 2 4 0 sporadicBurstNoise # 0x3b488400
1 1 9 3 9 0 sporadicBurstNoise # 0x3b490900
1 1 9 3 13 0 sporadicBurstNoise # 0x3b490d00
0 1 8 10 60 0 sporadicBurstNoise # 0x3944bc00
0 1 8 10 127 0 highNoiseHG # 0x3944ff00
0 1 8 14 43 0 sporadicBurstNoise # 0x3946ab00
0 1 8 14 47 0 sporadicBurstNoise # 0x3946af00
0 1 8 14 56 0 &nbsp;unflaggedByLADIeS # 0x3946b800
0 1 8 14 60 0 highNoiseHG # 0x3946bc00
1 1 9 3 4 0 sporadicBurstNoise # 0x3b490400
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 119 0 sporadicBurstNoise # 0x3aaf7700
1 0 2 2 84 0 highNoiseHG # 0x3a10d400
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 18 1 14 0 sporadicBurstNoise # 0x38900e00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 8 6 30 0 sporadicBurstNoise # 0x38429e00
0 0 8 6 31 0 sporadicBurstNoise # 0x38429f00
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 15 12 98 0 sporadicBurstNoise # 0x397de200
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise reflaggedByLADIeS # 0x39802b00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 1 23 1 96 0 sporadicBurstNoise # 0x39b86000
1 1 9 2 7 0 sporadicBurstNoise # 0x3b488700
1 1 9 3 11 0 sporadicBurstNoise # 0x3b490b00
1 1 9 3 12 0 sporadicBurstNoise # 0x3b490c00
1 1 9 3 3 0 sporadicBurstNoise # 0x3b490300
1 1 9 3 0 0 sporadicBurstNoise # 0x3b490000
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 26 14 60 0 sporadicBurstNoise # 0x38d6bc00
0 0 26 14 61 0 sporadicBurstNoise # 0x38d6bd00
0 0 26 14 62 0 sporadicBurstNoise # 0x38d6be00
0 0 26 14 56 0 sporadicBurstNoise # 0x38d6b800
0 0 26 14 63 0 sporadicBurstNoise # 0x38d6bf00
0 0 26 14 52 0 sporadicBurstNoise # 0x38d6b400
0 0 26 14 57 0 highNoiseHG # 0x38d6b900
0 0 26 14 53 0 &nbsp;unflaggedByLADIeS # 0x38d6b500
0 0 26 10 96 0 sporadicBurstNoise # 0x38d4e000
0 0 26 10 100 0 sporadicBurstNoise # 0x38d4e400
0 0 26 14 59 0 sporadicBurstNoise # 0x38d6bb00
0 0 26 14 55 0 sporadicBurstNoise # 0x38d6b700
0 0 26 10 101 0 sporadicBurstNoise # 0x38d4e500
0 0 26 10 104 0 sporadicBurstNoise # 0x38d4e800
0 0 26 14 24 0 sporadicBurstNoise # 0x38d69800
0 0 26 14 48 0 sporadicBurstNoise # 0x38d6b000
0 0 26 14 28 0 sporadicBurstNoise # 0x38d69c00
0 0 26 14 20 0 sporadicBurstNoise # 0x38d69400
0 0 26 14 29 0 highNoiseHG # 0x38d69d00
0 0 26 14 58 0 sporadicBurstNoise # 0x38d6ba00
0 0 26 10 97 0 sporadicBurstNoise # 0x38d4e100
0 0 26 14 54 0 highNoiseHG # 0x38d6b600
0 0 10 1 27 0 sporadicBurstNoise # 0x38501b00
0 0 11 1 24 0 sporadicBurstNoise # 0x38581800
0 0 9 9 35 0 sporadicBurstNoise # 0x384c2300
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
1 1 9 4 17 0 sporadicBurstNoise # 0x3b499100
1 1 9 2 68 0 sporadicBurstNoise # 0x3b48c400
1 1 9 4 20 0 sporadicBurstNoise # 0x3b499400
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 11 1 2 0 sporadicBurstNoise # 0x39580200
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 0 5 1 5 0 sporadicBurstNoise # 0x38280500
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 1 7 14 17 0 sporadicBurstNoise # 0x393e9100
1 1 9 3 1 0 sporadicBurstNoise # 0x3b490100
1 1 9 3 2 0 sporadicBurstNoise # 0x3b490200
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00

