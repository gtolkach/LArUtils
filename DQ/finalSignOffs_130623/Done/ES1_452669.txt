Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1156

== Noisy cells update for run 452669 (Tue, 23 May 2023 14:17:46 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x743_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:136
Flagged in PS:28
Unflagged by DQ shifters:4
Changed to SBN:14
Changed to HNHG:1
SBN:120
SBN in PS:23
HNHG:16
HNHG in PS:5

*****************
The treated cells were:
0 0 0 7 68 0 sporadicBurstNoise  # 0x38034400 -> 0x2d22883c
0 0 0 7 69 0 sporadicBurstNoise  # 0x38034500 -> 0x2d228a3c
0 0 0 7 70 0 sporadicBurstNoise  # 0x38034600 -> 0x2d228c3c
0 0 0 7 71 0 sporadicBurstNoise  # 0x38034700 -> 0x2d228e3c
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 1 60 0 sporadicBurstNoise  # 0x38283c00 -> 0x2d003828
0 0 7 14 50 0 sporadicBurstNoise  # 0x383eb200 -> 0x2d406882
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 9 1 66 0 sporadicBurstNoise  # 0x38484200 -> 0x2d00441a
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 11 1 28 0 sporadicBurstNoise  # 0x38581c00 -> 0x2d001810
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 16 5 19 0 sporadicBurstNoise  # 0x38821300 -> 0x2d21a67e
0 0 16 13 19 0 sporadicBurstNoise  # 0x38861300 -> 0x2d4049f8
0 0 17 2 11 0 sporadicBurstNoise  # 0x38888b00 -> 0x2d20167a
0 0 17 2 12 0 sporadicBurstNoise  # 0x38888c00 -> 0x2d20187a
0 0 17 2 13 0 sporadicBurstNoise  # 0x38888d00 -> 0x2d201a7a
0 0 17 10 20 0 sporadicBurstNoise  # 0x388c9400 -> 0x2d602bee
0 0 17 10 21 0 highNoiseHG  # 0x388c9500 -> 0x2d602bec
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 20 1 4 0 sporadicBurstNoise  # 0x38a00400 -> 0x2d00006c
0 0 23 1 5 0 highNoiseHG  # 0x38b80500 -> 0x2d000260
0 0 23 1 6 0 sporadicBurstNoise  # 0x38b80600 -> 0x2d000460
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 25 14 14 0 sporadicBurstNoise  # 0x38ce8e00 -> 0x2d40676a
0 0 26 10 96 0 unflaggedByLADIeS  # 0x38d4e000 -> 0x2d603156
0 0 26 10 100 0 unflaggedByLADIeS  # 0x38d4e400 -> 0x2d603356
0 0 26 10 104 0 sporadicBurstNoise  # 0x38d4e800 -> 0x2d603556
0 0 26 14 52 0 sporadicBurstNoise  # 0x38d6b400 -> 0x2d406b56
0 0 26 14 53 0 unflaggedByLADIeS  # 0x38d6b500 -> 0x2d406b54
0 0 26 14 55 0 sporadicBurstNoise  # 0x38d6b700 -> 0x2d406b50
0 0 26 14 56 0 sporadicBurstNoise  # 0x38d6b800 -> 0x2d406d56
0 0 26 14 57 0 highNoiseHG  # 0x38d6b900 -> 0x2d406d54
0 0 26 14 58 0 unflaggedByLADIeS  # 0x38d6ba00 -> 0x2d406d52
0 0 26 14 59 0 sporadicBurstNoise  # 0x38d6bb00 -> 0x2d406d50
0 0 26 14 60 0 sporadicBurstNoise  # 0x38d6bc00 -> 0x2d406f56
0 0 26 14 61 0 sporadicBurstNoise  # 0x38d6bd00 -> 0x2d406f54
0 0 26 14 62 0 sporadicBurstNoise  # 0x38d6be00 -> 0x2d406f52
0 0 26 14 63 0 sporadicBurstNoise  # 0x38d6bf00 -> 0x2d406f50
0 0 30 1 1 0 sporadicBurstNoise  # 0x38f00100 -> 0x2d000246
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 1 1 13 92 0 sporadicBurstNoise  # 0x390e5c00 -> 0x2dc04e18
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 8 1 71 0 sporadicBurstNoise  # 0x39404700 -> 0x2d804622
0 1 9 14 49 0 sporadicBurstNoise  # 0x394eb100 -> 0x2dc0689a
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 11 13 69 0 sporadicBurstNoise  # 0x395e4500 -> 0x2dc042ba
0 1 14 1 77 0 sporadicBurstNoise  # 0x39704d00 -> 0x2d804a3a
0 1 16 1 43 0 sporadicBurstNoise  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 18 5 43 0 sporadicBurstNoise  # 0x39922b00 -> 0x2da1d648
0 1 18 5 44 0 sporadicBurstNoise  # 0x39922c00 -> 0x2da1d848
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 27 1 1 0 sporadicBurstNoise  # 0x39d80100 -> 0x2d80026c
1 0 2 2 16 0 sporadicBurstNoise  # 0x3a109000 -> 0x2ca00036
1 0 2 2 17 0 sporadicBurstNoise  # 0x3a109100 -> 0x2ca40036
1 0 2 2 18 0 sporadicBurstNoise  # 0x3a109200 -> 0x2ca40236
1 0 2 2 19 0 sporadicBurstNoise  # 0x3a109300 -> 0x2ca40436
1 0 2 2 20 0 sporadicBurstNoise  # 0x3a109400 -> 0x2ca00034
1 0 2 2 21 0 sporadicBurstNoise  # 0x3a109500 -> 0x2ca40034
1 0 2 2 22 0 sporadicBurstNoise  # 0x3a109600 -> 0x2ca40234
1 0 2 2 23 0 sporadicBurstNoise  # 0x3a109700 -> 0x2ca40434
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 2 3 64 0 sporadicBurstNoise  # 0x3a114000 -> 0x2cc000de
1 0 2 3 65 0 sporadicBurstNoise  # 0x3a114100 -> 0x2cc000dc
1 0 2 3 66 0 sporadicBurstNoise  # 0x3a114200 -> 0x2cc000da
1 0 2 3 67 0 sporadicBurstNoise  # 0x3a114300 -> 0x2cc000d8
1 0 2 3 68 0 sporadicBurstNoise  # 0x3a114400 -> 0x2cc400de
1 0 2 3 69 0 sporadicBurstNoise  # 0x3a114500 -> 0x2cc400dc
1 0 2 3 71 0 sporadicBurstNoise  # 0x3a114700 -> 0x2cc400d8
1 0 2 3 72 0 sporadicBurstNoise  # 0x3a114800 -> 0x2cc402de
1 0 2 3 73 0 sporadicBurstNoise  # 0x3a114900 -> 0x2cc402dc
1 0 2 3 76 0 sporadicBurstNoise  # 0x3a114c00 -> 0x2cc404de
1 0 2 3 77 0 sporadicBurstNoise  # 0x3a114d00 -> 0x2cc404dc
1 0 2 3 80 0 highNoiseHG  # 0x3a115000 -> 0x2cc000d6
1 0 2 3 81 0 sporadicBurstNoise  # 0x3a115100 -> 0x2cc000d4
1 0 2 3 82 0 sporadicBurstNoise  # 0x3a115200 -> 0x2cc000d2
1 0 2 3 83 0 sporadicBurstNoise  # 0x3a115300 -> 0x2cc000d0
1 0 2 3 85 0 sporadicBurstNoise  # 0x3a115500 -> 0x2cc400d4
1 0 2 3 86 0 sporadicBurstNoise  # 0x3a115600 -> 0x2cc400d2
1 0 2 3 87 0 highNoiseHG  # 0x3a115700 -> 0x2cc400d0
1 0 2 3 90 0 sporadicBurstNoise  # 0x3a115a00 -> 0x2cc402d2
1 0 2 3 91 0 highNoiseHG  # 0x3a115b00 -> 0x2cc402d0
1 0 3 6 34 0 highNoiseHG  # 0x3a1aa200 -> 0x31097000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 2 72 0 sporadicBurstNoise  # 0x3aa8c800 -> 0x2ca0004a
1 0 21 2 73 0 sporadicBurstNoise  # 0x3aa8c900 -> 0x2ca4004a
1 0 21 4 32 0 sporadicBurstNoise  # 0x3aa9a000 -> 0x2cc0012e
1 0 21 4 33 0 sporadicBurstNoise  # 0x3aa9a100 -> 0x2cc0012c
1 0 21 4 34 0 highNoiseHG  # 0x3aa9a200 -> 0x2cc0012a
1 0 21 4 35 0 sporadicBurstNoise  # 0x3aa9a300 -> 0x2cc00128
1 0 21 4 36 0 sporadicBurstNoise  # 0x3aa9a400 -> 0x2cc4012e
1 0 21 4 37 0 sporadicBurstNoise  # 0x3aa9a500 -> 0x2cc4012c
1 0 21 4 38 0 sporadicBurstNoise  # 0x3aa9a600 -> 0x2cc4012a
1 0 21 4 39 0 sporadicBurstNoise  # 0x3aa9a700 -> 0x2cc40128
1 0 21 4 40 0 sporadicBurstNoise  # 0x3aa9a800 -> 0x2cc4032e
1 0 21 4 41 0 sporadicBurstNoise  # 0x3aa9a900 -> 0x2cc4032c
1 0 21 4 42 0 sporadicBurstNoise  # 0x3aa9aa00 -> 0x2cc4032a
1 0 21 4 43 0 sporadicBurstNoise  # 0x3aa9ab00 -> 0x2cc40328
1 0 21 4 47 0 sporadicBurstNoise  # 0x3aa9af00 -> 0x2cc40528
1 0 21 4 48 0 sporadicBurstNoise  # 0x3aa9b000 -> 0x2cc00126
1 0 21 4 52 0 sporadicBurstNoise  # 0x3aa9b400 -> 0x2cc40126
1 1 2 4 14 0 sporadicBurstNoise  # 0x3b118e00 -> 0x2e440444
1 1 2 11 2 0 sporadicBurstNoise  # 0x3b150200 -> 0x2e440644
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 12 4 14 0 sporadicBurstNoise  # 0x3b618e00 -> 0x2e2c1c38
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38

