Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1144

== Noisy cells update for run 452202 (Wed, 17 May 2023 20:03:20 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x742_h420)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:129
Flagged in PS:52
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:113
SBN in PS:44
HNHG:16
HNHG in PS:8

*****************
The treated cells were:
0 0 4 1 14 0 sporadicBurstNoise  # 0x38200e00 -> 0x2d000c2c
0 0 4 13 37 0 sporadicBurstNoise  # 0x38262500 -> 0x2d4052bc
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 sporadicBurstNoise  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 10 102 0 sporadicBurstNoise  # 0x382ce600 -> 0x2d6032a2
0 0 5 10 103 0 highNoiseHG  # 0x382ce700 -> 0x2d6032a0
0 0 6 1 13 0 sporadicBurstNoise  # 0x38300d00 -> 0x2d000a24
0 0 7 14 36 0 sporadicBurstNoise  # 0x383ea400 -> 0x2d406a8e
0 0 7 14 40 0 highNoiseHG  # 0x383ea800 -> 0x2d406c8e
0 0 8 6 31 0 sporadicBurstNoise  # 0x38429f00 -> 0x2d223e1e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 9 10 80 0 sporadicBurstNoise  # 0x384cd000 -> 0x2d602866
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 15 0 sporadicBurstNoise  # 0x38580f00 -> 0x2d000e10
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 11 1 28 0 sporadicBurstNoise  # 0x38581c00 -> 0x2d001810
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 0 13 1 27 0 sporadicBurstNoise  # 0x38681b00 -> 0x2d001e0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 11 10 0 sporadicBurstNoise  # 0x387d0a00 -> 0x2d40040a
0 0 15 11 23 0 sporadicBurstNoise  # 0x387d1700 -> 0x2d400a08
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 16 13 14 0 sporadicBurstNoise  # 0x38860e00 -> 0x2d4047fa
0 0 17 2 12 0 sporadicBurstNoise  # 0x38888c00 -> 0x2d20187a
0 0 18 1 4 0 sporadicBurstNoise  # 0x38900400 -> 0x2d000074
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 18 12 2 0 sporadicBurstNoise  # 0x38958200 -> 0x2d4021da
0 0 20 1 4 0 highNoiseHG  # 0x38a00400 -> 0x2d00006c
0 0 20 1 5 0 sporadicBurstNoise  # 0x38a00500 -> 0x2d00026c
0 0 21 11 65 0 sporadicBurstNoise  # 0x38ad4100 -> 0x2d4001a4
0 0 22 6 117 0 sporadicBurstNoise  # 0x38b2f500 -> 0x2d226a64
0 0 22 6 118 0 sporadicBurstNoise  # 0x38b2f600 -> 0x2d226c64
0 0 22 11 12 0 sporadicBurstNoise  # 0x38b50c00 -> 0x2d40079e
0 0 23 1 5 0 highNoiseHG  # 0x38b80500 -> 0x2d000260
0 0 23 1 6 0 sporadicBurstNoise  # 0x38b80600 -> 0x2d000460
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 25 14 14 0 sporadicBurstNoise  # 0x38ce8e00 -> 0x2d40676a
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 1 63 0 sporadicBurstNoise  # 0x38d03f00 -> 0x2d003e54
0 0 26 10 96 0 highNoiseHG  # 0x38d4e000 -> 0x2d603156
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 29 10 55 0 sporadicBurstNoise  # 0x38ecb700 -> 0x2d24032e
0 0 29 11 106 0 sporadicBurstNoise  # 0x38ed6a00 -> 0x2d401522
0 0 30 1 1 0 sporadicBurstNoise  # 0x38f00100 -> 0x2d000246
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 0 11 62 0 sporadicBurstNoise  # 0x39053e00 -> 0x2dc01e04
0 1 2 11 46 0 sporadicBurstNoise  # 0x39152e00 -> 0x2dc01624
0 1 2 13 83 0 sporadicBurstNoise  # 0x39165300 -> 0x2dc0482e
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 8 1 70 0 sporadicBurstNoise  # 0x39404600 -> 0x2d804422
0 1 8 1 71 0 sporadicBurstNoise  # 0x39404700 -> 0x2d804622
0 1 8 1 76 0 sporadicBurstNoise  # 0x39404c00 -> 0x2d804822
0 1 8 1 77 0 sporadicBurstNoise  # 0x39404d00 -> 0x2d804a22
0 1 8 1 78 0 sporadicBurstNoise  # 0x39404e00 -> 0x2d804c22
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 21 0 sporadicBurstNoise  # 0x39481500 -> 0x2d801226
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 13 12 1 0 sporadicBurstNoise  # 0x396d8100 -> 0x2dc020d2
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 14 1 77 0 sporadicBurstNoise  # 0x39704d00 -> 0x2d804a3a
0 1 15 10 42 0 sporadicBurstNoise  # 0x397caa00 -> 0x2de034f4
0 1 15 10 43 0 sporadicBurstNoise  # 0x397cab00 -> 0x2de034f6
0 1 16 1 43 0 sporadicBurstNoise  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 17 1 36 0 sporadicBurstNoise  # 0x39882400 -> 0x2d802046
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 17 6 32 0 sporadicBurstNoise  # 0x398aa000 -> 0x2da24044
0 1 18 5 43 0 sporadicBurstNoise  # 0x39922b00 -> 0x2da1d648
0 1 18 5 44 0 sporadicBurstNoise  # 0x39922c00 -> 0x2da1d848
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 19 1 25 0 sporadicBurstNoise  # 0x39981900 -> 0x2d801a4c
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 11 10 0 sporadicBurstNoise  # 0x39ad0a00 -> 0x2dc00554
0 1 22 13 123 0 sporadicBurstNoise  # 0x39b67b00 -> 0x2dc05d6e
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 23 14 7 0 sporadicBurstNoise  # 0x39be8700 -> 0x2dc06376
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 25 1 7 0 sporadicBurstNoise  # 0x39c80700 -> 0x2d800666
0 1 26 11 65 0 sporadicBurstNoise  # 0x39d54100 -> 0x2dc001aa
0 1 27 1 1 0 sporadicBurstNoise  # 0x39d80100 -> 0x2d80026c
0 1 28 2 102 0 sporadicBurstNoise  # 0x39e0e600 -> 0x2da04c72
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 9 2 28 0 sporadicBurstNoise  # 0x3a489c00 -> 0x2ca00010
1 0 9 3 113 0 sporadicBurstNoise  # 0x3a497100 -> 0x2cc00044
1 0 10 5 94 0 highNoiseHG  # 0x3a525e00 -> 0x31048000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 13 11 63 0 sporadicBurstNoise  # 0x3a6d3f00 -> 0x2cc425e0
1 0 13 11 115 0 sporadicBurstNoise  # 0x3a6d7300 -> 0x2cc427e0
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 1 2 11 2 0 sporadicBurstNoise  # 0x3b150200 -> 0x2e440644
1 1 9 4 16 0 sporadicBurstNoise  # 0x3b499000 -> 0x2e4000c8
1 1 9 4 17 0 sporadicBurstNoise  # 0x3b499100 -> 0x2e4000ca
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38

