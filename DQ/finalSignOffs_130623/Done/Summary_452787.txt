Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1162

** Defect LAR_EMBA_NOISEBURST **
Affecting 4 LBs: 65, 66, 107, 108 (0 recoverable) 
** Defect LAR_HECA_NOISEBURST **
Affecting 46 LBs: 29, 30, 43, 44, 46, 47, 48, 49, 58, 59, 60, 61, 63, 64, 66, 67, 81, 82, 83, 84, 92, 93, 97, 98, 100, 101, 102, 105, 106, 107, 110, 111, 127, 128, 140, 141, 169, 170, 179, 180, 185, 186, 192, 193, 198, 199 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 14 LBs: 76, 77, 78, 79, 105, 106, 169, 170, 174, 175, 183, 184, 192, 193 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 46 LBs: 29, 30, 43, 44, 46, 47, 48, 49, 58, 59, 60, 61, 63, 64, 66, 67, 81, 82, 83, 84, 92, 93, 97, 98, 100, 101, 102, 105, 106, 107, 110, 111, 127, 128, 140, 141, 169, 170, 179, 180, 185, 186, 192, 193, 198, 199 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 14 LBs: 76, 77, 78, 79, 105, 106, 169, 170, 174, 175, 183, 184, 192, 193 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 46 LBs: 29, 30, 43, 44, 46, 47, 48, 49, 58, 59, 60, 61, 63, 64, 66, 67, 81, 82, 83, 84, 92, 93, 97, 98, 100, 101, 102, 105, 106, 107, 110, 111, 127, 128, 140, 141, 169, 170, 179, 180, 185, 186, 192, 193, 198, 199 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 14 LBs: 76, 77, 78, 79, 105, 106, 169, 170, 174, 175, 183, 184, 192, 193 (0 recoverable) 
