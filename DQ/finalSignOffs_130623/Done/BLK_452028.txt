Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1139

== Noisy cells update for run 452028 (Sun, 28 May 2023 11:56:17 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 300 events
Flagged cells:70
Flagged in PS:16
Unflagged by DQ shifters:4
Changed to SBN:39
Changed to HNHG:1
SBN:67
SBN in PS:14
HNHG:3
HNHG in PS:2

*****************
The treated cells were:
0 0 5 1 5 0 highNoiseHG sporadicBurstNoise  # 0x38280500 -> 0x2d000228
0 0 7 10 41 0 sporadicBurstNoise  # 0x383ca900 -> 0x2d60348c
0 0 7 10 42 0 sporadicBurstNoise  # 0x383caa00 -> 0x2d60348a
0 0 7 14 44 0 sporadicBurstNoise  # 0x383eac00 -> 0x2d406e8e
0 0 11 1 105 0 sporadicBurstNoise  # 0x38586900 -> 0x2d006a12
0 0 11 14 36 0 sporadicBurstNoise  # 0x385ea400 -> 0x2d406a4e
0 0 11 14 40 0 sporadicBurstNoise  # 0x385ea800 -> 0x2d406c4e
0 0 11 14 44 0 sporadicBurstNoise  # 0x385eac00 -> 0x2d406e4e
0 0 11 14 45 0 sporadicBurstNoise  # 0x385ead00 -> 0x2d406e4c
0 0 11 14 54 0 sporadicBurstNoise  # 0x385eb600 -> 0x2d406a42
0 0 11 14 58 0 sporadicBurstNoise  # 0x385eba00 -> 0x2d406c42
0 0 11 14 62 0 sporadicBurstNoise  # 0x385ebe00 -> 0x2d406e42
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 1 8 1 77 0 sporadicBurstNoise  # 0x39404d00 -> 0x2d804a22
0 1 8 1 78 0 sporadicBurstNoise  # 0x39404e00 -> 0x2d804c22
0 1 17 6 30 0 sporadicBurstNoise  # 0x398a9e00 -> 0x2da23c44
0 1 17 6 32 0 sporadicBurstNoise  # 0x398aa000 -> 0x2da24044
0 1 26 11 65 0 sporadicBurstNoise  # 0x39d54100 -> 0x2dc001aa
1 0 2 13 19 0 sporadicBurstNoise  # 0x3a161300 -> 0x2ce024e8
1 0 2 13 23 0 sporadicBurstNoise  # 0x3a161700 -> 0x2ce026e8
1 0 2 14 39 0 sporadicBurstNoise  # 0x3a16a700 -> 0x2cc450e8
1 0 2 14 43 0 sporadicBurstNoise  # 0x3a16ab00 -> 0x2cc452e8
1 0 2 14 47 0 sporadicBurstNoise  # 0x3a16af00 -> 0x2cc454e8
1 0 2 14 52 0 sporadicBurstNoise  # 0x3a16b400 -> 0x2cc450e6
1 0 2 14 53 0 sporadicBurstNoise  # 0x3a16b500 -> 0x2cc450e4
1 0 2 14 56 0 highNoiseHG  # 0x3a16b800 -> 0x2cc452e6
1 0 2 14 57 0 sporadicBurstNoise  # 0x3a16b900 -> 0x2cc452e4
1 0 2 14 58 0 unflaggedByLADIeS  # 0x3a16ba00 -> 0x2cc452e2
1 0 2 14 60 0 unflaggedByLADIeS  # 0x3a16bc00 -> 0x2cc454e6
1 0 2 14 61 0 unstable lowNoiseHG unflaggedByLADIeS  # 0x3a16bd00 -> 0x2cc454e4
1 0 2 14 62 0 unflaggedByLADIeS  # 0x3a16be00 -> 0x2cc454e2
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500
1 1 9 2 0 0 sporadicBurstNoise  # 0x3b488000 -> 0x2e200020
1 1 9 2 1 0 sporadicBurstNoise  # 0x3b488100 -> 0x2e240020
1 1 9 2 3 0 sporadicBurstNoise  # 0x3b488300 -> 0x2e240420
1 1 9 2 4 0 sporadicBurstNoise  # 0x3b488400 -> 0x2e200022
1 1 9 2 5 0 sporadicBurstNoise  # 0x3b488500 -> 0x2e240022
1 1 9 2 6 0 sporadicBurstNoise  # 0x3b488600 -> 0x2e240222
1 1 9 2 7 0 sporadicBurstNoise  # 0x3b488700 -> 0x2e240422
1 1 9 3 4 0 sporadicBurstNoise  # 0x3b490400 -> 0x2e440080
1 1 9 3 5 0 sporadicBurstNoise  # 0x3b490500 -> 0x2e440082
1 1 9 3 8 0 sporadicBurstNoise  # 0x3b490800 -> 0x2e440280
1 1 9 3 9 0 sporadicBurstNoise  # 0x3b490900 -> 0x2e440282
1 1 9 3 11 0 deadCalib sporadicBurstNoise  # 0x3b490b00 -> 0x2e440286
1 1 9 3 13 0 sporadicBurstNoise  # 0x3b490d00 -> 0x2e440482

