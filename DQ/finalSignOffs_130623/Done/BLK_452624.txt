Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Noisy cells update for run 452624 (Tue, 30 May 2023 09:05:48 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1352_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:2
Flagged in PS:0
Unflagged by DQ shifters:1
Changed to SBN:0
Changed to HNHG:0
SBN:2
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
1 1 12 4 15 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3b618f00 -> 0x2e2c1e38

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Event Veto for run 452624 (Wed, 24 May 2023 02:54:13 +0200) ==
Found Noise or data corruption in run 452624
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452624_Main.db

Found project tag data23_13p6TeV for run 452624
Found 7 Veto Ranges with 19 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452624
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 3.50 seconds
Lumi loss due to corruption: 67.19 nb-1 out of 219204.04 nb-1 (0.31 per-mil)
Overall Lumi loss is 67.19298585912226 by 3.50184171 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Event Veto for run 452624 (Tue, 23 May 2023 20:31:58 +0200) ==
Found Noise or data corruption in run 452624
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452624_Main.db

Found project tag data23_13p6TeV for run 452624
Found 7 Veto Ranges with 19 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452624
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 3.50 seconds
Lumi loss due to corruption: 67.19 nb-1 out of 219204.04 nb-1 (0.31 per-mil)
Overall Lumi loss is 67.19298585912226 by 3.50184171 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Event Veto for run 452624 (Tue, 23 May 2023 19:31:54 +0200) ==
Found Noise or data corruption in run 452624
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452624_Main.db

Found project tag data23_13p6TeV for run 452624
Found 7 Veto Ranges with 19 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452624
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 3.50 seconds
Lumi loss due to corruption: 67.19 nb-1 out of 219204.04 nb-1 (0.31 per-mil)
Overall Lumi loss is 67.19298585912226 by 3.50184171 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Event Veto for run 452624 (Tue, 23 May 2023 19:00:55 +0200) ==
Found Noise or data corruption in run 452624
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452624_Main.db

Found project tag data23_13p6TeV for run 452624
Found 7 Veto Ranges with 19 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452624
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 3.50 seconds
Lumi loss due to corruption: 67.19 nb-1 out of 219204.04 nb-1 (0.31 per-mil)
Overall Lumi loss is 67.19298585912226 by 3.50184171 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Event Veto for run 452624 (Tue, 23 May 2023 18:12:01 +0200) ==
Found Noise or data corruption in run 452624
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452624_Main.db

Found project tag data23_13p6TeV for run 452624
Found 7 Veto Ranges with 19 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452624
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452624_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 3.50 seconds
Lumi loss due to corruption: 67.19 nb-1 out of 219204.04 nb-1 (0.31 per-mil)
Overall Lumi loss is 67.19298585912226 by 3.50184171 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1154

== Missing EVs for run 452624 (Sat, 20 May 2023 09:17:15 +0200) ==

Found a total of 66 noisy periods, covering a total of 0.07 seconds
Found a total of 446 Mini noise periods, covering a total of 0.49 seconds
Lumi loss due to noise-bursts: 1.29 nb-1 out of 219204.04 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 9.17 nb-1 out of 219204.04 nb-1 (0.04 per-mil)
Overall Lumi loss is 10.464949460214665 by 0.56184521 s of veto length


