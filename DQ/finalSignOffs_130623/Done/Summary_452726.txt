Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1160

** Defect LAR_EMBA_NOISEBURST **
Affecting 2 LBs: 871, 872 (0 recoverable) 
** Defect LAR_HECA_NOISEBURST **
Affecting 25 LBs: 770, 771, 772, 773, 821, 822, 823, 826, 827, 828, 835, 836, 849, 850, 858, 859, 862, 863, 868, 869, 870, 871, 872, 884, 885 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 4 LBs: 826, 827, 882, 883 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 25 LBs: 770, 771, 772, 773, 821, 822, 823, 826, 827, 828, 835, 836, 849, 850, 858, 859, 862, 863, 868, 869, 870, 871, 872, 884, 885 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 4 LBs: 826, 827, 882, 883 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 25 LBs: 770, 771, 772, 773, 821, 822, 823, 826, 827, 828, 835, 836, 849, 850, 858, 859, 862, 863, 868, 869, 870, 871, 872, 884, 885 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 4 LBs: 826, 827, 882, 883 (0 recoverable) 
