Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1142

== Noisy cells update for run 452163 (Tue, 16 May 2023 17:20:18 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x742_h420)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:121
Flagged in PS:44
Unflagged by DQ shifters:0
Changed to SBN:1
Changed to HNHG:0
SBN:106
SBN in PS:37
HNHG:15
HNHG in PS:7

*****************
The treated cells were:
0 0 1 13 121 0 sporadicBurstNoise  # 0x380e7900 -> 0x2d405ce4
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 10 102 0 sporadicBurstNoise  # 0x382ce600 -> 0x2d6032a2
0 0 5 10 103 0 sporadicBurstNoise  # 0x382ce700 -> 0x2d6032a0
0 0 6 1 13 0 sporadicBurstNoise  # 0x38300d00 -> 0x2d000a24
0 0 7 12 89 0 sporadicBurstNoise  # 0x383dd900 -> 0x2d402c84
0 0 7 14 36 0 sporadicBurstNoise  # 0x383ea400 -> 0x2d406a8e
0 0 8 6 31 0 sporadicBurstNoise  # 0x38429f00 -> 0x2d223e1e
0 0 8 6 32 0 sporadicBurstNoise  # 0x3842a000 -> 0x2d22401e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 13 67 0 sporadicBurstNoise  # 0x38564300 -> 0x2d404050
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 21 0 sporadicBurstNoise  # 0x38581500 -> 0x2d001210
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 11 1 28 0 sporadicBurstNoise  # 0x38581c00 -> 0x2d001810
0 0 11 14 44 0 sporadicBurstNoise  # 0x385eac00 -> 0x2d406e4e
0 0 11 14 45 0 sporadicBurstNoise  # 0x385ead00 -> 0x2d406e4c
0 0 11 14 55 0 sporadicBurstNoise  # 0x385eb700 -> 0x2d406a40
0 0 11 14 62 0 sporadicBurstNoise  # 0x385ebe00 -> 0x2d406e42
0 0 11 14 63 0 sporadicBurstNoise  # 0x385ebf00 -> 0x2d406e40
0 0 12 11 75 0 sporadicBurstNoise  # 0x38654b00 -> 0x2d400430
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 0 13 1 27 0 sporadicBurstNoise  # 0x38681b00 -> 0x2d001e0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 11 4 0 deadReadout highNoiseHG  # 0x386d0400 -> 0x2d40022e
0 0 14 1 4 0 sporadicBurstNoise  # 0x38700400 -> 0x2d000004
0 0 14 1 5 0 sporadicBurstNoise  # 0x38700500 -> 0x2d000204
0 0 14 1 66 0 sporadicBurstNoise  # 0x38704200 -> 0x2d004406
0 0 14 2 28 0 sporadicBurstNoise  # 0x38709c00 -> 0x2d203806
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 11 10 0 sporadicBurstNoise  # 0x387d0a00 -> 0x2d40040a
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 16 1 78 0 sporadicBurstNoise  # 0x38804e00 -> 0x2d004c7c
0 0 17 2 12 0 sporadicBurstNoise  # 0x38888c00 -> 0x2d20187a
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 22 1 1 0 sporadicBurstNoise  # 0x38b00100 -> 0x2d000266
0 0 22 7 1 0 sporadicBurstNoise  # 0x38b30100 -> 0x2d228266
0 0 22 7 2 0 sporadicBurstNoise  # 0x38b30200 -> 0x2d228466
0 0 23 2 15 0 sporadicBurstNoise  # 0x38b88f00 -> 0x2d201e62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 1 63 0 sporadicBurstNoise  # 0x38d03f00 -> 0x2d003e54
0 0 26 10 96 0 highNoiseHG  # 0x38d4e000 -> 0x2d603156
0 0 26 14 20 0 sporadicBurstNoise  # 0x38d69400 -> 0x2d406356
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 29 14 18 0 sporadicBurstNoise  # 0x38ee9200 -> 0x2d406122
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 1 0 11 6 0 sporadicBurstNoise  # 0x39050600 -> 0x2dc00204
0 1 0 11 62 0 sporadicBurstNoise  # 0x39053e00 -> 0x2dc01e04
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 3 10 93 0 sporadicBurstNoise  # 0x391cdd00 -> 0x2de02e3a
0 1 3 10 94 0 sporadicBurstNoise  # 0x391cde00 -> 0x2de02e3c
0 1 3 10 95 0 sporadicBurstNoise  # 0x391cdf00 -> 0x2de02e3e
0 1 3 10 97 0 sporadicBurstNoise  # 0x391ce100 -> 0x2de0303a
0 1 3 13 127 0 sporadicBurstNoise  # 0x391e7f00 -> 0x2dc05e3e
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 7 12 68 0 sporadicBurstNoise  # 0x393dc400 -> 0x2dc02278
0 1 8 1 71 0 sporadicBurstNoise  # 0x39404700 -> 0x2d804622
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 21 0 sporadicBurstNoise  # 0x39481500 -> 0x2d801226
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 11 11 77 0 sporadicBurstNoise  # 0x395d4d00 -> 0x2dc006ba
0 1 11 13 69 0 sporadicBurstNoise  # 0x395e4500 -> 0x2dc042ba
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 16 1 43 0 sporadicBurstNoise  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 18 5 44 0 sporadicBurstNoise  # 0x39922c00 -> 0x2da1d848
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 11 10 0 sporadicBurstNoise  # 0x39ad0a00 -> 0x2dc00554
0 1 22 13 123 0 sporadicBurstNoise  # 0x39b67b00 -> 0x2dc05d6e
0 1 23 1 96 0 sporadicBurstNoise  # 0x39b86000 -> 0x2d80605c
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 24 13 27 0 sporadicBurstNoise  # 0x39c61b00 -> 0x2dc04d86
0 1 25 1 7 0 sporadicBurstNoise  # 0x39c80700 -> 0x2d800666
0 1 28 2 102 0 sporadicBurstNoise  # 0x39e0e600 -> 0x2da04c72
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 9 3 113 0 sporadicBurstNoise  # 0x3a497100 -> 0x2cc00044
1 0 10 5 94 0 highNoiseHG  # 0x3a525e00 -> 0x31048000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 1 9 2 68 0 sporadicBurstNoise  # 0x3b48c400 -> 0x2e200032
1 1 9 4 16 0 sporadicBurstNoise  # 0x3b499000 -> 0x2e4000c8
1 1 9 4 17 0 sporadicBurstNoise  # 0x3b499100 -> 0x2e4000ca
1 1 9 4 20 0 sporadicBurstNoise  # 0x3b499400 -> 0x2e4400c8
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 21 4 103 0 sporadicBurstNoise  # 0x3ba9e700 -> 0x2e4401f6

