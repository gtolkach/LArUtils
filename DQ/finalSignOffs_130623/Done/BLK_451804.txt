Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1127

== Noisy cells update for run 451804 (Sat, 27 May 2023 17:32:11 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:40
Flagged in PS:5
Unflagged by DQ shifters:1
Changed to SBN:22
Changed to HNHG:0
SBN:38
SBN in PS:5
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
0 0 6 14 51 0 sporadicBurstNoise  # 0x3836b300 -> 0x2d406890
0 0 6 14 60 0 sporadicBurstNoise  # 0x3836bc00 -> 0x2d406e96
0 0 6 14 63 0 sporadicBurstNoise  # 0x3836bf00 -> 0x2d406e90
0 0 13 1 5 0 sporadicBurstNoise  # 0x38680500 -> 0x2d000208
0 0 13 2 68 0 sporadicBurstNoise  # 0x3868c400 -> 0x2d200808
0 0 13 2 69 0 sporadicBurstNoise  # 0x3868c500 -> 0x2d200a08
0 0 13 2 70 0 sporadicBurstNoise  # 0x3868c600 -> 0x2d200c08
0 0 13 2 71 0 sporadicBurstNoise  # 0x3868c700 -> 0x2d200e08
0 0 15 10 98 0 sporadicBurstNoise  # 0x387ce200 -> 0x2d603002
0 0 16 8 40 0 sporadicBurstNoise  # 0x3883a800 -> 0x2d23507e
0 0 16 8 41 0 sporadicBurstNoise  # 0x3883a900 -> 0x2d23527e
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 10 10 20 0 sporadicBurstNoise  # 0x39549400 -> 0x2de02aa0
0 1 10 10 21 0 sporadicBurstNoise  # 0x39549500 -> 0x2de02aa2
0 1 10 10 22 0 sporadicBurstNoise  # 0x39549600 -> 0x2de02aa4
0 1 10 13 39 0 sporadicBurstNoise  # 0x39562700 -> 0x2dc052a6
0 1 10 13 41 0 sporadicBurstNoise  # 0x39562900 -> 0x2dc054a2
0 1 10 13 42 0 sporadicBurstNoise  # 0x39562a00 -> 0x2dc054a4
1 0 2 13 23 0 sporadicBurstNoise  # 0x3a161700 -> 0x2ce026e8
1 0 2 13 25 0 sporadicBurstNoise  # 0x3a161900 -> 0x2ce024e4
1 0 2 13 28 0 highNoiseHG  # 0x3a161c00 -> 0x2ce026e6
1 0 2 13 29 0 sporadicBurstNoise  # 0x3a161d00 -> 0x2ce026e4
1 0 2 14 39 0 sporadicBurstNoise  # 0x3a16a700 -> 0x2cc450e8
1 0 2 14 47 0 sporadicBurstNoise  # 0x3a16af00 -> 0x2cc454e8
1 0 2 14 48 0 sporadicBurstNoise  # 0x3a16b000 -> 0x2cc44ee6
1 0 2 14 54 0 sporadicBurstNoise  # 0x3a16b600 -> 0x2cc450e2
1 0 2 14 57 0 highNoiseHG  # 0x3a16b900 -> 0x2cc452e4
1 0 2 14 58 0 sporadicBurstNoise  # 0x3a16ba00 -> 0x2cc452e2
1 0 2 14 61 0 unstable lowNoiseHG unflaggedByLADIeS  # 0x3a16bd00 -> 0x2cc454e4
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 126 0 sporadicBurstNoise  # 0x3aae7e00 -> 0x2ce02702
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 118 0 sporadicBurstNoise  # 0x3aaf7600 -> 0x2cc45102
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 0 21 15 122 0 sporadicBurstNoise  # 0x3aaf7a00 -> 0x2cc45302
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1127

== Event Veto for run 451804 (Mon, 22 May 2023 04:31:27 +0200) ==
Found Noise or data corruption in run 451804
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451804_Main.db

Found project tag data23_13p6TeV for run 451804
Found 4 Veto Ranges with 574 events
Found 30 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451804_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451804
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451804_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.44 seconds
Lumi loss due to corruption: 28.95 nb-1 out of 337371.50 nb-1 (0.09 per-mil)
Overall Lumi loss is 28.95337100640875 by 2.444644415 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1127

== Event Veto for run 451804 (Mon, 22 May 2023 03:58:22 +0200) ==
Found Noise or data corruption in run 451804
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451804_Main.db

Found project tag data23_13p6TeV for run 451804
Found 4 Veto Ranges with 574 events
Found 30 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451804_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451804
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451804_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.44 seconds
Lumi loss due to corruption: 28.95 nb-1 out of 337371.50 nb-1 (0.09 per-mil)
Overall Lumi loss is 28.95337100640875 by 2.444644415 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1127

== Missing EVs for run 451804 (Thu, 11 May 2023 09:20:54 +0200) ==

Found a total of 457 noisy periods, covering a total of 0.42 seconds
Found a total of 985 Mini noise periods, covering a total of 0.90 seconds
Lumi loss due to noise-bursts: 4.50 nb-1 out of 337371.50 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 9.34 nb-1 out of 337371.50 nb-1 (0.03 per-mil)
Overall Lumi loss is 13.838388207715688 by 1.322497 s of veto length


