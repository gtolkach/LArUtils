Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1163

== Noisy cells update for run 452799 (Fri, 9 Jun 2023 17:08:19 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1354_h423)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:26
Flagged in PS:9
Unflagged by DQ shifters:1
Changed to SBN:7
Changed to HNHG:0
SBN:15
SBN in PS:4
HNHG:11
HNHG in PS:5

*****************
The treated cells were:
0 0 5 1 0 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38280000 -> 0x2d00002a
0 0 5 1 6 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38280600 -> 0x2d000428
0 0 7 14 53 0 sporadicBurstNoise  # 0x383eb500 -> 0x2d406a84
0 0 7 14 57 0 sporadicBurstNoise  # 0x383eb900 -> 0x2d406c84
0 0 8 1 16 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38401000 -> 0x2d00101e
0 0 8 1 18 0 sporadicBurstNoise unflaggedByLADIeS  # 0x38401200 -> 0x2d00141e
0 0 16 1 67 0 highNoiseHG  # 0x38804300 -> 0x2d00467e
0 0 25 1 64 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38c84000 -> 0x2d00405a
0 0 26 10 100 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38d4e400 -> 0x2d603356
0 0 26 14 53 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38d6b500 -> 0x2d406b54
0 0 26 14 56 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x38d6b800 -> 0x2d406d56
0 0 26 14 58 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38d6ba00 -> 0x2d406d52
0 0 26 14 61 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38d6bd00 -> 0x2d406f54
0 1 5 1 67 0 sporadicBurstNoise  # 0x39284300 -> 0x2d804614
0 1 8 1 78 0 sporadicBurstNoise  # 0x39404e00 -> 0x2d804c22
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
0 1 21 11 10 0 sporadicBurstNoise  # 0x39ad0a00 -> 0x2dc00554
1 1 5 10 33 0 sporadicBurstNoise  # 0x3b2ca100 -> 0x2e440e72
1 1 5 10 42 0 sporadicBurstNoise  # 0x3b2caa00 -> 0x2e441274
1 1 5 10 56 0 sporadicBurstNoise  # 0x3b2cb800 -> 0x2e441278
1 1 5 10 58 0 sporadicBurstNoise  # 0x3b2cba00 -> 0x2e44127c
1 1 13 12 32 0 sporadicBurstNoise  # 0x3b6da000 -> 0x2e442f10
1 1 13 12 34 0 sporadicBurstNoise  # 0x3b6da200 -> 0x2e442f14
1 1 13 12 35 0 sporadicBurstNoise  # 0x3b6da300 -> 0x2e442f16
1 1 13 12 41 0 sporadicBurstNoise  # 0x3b6da900 -> 0x2e443312

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1163

== Event Veto for run 452799 (Tue, 30 May 2023 09:31:50 +0200) ==
Found Noise or data corruption in run 452799
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452799_Main.db

Found project tag data23_13p6TeV for run 452799
Found 24 Veto Ranges with 58 events
Found 41 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452799_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452799
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452799_Main.db;dbname=CONDBR2
Found a total of 24 corruption periods, covering a total of 12.00 seconds
Lumi loss due to corruption: 213.53 nb-1 out of 756068.57 nb-1 (0.28 per-mil)
Overall Lumi loss is 213.52843167046058 by 12.004487235 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1163

== Event Veto for run 452799 (Tue, 30 May 2023 08:53:23 +0200) ==
Found Noise or data corruption in run 452799
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto452799_Main.db

Found project tag data23_13p6TeV for run 452799
Found 24 Veto Ranges with 58 events
Found 41 isolated events
Reading event veto info from db sqlite://;schema=EventVeto452799_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 452799
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto452799_Main.db;dbname=CONDBR2
Found a total of 24 corruption periods, covering a total of 12.00 seconds
Lumi loss due to corruption: 213.53 nb-1 out of 756068.57 nb-1 (0.28 per-mil)
Overall Lumi loss is 213.52843167046058 by 12.004487235 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1163

== Missing EVs for run 452799 (Thu, 25 May 2023 21:32:33 +0200) ==

Found a total of 246 noisy periods, covering a total of 0.32 seconds
Found a total of 1350 Mini noise periods, covering a total of 1.43 seconds
Lumi loss due to noise-bursts: 5.37 nb-1 out of 756068.57 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 24.70 nb-1 out of 756068.57 nb-1 (0.03 per-mil)
Overall Lumi loss is 30.064556819337934 by 1.74809146 s of veto length


