import json
import sys, os, errno
import numpy as np
sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-00-00/installed/share/bin/")
#sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/software/23.0/tdaq/tdaq-10-00-00/installed/share/bin/")
import extractPrescales
from datetime import datetime
from argparse import ArgumentParser
from CoolLumiUtilities.CoolDataReader import CoolDataReader
import math

import ROOT as R
R.gROOT.SetBatch(True)


def getLBDict(runnum):
    # Don't really need all this now I have the LBs from the since() and until()... but will use it for number of LBs
    LBready = CoolDataReader('COOLONL_TDAQ/CONDBR2', '/TDAQ/RunCtrl/DataTakingMode')
    LBready.setIOVRangeFromRun(runnum)
    LBready.readData()
    LBdict = {}
    for i, data in enumerate(LBready.data):
        tlo = data.since()
        thi = data.until()
        r1=tlo>>32
        lb1=tlo & 0xFFFFFFFF
        r2=thi>>32
        lb2=thi & 0xFFFFFFFF
        lbs = list(range(lb1, lb2))
        isReady = data.payload()['ReadyForPhysics']
        for r in lbs:
            LBdict[r] = isReady

    if len(list(LBdict.keys())) == 0:
        return None, None, None
    maxLB = max(list(LBdict.keys()))
    minLB = min(list(LBdict.keys()))
    for lb in range(0, minLB):
        LBdict[lb] = 0
    print(f"Run has {maxLB} LBs")
    return LBdict, maxLB, minLB    




def plotPS(runnum, folder, dbalias="TRIGGERDB_RUN3", doPlot=True, outDir="."):
    LBdict, maxLB, minLB = getLBDict(runnum)
    if LBdict is None:
        print("NO LB FOR",runnum)
        if doPlot is False:
            return [], None
        else:
            return 0
    DBps = CoolDataReader('COOLONL_TRIGGER/CONDBR2', folder)
    DBps.setIOVRangeFromRun(runnum)
    DBps.readData()
    LBs = []
    psPoints = {}
    psPoints_ready = {}
    for data in DBps.data:
        tlo = data.since()
        thi = data.until()
        r1=tlo>>32
        lb1=tlo & 0xFFFFFFFF
        r2=thi>>32
        lb2=thi & 0xFFFFFFFF
        
        if folder == '/TRIGGER/HLT/PrescaleKey':
            itemkey = 'HltPrescaleKey'
        else:
            itemkey = 'Lvl1PrescaleConfigurationKey'
        key = data.payload()[itemkey]
        if lb2 == 0: lb2 = maxLB
        lbs = range(lb1, lb2)

        if folder == '/TRIGGER/HLT/PrescaleKey':
            l1psk = 1
            hltpsk = key
        else:
            l1psk = key
            hltpsk = 1
        print("**",key, f"(run {runnum})")
        # Opening JSON file
        #f = open('data.json')
        with extractPrescales.PrescaleExtractor(dbalias) as extractor:
            l1ps, hltps = extractor.extractPrescales(l1psk, hltpsk)
            if folder == '/TRIGGER/HLT/PrescaleKey':
                pstocheck = hltps
                keytocheck = 'prescales'
            else:
                pstocheck = l1ps
                keytocheck = 'cutValues'
            pstocheck = json.loads(pstocheck)
            prescales = pstocheck[keytocheck]

            if folder == '/TRIGGER/HLT/PrescaleKey':
                larnoisekeys = [ i for i in prescales.keys() if any( x in i.lower() for x in ["larnoise", "larpsall","larpebnoise"]) ]
            else:
                larnoisekeys = [ i for i in prescales.keys() if any( x in i.lower() for x in ["empty"]) ]

            for lnk in larnoisekeys:
                if folder == '/TRIGGER/HLT/PrescaleKey':
                    theinfo = prescales[lnk]['prescale']
                else:
                    theinfo = int(float(prescales[lnk]['info'].split("prescale: ")[1]))
                print(lnk, theinfo)

                if lnk not in psPoints.keys():
                    psPoints[lnk] = []
                    psPoints_ready[lnk] = []
                for lb in lbs:
                    psPoints[lnk].append(theinfo)
                    if LBdict[lb] == 1:
                        psPoints_ready[lnk].append(theinfo)
        for lb in lbs:
            LBs.append(lb)


    areadyLB = [ l for l in LBs if LBdict[l] == 1 ]

    if doPlot is False:
        return areadyLB, psPoints_ready

    canv = R.TCanvas()
    ppp = 6
    canv.Divide(2,int(ppp/2))
    outStr = folder.replace("/","_")
    if outStr.startswith("_"): outStr = outStr[1:]

    pdfName = "plots_"+outStr+"_"+str(runnum)+".pdf"

    print("*"*30)
    print(len(LBs))
    grs = {}
    grs_ready = {}
    for lnk in psPoints.keys():
        print("**", lnk)
        ind = list(psPoints.keys()).index(lnk)+1

        if ind%ppp != 0:
            canv.cd(ind%ppp)
            print(f"plot number {ind} in canvas slot {ind%ppp}")
        else:
            canv.cd(ppp)
            print(f"plot number {ind} in canvas slot {ppp}")

        if ind == 1:
            canv.Print(outDir+"/"+pdfName+"[")# Opens output file for multipage
            print("First print to pdf")

        try:
            grs[lnk] = R.TGraph(len(LBs), np.array(LBs, dtype='d'), np.array(psPoints[lnk], dtype='d'))
        except Exception as e:
            print(e)
            print("LBs:", LBs)
            print("psPoints:", psPoints[lnk])
        grs[lnk].SetTitle(lnk+" for run "+str(runnum))
        grs[lnk].GetYaxis().SetTitle("Prescale")
        grs[lnk].GetXaxis().SetTitle("LB")
        grs[lnk].SetMarkerStyle(20)
        grs[lnk].SetMarkerSize(1)

        grs[lnk].Draw("APl")

        if len(areadyLB) > 0:
            try:
                grs_ready[lnk] = R.TGraph(len(areadyLB), np.array(areadyLB, dtype='d'), np.array(psPoints_ready[lnk], dtype='d'))
            except Exception as e:
                print(e)
                print("areadyLB:",areadyLB)
                print("psPoints_ready:",psPoints_ready[lnk])
            grs_ready[lnk].SetMarkerColor(R.kBlue)
            grs_ready[lnk].SetMarkerStyle(20)
            grs_ready[lnk].SetMarkerSize(0.9)
            grs_ready[lnk].Draw("sameP")


        if ind%ppp == 0: #ppp-1:
            canv.Print(outDir+"/"+pdfName)# Print Page
            print("next print to pdf and clear")
            canv.Clear()
            canv.Divide(2,int(ppp/2))
    
        if ind == len(list(psPoints.keys())):
            canv.Print(outDir+"/"+pdfName)# Print Page
            canv.Print(outDir+"/"+pdfName+"]")# Close multipage
            print("Close pdf and clear")
        #canv.Print(outDir+"/"+str(runnum)+"_"+outname+"_"+lnk+".png")
        #isReady = [ LBdict[lb] for lb in lbs ]


def plotMultiRun(runlist, folder='/TRIGGER/HLT/PrescaleKey', outDir="."):
    plot_run = []
    plotval_ps_max = {}
    plotval_ps_min = {}
    plotval_ps_mean = {}

    plotval_l1_max = {}
    plotval_l1_min = {}
    plotval_l1_mean = {}

    outStr = ("_").join(folder.split("/"))
    if outStr.startswith("_"): outStr = outStr[1:]

    pdfName = "plots_"+outStr+"_allruns.pdf"


    for rl in runlist:
        areadyLB_ps, psPoints_ready_ps = plotPS(rl,folder, doPlot=False)
        if len(areadyLB_ps) == 0: continue

        plot_run.append(rl)

        for trig in psPoints_ready_ps.keys():
            if trig not in plotval_ps_max.keys(): plotval_ps_max[trig] = []
            if trig not in plotval_ps_min.keys(): plotval_ps_min[trig] = []
            if trig not in plotval_ps_mean.keys(): plotval_ps_mean[trig] = []
            
            plotval_ps_max[trig].append( max(psPoints_ready_ps[trig]) )
            plotval_ps_min[trig].append( min(psPoints_ready_ps[trig]) )
            plotval_ps_mean[trig].append( sum(psPoints_ready_ps[trig]) / len(psPoints_ready_ps[trig]) )
                
    ppp = 6
    canv = R.TCanvas()
    canv.Divide(2,int(ppp/2))
    grs = {}

    npage_ps = math.ceil(len(list(plotval_ps_max.keys()))/ppp)
    
                       
    print(f"Will write {npage_ps} pages for {len(list(plotval_ps_max.keys()))} {outStr} plots")

    for ps in plotval_ps_max.keys():
        ind = list(plotval_ps_max.keys()).index(ps)+1
        grs[ps+"_max"]  = R.TGraph(len(plot_run), np.array(plot_run, dtype='d'), np.array(plotval_ps_max[ps], dtype='d'))
        grs[ps+"_min"]  = R.TGraph(len(plot_run), np.array(plot_run, dtype='d'), np.array(plotval_ps_min[ps], dtype='d'))
        grs[ps+"_mean"]  = R.TGraph(len(plot_run), np.array(plot_run, dtype='d'), np.array(plotval_ps_mean[ps], dtype='d'))

        print("**",ps,"**")
        print({r:pm for r,pm in zip(plot_run,plotval_ps_mean[ps])})
        print("-")

        ps_max = max([ grs[ps+"_max"].GetHistogram().GetMaximum(), grs[ps+"_min"].GetHistogram().GetMaximum(), grs[ps+"_mean"].GetHistogram().GetMaximum()])
        ps_min = min([ grs[ps+"_max"].GetHistogram().GetMinimum(), grs[ps+"_min"].GetHistogram().GetMinimum(), grs[ps+"_mean"].GetHistogram().GetMinimum()])

        grs[ps+"_max"].SetLineColor(R.kRed)
        grs[ps+"_mean"].SetLineColor(R.kGreen)
        grs[ps+"_min"].SetLineColor(R.kBlue)
        grs[ps+"_max"].SetMarkerColor(R.kRed)
        grs[ps+"_mean"].SetMarkerColor(R.kGreen)
        grs[ps+"_min"].SetMarkerColor(R.kBlue)
        grs[ps+"_max"].SetMarkerStyle(20)
        grs[ps+"_mean"].SetMarkerStyle(20)
        grs[ps+"_min"].SetMarkerStyle(20)
        grs[ps+"_max"].SetMarkerSize(0.9)
        grs[ps+"_mean"].SetMarkerSize(0.9)
        grs[ps+"_min"].SetMarkerSize(0.9)

        grs[ps+"_max"].SetTitle(ps)

        if ind%ppp != 0:
            canv.cd(ind%ppp)
            print(f"plot number {ind} in canvas slot {ind%ppp}")
        else:
            canv.cd(ppp)
            print(f"plot number {ind} in canvas slot {ppp}")

        if ind == 1:
            canv.Print(outDir+"/"+pdfName+"[")# Opens output file for multipage
            print("First print to pdf")
        #if list(plotval_ps_max.keys()).index(ps) == 0:
        grs[ps+"_max"].Draw("Apl")
        grs[ps+"_max"].SetMaximum(ps_max)
        grs[ps+"_max"].GetXaxis().SetMaxDigits(6)
        grs[ps+"_max"].SetMinimum(ps_min)

        grs[ps+"_max"].GetXaxis().SetTitle("Run no.")
        grs[ps+"_max"].GetYaxis().SetTitle("Prescale")


        print(ps," max:",ps_max, "  min:",ps_min)
        #else:
        #grs[ps+"_max"].Draw("same,pl")
        grs[ps+"_min"].Draw("same,pl")
        grs[ps+"_mean"].Draw("same,pl")

        if ind%ppp == 0: #ppp-1:
            canv.Print(outDir+"/"+pdfName)# Print Page
            print("next print to pdf and clear")
            canv.Clear()
            canv.Divide(2,int(ppp/2))
    
        if ind == len(list(plotval_ps_max.keys())):
            canv.Print(outDir+"/"+pdfName)# Print Page
            canv.Print(outDir+"/"+pdfName+"]")# Close multipage
            print("Close pdf and clear")
            #canv.Clear()
        #canv.Print(outDir+"/"+ps+"_allruns.png")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-r', '--runnum', dest='runnum', help='Run number(s) which you would like to make plots for', type=int, nargs='+', default=None,required=True)
    parser.add_argument('-outDir', '--outDir', dest='outDir', type=str, default='trigPlots', help='Output directory for plots. Default %(default)s')

    args = parser.parse_args()

    try:
        os.makedirs(args.outDir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    if len(args.runnum) == 1:
        plotPS(int(args.runnum[0]),'/TRIGGER/HLT/PrescaleKey', outDir=args.outDir)
        plotPS(int(args.runnum[0]),'/TRIGGER/LVL1/Lvl1ConfigKey', outDir=args.outDir)
    else:
        plotMultiRun(args.runnum,'/TRIGGER/HLT/PrescaleKey', outDir=args.outDir)
        plotMultiRun(args.runnum,'/TRIGGER/LVL1/Lvl1ConfigKey', outDir=args.outDir)



    
    
#print("l1")
#L1ps = CoolDataReader('COOLONL_TRIGGER/CONDBR2', '/TRIGGER/LVL1/Lvl1ConfigKey')
#L1ps.setIOVRangeFromRun(runnum)
#L1ps.readData()

#for data in L1ps.data:
#    print(data.payload()[0])





