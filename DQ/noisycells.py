DETlist = ['EMB','EMEC','HEC','FCAL']
dbfile = '/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArBad.db'
from threading import Thread

import sqlite3
import os,re,time,sys,cgi,math
import xmlrpc.client

sys.path.append(sys.path[0]+"/../nearestNeighbourFinder")
import nearestNeighboursFinder

passfile = open("/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt")
passwd = passfile.read().strip(); passfile.close()  
passurl = 'https://%s@atlasdqm.cern.ch' % passwd

global Parts
Parts = []

global debug
#debug = True
debug = False

import print_rows
import print_top
from run_report import dqmfResLB,dqmfResLB_display,dqmfCaloBaseline_display,firstGeneralColumn,hotSpotInHIST_commandLine

global flagParam
flagParam = {}
# Maximal number of remaining events accepted to flag a cell sporadicBurstNoise
flagParam['sporadic_threshold'] = 1 # CaloCells and sporadic columns - Overriden by advOptions - Options now relative to the minimal number of clusters
flagParam['sporadic_threshold_PS'] = 1 # CaloCells and sporadic columns - Overriden by advOptions  - Options now relative to the minimal number of clusters
flagParam['sporadic_threshold_unmatched'] =  1000 # CaloCells and sporadic columns
flagParam['sporadic_threshold_lce'] =  500 # LCE column
flagParam['sporadic_threshold_lce_unmatched'] =  5000 # LCE column

# CaloCells column : Threshold on the maximum between number of events above Eth/5sigma (max (R[1][0],R[1][1])
flagParam['calocells_absoluteEvt_Eth'] =  1000 # Cluster matched
flagParam['calocells_absoluteEvt_unmatched_Eth'] =  2000 #100 #2000 # Cluster unmatched
flagParam['calocells_absoluteEvt_5sigma'] =  10000 # Cluster matched
flagParam['calocells_absoluteEvt_unmatched_5sigma'] =  200000 #100 #200000 # Cluster unmatched
#CaloCells column : Threshold on (nb of events above Eth divided by nb of clusters)
flagParam['calocells_ratioClus_Eth'] = 0.8
# Sporadic column : Threshold on number of events above 5-15GeV (R[2][0])
flagParam['sporadic_absoluteEvt'] = 50
flagParam['sporadic_absoluteEvt_unmatched'] = 200
# LCE column : threshold on mean energy and number of events
flagParam['lce_meanEnergy'] = 5
flagParam['lce_meanEnergy_unmatched'] = 25
flagParam['lce_nbEvt'] = 50
flagParam['lce_nbEvt_unmatched'] = 200

ACmap = {1:'A+','1':'A+','A+':'A+',-1:'C-','-1':'C-','C-':'C-'}
ACmap2 = {'A':1,'C':-1}
inv_ACmap2 = {1:'A+','1':'A+','A+':'A+',-1:'C-','-1':'C-','C-':'C-'}
dd=[0.,0.,0.,0.]
## CaloCell bins variations to match real cells: [EMB[sampling],EMEC[sampling],HEC[sampling],FCAL[sampling]]
# Run 2 below
# de=[[0.01,0.001,0.005,0.01],[0.01,0.001,0.01,0.01],[0.001,0.001,0.001,0.001],[0.,0.02,0.1,0.1]]
# New Run3 - 09/07/2022
de=[[0.0125,0.0015,0.0125,0.025],[0.01,0.001,0.01,0.01],[0.001,0.001,0.001,0.001],[0.,0.02,0.1,0.1]]
#dp=[[0.01,0.003,0.005,0.02],[0.01,0.005,0.01,0.01],[0.05,0.05,0.05,0.05],[0.,0.05,0.03,0.02]]
# August 2015 : BT redefine the width of phi window in 1st sampling of EMB to cope with a problem seen in 1st layer of EMBA in run 277806
# Again in August 2016. Was changed into 0.004 (0.0035 before)
# Run 2 below
# dp=[[0.01,0.004,0.005,0.02],[0.01,0.005,0.01,0.01],[0.05,0.05,0.05,0.05],[0.,0.05,0.03,0.02]]
# New Run3 - 09/07/2022
dp=[[0.05,0.05,0.0125,0.02125],[0.01,0.005,0.01,0.01],[0.05,0.05,0.05,0.05],[0.,0.05,0.03,0.02]]
cols = ['OFF_ID','ONL_ID','DET','AC','FT','SL','CH','SAM','ETA','PHI','HVLINES','HVCORR','CL','TT_COOL_ID']
cols = ['ONL_ID','DET','AC','FT','SL','CH','SAM','ETA','PHI','HVLINES','HVCORR','OFF_ID','CL','TT_COOL_ID']

ClusterTypes = ['EMTopoClusters','CaloTopoClusters']
BadChannels = ['deadReadout','deadCalib','deadPhys','almostDead','short','unstable','distorted','lowNoiseHG','highNoiseHG','unstableNoiseHG','lowNoiseMG','highNoiseMG','unstableNoiseMG','lowNoiseLG','highNoiseLG','unstableNoiseLG','missingFEB','peculiarCL','problematicFor?','sporadicBurstNoise','deadSCACell','badFirstSample','unflaggedByLADIeS']

########################################################################
mp = ['noise_acceptance_AVG','acceptance_AVG','signal_AVG','quality_AVG','fractionOverQ','DB','5Sigma','hiEth','Quality','ENLB','EN','Warning','Error','Cluster','LCE0','LCE1','LCE2','LCE3']
# 0:GREEN 1:RED 2:BLUE 3:PINK 4:YELLOW 5:BLACK  
col = [1,0,2,3,3,2,1,0,3,2,1,4,1,5,0,1,2,3]
def GetClass(plot):
  for a in range(18):
    if mp[a] in plot:
     return col[a]


########################################################################
def stripLfromNumber(dicWithL):
   k=dicWithL.keys()
   v=dicWithL.values()
   map_k=map(int,k)
   map_v=map(int,v)
   dicWithoutL = zip(map_k,map_v)
   return dicWithoutL


########################################################################
def get_id(c):
  a = 7<<27
  be=0
  if c[0]>0: be=1
  a |= be<<25
  ac=0
  if c[1]>0: ac=1
  a |= ac<<24
  a |= c[2]<<19
  a |= (c[3]-1)<<15
  a |= c[4]<<8
  return a


########################################################################
class db(Thread):
  def __init__(self,cmd,plot,par,V):
    Thread.__init__(self)
    self.cmd = cmd
    self.plot = plot
    self.par = par
    self.V = V
  def run(self):
    con = sqlite3.connect('LArId.db')
    cur = con.cursor()
    cur.execute(self.cmd)
    self.R = cur.fetchall()
    con.close()

########################################################################
class mc2(Thread):
  def __init__(self,passurl):
    Thread.__init__(self)
    self.server = xmlrpc.client.ServerProxy(passurl)
    self.multicall = xmlrpc.client.MultiCall( self.server )
  def run(self):
    self.results = tuple(self.multicall())

########################################################################
class mc3(Thread):
  def __init__(self,passurl,P,run_spec):
    Thread.__init__(self)
    self.server = xmlrpc.client.ServerProxy(passurl)
    self.multicall = xmlrpc.client.MultiCall( self.server )
    self.P = P
    self.run_spec = run_spec
    self.passurl=passurl

  def run(self):
    self.results = tuple(self.multicall())
    ##print "DEBUG - mc3 - 1:",self.P,len(self.multicall._MultiCall__call_list),time.time(),"<br>"  
    ##print "DEBUG - mc3 - 2:",self.multicall._MultiCall__call_list,"<br>" 
    ##print "DEBUG - mc3 - 3:",self.results,"<br>" 
    N = "%d"%(self.run_spec['run_list'][0])
    H = self.results[0][N]['hists']
    calls = []
    i=0
    for h in H:
      ##print "DEBUG", h,"<br>"
      if not 'Sl' in h: continue
      if i%50==0: calls.append( mc2(self.passurl) )
      calls[-1].multicall.get_dqmf_all_results(self.run_spec,'LAr/'+self.P+'/Occupancy-Noise/Single_Cells/Non_Gaussian_Noise/'+h)
      i+=1

    self.h2p = []
    for c in calls: c.start()
    for c in calls:
      c.join()
      for i,I in enumerate(c.multicall._MultiCall__call_list):
        plot = I[1][-1].split('/')[-1]
        self.h2p.append( [plot,c.results[i][N]] )

########################################################################
class queryPrimaryTable(Thread):
  def __init__(self,passurl,run_spec,P):
    Thread.__init__(self)
    self.P = P
    self.run_spec = run_spec
    if debug:
      print("Starting queryPrimaryTable",run_spec)
    self.namedResult=dict()
    self.server = xmlrpc.client.ServerProxy(passurl)
    self.multicall = xmlrpc.client.MultiCall( self.server )
    self.multicallName=[]

    suspiciousHistoName = {"EMBC":"BarrelC","EMBA":"BarrelA","EMECC":"EndcapC","EMECA":"EndcapA"}
    self.multicall.get_hist_binvals(run_spec,'LAr/'+P+'/Detector_Status/DBSuspiciousMNBFEBs'+suspiciousHistoName[P])
    self.multicallName.append("MNB_suspiciousFEB")

    self.multicall.get_dqmf_all_results(run_spec,'LAr/'+P+'/Occupancy-Noise/Mini_NB/MNBTightEvent_'+P)
    self.multicallName.append("MNB")
    self.multicall.get_dqmf_all_results(run_spec,'LAr/'+P+'/Occupancy-Noise/Mini_NB/MNBTightEvent_TimeVeto_'+P)
    self.multicallName.append("MNB_TimeVeto")

    self.multicall.get_hist_binvals(run_spec,'LAr/'+P+'/Occupancy-Noise/Mini_NB/All-FEBs/CandidateMNBTightFEBPerEvt_'+P)
    self.multicallName.append("MNB_newTightFEB")    

    self.multicall.get_dqmf_all_results(run_spec,'LAr/'+P+'/Occupancy-Noise/Noise_Burst/NoisyEvent_'+P)
    self.multicallName.append("NB")
    self.multicall.get_dqmf_all_results(run_spec,'LAr/'+P+'/Occupancy-Noise/Noise_Burst/NoisyEvent_TimeVeto_'+P)
    self.multicallName.append("NB_TimeVeto")

    if ("CosmicCalo" in run_spec["stream"]): 
      run_specColl = run_spec.copy()
      run_specColl['stream'] = run_spec['stream_coll']
      run_specColl['proc_ver'] = run_spec['proc_ver_coll']
      self.multicall.get_dqmf_all_results(run_specColl,'LAr/'+P+'/Occupancy-Noise/Mini_NB/MNBTightEvent_'+P)
      self.multicallName.append("MNB_coll")
      self.multicall.get_dqmf_all_results(run_specColl,'LAr/'+P+'/Occupancy-Noise/Mini_NB/MNBTightEvent_TimeVeto_'+P)
      self.multicallName.append("MNB_TimeVeto_coll")
      self.multicall.get_hist_binvals(run_specColl, "LAr/LAR_GLOBAL/Run_Parameters/NbOfEventsVsLB") 
      self.multicallName.append("NbOfEventsVsLB_coll")

    if debug:
      print("queryPrimaryTable over")


  def run(self):
    for (resName,res) in zip(self.multicallName,self.multicall()): #self.results):
      self.namedResult[resName]=res.get("%d"%self.run_spec['run_list'][0])

    del self.multicall
    del self.server
    pass

########################################################################
class GetLArCellEmpty(Thread):
  def __init__(self,cluster_threshold,cLCE_HEC,lce_inputfile):
    Thread.__init__(self)
    self.cluster_threshold = cluster_threshold
    self.cLCE_HEC = cLCE_HEC
    self.lce_inputfile = lce_inputfile
  def run(self):

    self.Chans = {}
    f = open(self.lce_inputfile,"r")
    for line in f:
      fields=str.split(line)
      
      if fields[0]=='onlid':continue
      # skip cells found only with algo 1
      algo=int(fields[10])

      if algo == 1: continue
      # retrieve all cells
      onlid = fields[0]
      #part=fields[1][0:-2]
      #side=fields[1][-1]
      #FT=int(fields[2])
      #SL=int(fields[3])
      #CH=int(fields[4])
      evt=int(fields[6])
      emean=float(fields[7])
      qf=float(fields[8])
      lb=float(fields[9])
      lblist=fields[9].split(";")
      comment="Nb of LBs:"
      for item in lblist:comment+=" %s"%item
      # Before 2016, the online id was recomputed fom det/FT/slot/ch
      # This was apparently useless and with a buggy list, this lead to nonsense output
      #d=DETlist.index(part)
      #id = get_id((d,ACmap2[side],FT,SL,CH))
      id = int(onlid,16)
      # store the channels
      # cut to match the cluster threshold
      if evt < self.cluster_threshold or emean < float(flagParam['lce_meanEnergy']): continue # return output only if mean energy > lce_MeanEnergy (assume lower than unmatched one)
      # now store the channel
      if (not fields[1][0:-2] == "HEC") or self.cLCE_HEC:
        if id not in self.Chans: self.Chans[id] = [ [],[],[],[] ]
        self.Chans[id][0].append([int(emean),'&lt;E&gt;GeV','LCE0'])
        self.Chans[id][1].append([evt,'Nevts with E&gt;1GeV','LCE1'])
        self.Chans[id][2].append([int(lb),comment,'LCE2'])
        self.Chans[id][3].append([int(qf*100),'% of evts with Q&gt;4000','LCE3'])
       
########################################################################
class GetRawChannels(Thread):
  def __init__(self,run_spec,Parts):
    Thread.__init__(self)
    self.run_spec = run_spec
    self.Parts = Parts

  def run(self):  
    RE = re.compile(r'\S*\((?P<SSLN>\S+)\.0*\)\s*y-axis\s*\((?P<CH>\S+)\.0*\)\S+')
    N = "%d"%(self.run_spec['run_list'][0])
    calls = []
    Plots = ['acceptance_AVG','noise_acceptance_AVG','signal_AVG','quality_AVG']
    for P in self.Parts:
      calls.append( mc2(passurl) )
      for p in Plots: calls[-1].multicall.get_dqmf_all_results(self.run_spec,'LAr/'+P+'/Occupancy-Noise/Single_Cells/'+P+'_'+p)
      calls[-1].start()

    self.Chans = {}
    #print "DEBUG %i <br>"%(len(calls))
    for c in calls:
      c.join()
      for i,I in enumerate(c.multicall._MultiCall__call_list):
        plot = I[1][-1]
        part = plot.split('/')[1]
        d = DETlist.index(part[:-1])
        for p,P in enumerate(Plots): 
          if P in plot: pi=p; break
        r = c.results[i][N]
        K = list(r.keys())
        for k in K:
          if not 'x-axis' in k: continue
          m = RE.search(k).groupdict()
          SSLN = int(m['SSLN'])
          if d: FT = (SSLN-1)/15; SL = SSLN-FT*15
          else: FT = (SSLN-1)/14; SL = SSLN-FT*14
          CH = int(m['CH'])
          id = get_id((d,ACmap2[part[-1]],int(FT),int(SL),CH))
          if id not in self.Chans: self.Chans[id] = [ [],[],[],[] ]
          if 'noise_acceptance' in plot: self.Chans[id][1].append( [r[k],k,plot] )
          elif 'acceptance' in plot:  self.Chans[id][0].append( [r[k],k,plot] )
          elif 'signal' in plot: self.Chans[id][2].append( [r[k],k,plot] )
          elif 'quality' in plot: self.Chans[id][3].append( [r[k],k,plot] )

########################################################################
class GetClusters(Thread):
  def __init__(self,run_spec,Sides,Clusters,Duplicated,cluster_threshold,cluster_E):
    Thread.__init__(self)
    self.run_spec = run_spec
    self.Sides = Sides
    self.Clusters = Clusters
    self.Duplicated = Duplicated
    self.cluster_threshold = cluster_threshold
    self.cluster_E = cluster_E
    self.server = xmlrpc.client.ServerProxy(passurl)
    self.multicall = xmlrpc.client.MultiCall( self.server )


  def run(self):

    # Distance cells/clusters; 
    DR_match = 0.2;
    # Decoding the DQ bin output
    RE = re.compile(r'(?P<cat>\S+)-\(eta,phi\)\[OSRatio\]=\((?P<eta>\S+),(?P<phi>\S+)\)\[(?P<sigma>\S+)\]')
    N = "%d"%(self.run_spec['run_list'][0])
    for j in self.Sides:
      for i in ClusterTypes:
        if i=='EMTopoClusters':
          if self.cluster_E == 'Et > 4/10GeV': self.multicall.get_dqmf_all_results(self.run_spec,'CaloTopoClusters/CalEM%s/EMThresh1%sOcc' % (j,j) ) # 4GeV
          elif self.cluster_E == 'Et > 10/25GeV' or self.cluster_E == 'Et > 10/25GeV': self.multicall.get_dqmf_all_results(self.run_spec,'CaloTopoClusters/CalEM%s/EMThresh2%sOcc' % (j,j) ) # 10GeV
          else: self.multicall.get_dqmf_all_results(self.run_spec,'CaloTopoClusters/CalEM%s/EMThresh4%sOcc' % (j,j) ) # 25GeV
        if i=='CaloTopoClusters':
          #if self.cluster_E == 'Et > 4/10GeV' or self.cluster_E == 'Et > 10/10GeV': self.multicall.get_dqmf_all_results(self.run_spec,'CaloMonitoring/CaloMonShift/CaloMon%s/%s%s/m_clus_etaphi_Et_thresh1@%s' % (j,i,j,j) ) # 10GeV
          if self.cluster_E == 'Et > 4/10GeV' or self.cluster_E == 'Et > 10/10GeV': self.multicall.get_dqmf_all_results(self.run_spec,'%s/Cal%s/Thresh1%sOcc' % (i,j,j) ) # 10GeV
          elif self.cluster_E == 'Et > 10/25GeV':self.multicall.get_dqmf_all_results(self.run_spec,'%s/Cal%s/Thresh3%sOcc' % (i,j,j) ) # 25GeV
          else: self.multicall.get_dqmf_all_results(self.run_spec,'%s/Cal%s/Thresh4%sOcc' % (i,j,j)  ) # 50 GeV
    
    #print( "DEBUG - clusters :",len(self.multicall._MultiCall__call_list),time.time(),"<br>")
    R = tuple(self.multicall())
    #print( "@@DEBUG", R,"<br>")
    for i,I in enumerate(self.multicall._MultiCall__call_list):
      r = R[i][N]
      K = list(r.keys())
      #print( "DEBUG",r,K, "<br>" )
      for k in K:
        if k[0] == "Y" or k[0] == "R":
          # select clusters above threshold
          #print( "DEBUG",k,r[k],self.cluster_threshold,"<br>")
          if int(r[k])<self.cluster_threshold: continue
          m = RE.search(k).groupdict()
          # kDuplciated clusters are a priori obsolete - To be confirmed...
          for cl in self.Clusters:
            #print("@@cl:",cl)
            if k.split("=")[1] == cl[4].split("=")[1]:
              self.Duplicated.append( [float(m['eta']),float(m['phi']),DR_match,I[1][-1],k,int(r[k])] )
              self.Duplicated.append( cl )
          # Duplicated clusters are stored both
          self.Clusters.append( [float(m['eta']),float(m['phi']),DR_match,I[1][-1],k,int(r[k])] ) 
    
    #print("@@self.Clusters",self.Clusters)
    #print("Threshold000", self.cluster_E)


########################################################################
class GetCaloCells(Thread):
  def __init__(self,run_spec,Parts,oldCaloMonitoring):
    Thread.__init__(self)
    self.run_spec = run_spec
    self.Parts = Parts
    self.newCaloMonitoring = not oldCaloMonitoring

  def run(self):
    N = "%d"%(self.run_spec['run_list'][0])
    RE = re.compile(r'.*=\((?P<eta>\S+),(?P<phi>\S+)\).*')
    RE1 = re.compile(r'(?P<cat>\S+)-\(eta,phi\)\[OSRatio\]=\((?P<eta>\S+),(?P<phi>\S+)\)\[(?P<sigma>\S+)\]')
    RE2 = re.compile(r'x-axis\s*\S*\((?P<eta>\S+)\)\s*y-axis\s*\((?P<phi>\S+)\)\)')
    S  = {0:'P',1:'1',2:'2',3:'3'}
    Si = {'r':0,'0':0,'1':1,'2':2,'3':3}
    calls = []

    # Loop on all partitions to retrieve all the DQMF results for the 4 different plots
    for P in self.Parts:
      d = DETlist.index(P[:-1])
      calls.append( mc2(passurl) )
      plot = 'LAr/'+P+'/Occupancy-Noise/Single_Cells/'
      for s in range(4):
        sam = 'Sampling'+str(s)
        s2 = S[s]
        if s==0:
          if d==3: continue
          if d==2: s2='0'
          else: sam='Presampler'
  
        #suffix='_hiEth'
        suffix='_hiEth'
        comment1='Nevts with E&gt;Eth'
        comment2='%qfac>4000(E&gt;Eth)'

       # added CSCveto as suffix as we have named histogram in WD as it should be now in 2021 
        #calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/fractionOverQthVsEtaPhi_%s'%(sam,P[:-1]+s2+P[-1])+suffix )
        #calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/CellOccupancyVsEtaPhi_%s_hiEth'%(sam,P[:-1]+s2+P[-1]) )
        #calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/CellOccupancyVsEtaPhi_%s_5Sigma'%(sam,P[:-1]+s2+P[-1]) )
# Line below commented by Benjamin as the EnergyRMS plot is not fully reliable
# NB: removing a DQ checks result does not affect the rest of the code. Software is apparently robust enough :-)
#        calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/ExtraPlots/EnergyRMSvsDB_%s_noEth'%(sam,P[:-1]+s2+P[-1]) )
        #calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/CellOccupancyVsEtaPhi_%s_5Sigma'%(sam,P[:-1]+s2+P[-1]) )
        calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/fractionOverQthVsEtaPhi_%s_hiEth_CSCveto'%(sam,P[:-1]+s2+P[-1]) )
        calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/CellOccupancyVsEtaPhi_%s_hiEth_CSCveto'%(sam,P[:-1]+s2+P[-1]) )
        calls[-1].multicall.get_dqmf_all_results(self.run_spec,plot+'%s/CellOccupancyVsEtaPhi_%s_5Sigma_CSCveto'%(sam,P[:-1]+s2+P[-1]) )
                    
      calls[-1].start()

    DB = []

    # Loop on all results and prepare a LArID DB query to retrieve the cell identifier
    nbOfQueries = {}
    nbOfQueriescmd = {}
    nbOfQueriescmd14 = {}
    queriesDic = {}
    for p,P in enumerate(self.Parts):
      c = calls[p] 
      c.join()
      d = DETlist.index(P[:-1])
      AC = P[-1]
      for i,I in enumerate(c.multicall._MultiCall__call_list):   
        plot = I[1][-1]
        sam = Si[plot.split('/')[4][-1]] 
        par = (d,AC,sam)      
        V = [] 
        #cmd = 'select ONL_ID,ETA,PHI from LARID where DET=%d and AC=%d and (' % (d,ACmap2[AC])
        cmd = 'select ONL_ID,ETA,PHI from LARID where DET=%d and AC=%d and SAM=%d and (' % (d,ACmap2[AC],sam)
        # Special tretament for the barrel end where sampling 1/2 cells are monitored in sampling 2/3 histos
        # for binning reasons (only in new CaloMonitoring)
        if (self.newCaloMonitoring and d==0 and (sam==2 or sam==3)):
          cmd14 = 'select ONL_ID,ETA,PHI from LARID where DET=%d and AC=%d and SAM=%d and (' % (d,ACmap2[AC],sam-1)
          par14 = (d,AC,sam-1)      
        
        r = c.results[i][N]
        K = list(r.keys())   
        count = 0
        nbOfQueries[plot] = 0
        nbOfQueriescmd[plot] = 0
        nbOfQueriescmd14[plot] = 0
        for k in K:
#          count = count + 1
#          if count == 900 or count == 1800 or count == 2700 or count == 3600 or count == 4500 or count == 5400 or count == 6300: # LArIdTranslator can not handle more than 997 cells
          if nbOfQueries[plot] == 900 or nbOfQueries[plot] == 1800 or nbOfQueries[plot] == 2700 or nbOfQueries[plot] == 3600 or nbOfQueries[plot] == 4500 or nbOfQueries[plot] == 5400 or nbOfQueries[plot] == 6300: # LArIdTranslator can not handle more than 997 cells
            DB.append( db(cmd.rstrip(' or ')+')',plot,par,V) ) # Close the query and send it to the LArIdTranslator
            DB[-1].start()
            #cmd = 'select ONL_ID,ETA,PHI from LARID where DET=%d and AC=%d and (' % (d,ACmap2[AC])
            cmd = 'select ONL_ID,ETA,PHI from LARID where DET=%d and AC=%d and SAM=%d and (' % (d,ACmap2[AC],sam) # Define a new query
          if 'NBins' in k: 
            continue
          elif 'eta' in k:
            m = RE.search(k).groupdict()
          elif 'axis' in k: 
            m = RE2.search(k).groupdict()
          else: continue  
          if 'CellOccupancy' in plot and r[k]<10: # Ignore cells in occupancy if less than 10 events
            continue              
        
          nbOfQueries[plot] += 1
          eta = float(m['eta'])
          if d==1 and sam in [1,2]: eta+=0.0005 # EMEC
          phi = float(m['phi'])
          # Define the phi/eta range that will be used to send a query to LArID
          if AC=='A': phi-=dd[sam]
          else:       phi+=dd[sam] 
          # The command line below can be tested in LArIDTranslator
          if not (self.newCaloMonitoring and d==0 and (sam==2 or sam==3) and (eta>1.4 or eta<-1.4)):
            limits = (eta-de[d][sam],eta+de[d][sam],phi-dp[d][sam],phi+dp[d][sam])
            V.append([limits,r[k],k])
            cmd += '(ETA between %f and %f and PHI between %f and %f) or ' % limits
            nbOfQueriescmd[plot] += 1
            if (abs(eta)<0.005 and d == 0 and sam==1): # Barrel / sampling 1 at eta = 0 - Added by BT on 09/07/2022
              if (eta>0): limits = (0,0.005,phi-dp[d][sam],phi+dp[d][sam])
              else: limits = (-0.005,0,phi-dp[d][sam],phi+dp[d][sam])
          else: # Barrel end in the new CaloMonitoring binning
            # Special hack to match the bin center of histogram with the LArIdTranslator coordinates
            # Sampling 3 / 1.4125 (histo) -> Sampling 2 / 1.4375 (LArIdTranslator)
            # Sampling 2 / 1.4125 (histo) -> Sampling 1 / 1.4125 (LArIdTranslator) + 2 other coordinates also match
            if (sam == 3):
              if (eta>1.4): limits = (eta,(eta+0.03),phi-dp[d][sam-1],phi+dp[d][sam-1])
              else:         limits = ((eta-0.03),eta,phi-dp[d][sam-1],phi+dp[d][sam-1])
            else:           
              limits = ((eta-0.01),(eta+0.01),phi-dp[d][sam-1],phi+dp[d][sam-1])

            V.append([limits,r[k],k])
            cmd14 += '(ETA between %f and %f and PHI between %f and %f) or ' % limits
            nbOfQueriescmd14[plot] += 1
          queriesDic[plot] = {"limits":limits}

        # End of loop on cells - Close the queries
        #print("nbOfQueriescmd14["+plot+"]",nbOfQueriescmd14[plot])
        #print("nbOfQueriescmd["+plot+"]",nbOfQueriescmd[plot])
        #print("nbOfQueries["+plot+"]",nbOfQueries[plot])
        #print("################################################")
        if 'or' in cmd:
          DB.append( db(cmd.rstrip(' or ')+')',plot,par,V) )
          DB[-1].start()
          
        if (self.newCaloMonitoring and d==0 and (sam==2 or sam==3)):
          if 'or' in cmd14:
            #if "fractionOverQthVsEtaPhi_EMB2A_hiEth_CSCveto" in plot:
            #  print("inside cmd14 ", cmd14)
            DB.append( db(cmd14.rstrip(' or ')+')',plot,par14,V) )
            DB[-1].start()
            #print("######## cmd14: ",cmd14)

    nbOfRetrieved = {}
    retrievedDic = {}
    for iPlot in list(nbOfQueries.keys()):
      #print(iPlot)
      nbOfRetrieved[iPlot] = 0

    # Once the LArId cells have been retrieved, associate them with the plot output
    self.Chans = {}
    for s in DB:
      s.join()
      #print("s={}".format(s))
      for r in s.R:
        #print("r={}".format(r))
        id = r[0]
        com = []
        for v in s.V:
          #print("v={}".format(v))
          if r[1]>=v[0][0] and r[1]<v[0][1]:
            if r[2]>=v[0][2] and r[2]<v[0][3]: com.append([v[1],v[2]]) 
        #print("com={}".format(com))
        if id not in self.Chans.keys(): self.Chans[id] = [[],[],[],[]]
        for a in com:
          retrievedDic[s.plot]={"limits":a}
          #print("s.plot={}".format(s.plot))
          nbOfRetrieved[s.plot] += 1
          if 'CellOccupancy' in s.plot:
            if 'hiEth' in s.plot:
              self.Chans[id][0].append( [a[0],comment1,s.plot] )
            if '5S' in s.plot:
              self.Chans[id][1].append( [a[0],'Nevts with E&gt;5&#963;',s.plot] )
          if 'DB' in s.plot:
            self.Chans[id][2].append( [a[0],'DB noise deviation',s.plot] )
          if 'Qth' in s.plot:
            self.Chans[id][3].append( [a[0]*100,comment2,s.plot] )
    #print("nbOfQueries.keys",nbOfQueries.keys())

    for iPlot in list(nbOfQueries.keys()):
      if (nbOfQueries[iPlot] != nbOfRetrieved[iPlot]):# and not ("FCAL" in iPlot):
        print("Problem in LArIdTranslator retrieval - Please report it to the developer")
        print("%s: %d queries but %d retrieved"%(iPlot,nbOfQueries[iPlot],nbOfRetrieved[iPlot]))
        print("<br>")

 
########################################################################
class GetSporadic(Thread):
  def __init__(self,run_spec,Parts,excludeSporadicLBs):
    Thread.__init__(self)
    self.run_spec = run_spec
    self.Parts = Parts
    self.exclude = excludeSporadicLBs

  def run(self):
    N = "%d"%(self.run_spec['run_list'][0])
    RE0 = re.compile(r'FT(?P<FT>\S+)Sl(?P<SL>\S+)Ch(?P<CH>\S+)')
    RE1 = re.compile(r'\((?P<LB>\S+)\.0*\)')
    calls = []
    for P in self.Parts:
      calls.append( mc3(passurl,P,self.run_spec) )
      calls[-1].multicall.get_dqmf_sub_assessments(self.run_spec,'LAr/'+P+'/Occupancy-Noise/Single_Cells/Non_Gaussian_Noise')
      calls[-1].start()

    self.Chans = {}
    LBs = {}

    for p,P in enumerate(self.Parts):
      calls[p].join()
      plot = 'LAr/'+P+'/Occupancy-Noise/Single_Cells/Non_Gaussian_Noise/'
      for h in calls[p].h2p:
        m = RE0.search(h[0].split('_')[1]).groupdict() # 3/5/2016 : Bug fix here to restore sproadicPlot (broken since when??)
        SL = int(m['SL'])
        FT = int(m['FT'])
        CH = int(m['CH'])
        d = DETlist.index(P[:-1])
        id = get_id((d,ACmap2[P[-1]],FT,SL,CH))
        dev = 0.
        if 'SideBands' in h[1]:
       
          if 'EN' in plot+h[0]:
            title = "Nb of evts with E&gt;5-15GeV"
          else:
            title = "% of evts with Q&gt;4000"            
          dev = float(h[1]['SideBands'])
          if str(dev)=='nan': dev=-0.01
        elif 'NBins' in h[1]: 
          title = 'Affected LBs: '
#          dev = int(h[1]['NBins'])
          LB = []
          nLBkept = 0
          for lb in list(h[1].keys()):
            if 'NBins' in lb: continue
            m = RE1.search(lb).groupdict()
            if ((m['LB']) not in (self.exclude).split(" ")): # Check that the LB is not in the excludeLB list
              nLBkept = nLBkept + 1
            LB.append( int(m['LB']) )
            title+=m['LB']+' '
          LBs[id] = LB
          dev = nLBkept
        if id not in self.Chans: self.Chans[id] = [ [],[],[] ]
        if 'ENLB' in h[0]: self.Chans[id][1].append( [dev,title,plot+h[0]] )
        elif 'EN' in h[0]: self.Chans[id][0].append( [dev,title,plot+h[0]] )
        else: self.Chans[id][2].append( [100*dev,title,plot+h[0]] )

    # Final cleaning 
    missingPlots = 0
    for id in list(self.Chans.keys()):
      # First check that 3 plots have been retrieved. Otherwise, delete the key and send a warning
      if (self.Chans[id][0] == [] or self.Chans[id][1] == [] or self.Chans[id][2] == []):
        missingPlots = missingPlots + 1
        del self.Chans[id]
      # Channels with less than sporadic_absoluteEvt are deleted  
      elif (self.Chans[id][0][0][0] < flagParam['sporadic_absoluteEvt']):
        del self.Chans[id]
      # Channels with 0 LB affected (because of excludeLBSporadic) have a number of events equal to 0
      # LB affected are however kept
      elif (self.Chans[id][1][0][0] == 0):
        self.Chans[id][0][0][0] = 0

    if missingPlots>0:
      print("%d channels with uncomplete output in sporadic column. Maybe a timeout problems..."%(missingPlots))
      print("Please try resending the request...")


########################################################################
def NoisyLArCells(run_spec,opts_noisycells,Sides,Clusters,Duplicated,Clusters_found,cursor,advOptions):

  min_signifDQM = 25.# so far the same as the one used in Webdipslay to get a cluster YELLOW
  #print("@@Clusters:",Clusters)
  if debug:
    print("Starting NoisyLArCells")

  # Redefinition of flagParam in function of advOptions
  if (advOptions['sporadic_threshold'] == 'NClusters / NClusters (PS)'):
    flagParam['sporadic_threshold'] = 1 # CaloCells and sporadic columns
    flagParam['sporadic_threshold_PS'] = 1 # CaloCells and sporadic columns
  elif (advOptions['sporadic_threshold'] == 'NClusters / 2*NClusters (PS)'):
    flagParam['sporadic_threshold'] = 1 # CaloCells and sporadic columns
    flagParam['sporadic_threshold_PS'] = 2 # CaloCells and sporadic columns
  elif (advOptions['sporadic_threshold'] == 'NClusters / 10*NClusters (PS)'):
    flagParam['sporadic_threshold'] = 1 # CaloCells and sporadic columns
    flagParam['sporadic_threshold_PS'] = 10 # CaloCells and sporadic columns

  flagParam['calocells_absoluteEvt_Eth'] = 5*advOptions['cluster_threshold']


  if (run_spec['useAtlasdqm_dev']):
    passurl = 'https://%s@atlasdqm-dev.cern.ch' % passwd

  if debug:
    print("Strting NoisyLArCells")
    firstDebug = True
    print(advOptions)
    print(opts_noisycells)

  # Queries 
  DETs = list(range(4))
  if 'DET' in opts_noisycells:
    if True: 
      if int(opts_noisycells['DET'])==0: DETs = [0]
      else: DETs = [1,2,3]
    else: DETs = [int(opts_noisycells['DET'])]
  sides = ['A','C']
  if 'AC' in opts_noisycells: 
    if 0 in DETs: pass
    else: sides = [ACmap[opts_noisycells['AC']][0]]

  lce_exists = True
  lce_message = False
  try:open(opts_noisycells['lce_inputfile'])
  except IOError:
    print('<h3><font color=red>Warning: LArCellEmpty file not found!</font>:')
    lce_exists = False
    lce_message = True
  if lce_exists:  
    size = os.path.getsize(opts_noisycells['lce_inputfile'])
    if (size < 1000):
      print('<h3><font color=red>Warning:  LArCellEmpty file is very small! </font>:')
      lce_message = True
  if not opts_noisycells['larcellempty']:
    print('<h3><font color=red>Warning: LArCellEmpty channels were not retrieved! </font>:')
    lce_message = True
  if lce_message and (not run_spec['isexpress']): print('This is however not problematic for the bulk, as this output is not used for flagging proposal.</font></h3>')
  else: print('</font></h3>')

  # Box with the flagging policy details (to be shown on request)
  print('<br><div style="text-align:left" id="flaggingPolicy" class="advancedrectangle">\n')
  print('<p  onclick="showit2(this);"><u style="font-family:Arial;color:blue;">Policy to flag cells matched to a cluster hot spot (> %d clusters) </u> <!--f <br />'%(advOptions['cluster_threshold']))
  print("<b> == CaloCells column </b>: cell suspicious if <br>* (more than %d events (%d) in <font color=green> Eth </font> (<font color=red>5sigma</font>) plots),<br>* or (more than %.1f times (nb of clusters) events in in <font color=green> Eth plot</font>)<br>* or (more than %.1f times (nb of clusters) events in in <font color=green> Eth plot</font> for the noisiest cell matched to the cluster). <br>"%(flagParam['calocells_absoluteEvt_Eth'],flagParam['calocells_absoluteEvt_5sigma'],flagParam['calocells_ratioClus_Eth'],0.75*flagParam['calocells_ratioClus_Eth']))
  print("SBN proposed if less than %d events (%d in the PS) above <font color=green> Eth </font> remaining (based on <font color=pink> q factor plot </font>), otherwise HNHG <br><br>"%(flagParam['sporadic_threshold']*advOptions['cluster_threshold'],flagParam['sporadic_threshold_PS']*advOptions['cluster_threshold']))
  print("<b> == Sporadic column</b>: cell suspicious if more than %d events in <font color=red>5-15 GeV plots</font> <br>"%(flagParam['sporadic_absoluteEvt']))
  print("SBN proposed if less than %d events (%d in the PS) remaining (based on <font color=pink> q factor plot </font>), otherwise HNHG <br><br>"%(flagParam['sporadic_threshold']*advOptions['cluster_threshold'],flagParam['sporadic_threshold_PS']*advOptions['cluster_threshold']))
  print("<b> == LArCellsEmpty column</b>: cell suspicious if more than <font color=red> %d events </font> with <font color=green> a mean value </font> > %d GeV<br>"%(flagParam['lce_nbEvt'],flagParam['lce_meanEnergy']))
  print("SBN proposed if less than %d events remaining, otherwise HNHG <br>"%(flagParam['sporadic_threshold_lce']))
  if advOptions['MNB_treat'] == "No special treatment":print("<br><b> == Suspicious MNB FEBs</b>: ")    
  elif advOptions['MNB_treat'] == "highNoiseHG forbidden":print("<br><b> == Suspicious MNB FEBs</b>: the HNHG proposals are forced to SBN (except if the <font color=pink> q factor plot </font> indicate a quality factor always lower than 4000 - In such a case:no proposal)")    
  elif advOptions['MNB_treat'] == "No flag proposed (except >4 nbclus)":print("<br><b> == Suspicious MNB FEBs</b>: no flag proposal in low noise channels (< %d events in <font color=green> Eth </font> plot). For high noise channels, SBN proposed if relevant, HNHG forbidden."%(4*advOptions['cluster_threshold']))    
  elif advOptions['MNB_treat'] == "No flag proposed":print("<br><b> == Suspicious MNB FEBs</b>: no flag proposal in any channel")    

  print(' f--></p>')

  print('<p  onclick="showit2(this);"><u style="font-family:Arial;color:blue;">Policy to flag cells not matched to a cluster hot spot</u> <!--f <br />')
  print("<b> == CaloCells column </b>: cell suspicious if more than %d events (%d) in <font color=green> Eth </font> (<font color=red>5sigma</font>) plots <br>"%(flagParam['calocells_absoluteEvt_unmatched_Eth'],flagParam['calocells_absoluteEvt_unmatched_5sigma']))
  print("SBN proposed if less than %d events above <font color=green> Eth </font> remaining (based on <font color=pink> q factor plot </font>), otherwise HNHG <br><br>"%(flagParam['sporadic_threshold_unmatched']))
  print("<b> == Sporadic column</b>: cell suspicious if more than %d events in <font color=red>5-15 GeV plots</font> <br>"%(flagParam['sporadic_absoluteEvt_unmatched']))
  print("SBN proposed if less than %d events remaining, otherwise HNHG <br><br>"%(flagParam['sporadic_threshold_unmatched']))
  print("<b> == LArCellsEmpty column</b>: cell suspicious if more than <font color=red> %d events </font> with <font color=green> a mean value </font> > %d GeV<br>"%(flagParam['lce_nbEvt_unmatched'],flagParam['lce_meanEnergy_unmatched']))
  print("SBN proposed if less than %d events remaining, otherwise HNHG <br><br>"%(flagParam['sporadic_threshold_lce_unmatched']))
  print("<b>NB</b>: unmatched cells displayed only if more than %d events (%d) in <font color=green> Eth </font> (<font color=red>5sigma</font>) plots <br>"%(flagParam['calocells_absoluteEvt_unmatched_Eth']/5,flagParam['calocells_absoluteEvt_unmatched_5sigma']/5))
  print(' f--></p><br>')
#  print ' f--></p></div><br>'

  # Box with the trigger prescale plots (to be shown on request)
#  print '<br><div style="text-align:left" id="trigger_prescale" class="advancedrectangle">\n'
  print('<p  onclick="showit2(this);"><u style="font-family:Arial;color:blue;">Show trigger prescales in CosmicCalo stream</u> <!--f <br />')
  for iTrig in ["L1_EM3_EMPTY","L1_EM7_EMPTY","L1_J12_EMPTY","L1_J30.31ETA49_EMPTY","L1_J30_EMPTY","L1_RD1_EMPTY","L1_TAU8_EMPTY"]:
    print('<a href="https://atlasdaq.cern.ch/info/mda/db/ATLAS/%d/LArHistogramming.LArTrigger_prescale./LAr/CosmicCalo/%s"><img style="width:400px" src="https://atlasdaq.cern.ch/info/mda/get/MDA-LAr-All/r0000%d_lEoR_ATLAS_LAr-MDA_LAr-Histogramming.root//LArHistogramming/LArTrigger_prescale/LAr/CosmicCalo/%s/"/></a>\n'%(run_spec['run_list'][0],iTrig,run_spec['run_list'][0],iTrig))
  print(' f--></p><br>')

  print('<p  onclick="showit2(this);"><u style="font-family:Arial;color:blue;">Show trigger prescales in LArCellsEmpty stream</u> <!--f <br />')
  for iTrig in ["L1_EM3_EMPTY","L1_EM7_EMPTY","L1_EM7_FIRSTEMPTY","L1_J12_EMPTY","L1_J12_FIRSTEMPTY","L1_J30.31ETA49_EMPTY","L1_J30_FIRSTEMPTY","L1_TAU8_EMPTY","L1_TAU8_FIRSTEMPTY"]:
    print('<a href="https://atlasdaq.cern.ch/info/mda/db/ATLAS/%d/LArHistogramming.LArTrigger_prescale./LAr/LArCellsEmpt/%s"><img style="width:400px" src="https://atlasdaq.cern.ch/info/mda/get/MDA-LAr-All/r0000%d_lEoR_ATLAS_LAr-MDA_LAr-Histogramming.root//LArHistogramming/LArTrigger_prescale/LAr/LArCellsEmpt/%s/"/></a>\n'%(run_spec['run_list'][0],iTrig,run_spec['run_list'][0],iTrig))
  print(' f--></p></div><br>')


  for d in DETs:    
    for AC in sides:      
      Parts.append( DETlist[d]+AC )      
  if 'EMECC' in Parts or 'HECC' in Parts or 'FCALC' in Parts: Sides.append('ECC')
  if 'EMBA'  in Parts or 'EMBC' in Parts: Sides.append('BAR')
  if 'EMECA' in Parts or 'HECA' in Parts or 'FCALA' in Parts: Sides.append('ECA')
  #print("##Sides:",Sides)

  # fetch DQ results
  jobs = [0,0,0,0]
  if opts_noisycells['rawchannels']: raw = GetRawChannels(run_spec,Parts); raw.start(); jobs[0] = raw
  #print("I DID raw cahnnel")
  if opts_noisycells['calocells']: calo = GetCaloCells(run_spec,Parts,advOptions['oldCaloMonitoring']); calo.start(); jobs[1] = calo
  #print("I did calo cells")
  if opts_noisycells['sporadic']: spo = GetSporadic(run_spec,Parts,advOptions['excludeLBSporadic']); spo.start(); jobs[2] = spo
  #print("I did sporadic")
  if opts_noisycells['larcellempty'] and lce_exists: lce = GetLArCellEmpty(advOptions['cluster_threshold'],advOptions['considerLCE_HEC'],opts_noisycells['lce_inputfile']); lce.start();jobs[3] = lce  #remove the comment
  if (advOptions["cluster_E"] != "Et > 4/10GeV"):
      print("<br> <h3><font color=red>Warning : </font>: the cluster matching will be based on %s cluster plots"%(advOptions["cluster_E"]))
  clus = GetClusters(run_spec,Sides,Clusters["stream:%s"%advOptions['cluster_E']],Duplicated,advOptions['cluster_threshold'],advOptions['cluster_E']); clus.start()
  #print("-Clusters",Clusters)
  # Remove the lower Et reference to not retrieve them as useless
  if (advOptions['cluster_E'] == "Et > 10/25GeV" or advOptions['cluster_E'] == "Et > 10/10GeV"):
    del Clusters["stream: Et > 4/10GeV"]
  if (advOptions['cluster_E'] == "Et > 25/50GeV"):
    del Clusters["stream: Et > 4/10GeV"]
    del Clusters["stream: Et > 10/25GeV"]

  # Retrieves the clusters for collision stream and the upper Et values of CosmicCalo
  if ("CosmicCalo" in run_spec["stream"]): 
    run_specColl = run_spec.copy()
    run_specColl['stream'] = run_spec['stream_coll']
    run_specColl['proc_ver'] = run_spec['proc_ver_coll']

    DuplicatedColl = [] # Fake one, not used
    for etThresh in list(Clusters.keys()):
      if "stream" in etThresh and advOptions['cluster_E'] not in etThresh:
        clusStream = GetClusters(run_spec,Sides,Clusters[etThresh],DuplicatedColl,advOptions['cluster_threshold'],etThresh.split(":")[1]); clusStream.start()
      if "collisions" in etThresh:
        clusColl = GetClusters(run_specColl,Sides,Clusters[etThresh],DuplicatedColl,advOptions['cluster_threshold'],etThresh.split(":")[1]); clusColl.start()

#  print("---Clusters",Clusters)

  # fetch badchannels
  Bad = [{},{},{},{}]
  run = run_spec['run_list'][0]
  if run>0:
    b_conn = sqlite3.connect(dbfile)
    b_cursor = b_conn.cursor()
    for u,upd in enumerate(['1','4']):
      for b,bad in enumerate(['BadChannels','MissingFEBs']):
        b_cursor.execute('select status from %s where %d>=since and %d<=until'%(bad+'UPD'+upd,run,run))
        for d in b_cursor.fetchall(): 
           dic_withoutL = d[0].decode("utf-8").replace('L','')
           #d_withoutL = stripLfromNumber(d_withL)
           #print(d_withoutL)
           Bad[2*u+b].update(eval(str(dic_withoutL))) ###Need to be added back once
        #for d in b_cursor.fetchall(): print((str(d[0].decode("utf-8")))) ###Need to be added back once
    b_conn.close()

  # Collect all channels found in WebDisplay
  R = []
  Chans = set([])
#  print("sumit",jobs[1])
  for i,j in enumerate(jobs):
 
    if j==0: continue
    j.join()
    try: list(j.Chans.keys())
    except:
      print('<h3><font color=red> Histograms are not found.  Please verify that the plots in the webdisplay exist. </font></h3>')
      sys.exit()
    C = list(j.Chans.keys())
    for ch in C:Chans.add(ch)

  #print("Chans={}".format(Chans)) # It prints all the Channels !!!
  # Send multiple queries to LArIdTranslator to retrieve the exact online id of suspicious cells
  R1 = []
  if len(Chans)>0:
    cmd = 'select '+','.join(cols[:11])+',FEB_ID from LARID where ONL_ID in ('    
    for ch in Chans: cmd += '%d,'%(ch)   
    cursor.execute(cmd.rstrip(',')+')')   
    R1 = cursor.fetchall()

  R2 = []
  for r in R1:
    id = r[0]
    feb_id = r[-1]
    upd1=[0,0] 
    if id in Bad[0]: upd1[0] = Bad[0][id]
    if feb_id in Bad[1]: upd1[1] = Bad[1][feb_id]
    upd4=[0,0] 
    if id in Bad[2]: upd4[0] = Bad[2][id] 
    if feb_id in Bad[3]: upd4[1] = Bad[3][feb_id]
    i = list(r[:-3])+[ "<a title='Corr. factor: %.2f'> %s </a>"%(r[-2],r[-3]),upd1,upd4, [[],[],[],[]] , [[],[],[],[]] , [[],[],[]] ,[[],[],[],[]] ,-1 ]
    for j,J in enumerate(jobs):
      if J:
        if id in J.Chans:
          i[12+j] = J.Chans[id]
          #print "JL DEBUG",j,J,id,i,"<br><br>"
    R2.append(i)
  #print("R2={}".format(R2))
# Treatment of noisy cells with cluster matching
  if ("CosmicCalo" in run_spec["stream"]):
    clusColl.join()    
#  print ClustersColl

  alreadyProp = [] # list of cells proposed for flagging

  clus.join()
  from math import pi,sqrt
  R_used = len(R2)*[0]

  Clusters["stream:%s"%advOptions['cluster_E']].sort(key=lambda x: x[5],reverse=True)
  #print("Clusters={}".format(Clusters["stream:%s"%advOptions['cluster_E']]))
  # Now will loop on all clusters
  origFlagList = "" # This list is used only to find the potential holes. Not dynamically updated

  for cl in Clusters["stream:%s"%advOptions['cluster_E']]:
#    print("sumit",cl[3].split("/"))
#    if debug:
#      print("<br>=============Now examining cluster",cl)
    # Clusters can be duplicated between barrel and endcap (only histo binning consequence)
    # Keep only the endcap clusters
    if (cl in Duplicated and "BAR/" in cl[3]):
      continue

    R3 = [] # R3 is the list of suspicious cells in the vicinity of clusters
    R3Eth = []

    # Line below was for run 2
    # clusterType=cl[3].split("/")[0].split("Topo")[0]
    # Line below added by BT on 9/7/2022 to use the new naming scheme of CaloMonitoring
    if ("EM" in cl[3].split("/")[1]): clusterType = "EM"
    else: clusterType = "Calo"


    signifDQM = float(cl[4].split('[')[2].split(']')[0])
    if (signifDQM<min_signifDQM): continue

    if debug:
#      print("Looping on cells...")
      if firstDebug:
        #print(R2)
        firstDebug = False

    for i in R2: # R2 is the list of all channels identified by DQMF checks
      etaPhi_string = cl[4].split("=")[1].split("[")[0]
      eta = i[7]
      phi = i[8]
      det = i[1]
      sampling = i[6]

      ##### Match EM clusters with EMB (det=0),EMEC (1) cells #####
      ##### Match TopoClusters with HEC (2),FCAL(3) cells #####
      # EMTopo clusters and detectors != EMBC/EMEC
      if clusterType=="EM" and not(det==0 or det ==1):continue 
      # CaloTopo at eta<2.5 clusters and detectors != HEC/FCal
      # Line below commented by BT on 11/07/2022 because of CaloTopo created by EM channel (TO BE UNDERSTOOD)
      #if clusterType=="Calo" and abs(eta)<2.5 and not(det==2 or det==3):continue
      # CaloTopo at abs(eta)>2.5 clusters and detectors != EMEC/HEC/FCal 
      # At abs(eta)>2.5, EMEC noisy channels do not create EMTopoClusters, hence need to monitor them with CaloTopoClusters
      if clusterType=="Calo" and abs(eta)>2.5 and not(det==1 or det==2 or det==3):continue 

      #if (debug and cl[3] == 'CaloTopoClusters/CalECA/Thresh2ECAOcc'):
        #print("I found relevant partitions",det)
        #print (i)
        #print("==============")
        
      if i[2]>0: phi-=dd[i[6]]
      else:      phi+=dd[i[6]]
      deta = abs( eta-cl[0] )
      dphi = abs( phi-cl[1] )
      if dphi>2*pi: dphi-=2*pi
      dR = sqrt(deta*deta+dphi*dphi)

      if dR<cl[2]: # Collect all identified channels in vicinity of the cluster hot spot
        R_used[ R2.index(i) ] += 1
        i[-1] = dR
        R3.append(i)
        # Store the value of CaloCells to reorder the cells
        if len(i[13][0])>0:
          if len(i[13][0][0])>0: # Number of events above Eth
            R3Eth.append(float(i[13][0][0][0]))
        else:
          if len(i[13][1])>0:# Number of events above 5sigma
            R3Eth.append(float(i[13][1][0][0])/100000)
          else:
            R3Eth.append(0.)

    # When all cells are collected in R3, reorder them by the lvel of their noise
    R3Eth_sorted = sorted(list(range(len(R3Eth))),key=lambda x:R3Eth[x], reverse=True)
    R3_sorted = [R3[i] for i in R3Eth_sorted ]
    R3 = R3_sorted

    if cl[4][0] == "Y":Class = GetClass("Warning")
    if cl[4][0] == "R":Class = GetClass("Error")

#    if debug:
#      print("Found %d suspicious cells"%(len(R3)))

    # If at least one channel in vicinity, display the cluster and try to propose flag.
    # This cut usually avoids to display CaloTopoClusters initiated by EM cells but is not robust against
    # the situation where the CaloTopoCluster accumulation is above "Min nb of clusters" and the EMTopoClusters below
    # This leads to a situation with a broken link in unmatched clusters
    if len(R3)>0: 
      if ("EM" in cl[3]):
        scriptCommandLine = hotSpotInHIST_commandLine(run_spec,cl[0],cl[1],"EMTopoClusters")
      else:
        scriptCommandLine = hotSpotInHIST_commandLine(run_spec,cl[0],cl[1],"TopoClusters")

      nbClus = cl[5]
      
      # Line below was for run 2
      #clusterType = cl[3].split('Clusters')[0].split("/")[3]
      # Line below added by BT on 9/7/2022 to use the new naming scheme of CaloMonitoring
      if ("EM" in cl[3].split("/")[1]): clusterType = "EMTopo"
      else: clusterType = "CaloTopo"


      if debug:
        print("Sending print_rows for the %d clusters"%(cl[5]))
      (out,Clusters_found[ Clusters["stream:%s"%advOptions['cluster_E']].index(cl) ],subList,rows_checkCorrelHistCommandLine,rows_checkCorrelHistCommandLineAll) = print_rows.PrintRows(flagParam,advOptions,nbClus,run_spec['url_webdisplay'],run_spec,alreadyProp,R3)
      checkCorrelHistCommandLine = "python -i checkCorrelInHIST.py -r %d -s %s -a %s -x %.2f -y %.2f -d 0.15 --histoWD 2d %s %s"%(run_spec['run_list'][0],run_spec['stream'].split("_")[1],run_spec["amiTag"].split("_")[0],cl[0],cl[1],cl[3],rows_checkCorrelHistCommandLine)
      checkCorrelHistCommandLineAll = "python -i checkCorrelInHIST.py -r %d -s %s -a %s -x %.2f -y %.2f -d 0.15 --histoWD 2d %s %s"%(run_spec['run_list'][0],run_spec['stream'].split("_")[1],run_spec["amiTag"].split("_")[0],cl[0],cl[1],cl[3],rows_checkCorrelHistCommandLineAll)
      # Now print the top cluster row
      addInfo = ""
      for iHigherEt in list(Clusters.keys()):
        if (iHigherEt != "stream:%s"%advOptions['cluster_E']):
          for iCluster in Clusters[iHigherEt]:
            if (etaPhi_string in iCluster[4] and  clusterType in iCluster[3]):
              if ("stream" in iHigherEt):
                if clusterType == "EMTopo":
                  addInfo += "    // %.0f%% (%d) remaining at %s GeV"%(100*iCluster[5]/cl[5],iCluster[5],iHigherEt.split("> ")[1].split("/")[0])
                else:
                  addInfo += "    // %.0f%% (%d) remaining at %s"%(100*iCluster[5]/cl[5],iCluster[5],iHigherEt.split("> ")[1].split("/")[1])
              else:
                signif = float((iCluster[4].split("[")[2]).split("]")[0])
                if signif > 0.:
                  addInfo += "    // Excess spotted in <a href='#ShowCollisionPlots' target='_self'> collision stream "
                else:
                  addInfo += "    // Deficit spotted in <a href='#ShowCollisionPlots' target='_self'> collision stream "
                if clusterType == "EMTopo":
                  addInfo += "</a>for Et > %s GeV "%(iHigherEt.split("> ")[1].split("/")[0])
                else:
                  addInfo += "</a>for Et > %s GeV "%(iHigherEt.split("> ")[1].split("/")[1])

                
      print('<tr><td colspan=19><div id="Cluster-%s"> <font style="color:#FFFFFF"> <a class="det%d" href="%s/%s"> %d %s </a>at position %s  %s </font>' % (cl[4],Class,run_spec['url_webdisplay'],cl[3],cl[5],clusterType,etaPhi_string,addInfo))      
      print('<font style="color:#FFFFFF"><p  onclick="showit2(this);"> hotSpotInHIST command line: <u style="font-family:Arial;color:blue;">show</u> <!--f %s f--></p><p  onclick="showit2(this);">checkCorrelInHIST command line (flagged cells): <u style="font-family:Arial;color:blue;">show</u> <!--f  %s  f--></p> <p  onclick="showit2(this);">checkCorrelInHIST command line (all cells): <u style="font-family:Arial;color:blue;">show</u> <!--f  %s  f--></p></font></td></tr>' % (scriptCommandLine,checkCorrelHistCommandLine,checkCorrelHistCommandLineAll))
      print(out)
      if False and (checkCorrelHistCommandLine):
        print('<tr><td colspan=19>checkCorrelInHIST command line: %s</td></tr>' % (checkCorrelHistCommandLine))
        print('<tr><td colspan=19>checkCorrelInHIST command line: <u style="font-family:Arial;color:blue;">show</u> <!--f %s f--></p></td></tr>' % (checkCorrelHistCommandLine))
      origFlagList += subList

  # End of loop on clusters
  # Now collect cells not matched to any cluster
#  print("Before print field")
  print_top.PrintFields()
#  print("After print field")
  R4 = []

  for i in range(len(R2)): 
    if R_used[i]==0: R4.append( R2[i] )
  nbClus = 0.
  #print("R4={}".format(R4))
  
  (out,dummyOutput,subList,dummyCommandLine,dummyCommandLine2) = print_rows.PrintRows(flagParam,advOptions,nbClus,run_spec['url_webdisplay'],run_spec,alreadyProp,R4)
  #if debug :print(out)
  print(out)
  origFlagList += subList

  if debug:
    print("Entering NNF with the list",origFlagList)
  nearestOutput = nearestNeighboursFinder.get_html(origFlagList,2,5,True,[run_spec['run_list'][0]])
  if debug:
    print("I am done with NNF")

  return nearestOutput



########################################################################
def PrimaryTable(run_spec,run_COOL,advOptions):
  print('<br><table class="report">\n')

  streamName = run_spec['stream'].split('_')[1]
  streamNameColl = run_spec['stream_coll'].split('_')[1]
  if "tmp" in run_spec['stream']:
    streamName = "<b> [Partial] </b>%s"%(run_spec['stream'].split('_')[1])
  else:
    streamName = run_spec['stream'].split('_')[1]

  print('''
<tr class="row"><td><b>Run info</b></td><td><b>LAr Global</b></td><td><b>Partition</b></td><td colspan="4"><b>Mini noise bursts</b></td><td><b>Noise bursts</b></td></tr>
<tr class="row"><b><td></td><td></td><td></td><td>Suspicious FEBs</td><td colspan="2">Veto cleaning</td><td>New suspicious FEBs</td><td></td></tr></b>
<tr class="row"><td>Stream: %s</td><td></td><td></td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>
'''%(streamName,streamName,streamName,streamNameColl,streamName,streamName))#*6)#,streamNameColl,(streamName,)*2)
  
  if debug:
    print("Start CaloMonFilter")
  server = xmlrpc.client.ServerProxy(passurl)
  multicall = xmlrpc.client.MultiCall(server )    
  multicallName=[]
  multicall.get_hist_binvals(run_spec,'LAr/LAR_GLOBAL/Run_Parameters/nEvtsRejectByDifferentTool')
  multicallName.append("CaloMonFilter")

  multicall.get_dqmf_all_results(run_spec,'LAr/LAR_GLOBAL/Collisions-Bkg/LArCollTimeLumiBlockTimeCut')
  multicallName.append("CollisionsCandidate")

  multicall.get_hist_binvals(run_spec, "LAr/LAR_GLOBAL/Baseline/hprof1d_pedestalMonEM_AllEta") 
  multicallName.append("hprof1d_pedestalMon_EM_AllEta")
  multicall.get_hist_binvals(run_spec, "LAr/LAR_GLOBAL/Baseline/hprof1d_pedestalMonHEC+FCal_AllEta") 
  multicallName.append("hprof1d_pedestalMon_HEC+FCal_AllEta")      
  multicall.get_hist_binvals(run_spec, "LAr/LAR_GLOBAL/Baseline/hprof1d_pedestalMon_EM_LB") 
  multicallName.append("hprof1d_pedestalMon_EM_LB")
  multicall.get_hist_binvals(run_spec, "LAr/LAR_GLOBAL/Baseline/hprof1d_pedestalMon_HEC+FCal_LB") 
  multicallName.append("hprof1d_pedestalMon_HEC+FCal_LB")      

  namedResult=dict()
  multicall()
  for (resName,res) in zip(multicallName,multicall()):
    namedResult[resName]=res.get(str(run_spec['run_list'][0]))

  out = '<tr class="out0"><td rowspan="4" style="vertical-align:top">%s</td>\n'%firstGeneralColumn(run_spec,run_COOL)
  out += '<td rowspan="4" style="vertical-align:top;text-align:right">\n'
  if namedResult['CollisionsCandidate'] is None:
    out += 'Not available<br>'
  else:
    collisionsCand = dqmfResLB(namedResult["CollisionsCandidate"],run_COOL['ATLASREADY'])
    colorThreshold = {}
    colorThreshold['red'] = 10
    colorThreshold['orange'] = 1
    colorThreshold['black'] = 0

    dqmfResLB_out = dqmfResLB_display("Collision candidates<br>","%s/LAr/LAR_GLOBAL/Collisions-Bkg/LArCollTimeLumiBlockTimeCut"%(run_spec['url_webdisplay']),collisionsCand,run_COOL['NevtPerLB'],4,colorThreshold,True,False,True)
    out += dqmfResLB_out['html']


  if debug:
    print("CaloMonFilter displayed")
  out += '<hr /><b><a href="%s/LAr/LAR_GLOBAL/Run_Parameters/nEvtsRejectByDifferentTool"> CaloMonitoring filtering</a></b> <br><br>'%run_spec['url_webdisplay']
  if namedResult["CaloMonFilter"] is None:
    out += "Not available"
  else:
    totNb = namedResult["CaloMonFilter"][1]
    out += 'Total nb of events: %d <br><br>'%(totNb)
    resStr = []
    for i in range(1,7):
      if i==4: # Fix for beam background that do no longer exist
        diffFilter = 0
      else:
        if i==5:
          diffFilter = namedResult["CaloMonFilter"][i-1]-namedResult["CaloMonFilter"][i+1]
        else:
          diffFilter = namedResult["CaloMonFilter"][i]-namedResult["CaloMonFilter"][i+1]

      if (diffFilter != 0):
        resStr.append("%d filtered events (%.1f &#37)"%(diffFilter,diffFilter/totNb*100.))
      else:
        resStr.append("no filtered event")

    out += 'ATLAS not ready: %s<br>'%(resStr[0])
    out += 'LAr intolerable defect: %s<br>'%(resStr[1])
    out += 'Collisions: %s<br>'%(resStr[2])
    #out += 'Beam background : %s<br>'%(resStr[3]) # July 2022 : Skipping "Beam background" as not yet implemented in filtering
    out += 'Trigger         : %s<br>'%(resStr[4]) 
    out += 'EventInfo::LArError: %s<br>'%(resStr[5])

  if ((namedResult["hprof1d_pedestalMon_EM_LB"] is None) or (namedResult["hprof1d_pedestalMon_HEC+FCal_LB"] is None)):
    out += "CaloCells baseline not available"
  else:
    baselineThreshold = {"EM":[20000,20000],"HEC+FCal":[20000,20000]} # [eta threshold;LB threshold]
    out += dqmfCaloBaseline_display(namedResult,run_COOL['ATLASREADY'],"pedestal",baselineThreshold,run_spec['url_webdisplay'])

  out += '</td>\n'

  #Launch queries in separate threads for the MNB queries
  calls = []

  Part = ["EMBC","EMBA","EMECC","EMECA"]
  for P in Part:
    calls.append( queryPrimaryTable(passurl,run_spec,P) )
    calls[-1].start()
    pass
  if debug:
    print("queryPrimaryTable now really over")

  # Retrieve number of events per LB
  server = xmlrpc.client.ServerProxy(passurl)
  NevtPerLB2 = server.get_hist_binvals(run_spec, "LAr/LAR_GLOBAL/Run_Parameters/NbOfEventsVsLB") 
  del server
  NevtPerLB = dict()
  nTotEvt = 0.
  for i in range(len(NevtPerLB2["%d"%run_spec['run_list'][0]])-1):
    NevtPerLB[i]= NevtPerLB2["%d"%run_spec['run_list'][0]][i+1]
    nTotEvt += NevtPerLB[i]

  MNB_newTight_threshold = {"EMBC":0.05,"EMBA":0.05,"EMECC":0.3,"EMECA":0.3}
  colorThreshold = {}
  return_suspicious = ""

  for p,P in enumerate(Part):
    if p>0:out += '<tr class="out0">\n'
    out += '<td>%s</td>\n<td>'%P
    calls[p].join()
    if debug:
      print("In MNBtreatment")
      #print(calls[p].namedResult)
    
    if (calls[p].namedResult["MNB_suspiciousFEB"] != None):
      MNB_currentSuspFEB = calls[p].namedResult["MNB_suspiciousFEB"]
      for iX in range(len(MNB_currentSuspFEB)):
        for iY in range(len(MNB_currentSuspFEB[iX])):
          if (MNB_currentSuspFEB[iX][iY]):
            out += '<a href="%s/LAr/%s/Detector_Status/"> FT %2d Slot %2d </a> <br>'%(run_spec['url_webdisplay'],P,iY-1,iX)
            if advOptions['MNB_listSource'] == "Tier0 list":
              return_suspicious += "%s_%d_%d "%(P,iY-1,iX)

      out += '</td><td>\n'
    else:
      out += 'Not available</td><td>\n'
  
    if debug:
      print("MNB suspicious done")
    # Display MNB results
    if (calls[p].namedResult["MNB_TimeVeto"]):# and "EMB" in P): # protection for past runs where MNB monitoring is not available - EMEC no longer filtered
      # First look of time veto effect 
      lb_MNB_aV = dqmfResLB(calls[p].namedResult["MNB_TimeVeto"],run_COOL['ATLASREADY'])
      lb_MNB_bV = dqmfResLB(calls[p].namedResult["MNB"],run_COOL['ATLASREADY'])

      colorThreshold['red'] = 1e9
      colorThreshold['orange'] = 1000
      colorThreshold['black'] = 0
      dqmfResLB_out = dqmfResLB_display("After veto","%s/LAr/%s/Occupancy-Noise/Mini_NB/MNBTightEvent_TimeVeto_%s"%(run_spec['url_webdisplay'],P,P),lb_MNB_aV,run_COOL['NevtPerLB'],4,colorThreshold,len(lb_MNB_bV)>0)
      out += dqmfResLB_out['html']

      colorThreshold['red'] = 1e9
      colorThreshold['orange'] = 1000
      colorThreshold['black'] = 0
      dqmfResLB_out = dqmfResLB_display("Before veto","%s/LAr/%s/Occupancy-Noise/Mini_NB/MNBTightEvent_%s"%(run_spec['url_webdisplay'],P,P),lb_MNB_bV,run_COOL['NevtPerLB'],1,colorThreshold)
      out += dqmfResLB_out['html']

      out += '</td>\n'

      # Now look of time veto effect in collisions
      out += '<td>\n'
      if calls[p].namedResult["MNB_TimeVeto_coll"]:
        lb_MNB_aV = dqmfResLB(calls[p].namedResult["MNB_TimeVeto_coll"],run_COOL['ATLASREADY'])
        lb_MNB_bV = dqmfResLB(calls[p].namedResult["MNB_coll"],run_COOL['ATLASREADY'])
        NevtPerLB2= calls[p].namedResult["NbOfEventsVsLB_coll"]
        run_COOL['NevtPerLB_coll'] = len(NevtPerLB2)*[0]
        for i in range(len(NevtPerLB2)-1):
          run_COOL['NevtPerLB_coll'][i]= NevtPerLB2[i+1]
        
        colorThreshold['red'] = 1e9
        colorThreshold['orange'] = 1000
        colorThreshold['black'] = 0
        dqmfResLB_out = dqmfResLB_display("After veto","%s/LAr/%s/Occupancy-Noise/Mini_NB/MNBTightEvent_TimeVeto_%s"%(run_spec['url_webdisplay'].replace(run_spec['stream'],run_spec['stream_coll']),P,P),lb_MNB_aV,run_COOL['NevtPerLB_coll'],4,colorThreshold,len(lb_MNB_bV)>0)
        out += dqmfResLB_out['html']
      
        colorThreshold['red'] = 1e9
        colorThreshold['orange'] = 1000
        colorThreshold['black'] = 0
        dqmfResLB_out = dqmfResLB_display("Before veto","%s/LAr/%s/Occupancy-Noise/Mini_NB/MNBTightEvent_%s"%(run_spec['url_webdisplay'].replace(run_spec['stream'],run_spec['stream_coll']),P,P),lb_MNB_bV,run_COOL['NevtPerLB_coll'],1,colorThreshold)
        out += dqmfResLB_out['html']

      out += '</td>\n'

      # Now look at the new suspicious FEBs
      out += '<td>\n'
      MNB_newTightFEB = calls[p].namedResult["MNB_newTightFEB"]
      if(MNB_newTightFEB == None): MNB_newTightFEB = []
      newTightFEB = {}
      for iSlot in range(len(MNB_newTightFEB)):
        for iFT in range(len(MNB_newTightFEB[iSlot])):
          if (MNB_newTightFEB[iSlot][iFT] > MNB_newTight_threshold[P]):
            newTightFEB["FT %d Slot %d"%(iFT-1,iSlot)] = MNB_newTightFEB[iSlot][iFT]
      if (len(list(newTightFEB.keys()))):
        out += '<pre style="font-family:Arial;"><a href="%s/LAr/%s/Occupancy-Noise/Mini_NB/All-FEBs/CandidateMNBTightFEBPerEvt_%s" >%i FEBs with more than %.2f &#37  events </a>' % (run_spec['url_webdisplay'],P,P,len(newTightFEB),MNB_newTight_threshold[P])
        out += '<p  onclick="showit2(this);"><u style="font-family:Arial;color:blue;">show</u> <!--f <br />'
        for iFEB in list(newTightFEB.keys()):
          out += '%s: %.2f &#37 (%d)<br />' %(iFEB,newTightFEB[iFEB],round(newTightFEB[iFEB]*nTotEvt/100.))
        out +=' f--></p>'
      else:
        out += '<pre style="font-family:Arial;"><a href="%s/LAr/%s/Occupancy-Noise/Mini_NB/All-FEBs/CandidateMNBTightFEBPerEvt_%s" >No FEB with more than %.2f &#37  events </a>' % (run_spec['url_webdisplay'],P,P,MNB_newTight_threshold[P])
      out += '</td>\n'
    else:
      out += '</td><td></td><td></td>\n'
      #print("WARNING: MNB monitoring not available")

    if debug:
      print("MNB done")
    # Display NoiseBursts results
    # First look of time veto effect (with only known suspicious FEBs)
    if (calls[p].namedResult["NB"]): # protection for runs without noise burst monitoring (unlikely)
      out += '<td>\n'
      lb_NB_aV = dqmfResLB(calls[p].namedResult["NB_TimeVeto"],run_COOL['ATLASREADY'])
      lb_NB_bV = dqmfResLB(calls[p].namedResult["NB"],run_COOL['ATLASREADY'])

      colorThreshold['red'] = 1e9
      colorThreshold['orange'] = 10
      colorThreshold['black'] = 0
      dqmfResLB_out = dqmfResLB_display("After veto","%s/LAr/%s/Occupancy-Noise/Noise_Burst/NoisyEvent_TimeVeto_%s"%(run_spec['url_webdisplay'],P,P),lb_NB_aV,run_COOL['NevtPerLB'],4,colorThreshold,len(lb_NB_bV)>0)
      out += dqmfResLB_out['html']

      colorThreshold['red'] = 1e9
      colorThreshold['orange'] = 1000
      colorThreshold['black'] = 0
      dqmfResLB_out = dqmfResLB_display("Before veto","%s/LAr/%s/Occupancy-Noise/Noise_Burst/NoisyEvent_%s"%(run_spec['url_webdisplay'],P,P),lb_NB_bV,run_COOL['NevtPerLB'],1,colorThreshold)
      out += dqmfResLB_out['html']
      out += '</td>\n'
    else:
      out += '</td><td></td>\n'
      #print("WARNING: NB monitoring not available")
    out += '</tr>\n'
  out += '</table>\n'
  print(out)

  if debug:
    print("I am done with primary table")

  return return_suspicious
