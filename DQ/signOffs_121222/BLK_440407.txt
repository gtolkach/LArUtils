Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-820

== Noisy cells update for run 440407 (Tue, 6 Dec 2022 16:50:23 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1321_h410)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:4
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:2
SBN in PS:0
HNHG:2
HNHG in PS:1

*****************
The treated cells were:
0 0 24 1 75 0 highNoiseHG # 0x38c04b00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 highNoiseHG # 0x39b88200

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-820

== Event Veto for run 440407 (Sat, 3 Dec 2022 09:32:54 +0100) ==
Found Noise or data corruption in run 440407
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440407_Main.db

Found project tag data22_13p6TeV for run 440407
Found 5 Veto Ranges with 11 events
Found 4 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440407_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440407
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440407_Main.db;dbname=CONDBR2
Found a total of 5 corruption periods, covering a total of 2.50 seconds
Lumi loss due to corruption: 46.21 nb-1 out of 76608.30 nb-1 (0.60 per-mil)
Overall Lumi loss is 46.21440310922845 by 2.5009245 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-820

== Missing EVs for run 440407 (Thu, 24 Nov 2022 08:15:29 +0100) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 17 Mini noise periods, covering a total of 0.02 seconds
Lumi loss due to noise-bursts: 0.02 nb-1 out of 76608.30 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.32 nb-1 out of 76608.30 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.338095689997168 by 0.018456025 s of veto length


