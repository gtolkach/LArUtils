Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1365

== Noisy cells update for run 456151 (Mon, 10 Jul 2023 19:28:47 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x754_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:79
Flagged in PS:22
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:68
SBN in PS:17
HNHG:11
HNHG in PS:5

*****************
The treated cells were:
0 0 1 14 41 0 sporadicBurstNoise  # 0x380ea900 -> 0x2d406cec
0 0 4 1 14 0 sporadicBurstNoise  # 0x38200e00 -> 0x2d000c2c
0 0 5 1 0 0 sporadicBurstNoise  # 0x38280000 -> 0x2d00002a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 7 14 4 0 sporadicBurstNoise  # 0x383e8400 -> 0x2d40628e
0 0 7 14 32 0 highNoiseHG  # 0x383ea000 -> 0x2d40688e
0 0 7 14 36 0 sporadicBurstNoise  # 0x383ea400 -> 0x2d406a8e
0 0 7 14 37 0 sporadicBurstNoise  # 0x383ea500 -> 0x2d406a8c
0 0 7 14 40 0 sporadicBurstNoise  # 0x383ea800 -> 0x2d406c8e
0 0 7 14 44 0 sporadicBurstNoise  # 0x383eac00 -> 0x2d406e8e
0 0 7 14 45 0 sporadicBurstNoise  # 0x383ead00 -> 0x2d406e8c
0 0 7 14 62 0 sporadicBurstNoise  # 0x383ebe00 -> 0x2d406e82
0 0 7 14 63 0 sporadicBurstNoise  # 0x383ebf00 -> 0x2d406e80
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 9 78 0 sporadicBurstNoise  # 0x38544e00 -> 0x2d600652
0 0 10 11 90 0 sporadicBurstNoise  # 0x38555a00 -> 0x2d400c52
0 0 10 11 91 0 sporadicBurstNoise  # 0x38555b00 -> 0x2d400c50
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 18 1 12 0 sporadicBurstNoise  # 0x38900c00 -> 0x2d000874
0 0 22 7 1 0 sporadicBurstNoise  # 0x38b30100 -> 0x2d228266
0 0 22 7 2 0 sporadicBurstNoise  # 0x38b30200 -> 0x2d228466
0 0 22 7 3 0 sporadicBurstNoise  # 0x38b30300 -> 0x2d228666
0 0 23 1 5 0 sporadicBurstNoise  # 0x38b80500 -> 0x2d000260
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 12 40 0 sporadicBurstNoise  # 0x38cda800 -> 0x2d40356e
0 0 25 14 14 0 sporadicBurstNoise  # 0x38ce8e00 -> 0x2d40676a
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 1 8 1 67 0 sporadicBurstNoise  # 0x39404300 -> 0x2d804620
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 18 5 43 0 sporadicBurstNoise  # 0x39922b00 -> 0x2da1d648
0 1 18 5 44 0 sporadicBurstNoise  # 0x39922c00 -> 0x2da1d848
0 1 21 12 42 0 sporadicBurstNoise  # 0x39adaa00 -> 0x2dc03554
0 1 22 7 109 0 sporadicBurstNoise  # 0x39b36d00 -> 0x2da2da5a
0 1 22 7 110 0 sporadicBurstNoise  # 0x39b36e00 -> 0x2da2dc5a
0 1 22 7 111 0 sporadicBurstNoise  # 0x39b36f00 -> 0x2da2de5a
0 1 23 5 82 0 sporadicBurstNoise  # 0x39ba5200 -> 0x2da1a45e
0 1 24 1 13 0 sporadicBurstNoise  # 0x39c00d00 -> 0x2d800a62
0 1 24 12 28 0 sporadicBurstNoise  # 0x39c59c00 -> 0x2dc02f80
0 1 27 12 41 0 sporadicBurstNoise  # 0x39dda900 -> 0x2dc035b2
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 3 6 115 0 highNoiseHG  # 0x3a1af300 -> 0x318d3000
1 0 9 12 28 0 sporadicBurstNoise  # 0x3a4d9c00 -> 0x2cb40010
1 0 9 12 29 0 distorted sporadicBurstNoise  # 0x3a4d9d00 -> 0x2cb40210
1 0 9 12 30 0 sporadicBurstNoise  # 0x3a4d9e00 -> 0x2cb40410
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 15 13 17 0 sporadicBurstNoise  # 0x3a7e1100 -> 0x2ce025ec
1 0 15 13 20 0 sporadicBurstNoise  # 0x3a7e1400 -> 0x2ce027ee
1 0 15 13 21 0 sporadicBurstNoise  # 0x3a7e1500 -> 0x2ce027ec
1 0 15 13 22 0 sporadicBurstNoise  # 0x3a7e1600 -> 0x2ce027ea
1 0 15 14 36 0 sporadicBurstNoise  # 0x3a7ea400 -> 0x2cc451ee
1 0 15 14 37 0 sporadicBurstNoise  # 0x3a7ea500 -> 0x2cc451ec
1 0 15 14 38 0 sporadicBurstNoise  # 0x3a7ea600 -> 0x2cc451ea
1 0 15 14 40 0 sporadicBurstNoise  # 0x3a7ea800 -> 0x2cc453ee
1 0 15 14 41 0 sporadicBurstNoise  # 0x3a7ea900 -> 0x2cc453ec
1 0 15 14 42 0 sporadicBurstNoise  # 0x3a7eaa00 -> 0x2cc453ea
1 0 15 14 44 0 sporadicBurstNoise  # 0x3a7eac00 -> 0x2cc455ee
1 0 15 14 45 0 sporadicBurstNoise  # 0x3a7ead00 -> 0x2cc455ec
1 0 15 14 46 0 sporadicBurstNoise  # 0x3a7eae00 -> 0x2cc455ea
1 0 15 14 47 0 sporadicBurstNoise  # 0x3a7eaf00 -> 0x2cc455e8
1 0 19 11 33 0 sporadicBurstNoise  # 0x3a9d2100 -> 0x2cc41f6c

