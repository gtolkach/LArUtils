Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1669

== Noisy cells update for run 463017 (Sat, 21 Oct 2023 13:07:28 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x797_h448)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:0
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:


