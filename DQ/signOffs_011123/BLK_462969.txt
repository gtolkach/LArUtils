Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1666

== Noisy cells update for run 462969 (Mon, 30 Oct 2023 11:35:49 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1406_h448)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:2
Flagged in PS:0
Unflagged by DQ shifters:2
Changed to SBN:0
Changed to HNHG:1
SBN:0
SBN in PS:0
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
1 0 6 10 115 0 unflaggedByLADIeS  # 0x3a34f300 -> 0x34b8a000
1 0 6 12 115 0 highNoiseHG unflaggedByLADIeS  # 0x3a35f300 -> 0x34b9a000
1 1 6 14 98 0 highNoiseHG reflaggedByLADIeS  # 0x3b36e200 -> 0x371aa000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1666

== Missing EVs for run 462969 (Fri, 20 Oct 2023 22:15:35 +0200) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 3 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.2502895700255176e-08 by 0.004044815 s of veto length


