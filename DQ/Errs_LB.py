import sys
import ROOT as R
import glob
import argparse

R.gROOT.SetBatch(True)

def getRootFiles(run, stream, camp, amistr="x"):
    rootpath = "/eos/atlas/atlastier0/tzero/prod/"+camp+"/"+stream+"/*"+str(run)+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*SFO*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    amis = sorted(list(set([r.split("/")[-2].split(".")[-1] for r in rootfiles])))
    ami = amis[-1]
    if len(amis) > 1:
        newest = [ r for r in rfile if "."+amis[-1]+"." in r]
        return newest, ami
    return rootfiles, ami

def fillGr(hist, contentStr, grs, grCounts, LBs, LB, xbin=None, ybin=None, content=None):
    if contentStr not in grs.keys(): 
        grs[contentStr] = R.TGraph(len(LBs))
        grCounts[contentStr] = 1
    if ybin is not None and xbin is not None:
        content = hist.GetBinContent(xbin,ybin)
    elif xbin is not None:
        content = hist.GetBinContent(xbin)
    if grs[contentStr].Eval(LB) != 0: 
        content += grs[contentStr].Eval(LB)
    grs[contentStr].SetPoint(grCounts[contentStr], LB, content)
    grCounts[contentStr]+=1
           
def scanCorr(runs, camp, stream, amistr, verbose=False):

    print(f"Tag: {camp}")
    print(f"Stream: {stream}")
    print(f"ami: {amistr}")
    
    if runs is None:
        rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/*/"+camp+".*."+stream+".merge.HIST."+amistr+"*/"+camp+".*."+stream+".merge.HIST."+amistr+"*.1"
        #print("Wildcard:",rootpath)
        rootfiles = glob.glob(rootpath)
        runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
        runs = sorted(list(set(runs)),reverse=True)
    print(f"{len(runs)} Run(s): {runs}")

    histsToCheck = ["GLOBAL/DQTDataFlow/eventflag_summary_lowStat", "LAr/FEBMon/Summary/EventsRejectedYield", "LAr/FEBMon/Summary/NbOfLArFEBMonErrors_dE"]
    for run in runs:
        rootfiles, amiTag = getRootFiles(run, stream, camp, amistr)

        if len(rootfiles) == 0 : continue

        LBs = sorted(list([int(lbf.split(".")[-4].split("_lb")[1]) for lbf in rootfiles]))

        grs = {}
        grCounts = {}
        for lbf in rootfiles:
            LB = int(lbf.split(".")[-4].split("_lb")[1])
            if verbose: 
                print(f"**** LB {LB} ****")
            else:
                if LB%50 == 0:
                    print(f"LB {LB} / {max(LBs)}")
                    print(lbf)
            histFile = R.TFile(lbf, "READ")
            for htc in histsToCheck:
                this_htc = htc
                if "EventsRejectedYield" in htc: this_htc = htc.replace("EventsRejectedYield","EventsRejected")
                this_htc = f"run_{run}/{this_htc}"
                hist = histFile.Get(this_htc)
                if not isinstance(hist, R.TH1): 
                    continue
                hname = this_htc.split("/")[-1]
                xlabels = [ l.GetName() for l in hist.GetXaxis().GetLabels() ]
                if isinstance(hist,R.TH2):
                    ylabels = [ l.GetName() for l in hist.GetYaxis().GetLabels() ]
                if 'LAr' in xlabels:
                    xbin = xlabels.index('LAr')+1
                    for yl in ylabels:
                        ybin = ylabels.index(yl)+1
                        if verbose: print(f"{hname} {yl}: {hist.GetBinContent(xbin,ybin)}")
                        contentStr = "LAr_"+yl
                        fillGr(hist, contentStr, grs, grCounts, LBs, LB, xbin, ybin)
                    
                else:
                    if "LAr/FEBMon/Summary/NbOfLArFEBMonErrors_dE" in htc:
                        ylabels = [ l.GetName() for l in hist.GetYaxis().GetLabels() ]
                        for xl in xlabels:
                            xbin = xlabels.index(xl)+1
                            totalContent = 0
                            for yl in ylabels: # y labels are partitions
                                ybin = ylabels.index(yl)+1
                                totalContent += hist.GetBinContent(xbin,ybin)
                                contentStr = xl+"_"+yl
                                if hist.GetBinContent(xbin,ybin) > 0:
                                    print(f"LB {LB} {yl} {xl} {hist.GetBinContent(xbin,ybin)}")
                                fillGr(hist, contentStr, grs, grCounts, LBs, LB, xbin, ybin)

                            contentStr = xl+"_allParts"
                            fillGr(hist, contentStr, grs, grCounts, LBs, LB, content=totalContent)
                    if not isinstance(hist,R.TH2):
                        for xl in xlabels:
                            xbin = xlabels.index(xl)+1
                            if verbose: print(f"{hname} {xl}: {hist.GetBinContent(xbin)}")    
                            contentStr = xl
                            fillGr(hist, contentStr, grs, grCounts, LBs, LB, xbin)

        canv = R.TCanvas()
        for graphName in grs.keys():
            grs[graphName].SetTitle(graphName+f" for run {run} in {stream} {amiTag}")
            grs[graphName].SetMarkerStyle(20)
            grs[graphName].Draw("AP")
            grs[graphName].GetXaxis().SetTitle("LB")
            graphName = graphName.replace(" ","_")
            graphName = graphName.replace(">","gt")
            graphName = graphName.replace("=","eq")
            graphName = graphName.replace("/","slash")
            graphName = graphName.replace("#","num")
            canv.Print(str(run)+"_"+camp+"_"+stream+"_"+amiTag+"_"+graphName+".png")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Print bin content for some data corruption histograms across LBs for given runs')
    parser.add_argument('-r','--run',type=int,dest='runs',nargs='+',default=None,help="Run number(s) to check. By default will loop through all runs with available per-LB data for the requested stream")
    parser.add_argument('-s','--stream',type=str,dest='stream', default='physics_CosmicCalo', help="Stream in which to find data. Default %(default)s")
    parser.add_argument('-t','--tag',type=str,dest='tag', default='data23_13p6TeV', help="The tag for the data you want to check. Default %(default)s")
    parser.add_argument('-a','--amiTag',type=str,dest='amiTag',default="x",help="AMI tag to use for hist file search. Can be the full tag or simply x/f (for express/bulk). Default %(default)s")
    parser.add_argument('-v','--verbose', action='store_true')

    args = parser.parse_args()
    scanCorr(args.runs, args.tag, args.stream, args.amiTag)
