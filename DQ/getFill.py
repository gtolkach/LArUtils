from CoolLumiUtilities.CoolDataReader import CoolDataReader
from CoolLumiUtilities.LumiDBCache import LumiDBCache
import time

def getRuns(starttime, endtime):
    lhc = LumiDBCache('COOLOFL_DCS/CONDBR2', '/LHC/DCS/FILLSTATE')
    lhc.reader.setIOVRange(starttime, endtime)
    return lhc.reader.readData()
    
    lblb = CoolDataReader('COOLONL_TRIGGER/CONDBR2', '/TRIGGER/LUMI/LBLB')
    lblb.reader.setIOVRange(starttime, endtime)
    return lblb.reader.readData()


def getRunTimes(runnum=456729):
    lblb = CoolDataReader('COOLONL_TRIGGER/CONDBR2', '/TRIGGER/LUMI/LBLB')
    runnum = int(runnum)
    lblb.setIOVRangeFromRun(runnum)
    lblb.readData()
    tlo = lblb.data[0].payload()['StartTime']
    thi = lblb.data[-1].payload()['EndTime']
    return tlo, thi

def getRunFill(runnum=456729):
    tlo, thi = getRunTimes(runnum)
    lhc = None
    lhc = LumiDBCache('COOLOFL_DCS/CONDBR2', '/LHC/DCS/FILLSTATE')
    lhc.reader.setIOVRange(tlo, thi)
    lhc.reader.readData()
    pl = lhc.getPayload(tlo)
    return pl['FillNumber']

def getStableBeams(runnum=456729):
    tlo, thi = getRunTimes(runnum)
    lhc = None
    lhc = LumiDBCache('COOLOFL_DCS/CONDBR2', '/LHC/DCS/FILLSTATE')
    lhc.reader.setIOVRange(tlo, thi)
    lhc.reader.readData()
    pl = lhc.getPayload(tlo)
    return pl['StableBeams']

if __name__ == "__main__":
    infile = open("/afs/cern.ch/user/l/larmon/public/prod/LADIeS/RunList/all-2023-LADIeSAssessed.txt","r")
    
    #for line in infile:
    #    run = int(line.strip("\n"))
    #    print(run)

    # time.time_ns()

    from datetime import datetime, timezone, timedelta
    start = datetime(2023, 1, 1, 00, 00, 00)
    #end = datetime(2024, 1, 1, 00, 00, 00)
    end = datetime(2023, 10, 1, 00, 00, 00)
    startIoV = int(start.replace(tzinfo=timezone(timedelta(0))).timestamp()*10**9)
    endIoV = int(end.replace(tzinfo=timezone(timedelta(0))).timestamp()*10**9)
    # time.asctime(time.gmtime(startIoV/1e9)))

    data = getRuns(startIoV, endIoV)
    print(data)

    tlo, thi = getRunTimes()
    
    data2 = getRuns(tlo, thi)
    print(tlo, thi)
    print(data2)
