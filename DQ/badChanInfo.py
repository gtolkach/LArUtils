''' Pass a file containing a proposed noisy cell list from the WDE as the input argument. This script will collect some information and summarise it, to help spot common traits between the flagged cells '''
import sys
sys.path.append("..")
import printMapping
import pandas as pd

query = [ "ONL_ID", "DETNAME", "HVLINES", "CALIB_ID", "HVSECTOR", "QUADRANT", "FTNAME", "FEB" ]

criteria = {"want":query}
pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)
pmdf = pmdf.set_index("ONL_ID")

#for intval in ["SC_ONL_ID", "AC" ]:
#    pmdf = pmdf.astype({intval: int})

infile = sys.argv[1]

geom = {'ONL_ID':[]}
with open(infile, 'r') as f:
    for line in f:
        if " # 0x" not in line: continue
        line = line.strip("\n")
        chid = line.split("#")[1].strip(" ")
        geom['ONL_ID'].append(chid)
        row = pmdf.loc[pmdf.index == chid]
        for col in pmdf.columns:
            if col not in geom.keys():
                geom[col] = []
            val = row[col].values[0]
            if len(val.split(" ")) > 1:
                geom[col].extend([v for v in val.split(" ") if v != "" ])
            else:
                geom[col].append(val)
 
for key in geom.keys():
    thislist = list(set(geom[key]))
    thisnum = len(thislist)

    if key == 'ONL_ID':
        print(f"{thisnum} channels in list")
    else:


        counts = {}
        for item in thislist:
            counts[item] = geom[key].count(item)

        order_counts = counts.values()
        order_counts = list(set([m for m in order_counts if m != 1]) )
        order_counts = sorted(order_counts, reverse=True)
        #maxcount = max(counts.values())
        #maxitem = [ k for k in counts.keys() if counts[k] == maxcount][0]

        print(f"{thisnum} {key}: {' '.join(thislist)}")
        for i in range(0, 3):
            if len(order_counts) > i:
                maxitem = [ k for k in counts.keys() if counts[k] == order_counts[i]][0]
                if i == 0: 
                    suff = "st"
                elif i == 1:
                    suff = "nd"
                elif i ==2 :
                    suff = "rd"
                print(f"... {i+1}{suff} most frequent is {maxitem} with {order_counts[i]} occurences")
