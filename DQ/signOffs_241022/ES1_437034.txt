== Noisy cells update for run 437034 (Sun, 16 Oct 2022 09:57:46 +0100) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x705_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:1
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:1
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
1 0 9 4 15 0 sporadicBurstNoise # 0x3a498f00

