** Defect LAR_FCALC_NOISEBURST** 
In LB 451 - 452 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 508 - 510 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 516 - 517 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 562 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 663 - 664 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_HECA_NOISEBURST** 
In LB 435 - 436 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 444 - 445 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 466 - 468 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 481 - 482 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 517 - 518 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 536 - 537 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 538 - 539 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 549 - 550 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 552 - 553 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 554 - 555 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 560 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 574 - 575 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 582 - 583 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 586 - 587 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 591 - 592 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 607 - 608 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 627 - 628 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 656 - 657 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 659 - 660 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 691 - 693 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 695 - 696 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 705 - 706 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 708 - 709 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_HECC_NOISEBURST** 
In LB 451 - 452 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 508 - 510 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 516 - 517 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 562 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 663 - 664 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_HECA_SEVNOISEBURST** 
In LB 696 - 698 with comment: More than 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Time veto not reliable enough - Irrecoverable. NOT RECOVERABLE.
** Defect LAR_FCALA_NOISEBURST** 
In LB 435 - 436 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 444 - 445 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 466 - 468 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 481 - 482 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 517 - 518 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 536 - 537 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 538 - 539 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 549 - 550 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 552 - 553 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 554 - 555 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 560 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 574 - 575 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 582 - 583 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 586 - 587 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 591 - 592 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 607 - 608 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 627 - 628 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 656 - 657 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 659 - 660 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 691 - 693 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 695 - 696 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 705 - 706 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 708 - 709 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_EMECA_SEVNOISEBURST** 
In LB 696 - 698 with comment: More than 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Time veto not reliable enough - Irrecoverable. NOT RECOVERABLE.
** Defect LAR_UNCHECKED_FINAL** 
In LB 1 - 723 with comment: Automatically set. RECOVERABLE.
** Defect LAR_EMBA_NOISEBURST** 
In LB 680 - 681 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EMBA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_EMECC_NOISEBURST** 
In LB 451 - 452 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 508 - 510 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 516 - 517 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 562 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 663 - 664 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_EMECA_NOISEBURST** 
In LB 435 - 436 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 444 - 445 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 466 - 468 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 481 - 482 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 488 - 489 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 517 - 518 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 536 - 537 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 538 - 539 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 549 - 550 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 552 - 553 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 554 - 555 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 560 - 563 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 574 - 575 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 582 - 583 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 586 - 587 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 591 - 592 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 607 - 608 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 627 - 628 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 656 - 657 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 659 - 660 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 691 - 693 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 695 - 696 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 705 - 706 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 708 - 709 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_FCALA_SEVNOISEBURST** 
In LB 696 - 698 with comment: More than 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Time veto not reliable enough - Irrecoverable. NOT RECOVERABLE.
** Defect LAR_DATACORRUPT** 
In LB 696 - 698 with comment: Between 1 % and 99 % corruption yield - Recoverable a priori. RECOVERABLE.
