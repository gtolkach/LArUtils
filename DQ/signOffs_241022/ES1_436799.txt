== Noisy cells update for run 436799 (Sat, 15 Oct 2022 21:20:23 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:test / 
Cluster matching: based on Et > 4/10GeV plots requiring at least 300 events
Flagged cells:138
Flagged in PS:49
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:126
SBN in PS:41
HNHG:12
HNHG in PS:8

*****************
The treated cells were:
0 0 23 5 12 0 highNoiseHG # 0x38ba0c00
0 0 23 5 13 0 sporadicBurstNoise # 0x38ba0d00
0 0 23 5 14 0 sporadicBurstNoise # 0x38ba0e00
0 0 23 5 11 0 sporadicBurstNoise # 0x38ba0b00
0 0 23 5 15 0 sporadicBurstNoise # 0x38ba0f00
0 0 23 5 10 0 sporadicBurstNoise # 0x38ba0a00
0 0 23 5 9 0 sporadicBurstNoise # 0x38ba0900
0 0 23 5 8 0 sporadicBurstNoise # 0x38ba0800
1 0 3 6 82 0 highNoiseHG # 0x3a1ad200
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 15 13 5 0 sporadicBurstNoise # 0x387e0500
0 1 10 13 35 0 sporadicBurstNoise # 0x39562300
0 1 10 13 32 0 sporadicBurstNoise # 0x39562000
0 1 10 13 36 0 sporadicBurstNoise # 0x39562400
0 1 10 13 34 0 sporadicBurstNoise # 0x39562200
0 1 10 13 33 0 sporadicBurstNoise # 0x39562100
0 1 10 13 38 0 sporadicBurstNoise # 0x39562600
0 1 10 13 37 0 highNoiseHG # 0x39562500
0 0 27 12 5 0 sporadicBurstNoise # 0x38dd8500
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
0 0 5 1 0 0 sporadicBurstNoise unflaggedByLADIeS # 0x38280000
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 1 21 5 102 0 sporadicBurstNoise # 0x39aa6600
0 1 21 5 101 0 sporadicBurstNoise # 0x39aa6500
0 0 27 1 64 0 sporadicBurstNoise # 0x38d84000
0 0 5 1 6 0 sporadicBurstNoise unflaggedByLADIeS # 0x38280600
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 1 11 11 1 0 sporadicBurstNoise # 0x395d0100
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 1 10 13 39 0 sporadicBurstNoise # 0x39562700
0 1 31 10 21 0 sporadicBurstNoise # 0x39fc9500
0 1 15 10 11 0 sporadicBurstNoise # 0x397c8b00
1 0 3 6 123 0 highNoiseHG # 0x3a1afb00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 1 7 2 4 0 sporadicBurstNoise # 0x39388400
0 1 7 2 5 0 sporadicBurstNoise # 0x39388500
0 1 7 2 3 0 sporadicBurstNoise # 0x39388300
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 25 1 29 0 sporadicBurstNoise # 0x38c81d00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 11 1 15 0 sporadicBurstNoise # 0x38580f00
0 1 23 1 96 0 sporadicBurstNoise # 0x39b86000
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 0 30 1 103 0 sporadicBurstNoise # 0x38f06700
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 11 1 23 0 highNoiseHG # 0x38581700
0 0 27 1 53 0 sporadicBurstNoise # 0x38d83500
0 1 15 3 64 0 sporadicBurstNoise # 0x39794000
0 1 15 2 127 0 sporadicBurstNoise # 0x3978ff00
0 1 15 2 126 0 sporadicBurstNoise # 0x3978fe00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
1 0 3 6 103 0 sporadicBurstNoise unflaggedByLADIeS # 0x3a1ae700
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 22 13 123 0 sporadicBurstNoise # 0x39b67b00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise # 0x39802b00
0 0 31 1 61 0 sporadicBurstNoise # 0x38f83d00
0 0 10 13 67 0 sporadicBurstNoise # 0x38564300
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 1 20 1 78 0 sporadicBurstNoise # 0x39a04e00
0 0 10 10 32 0 sporadicBurstNoise # 0x3854a000
0 0 10 10 35 0 sporadicBurstNoise # 0x3854a300
0 0 10 10 34 0 sporadicBurstNoise # 0x3854a200
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 1 21 1 3 0 sporadicBurstNoise # 0x39a80300
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 1 8 1 78 0 sporadicBurstNoise # 0x39404e00
0 1 8 1 76 0 highNoiseHG # 0x39404c00
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 6 14 44 0 sporadicBurstNoise # 0x3836ac00
0 0 6 14 63 0 sporadicBurstNoise # 0x3836bf00
0 0 6 14 45 0 sporadicBurstNoise # 0x3836ad00
0 0 6 14 40 0 sporadicBurstNoise # 0x3836a800
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 0 1 13 121 0 sporadicBurstNoise # 0x380e7900
0 1 5 1 65 0 sporadicBurstNoise # 0x39284100
0 0 22 11 12 0 sporadicBurstNoise # 0x38b50c00
0 0 10 9 78 0 sporadicBurstNoise # 0x38544e00
0 0 10 11 90 0 sporadicBurstNoise # 0x38555a00
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 0 16 8 41 0 sporadicBurstNoise # 0x3883a900
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 8 12 5 0 sporadicBurstNoise # 0x39458500
0 1 7 12 68 0 sporadicBurstNoise # 0x393dc400
0 1 3 1 65 0 sporadicBurstNoise # 0x39184100
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 0 8 1 17 0 highNoiseHG # 0x38401100
0 0 8 1 18 0 sporadicBurstNoise # 0x38401200
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 0 26 1 63 0 sporadicBurstNoise # 0x38d03f00
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 6 13 123 0 sporadicBurstNoise # 0x38367b00

