== Noisy cells update for run 437065 (Thu, 20 Oct 2022 16:56:04 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / 
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:8
Flagged in PS:3
Unflagged by DQ shifters:0
Changed to SBN:1
Changed to HNHG:0
SBN:6
SBN in PS:3
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
0 1 19 2 69 0 highNoiseHG # 0x3998c500
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 23 2 3 0 sporadicBurstNoise reflaggedByLADIeS # 0x39b88300
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 11 14 63 0 sporadicBurstNoise # 0x385ebf00
0 0 30 1 13 0 sporadicBurstNoise # 0x38f00d00
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 0 10 11 87 0 sporadicBurstNoise # 0x38555700

== Event Veto for run 437065 (Wed, 19 Oct 2022 13:58:53 +0200) ==
Found Noise or data corruption in run 437065
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437065_Main.db

Found project tag data22_13p6TeV for run 437065
Found 2 Veto Ranges with 5 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437065_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437065
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437065_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 17.63 nb-1 out of 147314.43 nb-1 (0.12 per-mil)
Overall Lumi loss is 17.631955170020536 by 1.00047 s of veto length



== Event Veto for run 437065 (Wed, 19 Oct 2022 12:59:45 +0200) ==
Found Noise or data corruption in run 437065
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437065_Main.db

Found project tag data22_13p6TeV for run 437065
Found 2 Veto Ranges with 5 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437065_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437065
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437065_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 17.63 nb-1 out of 147314.43 nb-1 (0.12 per-mil)
Overall Lumi loss is 17.631955170020536 by 1.00047 s of veto length



== Missing EVs for run 437065 (Sun, 16 Oct 2022 03:15:39 +0200) ==

Found a total of 37 Mini noise periods, covering a total of 0.04 seconds
Lumi loss due to mini-noise-bursts: 0.63 nb-1 out of 147314.43 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.6276812067039061 by 0.0370004 s of veto length


