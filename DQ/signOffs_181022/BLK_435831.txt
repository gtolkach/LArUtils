== Noisy cells update for run 435831 (Mon, 10 Oct 2022 15:43:18 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / 
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:2
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:2
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
1 0 9 3 82 0 sporadicBurstNoise # 0x3a495200
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200

== Event Veto for run 435831 (Thu, 6 Oct 2022 14:09:02 +0200) ==
Found Noise or data corruption in run 435831
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto435831_Main.db

Found project tag data22_13p6TeV for run 435831
Found 5 Veto Ranges with 5019 events
Found 24 isolated events
Reading event veto info from db sqlite://;schema=EventVeto435831_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 435831
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto435831_Main.db;dbname=CONDBR2
Found a total of 5 corruption periods, covering a total of 8.20 seconds
Lumi loss due to corruption: 78.93 nb-1 out of 435530.79 nb-1 (0.18 per-mil)
Overall Lumi loss is 78.92909579869756 by 8.202594565 s of veto length



== Event Veto for run 435831 (Thu, 6 Oct 2022 13:02:49 +0200) ==
Found Noise or data corruption in run 435831
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto435831_Main.db

Found project tag data22_13p6TeV for run 435831
Found 5 Veto Ranges with 5019 events
Found 24 isolated events
Reading event veto info from db sqlite://;schema=EventVeto435831_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 435831
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto435831_Main.db;dbname=CONDBR2
Found a total of 5 corruption periods, covering a total of 8.20 seconds
Lumi loss due to corruption: 78.93 nb-1 out of 435530.79 nb-1 (0.18 per-mil)
Overall Lumi loss is 78.92909579869756 by 8.202594565 s of veto length



== Missing EVs for run 435831 (Mon, 3 Oct 2022 04:15:57 +0200) ==

Found a total of 3 noisy periods, covering a total of 0.00 seconds
Found a total of 17 Mini noise periods, covering a total of 0.02 seconds
Lumi loss due to noise-bursts: 0.02 nb-1 out of 435530.79 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.15 nb-1 out of 435530.79 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.17455063269292967 by 0.020002364999999998 s of veto length


