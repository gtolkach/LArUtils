== Noisy cells update for run 436041 (Thu, 6 Oct 2022 08:52:21 +0100) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x701_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:31
Flagged in PS:8
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:28
SBN in PS:7
HNHG:3
HNHG in PS:1

*****************
The treated cells were:
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200
0 0 22 11 28 0 sporadicBurstNoise # 0x38b51c00
0 1 2 13 83 0 sporadicBurstNoise # 0x39165300
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 highNoiseHG # 0x383c5400
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 29 12 68 0 sporadicBurstNoise # 0x38edc400
0 0 23 5 12 0 sporadicBurstNoise # 0x38ba0c00
0 0 23 5 13 0 sporadicBurstNoise # 0x38ba0d00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
1 0 3 6 82 0 highNoiseHG # 0x3a1ad200
0 0 23 13 15 0 sporadicBurstNoise # 0x38be0f00
0 0 10 13 67 0 sporadicBurstNoise # 0x38564300
0 0 30 1 103 0 sporadicBurstNoise # 0x38f06700
0 0 15 11 87 0 sporadicBurstNoise # 0x387d5700
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 13 1 18 0 sporadicBurstNoise # 0x38681200
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00

