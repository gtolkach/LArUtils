** Defect LAR_FCALC_NOISEBURST** 
In LB 18 - 19 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 33 - 34 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 42 - 43 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 48 - 49 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_HECC_NOISEBURST** 
In LB 18 - 19 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 33 - 34 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 42 - 43 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 48 - 49 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_FCALA_NOISEBURST** 
In LB 10 - 12 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 13 - 14 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 17 - 18 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 19 - 21 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 36 - 37 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 41 - 42 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_HECA_NOISEBURST** 
In LB 10 - 12 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 13 - 14 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 17 - 18 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 19 - 21 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 36 - 37 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 41 - 42 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_UNCHECKED_FINAL** 
In LB 1 - 61 with comment: Automatically set. RECOVERABLE.
** Defect LAR_EMECC_NOISEBURST** 
In LB 18 - 19 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 33 - 34 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 42 - 43 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 48 - 49 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapC/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_EMECA_NOISEBURST** 
In LB 10 - 12 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 13 - 14 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 17 - 18 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 19 - 21 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 36 - 37 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 39 - 40 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 41 - 42 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
