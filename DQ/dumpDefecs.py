import sys
import xmlrpc.client
import time 

if len(sys.argv) == 1:
    print("Please provide a run number, and optionally a defect wildcard")
    sys.exit()

run = int(sys.argv[1])
defstr = None
if len(sys.argv) > 2:
    defstr = sys.argv[2]

dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"

with open(dqmpassfile, "r") as f:       
    cred = f.readline().strip()
if cred is None:
    print("Problem reading credentials from",dqmpassfile)
    sys.exit()
dqmapi = xmlrpc.client.ServerProxy('https://'+cred+'@atlasdqm.cern.ch')
run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0', 'run_list': [run] }

defects = dqmapi.get_defects_lb(run_spec,"", "HEAD", True, True, "Production", True)
if str(run) not in defects.keys():
    print("Problem retrieving defects for requested run")
    sys.exit()

defects = defects[str(run)]
defkeys = [ "lbstart", "lbend", "colour", "time", "comment", "recov", "user" ]

if defstr is None:
    myDefects = { k:v for k,v in defects.items() if "lar" in k.lower() }
else:
    myDefects = { k:v for k,v in defects.items() if defstr.lower() in k.lower() }


for df in myDefects.keys():
    print("**",df,"**")
    for entry in myDefects[df]:
        entry = { dk:li for dk,li in zip(defkeys,entry)}
        entry["time"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(entry["time"]))
        printstr = ""
        if entry["lbstart"] == entry["lbend"]:
            printstr += "LB "+str(entry["lbstart"])+" "
        else:
            printstr += "LBs "+str(entry["lbstart"])+"-"+str(entry["lbend"])+" "
        printstr += "("+entry["comment"]+") "
        printstr += "uploaded by "+entry["user"]+" at "+entry["time"]
        
        print(printstr)
