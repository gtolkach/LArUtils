Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-751

== Noisy cells update for run 438481 (Fri, 4 Nov 2022 09:54:23 +0000) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 700 events
Flagged cells:54
Flagged in PS:18
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:47
SBN in PS:14
HNHG:7
HNHG in PS:4

*****************
The treated cells were:
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 14 2 29 0 sporadicBurstNoise # 0x38709d00
0 0 14 2 27 0 sporadicBurstNoise # 0x38709b00
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 0 15 13 115 0 sporadicBurstNoise # 0x387e7300
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 1 22 13 112 0 sporadicBurstNoise # 0x39b67000
0 1 8 13 12 0 sporadicBurstNoise # 0x39460c00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 5 1 5 0 highNoiseHG # 0x38280500
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 0 8 1 17 0 sporadicBurstNoise # 0x38401100
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 highNoiseHG # 0x383c5400
0 1 28 3 69 0 sporadicBurstNoise # 0x39e14500
0 1 28 3 70 0 sporadicBurstNoise # 0x39e14600
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 4 16 0 sporadicBurstNoise # 0x38c99000
0 0 25 1 29 0 sporadicBurstNoise # 0x38c81d00
0 0 25 4 17 0 sporadicBurstNoise # 0x38c99100
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
1 1 21 3 66 0 sporadicBurstNoise # 0x3ba94200
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 30 9 119 0 sporadicBurstNoise # 0x38f47700
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 23 5 82 0 highNoiseHG # 0x39ba5200

