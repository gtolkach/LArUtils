Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Event Veto for run 436550 (Thu, 13 Oct 2022 20:43:42 +0200) ==
Found Noise or data corruption in run 436550
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436550_Main.db

Found project tag data22_13p6TeV for run 436550
Found 4 Veto Ranges with 11 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436550
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.88 nb-1 out of 257589.61 nb-1 (0.14 per-mil)
Overall Lumi loss is 34.88191013701434 by 2.00088376 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Event Veto for run 436550 (Thu, 13 Oct 2022 20:40:00 +0200) ==
Found Noise or data corruption in run 436550
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436550_Main.db

Found project tag data22_13p6TeV for run 436550
Found 4 Veto Ranges with 11 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436550
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.88 nb-1 out of 257589.61 nb-1 (0.14 per-mil)
Overall Lumi loss is 34.88191013701434 by 2.00088376 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Missing EVs for run 436550 (Tue, 11 Oct 2022 04:16:28 +0200) ==

Found a total of 5 noisy periods, covering a total of 0.01 seconds
Found a total of 361 Mini noise periods, covering a total of 0.36 seconds
Lumi loss due to noise-bursts: 0.09 nb-1 out of 257589.61 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 6.42 nb-1 out of 257589.61 nb-1 (0.02 per-mil)
Overall Lumi loss is 6.509500045481723 by 0.36601776 s of veto length


