Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1610

== Noisy cells update for run 462107 (Mon, 16 Oct 2023 21:18:41 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1396_h445)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:8
Flagged in PS:0
Unflagged by DQ shifters:8
Changed to SBN:6
Changed to HNHG:0
SBN:7
SBN in PS:0
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 0 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a300000 -> 0x341ee000
1 0 6 1 19 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a301300 -> 0x3438e000
1 0 6 1 25 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a301900 -> 0x342ce000
1 0 6 1 31 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a301f00 -> 0x3420e000
1 0 6 1 33 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a302100 -> 0x345ce000
1 0 6 1 49 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303100 -> 0x347ce000
1 0 6 1 57 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a303900 -> 0x346ce000
1 0 6 1 58 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303a00 -> 0x346ae000
1 0 6 1 59 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x3a303b00 -> 0x3468e000
1 0 6 1 60 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a303c00 -> 0x3466e000
1 0 6 1 62 0 lowNoiseHG sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a303e00 -> 0x3462e000
1 0 6 1 121 0 sporadicBurstNoise  # 0x3a307900 -> 0x346cc000
1 0 6 1 123 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a307b00 -> 0x3468c000
1 0 6 1 125 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a307d00 -> 0x3464c000
1 0 6 2 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30be00 -> 0x3462a000
1 0 6 2 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30bf00 -> 0x3460a000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1610

== Missing EVs for run 462107 (Wed, 4 Oct 2023 22:16:48 +0200) ==

Found a total of 153 noisy periods, covering a total of 0.15 seconds
Found a total of 253 Mini noise periods, covering a total of 0.25 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.01 per-mil)
Overall Lumi loss is 6.163914316671624e-07 by 0.407665125 s of veto length


