Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1616

== Event Veto for run 462201 (Thu, 12 Oct 2023 18:12:05 +0200) ==
Found Noise or data corruption in run 462201
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto462201_Main.db

Found project tag data23_hi for run 462201
Found 3 Veto Ranges with 565 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 462201
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 8.83 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.05 nb-1 (0.78 per-mil)
Overall Lumi loss is 4.0093556683429704e-05 by 8.825238895 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1616

== Event Veto for run 462201 (Thu, 12 Oct 2023 17:23:06 +0200) ==
Found Noise or data corruption in run 462201
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto462201_Main.db

Found project tag data23_hi for run 462201
Found 3 Veto Ranges with 565 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 462201
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 8.83 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.05 nb-1 (0.78 per-mil)
Overall Lumi loss is 4.0093556683429704e-05 by 8.825238895 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1616

== Event Veto for run 462201 (Thu, 12 Oct 2023 14:53:42 +0200) ==
Found Noise or data corruption in run 462201
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto462201_Main.db

Found project tag data23_hi for run 462201
Found 3 Veto Ranges with 565 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 462201
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 8.83 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.05 nb-1 (0.78 per-mil)
Overall Lumi loss is 4.0093556683429704e-05 by 8.825238895 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1616

== Event Veto for run 462201 (Thu, 12 Oct 2023 12:53:27 +0200) ==
Found Noise or data corruption in run 462201
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto462201_Main.db

Found project tag data23_hi for run 462201
Found 3 Veto Ranges with 565 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 462201
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto462201_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 8.83 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.05 nb-1 (0.78 per-mil)
Overall Lumi loss is 4.0093556683429704e-05 by 8.825238895 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1616

== Missing EVs for run 462201 (Fri, 6 Oct 2023 07:15:41 +0200) ==

Found a total of 91 noisy periods, covering a total of 0.09 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.05 nb-1 (0.01 per-mil)
Overall Lumi loss is 2.9885183848752654e-07 by 0.091975625 s of veto length


