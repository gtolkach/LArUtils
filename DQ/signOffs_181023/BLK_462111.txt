Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1611

== Noisy cells update for run 462111 (Mon, 16 Oct 2023 21:32:12 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1399_h446)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:4
Flagged in PS:0
Unflagged by DQ shifters:2
Changed to SBN:4
Changed to HNHG:0
SBN:4
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 27 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301b00 -> 0x3428e000
1 0 6 1 31 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a301f00 -> 0x3420e000
1 0 6 1 99 0 sporadicBurstNoise unflaggedByLADIeS reflaggedByLADIeS  # 0x3a306300 -> 0x3458c000
1 0 6 1 101 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306500 -> 0x3454c000
1 0 6 2 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30be00 -> 0x3462a000
1 0 6 2 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30bf00 -> 0x3460a000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1611

== Missing EVs for run 462111 (Thu, 5 Oct 2023 08:16:19 +0200) ==

Found a total of 177 noisy periods, covering a total of 0.18 seconds
Found a total of 272 Mini noise periods, covering a total of 0.27 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.03 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.03 nb-1 (0.01 per-mil)
Overall Lumi loss is 6.515083867870685e-07 by 0.45297062 s of veto length


