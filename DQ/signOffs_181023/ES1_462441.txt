Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1629

== Noisy cells update for run 462441 (Wed, 11 Oct 2023 10:00:17 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x791_h445)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:2
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 99 0 highNoiseHG  # 0x3a1ae300 -> 0x31893000

