Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1633

** Defect LAR_HECA_NOISEBURST **
Affecting 10 LBs: 352, 353, 415, 416, 422, 423, 455, 456, 515, 516 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 14 LBs: 352, 353, 358, 359, 415, 416, 422, 423, 455, 456, 515, 516, 594, 595 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 10 LBs: 352, 353, 415, 416, 422, 423, 455, 456, 515, 516 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 14 LBs: 352, 353, 358, 359, 415, 416, 422, 423, 455, 456, 515, 516, 594, 595 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 10 LBs: 352, 353, 415, 416, 422, 423, 455, 456, 515, 516 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 14 LBs: 352, 353, 358, 359, 415, 416, 422, 423, 455, 456, 515, 516, 594, 595 (0 recoverable) 
