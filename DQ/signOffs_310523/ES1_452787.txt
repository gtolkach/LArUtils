Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1162

== Noisy cells update for run 452787 (Thu, 25 May 2023 22:04:15 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x743_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:22
Flagged in PS:6
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:18
SBN in PS:4
HNHG:4
HNHG in PS:2

*****************
The treated cells were:
0 0 0 7 69 0 sporadicBurstNoise  # 0x38034500 -> 0x2d228a3c
0 0 0 7 70 0 sporadicBurstNoise  # 0x38034600 -> 0x2d228c3c
0 0 0 7 71 0 sporadicBurstNoise  # 0x38034700 -> 0x2d228e3c
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 1 60 0 sporadicBurstNoise  # 0x38283c00 -> 0x2d003828
0 0 8 6 31 0 sporadicBurstNoise  # 0x38429f00 -> 0x2d223e1e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 22 6 118 0 sporadicBurstNoise  # 0x38b2f600 -> 0x2d226c64
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 1 7 10 80 0 sporadicBurstNoise  # 0x393cd000 -> 0x2de02878
0 1 7 12 68 0 sporadicBurstNoise  # 0x393dc400 -> 0x2dc02278
0 1 8 7 44 0 sporadicBurstNoise  # 0x39432c00 -> 0x2da2d820
0 1 23 14 7 0 sporadicBurstNoise  # 0x39be8700 -> 0x2dc06376
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 1 13 12 33 0 sporadicBurstNoise  # 0x3b6da100 -> 0x2e442f12
1 1 13 12 37 0 sporadicBurstNoise  # 0x3b6da500 -> 0x2e443112

