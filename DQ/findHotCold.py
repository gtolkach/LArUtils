# TO DO
# reset axis range to remove empty bins either side (remove iphi where all eta 0 and vice versa)
# extend with hotdf2 instead of replace, remove duplicates

import sys,os,errno
import ROOT as R 
import argparse
import numpy as np
import pandas as pd
R.gROOT.SetBatch(True)
from itertools import groupby, product, combinations
from DataQualityUtils import pathExtract 
import xmlrpc.client

onIDhistFile = R.TFile("athenaPython/onIDs.root","READ")

dqmpassfile="/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"


wildcards = {}
wildcardplots = ["CellOccupancyVsEtaPhi", "fractionOverQthVsEtaPhi","DatabaseNoiseVsEtaPhi"]

for plot in wildcardplots:
  wildcards[plot] = {}
  wildcards[plot]["EMB"] = { "P":"Presampler","1":"Sampling1","2":"Sampling2","3":"Sampling3"}
  wildcards[plot]["EMEC"] = { "P":"Presampler","1":"Sampling1","2":"Sampling2","3":"Sampling3"}
  wildcards[plot]["HEC"] = { "0":"Sampling0","1":"Sampling1","2":"Sampling2","3":"Sampling3"}
  wildcards[plot]["FCAL"] = { "1":"Sampling1","2":"Sampling2","3":"Sampling3"}

# Tile/Cell/AnyPhysTrig/TileCellEneEtaPhi_SampB_AnyPhysTrig
# Tile/Cell/AnyPhysTrig/TileCellEtaPhiOvThr_SampB_AnyPhysTrig
wildcards["TileCellEneEtaPhi"] = [ "A", "B", "D", "E" ]
wildcards["TileCellEtaPhiOvThr"] = [ "A", "B", "D", "E" ]

def chmkDir( path ):
    """Safely create a directory"""
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def expandWildCard(histlist):
  newhistlist = []
  grouped = {}  # document the grouped plots, so we can show in one canvas
  for hist in histlist:
    if "*" in hist:      
      foundwc = False
      for wc in wildcards.keys():        
        if wc in hist:
          foundwc = True
          newpaths = []
          if "Tile" in wc:
            for samp in wildcards[wc]:
              tmp_path = hist
              new_path = tmp_path.replace("*",samp)
              newpaths.append(new_path)
          else:
            for part in wildcards[wc].keys():
              tmp_path = hist
              if part+"*" in tmp_path:
                for samp in wildcards[wc][part].keys():

                  new_path = tmp_path.replace(part+"*", part+samp)
                  #if "DatabaseNoise" not in hist: 
                  if "*" in new_path:
                    new_path = new_path.replace("*", wildcards[wc][part][samp])
                  newpaths.append(new_path)

          if len(newpaths) == 0: 
            print("Failed to get the full paths from the wildcard...")
            sys.exit()
          #histlist.remove(hist)
          print("Expanded",wc,"wildcard to give",len(newpaths),"histograms")
          newhistlist.extend(newpaths)      
      if foundwc is False:
        print("A wildcard has been used, but the requested histogram is not yet defined in this script. See the wildcards dictionary:",wildcards.keys())
        sys.exit()
      grouped[hist] = newpaths
    else:
      newhistlist.append(hist)
  return newhistlist, grouped

def setupDqmAPI():
  """ Connect to the atlasDQM web API service: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DQWebServiceAPIs """
  if (not os.path.isfile(dqmpassfile)):
    print("To connect to the DQ web service APIs, you need to generate an atlasdqm key and store it in the specified location ("+dqmpassfile+"). The contents should be yourname:key")
    print("To generate a key, go here : https://atlasdqm.cern.ch/dqauth/")
    sys.exit()

  passfile = open(dqmpassfile)
  passwd = passfile.read().strip(); passfile.close()
  passurl = 'https://%s@atlasdqm.cern.ch'%passwd
  s = xmlrpc.client.ServerProxy(passurl)
  return s

def groupCoord(coord_list):
    # Python3 code to demonstrate working of
    # from https://www.geeksforgeeks.org/python-group-adjacent-coordinates/
    # Group Adjacent Coordinates
    # Using product() + groupby() + list comprehension
    def Manhattan(tup1, tup2):
        return abs(tup1[0] - tup2[0]) + abs(tup1[1] - tup2[1])
    # Group Adjacent Coordinates
    # Using product() + groupby() + list comprehension
    #man_tups = [sorted(sub) for sub in product(coord_list, repeat = 2) if Manhattan(*sub) == 1]
    man_tups = [sorted(sub) for sub in product(coord_list, repeat = 2) if Manhattan(*sub) == 1 or Manhattan(*sub) == 2 ]

    test_tups = [ str(Manhattan(*sub))+"..."+str(sorted(sub)) for sub in product(coord_list, repeat = 2) ]

    res_dict = {ele: {ele} for ele in coord_list}
    for tup1, tup2 in man_tups:
        res_dict[tup1] |= res_dict[tup2]
        res_dict[tup2] = res_dict[tup1]

    res = [[*next(val)] for key, val in groupby(
    sorted(res_dict.values(), key = id), id)]

    return res


def getBoxes(coords, hist, col=R.kRed):
    ''' Give list of coordinates and the histogram on top of which the boxes will be displayed. 
    Groups the coordinates into neighbours, and makes boxes based on the upper and lower edges of bins '''
    grouped = groupCoord(coords)
    
    boxes = []
    for gr in grouped:
        x1 = min([c[0] for c in gr])
        y1 = min([c[1] for c in gr])
        x2 = max([c[0] for c in gr])
        y2 = max([c[1] for c in gr])
        etamin = hist.GetXaxis().GetBinLowEdge(x1)
        etamax = hist.GetXaxis().GetBinUpEdge(x2)
        phimin = hist.GetYaxis().GetBinLowEdge(y1)
        phimax = hist.GetYaxis().GetBinUpEdge(y2)
        box = R.TBox( etamin, phimin, etamax, phimax )
        box.SetLineColor(col)
        box.SetLineWidth(3)
        box.SetFillStyle(0)    
        boxes.append(box)
    return boxes


  
def findAndDraw(hist, hotthr=3, coldthr=3, nhottest=10, ncoldest=10):
    xvals = []
    yvals = []
    zvals = []
    xmins = []
    xmaxs = []
    ymins = [] 
    ymaxs = []
    ieta = []
    iphi = []
    xint = []
    yint = []
    nbin = []

    onIDhist = None
    posshists = [k.GetName().split("h_")[1] for k in onIDhistFile.GetListOfKeys() ]
    if any([ "_"+ph in hist.GetName() for ph in posshists]):
      print("Detect that this is a LAr single cells plot - will identify online IDs for entries")
      part = [ ph for ph in posshists if "_"+ph in hist.GetName() ][0]
      onIDhist = onIDhistFile.Get("h_"+part)

    for x in range(1, hist.GetNbinsX()+1):
        for y in range(1, hist.GetNbinsY()+1):
            ieta.append(x)
            iphi.append(y)
            thisnbin = hist.FindBin(hist.GetXaxis().GetBinCenter(x), hist.GetYaxis().GetBinCenter(y))
            if onIDhist is not None:
              thisnbin = int(onIDhist.GetBinContent(thisnbin))
            nbin.append(thisnbin)
            xvals.append(hist.GetXaxis().GetBinCenter(x))
            xmins.append(hist.GetXaxis().GetBinLowEdge(x))
            xmaxs.append(hist.GetXaxis().GetBinUpEdge(x))
            yvals.append(hist.GetYaxis().GetBinCenter(y))
            ymins.append(hist.GetYaxis().GetBinLowEdge(y))
            ymaxs.append(hist.GetYaxis().GetBinUpEdge(y))
            zvals.append(hist.GetBinContent(x,y))
            xint.append(pd.Interval(hist.GetXaxis().GetBinLowEdge(x), hist.GetXaxis().GetBinUpEdge(x), closed='both'))
            yint.append(pd.Interval(hist.GetYaxis().GetBinLowEdge(y), hist.GetYaxis().GetBinUpEdge(y), closed='both'))



    df = pd.DataFrame({'ieta': ieta, 'iphi': iphi, 'nbin/LAronlineID': nbin, 'eta': xvals, 'phi': yvals, 'etaint':xint, 'phiint':yint, 'noise': zvals})

    etas = df.eta.unique()
    phis = df.phi.unique()
    ietas = df.ieta.unique()
    iphis = df.iphi.unique()

    etas_before = len(ietas)
    phis_before = len(iphis)
    # Drop empty eta/phi values outside filled range
    for ieta in ietas:
      subset = df.loc[ (df.ieta == ieta) ]
      if (subset['noise'] == 0).all() == True:
        df.drop(df.loc[(df.ieta == ieta)].index, inplace=True)            
      #df.drop(df.loc[(df.ieta == ieta) & ((df.noise == 0).all())].index, inplace=True)            
    for iphi in iphis:
      subset = df.loc[ (df.iphi == iphi) ]
      if (subset['noise'] == 0).all() == True:
        df.drop(df.loc[(df.iphi == iphi)].index, inplace=True)            
      #df.drop(df.loc[ (df.iphi == iphi) & ((df.noise == 0).all())].index, inplace=True)

    etas = df.eta.unique()
    phis = df.phi.unique()
    ietas = df.ieta.unique()
    iphis = df.iphi.unique()

    if len(ietas) != etas_before:
      print(f"Removed ieta values out of fill range ... {etas_before} -> {len(ietas)}")
    if len(iphis) != phis_before:
      print(f"Removed iphi values out of fill range ... {phis_before} -> {len(iphis)}")

    maxeta = df.eta.max()
    maxphi = df.phi.max()
    mineta = df.eta.min()
    minphi = df.phi.min()

    meannoise = df[df.noise != 0].noise.mean()
    rmsnoise = df[df.noise != 0].noise.std()

    hotdf = pd.DataFrame(columns=list(df.columns))
    colddf = pd.DataFrame(columns=list(df.columns))

    
    for ieta in ietas:
        #rows = df.loc[ (df.ieta == ieta) & ((df.noise != 0).any())]
        rows = df.loc[ (df.ieta == ieta) ]
        # oioo iif not any, remove eta from df
        mean = rows.noise.mean()
        if mean == 0: continue
        rms = rows.noise.std()
        maxi = rows.noise.max()
        mini = rows.noise.min()
        hot = rows.loc[rows.noise > mean+(hotthr*rms)]

        cold = rows.loc[rows.noise < mean-(coldthr*rms)]
        # hotdf = hotdf.append(hot, ignore_index=True)
        hotdf = pd.concat([hotdf, hot], ignore_index=True)
        #colddf = colddf.append(cold, ignore_index=True)
        colddf = pd.concat([colddf, cold], ignore_index=True)

    for iphi in iphis:
        #rows = df.loc[(df.iphi == iphi) & ((df.noise != 0).any())]
        rows = df.loc[(df.iphi == iphi) ]
        mean = rows.noise.mean()
        if mean == 0: continue
        rms = rows.noise.std()
        maxi = rows.noise.max()
        mini = rows.noise.min()
        hot = rows.loc[rows.noise > mean+(hotthr*rms)]
        cold = rows.loc[rows.noise < mean-(coldthr*rms)]
        # hotdf = hotdf.append(hot, ignore_index=True)
        hotdf = pd.concat([hotdf, hot], ignore_index=True)
        #colddf = colddf.append(cold, ignore_index=True)
        colddf = pd.concat([colddf, cold], ignore_index=True)


    #  Also add the noisiest and least noisy bins, even if they didn't pass the rms selections
    tophot = df.nlargest(nhottest, 'noise')
    topcold = df.nsmallest(ncoldest, 'noise')    
    hotdf = pd.concat([hotdf, tophot], ignore_index=True)
    conddf = pd.concat([colddf, topcold], ignore_index=True)
    hotdf = hotdf.drop_duplicates()
    colddf = colddf.drop_duplicates()
    print(len(hotdf), "hot spot candidates and", len(colddf), "cold spot candidates identified")

    hotdf2 = hotdf.loc[hotdf.noise > meannoise+(hotthr*rmsnoise)]
    #hotdf2 = hotdf2.append(tophot, ignore_index=True)
    hotdf2 = hotdf2.drop_duplicates()

    if len(hotdf2) > 0:
      print(len(hotdf2), "hot spots have noise > overall mean + "+str(hotthr)+"* overall RMS. Adding these ones.")
      print(f"(mean = {meannoise}, RMS = {rmsnoise} ... {meannoise+(hotthr*rmsnoise)}")

      hotdf = pd.concat([hotdf, hotdf2], ignore_index=True)

    hotdf = hotdf.drop_duplicates()

    if len(hotdf) >= nhottest:
      print("Reducing list of hot spots")
      hotdf = hotdf.nlargest(nhottest, 'noise')
    print("** HOT SPOTS **")
    print(hotdf.sort_values(by='noise',ascending=False))
    print("*"*20)
    
    colddf2 = colddf.loc[colddf.noise < meannoise-(coldthr*rmsnoise)]
    #colddf2 = colddf2.append(topcold, ignore_index=True)
    colddf2 = colddf2.drop_duplicates()

    if len(colddf2) > 0:
      print(len(colddf2), "cold spots have noise < overall mean - "+str(coldthr)+"* overall RMS. Adding these ones.")
      print(f"(mean = {meannoise}, RMS = {rmsnoise} ... {meannoise-(coldthr*rmsnoise)}")

      colddf = pd.concat([colddf, colddf2], ignore_index=True)

    colddf = colddf.drop_duplicates()
    if len(colddf) >= ncoldest:
      print("Reducing list of cold spots")
      colddf = colddf.nsmallest(ncoldest, 'noise')
    print("** COLD SPOTS **")
    print(colddf.sort_values(by='noise'))
    print("*"*20)


    coldbox = []
    hotbox = []
    coldlist = []
    hotlist = []
    if len(colddf) > 0:
        coldlist = list(colddf[["ieta","iphi"]].itertuples(index=False, name=None))
        coldbox = getBoxes(coldlist, hist, R.kBlue)
    if len(hotdf) > 0:
        hotlist = list(hotdf[["ieta","iphi"]].itertuples(index=False, name=None))
        hotbox = getBoxes(hotlist, hist, R.kRed)

    print("Merged neighbouring spots to give", len(hotbox), "hot and", len(coldbox), "cold spot candidates")

    return hotbox, coldbox, hotlist, coldlist, hotdf, colddf, [mineta, maxeta, minphi, maxphi]



def findOverlap( df1, df2, str1, str2, isHot):


  if len(df1) == 0 or len(df2) == 0:
    return None
  colnames = list(df1.columns)
  df1 = df1.rename(columns={c: c+"_1" for c in colnames})
  df2 = df2.rename(columns={c: c+"_2" for c in colnames})
  
  df1["key"] = 0
  df2["key"] = 0

  def rowOverlap(row):
    eta1 = row.etaint_1
    eta2 = row.etaint_2
    phi1 = row.phiint_1
    phi2 = row.phiint_2
    if ( eta1.overlaps(eta2) or eta2.overlaps(eta1)) and (phi1.overlaps(phi2) or phi2.overlaps(phi1)):
      return 1
    else:
      return 0

  joined_df = pd.merge(df1, df2).drop(columns=['key'])

  joined_df['Overlap'] = joined_df.apply(lambda row: rowOverlap(row), axis=1)
  
  joined_df = joined_df[ joined_df['Overlap'] == 1 ]
  joined_df = joined_df.drop_duplicates()

  if len(joined_df.index.values) == 0: 
    #print(f"No overlap found between {str1} and {str2}")
    return None
  if isHot:
    outstr = "HOT SPOTS"
  else:
    outstr = "COLD SPOTS"
  print("*"*10, f"OVERLAPPING {outstr} FOUND!")
  print(f"**** Comparing {str1} and {str2} ****")
  print(f"Found {len(joined_df.index.values)} overlapping bins:")
  print(joined_df[["nbin/LAronlineID_1","noise_1","nbin/LAronlineID_2","noise_2"]])
  print("*"*30)
  print("\n")
  
  return joined_df

def main(args):    

    chmkDir(args.outDir)
    grouped = {}
    run_spec = {'stream': 'physics_CosmicCalo', 'proc_ver': 1,'source': 'tier0', 'low_run': args.runNumber, 'high_run':args.runNumber}
    dqmAPI = None
    if args.tag == "": # Try to retrieve the data project tag via atlasdqm
        dqmAPI = setupDqmAPI()
        run_info= dqmAPI.get_run_information(run_spec)
        if '%d'%args.runNumber not in list(run_info.keys()) or len(run_info['%d'%args.runNumber])<2:
            print("Unable to retrieve the data project tag via atlasdqm... Please double check your atlasdqmpass.txt or define it by hand with -t option")
            sys.exit()
        args.tag = run_info['%d'%args.runNumber][1]

    if len(args.histo) > 0: # The histograms ROOT file paths are directly provided 
        hArgs = args.histo
        for h in hArgs:
            if "*" in h:
                print("A wildcard was passed for histogram name input - this is not yet supported. Perhaps you meant to use the histoWD option?")
                sys.exit()
    elif len(args.histoWD) > 0: # The histograms paths are provided as webdisplay paths
        print("Web display paths provided: I will have to retrieve the ROOT file path of histograms")    
        args.histoWD, grouped = expandWildCard(args.histoWD)

        if dqmAPI is None:
            dqmAPI = setupDqmAPI()

            run_spec['stream'] = "%s%s"%(args.prefix,args.stream)
        hArgs = []
        for hist in args.histoWD:
            dqmf_config = dqmAPI.get_dqmf_configs(run_spec, hist)
            if len(dqmf_config.keys())== 0:
                print("Problem getting hist path from the provided WD string... is there a typo? You submitted",hist)
                print("Note - if you see two strings here perhaps you had a misplaced quote in your input arguments")

                print(f"get_dqmf_configs({run_spec}, {hist})")
                sys.exit()
            histpath = dqmf_config['%d'%args.runNumber]['annotations']['inputname']
            hArgs.append(histpath)
            if hist in [ val for k,v in grouped.items() for val in v ]:
                gk = [ k for k,b in grouped.items() if hist in grouped[k] ][0]
                gi = grouped[gk].index(hist)
                grouped[gk][gi] = histpath
    else:
        print("You need to define at least 1 histogram...")
        sys.exit()
  

    print("Requested histograms are",hArgs)
    histos = {}
    canvs = {}
    for h in hArgs:
      histos[h] = {}
    hotdfs = {}
    colddfs = {}
    print("Finding the path to the merged hist file")
    print("returnEosHistPath(", args.runNumber, args.stream, args.amiTag, args.tag,")" )
    mergedFilePath = pathExtract.returnEosHistPath( args.runNumber, args.stream, args.amiTag, args.tag )
    if ("FILE NOT FOUND" in mergedFilePath):
        print("No merged file found for this run")
        print("HINT: check if there is a folder like","/eos/atlas/atlastier0/tzero/prod/"+args.tag+"/"+args.stream+"/00"+str(args.runNumber)+"/"+args.tag+".00"+str(args.runNumber)+"."+args.stream+".*."+args.amiTag)
        sys.exit()
      
    print("Reading from file:",mergedFilePath)
    runFilePath = "root://eosatlas.cern.ch/%s"%(mergedFilePath).rstrip()


    drawngroup = {}
    f = R.TFile.Open(runFilePath)
    print("File is",runFilePath)

    R.gStyle.SetOptStat("")
    for hist in histos.keys():
      hpath = "run_%d/%s"%(args.runNumber,hist)
      print("**",hist,"**")
      histos[hist]["merged"] = f.Get(hpath)
      hotbox, coldbox, hotlist, coldlist, hotdf, colddf, plotranges = findAndDraw(histos[hist]["merged"], args.sigmahot, args.sigmacold, args.nhottest, args.ncoldest)

      hotdfs[hist] = hotdf
      colddfs[hist] = colddf
      groupname = None
      if hist in [ val for k,v in grouped.items() for val in v ]:
        groupname = [ k for k,b in grouped.items() if hist in grouped[k] ][0]
        if groupname not in canvs.keys():
          canvs[groupname] = R.TCanvas(groupname.replace("*","x"), groupname.replace("*","x"), 400*len(grouped[groupname]), 400)
          drawngroup[groupname] = 1
          if len(grouped[groupname]) <6:
            canvs[groupname].Divide(len(grouped[groupname]),1)
            print("dividing canvas", len(grouped[groupname]))
          else:
            print("Too many plots in the wildcard", groupname, len(grouped[groupname]))
            groupname = None

      if groupname is None:
        canvs[hist] = R.TCanvas(hist, hist)
        thiscanv = canvs[hist]
      else:
        thiscanv = canvs[groupname]       
        print("cd", drawngroup[groupname], groupname)
        thiscanv.cd(drawngroup[groupname])
        drawngroup[groupname] += 1
      
      # HERE store the hot and cold spots - are they present in other hists?
      #canv = R.TCanvas()
      if not hasattr(thiscanv, "objs"):
        thiscanv.objs = []
      R.gStyle.SetPalette(104)

      histos[hist]["merged"].Draw("COLZ")

      histos[hist]["merged"].GetXaxis().SetRangeUser(plotranges[0], plotranges[1])
      histos[hist]["merged"].GetYaxis().SetRangeUser(plotranges[2], plotranges[3])
      thiscanv.Update()
      for cb in coldbox:
          cb.Draw()
          thiscanv.objs.append(cb)
      for hb in hotbox:
        hb.Draw()
        thiscanv.objs.append(hb)

    for c in canvs.keys():
      outname = args.outDir+"/"+c.replace("/","")
      outname = outname.replace("*","--")
      outname += "_hotCold.png"
      print("Creating file",outname)
      canvs[c].Print(outname)
      
    print("\n\n")
    combos = list(combinations(list(hotdfs.keys()), 2))
    for comb in combos:
      if len(hotdfs[comb[0]])==0 or len(hotdfs[comb[1]])==0:
        continue
      #print("** Checking hot spot overlap **",comb[0], "**", comb[1], "**")
      findOverlap(hotdfs[comb[0]], hotdfs[comb[1]], comb[0], comb[1], True)
    combos = list(combinations(list(colddfs.keys()), 2))
    for comb in combos:
      if len(colddfs[comb[0]])==0 or len(colddfs[comb[1]])==0:
        continue
      #print("** Checking cold spot overlap **",comb[0], "**", comb[1], "**")
      findOverlap(colddfs[comb[0]], colddfs[comb[1]], comb[0], comb[1], False)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-r','--run',type=int,dest='runNumber',default=440199,help="Run number. Default %(default)s.")
    parser.add_argument('-sigmahot', type=int, dest='sigmahot', default=5, help='# sigma above mean for each eta/phi slice, over which we consider the bin as a hot spot. Default %(default)s.')
    parser.add_argument('-sigmacold', type=int, dest='sigmacold', default=5, help='# sigma below mean for each eta/phi slice, below which we consider the bin as a cold spot. Default %(default)s.')
    #parser.add_argument('-ll','--lowerlb',type=int,dest='lowerlb',default='0',help="Lower lb",action='store')
    #parser.add_argument('-ul','--upperlb',type=int,dest='upperlb',default='999999',help="Upper lb",action='store')
    parser.add_argument('-s','--stream',dest='stream',default='CosmicCalo',help="Stream without prefix: express/CosmicCalo/Main/ZeroBias/MinBias. Default %(default)s.")
    parser.add_argument('-t','--tag',dest='tag',default='',help="DAQ tag: data16_13TeV, data16_cos...By default retrieve it via atlasdqm.")
    parser.add_argument('-a','--amiTag',dest='amiTag',default='f',help="First letter of AMI tag: x->express / f->bulk. Default %(default)s.")
    parser.add_argument('--histo',dest='histo',default='',help='ROOT-file path of histograms - As many as you want with : [type("1d" or "2d")] [root path] [x] [y if 2d] [delta] (if not provided use global)',action='store',nargs="*")
    parser.add_argument('--histoWD',dest='histoWD',default='',help='Webdisplay path of histograms - As many as you want with : [type("1d" or "2d")] [root path] [x] [y if 2d] [delta] (if not provided use global)',action='store',nargs="*")
    parser.add_argument('--topN', dest='topN', default=20, type=int, help='Only plot boxes for top $N hottest/coldest spots. Default %(default)s.')
    parser.add_argument('-ncoldest', dest='ncoldest', default=None, type=int, help='Only plot boxes for top $N coldest spots. Default %(default)s.')
    parser.add_argument('-nhottest', dest='nhottest', default=None, type=int, help='Only plot boxes for top $N hottest spots. Default %(default)s.')
    parser.add_argument('-o','--outDir', dest='outDir', default="./hotColdPlots", help='Output directory for plots. Default %(default)s.')

    args = parser.parse_args()

    if args.ncoldest is None:
      args.ncoldest = args.topN
    if args.nhottest is None:
      args.nhottest = args.topN

    prefix = {'express':'express_','Egamma':'physics_','CosmicCalo':'physics_','JetTauEtmiss':'physics_','Main':'physics_','ZeroBias':'physics_','MinBias':'physics_','UPC':'physics_','HardProbes':'physics_'}
    if "_" in args.stream: args.stream = args.stream.split("_")[1]
    args.prefix = prefix[args.stream]
    print("Starting")
    main(args)
