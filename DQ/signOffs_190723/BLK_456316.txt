Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Event Veto for run 456316 (Sun, 16 Jul 2023 12:39:40 +0200) ==
Found Noise or data corruption in run 456316
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456316_Main.db

Found project tag data23_13p6TeV for run 456316
Found 35 Veto Ranges with 12572 events
Found 48 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456316
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2
Found a total of 35 corruption periods, covering a total of 24.15 seconds
Lumi loss due to corruption: 467.61 nb-1 out of 569846.09 nb-1 (0.82 per-mil)
Overall Lumi loss is 467.6071549169549 by 24.15294587 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Event Veto for run 456316 (Sun, 16 Jul 2023 12:16:32 +0200) ==
Found Noise or data corruption in run 456316
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456316_Main.db

Found project tag data23_13p6TeV for run 456316
Found 35 Veto Ranges with 12572 events
Found 48 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456316
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2
Found a total of 35 corruption periods, covering a total of 24.15 seconds
Lumi loss due to corruption: 467.61 nb-1 out of 569846.09 nb-1 (0.82 per-mil)
Overall Lumi loss is 467.6071549169549 by 24.15294587 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Missing EVs for run 456316 (Wed, 12 Jul 2023 13:28:02 +0200) ==

Found a total of 149 noisy periods, covering a total of 0.17 seconds
Found a total of 1178 Mini noise periods, covering a total of 1.29 seconds
Lumi loss due to noise-bursts: 3.25 nb-1 out of 569846.09 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 24.46 nb-1 out of 569846.09 nb-1 (0.04 per-mil)
Overall Lumi loss is 27.708875953729088 by 1.46438873 s of veto length


