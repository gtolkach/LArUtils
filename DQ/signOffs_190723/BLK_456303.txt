Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1373

== Missing EVs for run 456303 (Tue, 11 Jul 2023 15:15:40 +0200) ==

Found a total of 2 noisy periods, covering a total of 0.00 seconds
Found a total of 18 Mini noise periods, covering a total of 0.02 seconds
Lumi loss due to noise-bursts: 0.03 nb-1 out of 13539.27 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.18 nb-1 out of 13539.27 nb-1 (0.01 per-mil)
Overall Lumi loss is 0.20433554683257762 by 0.020603609999999998 s of veto length


