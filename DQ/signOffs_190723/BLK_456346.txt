Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1378

== Event Veto for run 456346 (Mon, 17 Jul 2023 02:42:16 +0200) ==
Found Noise or data corruption in run 456346
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456346_Main.db

Found project tag data23_13p6TeV for run 456346
Found 3 Veto Ranges with 7 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456346_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456346
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456346_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 1.50 seconds
Lumi loss due to corruption: 30.74 nb-1 out of 140176.25 nb-1 (0.22 per-mil)
Overall Lumi loss is 30.739741787948166 by 1.500866245 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1378

== Event Veto for run 456346 (Mon, 17 Jul 2023 02:34:56 +0200) ==
Found Noise or data corruption in run 456346
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456346_Main.db

Found project tag data23_13p6TeV for run 456346
Found 3 Veto Ranges with 7 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456346_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456346
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456346_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 1.50 seconds
Lumi loss due to corruption: 30.74 nb-1 out of 140176.25 nb-1 (0.22 per-mil)
Overall Lumi loss is 30.739741787948166 by 1.500866245 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1378

== Missing EVs for run 456346 (Wed, 12 Jul 2023 19:16:43 +0200) ==

Found a total of 44 noisy periods, covering a total of 0.05 seconds
Found a total of 291 Mini noise periods, covering a total of 0.32 seconds
Lumi loss due to noise-bursts: 0.92 nb-1 out of 140176.25 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 6.32 nb-1 out of 140176.25 nb-1 (0.05 per-mil)
Overall Lumi loss is 7.237845385270344 by 0.36724053 s of veto length


