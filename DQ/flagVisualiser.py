import sys
import ROOT as R
#import ctypes 

R.gErrorIgnoreLevel = R.kWarning
R.gROOT.SetBatch(True)


import numpy as np
import pandas as pd
from LArCellBinning import lArCellBinningScheme

PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"
sys.path.append(PrintMappingDir)
import printMapping as pm

def badChanFromFile(badChanFile, isSC=False):
    badchan = []
    if isSC is False:
        onid = "ONL_ID"
        layout = ["barrel_ec", "pos_neg", "FT", "SL", "CH", "0",  "status(es)",  "#", "ONL_ID"]
    else:
        onid = "SC_ONL_ID"
        layout = ["barrel_ec", "pos_neg", "FT", "SL", "CH", "0",  "status(es)",  "#", "SC_ONL_ID"]
    #layoutannot = ["barrel_ec", "pos_neg", "FT", "Slot", "channel", "0",  "status(es)",  "#", "ONL_ID", "->", " offlineID"]
    with open(badfile, "r") as f:
        lines = f.readlines()
        for line in lines: 
            if "->" in line: 
                line = line.split("->")[0]
            line = line.split(" ")
            line = [l for l in line if l != ""]
            thischan = {descr:val for descr,val in zip(layout[:6],line[:6])}
            thischan[onid] = line[-1]
            thischan["status"] = line[6:-2]
            badchan.append(thischan)
    return badchan
        

def makeHists():
    layerNames = ["EMBPA", "EMBPC", "EMB1A", "EMB1C", "EMB2A", "EMB2C", "EMB3A", "EMB3C", "HEC0A", "HEC0C", "HEC1A", "HEC1C", "HEC2A", "HEC2C", "HEC3A", "HEC3C", "EMECPA", "EMECPC", "EMEC1A", "EMEC1C", "EMEC2A", "EMEC2C", "EMEC3A", "EMEC3C", "FCAL1A", "FCAL1C", "FCAL2A", "FCAL2C", "FCAL3A", "FCAL3C"]

    hists = {}

    for part in layerNames:
        xbins = np.array(lArCellBinningScheme.etaRange[part], dtype='d')
        ybins = np.array(lArCellBinningScheme.phiRange[part], dtype='d')
        hists[part] = R.TH2D("h_"+part, "h_"+part,
                             len(xbins)-1, xbins, 
                             len(ybins)-1, ybins)
    return hists


def main(badchans, isSC, outf):
    hists = makeHists()
    if isSC is False:
        onid = "ONL_ID"
        eta = "ETA"
        phi = "PHI"
    else:
        onid = "SC_ONL_ID"
        eta = "SCETA"
        phi = "SCPHI"
    want = [ onid, "DETNAME", "SAM", eta, phi]
    print("Query printmapping")
    pmout = pm.query(want = want)
    pmdf = pd.DataFrame(pmout, columns = want)

    #print(pmdf)
    for bc in badchans:
        if "sporadicBurstNoise" not in bc["status"] and "highNoiseHG" not in bc["status"]:
            continue
        row = pmdf.loc[pmdf[onid] == bc[onid]]
        SAM = row['SAM'].values[0]
        DETNAME = row['DETNAME'].values[0]
        ETA = float(row[eta].values[0])
        PHI = float(row[phi].values[0])

        if DETNAME.startswith("EMB") or DETNAME.startswith("EMEC"):
            SAM2 = "P"
        else:
            SAM2 = SAM
        DNAME = DETNAME[:-1]+SAM2+DETNAME[-1]
        thishist = hists[DNAME]

        c = 0
        if "sporadicBurstNoise" in bc["status"]:
            c = 1
        if "highNoiseHG" in bc["status"]:
            c = 2
        #print("Filling",DNAME,"for",bc["status"])
        thishist.SetBinContent(thishist.FindBin(ETA,PHI),c)
        

    #palette = np.array([0,5,2], dtype='d')

    #R.gStyle.SetPalette(3,palette)


    
    NCont = 3
    r = np.array([ 0, 0, 1 ], dtype='d')
    g = np.array([ 0, 1, 0 ], dtype='d')
    b = np.array([ 0, 0, 0 ], dtype='d')
    s = np.array([ 0, 1, 2 ], dtype='d')
    FI = R.TColor.CreateGradientColorTable(3, s, r, g, b, NCont)
    R.gStyle.SetNumberContours(NCont)
    #MyPalette = np.array([ FI+i for i in range(0, NCont) ], dtype='d')
    #R.gStyle.SetPalette(NCont, MyPalette)

    cols = np.array([0, 3, 2],dtype='i')
    R.gStyle.SetPalette(len(cols), cols)
  

    R.gStyle.SetOptStat(0)

    filled = [ hists[h] for h in hists.keys() if hists[h].GetEntries() > 0 ]
    cols = 6
    rows = int(len(filled)/cols)+(len(filled)%cols>0)
    c = R.TCanvas("c","c",600*cols, 600*rows)
    print(len(filled), "histos - divide",cols,rows)
    c.Divide(cols,rows)

    n = 1
    for h in hists.keys():
        if hists[h].GetEntries() == 0: continue
        hists[h].GetXaxis().SetTitle("#eta")
        hists[h].GetYaxis().SetTitle("#phi")
        hists[h].GetZaxis().SetRangeUser(0,2)
        hists[h].SetTitle(h)
        c.cd(n)
        hists[h].Draw("COLZ")
        print(n, h, hists[h].GetEntries())
        n+=1
    c.Print(outf)

if __name__ == "__main__":
    badfile = sys.argv[1]
    if len(sys.argv)>2:
        outf = sys.argv[2]
    else:
        outf = "flagged.png"
    badchans = badChanFromFile(badfile)
    main(badchans,outf)

