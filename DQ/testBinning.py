import os, sys
import ROOT as R
import ctypes
import numpy as np
from LArCellBinning import lArCellBinningScheme
R.gROOT.SetBatch(True)

def convHname(hname):
  DET = { "EMB":"0", "EMEC":"1","HEC":"2","FCAL":"3" }
  AC = { "A":"1", "C":"-1" }
  SAM = { "P":"0" }
  for samp in range(0,4): SAM[str(samp)] = str(samp)
  for det in DET.keys():
    for sam in SAM.keys():
      for ac in AC.keys():  
        if det+sam+ac in hname:
          return DET[det], AC[ac], SAM[sam]
  return None, None, None

def LIDProposal(hname, eta, phi, delta=0.15):
  """ Print a proposed LArID translator (https://atlas-larmon.cern.ch/LArIdtranslator/) SQL query """
  det, ac, sam = convHname(hname)
  if det is None: return
  proposal = "DET="+det+" and AC="+ac+" and SAM="+sam+" and ETA between "+str(eta-delta)+" and "+str(eta+delta)+" and PHI between "+str(phi-delta)+" and "+str(phi+delta)
  print("*"*30)
  print("Proposal query for LArID translator for plot",hname,"is as follows:")
  print(proposal)

PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"
variables = [ "ONL_ID", "DETNAME", "ETA", "PHI", "DET", "AC", "SAM", "IETA", "IPHI" ]
cmd="python3 "+PrintMappingDir+"printMapping.py -w "+" ".join(variables)

out=os.popen(cmd).read()
outdict = {}
for line in out.split("\n"):
    vals = line.strip().split(" ")
    if len(vals) == 1: continue
    onid = vals[variables.index("ONL_ID")]
    outdict[onid] = { var:val for val,var in zip(vals,variables) }

def prCol(prt, col="red"):
  if col.lower() == "red":
    colcode="91"
  elif col.lower() == "blue":
    colcode="94"
  else:
    colcode="93"
  return("\033["+colcode+"m"+str(prt)+"\033[00m")

layerNames = ["EMBPA", "EMBPC", "EMB1A", "EMB1C", "EMB2A", "EMB2C", "EMB3A", "EMB3C", "HEC0A", "HEC0C", "HEC1A", "HEC1C", "HEC2A", "HEC2C", "HEC3A", "HEC3C", "EMECPA", "EMECPC", "EMEC1A", "EMEC1C", "EMEC2A", "EMEC2C", "EMEC3A", "EMEC3C", "FCAL1A", "FCAL1C", "FCAL2A", "FCAL2C", "FCAL3A", "FCAL3C"]

hists = {}
realbins = {}
fillval = {}

checking = "FCAL2A"

for part in layerNames:

    athxbins = np.array(lArCellBinningScheme.etaRange[part], dtype='d')
    athybins = np.array(lArCellBinningScheme.phiRange[part], dtype='d')

    realbins[part] = {}
    thissam = part[-2]
    if thissam == "P": thissam = 0
    etaval = sorted(list(set([ float(outdict[cell]["ETA"]) for cell in outdict.keys() if (int(outdict[cell]["SAM"]) == int(thissam) and outdict[cell]["DETNAME"] == part[:-2]+part[-1] ) ] ) ) )

    if part.endswith("C"):
      etaval = sorted([abs(e) for e in etaval])

    eta = [etaval[etaval.index(t)] - (etaval[etaval.index(t)+1]-etaval[etaval.index(t)])/2 for t in etaval if etaval.index(t) < len(etaval)-1]
    eta.append( etaval[-1] + abs(etaval[-1]-etaval[-2])/2 )
    if part.endswith("C"):
      eta = sorted([-e for e in eta])

    phival = sorted(list(set([ float(outdict[cell]["ETA"]) for cell in outdict.keys() if int(outdict[cell]["SAM"]) == int(thissam) and outdict[cell]["DETNAME"] == part[:-2]+part[-1] ])))
    phi = [phival[phival.index(t)] - (phival[phival.index(t)+1]-phival[phival.index(t)])/2 for t in phival if phival.index(t) < len(phival)-1]
    phi.append( phival[-1] + abs(phival[-1]-phival[-2])/2 )

    #if part == checking:
    #  for cell in outdict.keys():
    #    if outdict[cell]["DETNAME"] == part[:-2]+part[-1] and int(outdict[cell]["SAM"]) == int(thissam):
    #      print("oi", part, outdict[cell]["DETNAME"],  outdict[cell]["ETA"], outdict[cell]["PHI"])
                
    realbins[part]["eta"] = etaval #np.array([ round(e,5) for e in eta ], dtype='d') 
    realbins[part]["phi"] = phival #np.array([ round(p,5) for p in phi ], dtype='d')

    xbins = realbins[part]["eta"]
    ybins = realbins[part]["phi"]

    xbins = athxbins
    ybins = athybins
    hists[part] = R.TH2D("h_"+part, "h_"+part,
                         len(xbins)-1, xbins, 
                         len(ybins)-1, ybins)
    nbins = hists[part].GetSize()

    fillval[part] = {}

    #print("**", part, "**")
    #print(xbins)
  
#print(realbins["EMEC1A"]["eta"])
#print(lArCellBinningScheme.etaRange["EMEC1A"])
#print("****")



refilled={}
toprint_full = {}
toprint_empty = {}

for onID in outdict.keys():
    eta = float(outdict[onID]["ETA"])
    phi = float(outdict[onID]["PHI"])

    sam = outdict[onID]["SAM"]

    if "EMB" in outdict[onID]["DETNAME"] and abs(eta) > 1.4:
        sam = str(int(sam)+1)

    if int(sam) == 0 and "HEC" not in outdict[onID]["DETNAME"]:
        sam = "P"

    detname = outdict[onID]["DETNAME"][:-1]+sam+outdict[onID]["DETNAME"][-1]

    #if "EMB" in outdict[onID]["DETNAME"] and abs(eta) > 1.4:
    #    print("oioioi", eta, outdict[onID]["SAM"],sam, detname)

    thisbin = hists[detname].FindBin(eta,phi)
    if thisbin not in fillval[detname].keys():
      fillval[detname][thisbin] = []
    fillval[detname][thisbin].append([eta,phi])
    content = hists[detname].GetBinContent(thisbin)

    if content != 0:
        if detname == checking: print("lalal", thisbin, eta, phi, fillval[detname][thisbin])
        if detname not in refilled.keys():
            refilled[detname] = {}
        if thisbin not in refilled[detname].keys():
            refilled[detname][thisbin] = 1
        else:
            refilled[detname][thisbin] += 1
    hists[detname].Fill(eta,phi)

nrf = 0
for part in refilled.keys():
    toprint_full[part] = []    
    for nb in refilled[part].keys():
        x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
        hists[part].GetBinXYZ(nb, x, y, z)

        content = hists[part].GetBinContent(nb)
        xcent = hists[part].GetXaxis().GetBinCenter(x.value)
        xlow = hists[part].GetXaxis().GetBinLowEdge(x.value)
        if xlow not in toprint_full[part]:
          toprint_full[part].append(xlow)

        xhi = hists[part].GetXaxis().GetBinUpEdge(x.value)
        ycent = hists[part].GetYaxis().GetBinCenter(y.value)
        ylow = hists[part].GetYaxis().GetBinLowEdge(y.value)
        yhi = hists[part].GetYaxis().GetBinUpEdge(y.value)

        det, ac, sam = convHname(part)
        
        if "EMB" in part and (abs(xlow) > 1.4 or abs(xhi) > 1.4):
            sam = str(int(sam)-1)
            

        proposal = "DET="+det+" and AC="+ac+" and SAM="+sam+" and" 
        printstr = proposal+" ETA between "+str(format(xlow,".4f"))+" and "+str(format(xhi,".4f"))+" and PHI between "+str(format(ylow,".4f"))+" and "+str(format(yhi,".4f"))+" (content = "+str(int(content))+")"
        #if part == checking:
        #  print(part, nb, printstr)
        nrf += 1
empty={}
filledeta={}
for part in hists.keys():
  nbins = hists[part].GetSize()
  empty[part] = []
  filledeta[part] = []
  toprint_empty[part] = []
  for b in range(1, nbins+1):
    x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
    hists[part].GetBinXYZ(b, x, y, z)

    xlow = hists[part].GetXaxis().GetBinLowEdge(x.value)
    if hists[part].GetBinContent(b) == 0:
      empty[part].append(b)
      if xlow not in filledeta[part]:
        toprint_empty[part].append(xlow)
    else:
      if xlow in toprint_empty[part]:
        filledeta[part].append(xlow)
        toprint_empty[part].remove(xlow)
  print(len(empty[part]),"empty bins in",part)

print("Total of",nrf,"refilled bins in",{k:len(toprint_full[k]) for k in toprint_full.keys()})

plotdict = {"EMBA":["EMBPA", "EMB1A", "EMB2A", "EMB3A"],
            "EMBC":["EMBPC", "EMB1C", "EMB2C", "EMB3C"],
            "EMECA":["EMECPA", "EMEC1A", "EMEC2A", "EMEC3A"],
            "EMECC":["EMECPC", "EMEC1C", "EMEC2C", "EMEC3C"],
            "HECA":["HEC0A", "HEC1A", "HEC2A", "HEC3A"],
            "HECC":["HEC0C", "HEC1C", "HEC2C", "HEC3C"],
            "FCALA":["FCAL1A", "FCAL2A", "FCAL3A"],
            "FCALC":["FCAL1C", "FCAL2C", "FCAL3C"] }
for h in plotdict.keys():
  canv = R.TCanvas()
  canv.Divide(2,2)
  hn=1
  R.gStyle.SetOptStat(False)
  for item in plotdict[h]:
    canv.cd(hn)
    hists[item].GetZaxis().SetRangeUser(0,4)
    hists[item].Draw("COLZ")
    hn+=1
    canv.cd()
  canv.Print("plots_"+h+".png")

#for k in refilled.keys():
for k in hists.keys():
  if k != checking: continue
  print("****",k,"*****")
  printb = lArCellBinningScheme.etaRange[k]
  if k in refilled.keys():
    printb = ", ".join([ prCol(r,"red") if r in toprint_full[k] else str(r) for r in printb ])
  if k in empty.keys():
    if isinstance(printb, str):
      printb = printb.split(", ")
    printb = ", ".join([ prCol(r,"blue") if r in toprint_empty[k] else str(r) for r in printb ])
  print(f"{printb}")
  if k in refilled.keys():
        print(len(toprint_full[k]), "overlapping")
  print(len(toprint_empty[k]), "empty")
  print(len(empty[k]), "empty /",hists[k].GetSize())

  #print(realbins[k]["eta"])
  #print( [realbins[k]["eta"][realbins[k]["eta"].index(r)+1] - r for r in realbins[k]["eta"][:-1] ])
