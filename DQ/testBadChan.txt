Checking bad standard cells list
Reading info from text file
Getting info from LArIdTranslator
Also getting HV line info
**********
0x38280000 942145536
spBN : {'count': '3', 'runs': ['438219', ' 438234', ' 438323']}
hnHG : {'count': '5', 'runs': ['438252', ' 438266', ' 438277', ' 438364', ' 438411']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 5
SL : 1
CH : 0
SAM : 0
FEB : FEB_EMBC2_06R_PS
HVCORR : 1.425856590270996
HVLINES : ['281004', '283004']
**********
0x38480400 944243712
spBN : {'count': '0', 'runs': ['']}
hnHG : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 9
SL : 1
CH : 4
SAM : 0
FEB : FEB_EMBC1_04R_PS
HVCORR : 1.2072937488555908
HVLINES : ['284004', '286004']
**********
0x38480500 944243968
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '1', 'runs': ['438277']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 9
SL : 1
CH : 5
SAM : 0
FEB : FEB_EMBC1_04R_PS
HVCORR : 1.2072937488555908
HVLINES : ['284004', '286004']
**********
0x38580400 945292288
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 11
SL : 1
CH : 4
SAM : 0
FEB : FEB_EMBC1_03R_PS
HVCORR : 1.207287073135376
HVLINES : ['284012', '286012']
**********
0x38681700 946345728
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 13
SL : 1
CH : 23
SAM : 0
FEB : FEB_EMBC1_02R_PS
HVCORR : 1.20729398727417
HVLINES : ['285004', '287004']
**********
0x38681c00 946347008
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 13
SL : 1
CH : 28
SAM : 0
FEB : FEB_EMBC1_02R_PS
HVCORR : 1.20729398727417
HVLINES : ['285004', '287004']
**********
0x38682300 946348800
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 13
SL : 1
CH : 35
SAM : 0
FEB : FEB_EMBC1_02R_PS
HVCORR : 1.206507921218872
HVLINES : ['285005', '287005']
**********
0x38709c00 946904064
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 14
SL : 2
CH : 28
SAM : 1
FEB : FEB_EMBC1_02L_F0
HVCORR : 1.0
HVLINES : ['245000', '247000']
**********
0x387ea800 947824640
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 15
SL : 14
CH : 40
SAM : 2
FEB : FEB_EMBC1_01R_M3
HVCORR : 1.0
HVLINES : ['245014', '247014']
**********
0x38900500 948962560
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 18
SL : 1
CH : 5
SAM : 0
FEB : FEB_EMBC4_16L_PS
HVCORR : 1.2072927951812744
HVLINES : ['288008', '290008']
**********
0x38bb0800 951781376
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 23
SL : 7
CH : 8
SAM : 1
FEB : FEB_EMBC4_13R_F5
HVCORR : 1.0
HVLINES : ['253013', '255013']
**********
0x38bb0900 951781632
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 23
SL : 7
CH : 9
SAM : 1
FEB : FEB_EMBC4_13R_F5
HVCORR : 1.0
HVLINES : ['253013', '255013']
**********
0x38bb0a00 951781888
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 23
SL : 7
CH : 10
SAM : 1
FEB : FEB_EMBC4_13R_F5
HVCORR : 1.0
HVLINES : ['253013', '255013']
**********
0x38bea300 952017664
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 23
SL : 14
CH : 35
SAM : 2
FEB : FEB_EMBC4_13R_M3
HVCORR : 1.156738519668579
HVLINES : ['255014', '309000']
**********
0x38c81800 952637440
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 25
SL : 1
CH : 24
SAM : 0
FEB : FEB_EMBC3_12R_PS
HVCORR : 1.298463225364685
HVLINES : ['292004', '294004']
**********
0x38cd8f00 952995584
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 25
SL : 12
CH : 15
SAM : 2
FEB : FEB_EMBC3_12R_M1
HVCORR : 1.0
HVLINES : ['256010', '258010']
**********
0x38f00500 955254016
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 30
SL : 1
CH : 5
SAM : 0
FEB : FEB_EMBC3_10L_PS
HVCORR : 1.207292914390564
HVLINES : ['293008', '295008']
**********
0x38f00600 955254272
spBN : {'count': '0', 'runs': ['']}
hnHG : {'count': '7', 'runs': ['438219', ' 438234', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 30
SL : 1
CH : 6
SAM : 0
FEB : FEB_EMBC3_10L_PS
HVCORR : 1.207292914390564
HVLINES : ['293008', '295008']
**********
0x383c1500 943461632
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 7
SL : 9
CH : 21
SAM : 3
FEB : FEB_EMBC2_05R_B0
HVCORR : 1.0
HVLINES : ['237009', '239009']
**********
0x383c1600 943461888
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 7
SL : 9
CH : 22
SAM : 3
FEB : FEB_EMBC2_05R_B0
HVCORR : 1.0
HVLINES : ['237009', '239009']
**********
0x383c1700 943462144
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 7
SL : 9
CH : 23
SAM : 3
FEB : FEB_EMBC2_05R_B0
HVCORR : 1.0
HVLINES : ['237009', '239009']
**********
0x383c1900 943462656
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 7
SL : 9
CH : 25
SAM : 3
FEB : FEB_EMBC2_05R_B0
HVCORR : 1.0
HVLINES : ['237009', '239009']
**********
0x383c5400 943477760
spBN : {'count': '4', 'runs': ['438234', ' 438252', ' 438266', ' 438277']}
hnHG : {'count': '2', 'runs': ['438323', ' 438364']}
DET : 0
AC : -1
DETNAME : EMBC
FT : 7
SL : 9
CH : 84
SAM : 3
FEB : FEB_EMBC2_05R_B0
HVCORR : 1.0
HVLINES : ['237009', '237015', '239009', '239015']
**********
0x391b9b00 958110464
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 3
SL : 8
CH : 27
SAM : 1
FEB : FEB_EMBA1_03R_F6
HVCORR : 1.0
HVLINES : ['201014', '203014']
**********
0x391b9c00 958110720
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 3
SL : 8
CH : 28
SAM : 1
FEB : FEB_EMBA1_03R_F6
HVCORR : 1.0
HVLINES : ['201014', '203014']
**********
0x391b9d00 958110976
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 3
SL : 8
CH : 29
SAM : 1
FEB : FEB_EMBA1_03R_F6
HVCORR : 1.0
HVLINES : ['201014', '203014']
**********
0x392e9b00 959355648
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 5
SL : 14
CH : 27
SAM : 2
FEB : FEB_EMBA1_04R_M3
HVCORR : 1.0
HVLINES : ['204014', '206014']
**********
0x393e4200 960381440
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 7
SL : 13
CH : 66
SAM : 2
FEB : FEB_EMBA2_05R_M2
HVCORR : 1.0539278984069824
HVLINES : ['207012', '306002']
**********
0x39404600 960513536
spBN : {'count': '3', 'runs': ['438219', ' 438252', ' 438364']}
hnHG : {'count': '4', 'runs': ['438234', ' 438266', ' 438277', ' 438411']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 8
SL : 1
CH : 70
SAM : 0
FEB : FEB_EMBA2_05L_PS
HVCORR : 1.2083303928375244
HVLINES : ['268002', '270002']
**********
0x3982e100 964878592
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 16
SL : 6
CH : 97
SAM : 1
FEB : FEB_EMBA3_09L_F4
HVCORR : 1.0322580337524414
HVLINES : ['216004', '216007', '218004', '218007']
**********
0x3982e200 964878848
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 16
SL : 6
CH : 98
SAM : 1
FEB : FEB_EMBA3_09L_F4
HVCORR : 1.0322580337524414
HVLINES : ['216004', '216007', '218004', '218007']
**********
0x3982e300 964879104
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 16
SL : 6
CH : 99
SAM : 1
FEB : FEB_EMBA3_09L_F4
HVCORR : 1.0322580337524414
HVLINES : ['216004', '216007', '218004', '218007']
**********
0x39922b00 965880576
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 18
SL : 5
CH : 43
SAM : 1
FEB : FEB_EMBA3_10L_F3
HVCORR : 1.0
HVLINES : ['217003', '219003']
**********
0x39922c00 965880832
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 18
SL : 5
CH : 44
SAM : 1
FEB : FEB_EMBA3_10L_F3
HVCORR : 1.0
HVLINES : ['217003', '219003']
**********
0x3995cc00 966118400
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 18
SL : 12
CH : 76
SAM : 2
FEB : FEB_EMBA3_10L_M1
HVCORR : 1.0
HVLINES : ['217002', '219002']
**********
0x39a90a00 967379456
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 21
SL : 3
CH : 10
SAM : 1
FEB : FEB_EMBA3_12R_F1
HVCORR : 1.0
HVLINES : ['220015', '222015']
**********
0x39a90b00 967379712
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 21
SL : 3
CH : 11
SAM : 1
FEB : FEB_EMBA3_12R_F1
HVCORR : 1.0
HVLINES : ['220015', '222015']
**********
0x39a90c00 967379968
spBN : {'count': '8', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 21
SL : 3
CH : 12
SAM : 1
FEB : FEB_EMBA3_12R_F1
HVCORR : 1.0
HVLINES : ['220015', '222015']
**********
0x39c53700 969225984
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 24
SL : 11
CH : 55
SAM : 2
FEB : FEB_EMBA4_13L_M0
HVCORR : 1.0
HVLINES : ['224001', '226001']
**********
0x39fe3f00 972963584
spBN : {'count': '6', 'runs': ['438219', ' 438234', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 31
SL : 13
CH : 63
SAM : 2
FEB : FEB_EMBA1_01R_M2
HVCORR : 1.0
HVLINES : ['229013', '231013']
**********
0x39b88200 968393216
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 23
SL : 2
CH : 2
SAM : 1
FEB : FEB_EMBA4_13R_F0
HVCORR : 1.0
HVLINES : ['221008', '223008']
**********
0x39be2100 968761600
spBN : {'count': '6', 'runs': ['438234', ' 438252', ' 438266', ' 438277', ' 438323', ' 438364']}
hnHG : {'count': '0', 'runs': ['']}
DET : 0
AC : 1
DETNAME : EMBA
FT : 23
SL : 13
CH : 33
SAM : 2
FEB : FEB_EMBA4_13R_M2
HVCORR : 1.0
HVLINES : ['221013', '223013']
**********
0x3a9ce900 983361792
spBN : {'count': '7', 'runs': ['438219', ' 438234', ' 438252', ' 438266', ' 438277', ' 438364', ' 438411']}
hnHG : {'count': '0', 'runs': ['']}
DET : 1
AC : -1
DETNAME : EMECC
FT : 19
SL : 10
CH : 105
SAM : 2
FEB : FEB_EMECC2_10R_M0
HVCORR : 1.0
HVLINES : ['109009', '111009']
237009 : 5
239009 : 5
253013 : 3
255013 : 3
201014 : 3
203014 : 3
216004 : 3
216007 : 3
218004 : 3
218007 : 3
220015 : 3
222015 : 3
284004 : 2
286004 : 2
285004 : 2
287004 : 2
293008 : 2
295008 : 2
217003 : 2
219003 : 2
281004 : 1
283004 : 1
284012 : 1
286012 : 1
285005 : 1
287005 : 1
245000 : 1
247000 : 1
245014 : 1
247014 : 1
288008 : 1
290008 : 1
255014 : 1
309000 : 1
292004 : 1
294004 : 1
256010 : 1
258010 : 1
237015 : 1
239015 : 1
204014 : 1
206014 : 1
207012 : 1
306002 : 1
268002 : 1
270002 : 1
217002 : 1
219002 : 1
224001 : 1
226001 : 1
229013 : 1
231013 : 1
221008 : 1
223008 : 1
221013 : 1
223013 : 1
109009 : 1
111009 : 1
{'FEB_EMBC2_06R_PS': 1, 'FEB_EMBC1_04R_PS': 2, 'FEB_EMBC1_03R_PS': 1, 'FEB_EMBC1_02R_PS': 3, 'FEB_EMBC1_02L_F0': 1, 'FEB_EMBC1_01R_M3': 1, 'FEB_EMBC4_16L_PS': 1, 'FEB_EMBC4_13R_F5': 3, 'FEB_EMBC4_13R_M3': 1, 'FEB_EMBC3_12R_PS': 1, 'FEB_EMBC3_12R_M1': 1, 'FEB_EMBC3_10L_PS': 2, 'FEB_EMBC2_05R_B0': 5, 'FEB_EMBA1_03R_F6': 3, 'FEB_EMBA1_04R_M3': 1, 'FEB_EMBA2_05R_M2': 1, 'FEB_EMBA2_05L_PS': 1, 'FEB_EMBA3_09L_F4': 3, 'FEB_EMBA3_10L_F3': 2, 'FEB_EMBA3_10L_M1': 1, 'FEB_EMBA3_12R_F1': 3, 'FEB_EMBA4_13L_M0': 1, 'FEB_EMBA1_01R_M2': 1, 'FEB_EMBA4_13R_F0': 1, 'FEB_EMBA4_13R_M2': 1, 'FEB_EMECC2_10R_M0': 1}
