Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1103

== Noisy cells update for run 451046 (Sat, 6 May 2023 11:16:43 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (x737_h418)
Cluster matching: based on Et > 4/10GeV plots requiring at least 500 events
Flagged cells:237
Flagged in PS:39
Unflagged by DQ shifters:81
Changed to SBN:5
Changed to HNHG:0
SBN:143
SBN in PS:6
HNHG:94
HNHG in PS:33

*****************
The treated cells were:
0 0 7 14 50 0 sporadicBurstNoise  # 0x383eb200 -> 0x2d406882
0 0 12 11 75 0 sporadicBurstNoise  # 0x38654b00 -> 0x2d400430
0 0 13 11 4 0 deadReadout highNoiseHG  # 0x386d0400 -> 0x2d40022e
0 0 14 10 40 0 sporadicBurstNoise  # 0x3874a800 -> 0x2d60341e
0 0 14 10 41 0 highNoiseHG  # 0x3874a900 -> 0x2d60341c
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 17 10 20 0 sporadicBurstNoise  # 0x388c9400 -> 0x2d602bee
0 0 17 10 21 0 highNoiseHG  # 0x388c9500 -> 0x2d602bec
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 30 1 13 0 sporadicBurstNoise  # 0x38f00d00 -> 0x2d000a44
0 1 3 8 27 0 sporadicBurstNoise  # 0x391b9b00 -> 0x2da3360c
0 1 3 8 28 0 sporadicBurstNoise  # 0x391b9c00 -> 0x2da3380c
0 1 3 8 29 0 sporadicBurstNoise  # 0x391b9d00 -> 0x2da33a0c
0 1 21 5 101 0 sporadicBurstNoise  # 0x39aa6500 -> 0x2da1ca56
0 1 21 5 102 0 sporadicBurstNoise  # 0x39aa6600 -> 0x2da1cc56
1 0 1 1 1 0 highNoiseHG  # 0x3a080100 -> 0x2c800a36
1 0 1 1 3 0 highNoiseHG  # 0x3a080300 -> 0x2c800e36
1 0 1 1 17 0 highNoiseHG  # 0x3a081100 -> 0x2c800a32
1 0 1 1 19 0 highNoiseHG  # 0x3a081300 -> 0x2c800e32
1 0 1 1 25 0 highNoiseHG  # 0x3a081900 -> 0x2c801232
1 0 1 1 27 0 highNoiseHG  # 0x3a081b00 -> 0x2c801632
1 0 2 2 64 0 highNoiseHG  # 0x3a10c000 -> 0x2ca0002e
1 0 3 2 88 0 highNoiseHG  # 0x3a18d800 -> 0x2c400c2e
1 0 5 1 1 0 highNoiseHG  # 0x3a280100 -> 0x2c800a26
1 0 5 1 3 0 highNoiseHG  # 0x3a280300 -> 0x2c800e26
1 0 5 1 5 0 highNoiseHG  # 0x3a280500 -> 0x2c800a24
1 0 5 1 7 0 unflaggedByLADIeS  # 0x3a280700 -> 0x2c800e24
1 0 5 1 9 0 highNoiseHG  # 0x3a280900 -> 0x2c801226
1 0 5 1 11 0 highNoiseHG  # 0x3a280b00 -> 0x2c801626
1 0 5 1 13 0 highNoiseHG  # 0x3a280d00 -> 0x2c801224
1 0 5 1 15 0 highNoiseHG  # 0x3a280f00 -> 0x2c801624
1 0 5 1 17 0 highNoiseHG  # 0x3a281100 -> 0x2c800a22
1 0 5 1 19 0 highNoiseHG  # 0x3a281300 -> 0x2c800e22
1 0 5 1 21 0 highNoiseHG  # 0x3a281500 -> 0x2c800a20
1 0 5 1 23 0 highNoiseHG  # 0x3a281700 -> 0x2c800e20
1 0 5 1 25 0 highNoiseHG  # 0x3a281900 -> 0x2c801222
1 0 5 1 27 0 highNoiseHG  # 0x3a281b00 -> 0x2c801622
1 0 5 1 29 0 highNoiseHG  # 0x3a281d00 -> 0x2c801220
1 0 5 1 30 0 unflaggedByLADIeS  # 0x3a281e00 -> 0x2c801420
1 0 5 1 31 0 unflaggedByLADIeS  # 0x3a281f00 -> 0x2c801620
1 0 5 7 103 0 sporadicBurstNoise  # 0x3a2b6700 -> 0x2cb06e22
1 0 6 1 1 0 highNoiseHG  # 0x3a300100 -> 0x341ce000
1 0 6 1 29 0 highNoiseHG  # 0x3a301d00 -> 0x3424e000
1 0 6 1 31 0 highNoiseHG  # 0x3a301f00 -> 0x3420e000
1 0 6 1 64 0 highNoiseHG  # 0x3a304000 -> 0x341ec000
1 0 6 1 65 0 lowNoiseHG highNoiseHG  # 0x3a304100 -> 0x341cc000
1 0 6 1 66 0 highNoiseHG  # 0x3a304200 -> 0x341ac000
1 0 6 1 67 0 highNoiseHG  # 0x3a304300 -> 0x3418c000
1 0 6 1 69 0 highNoiseHG  # 0x3a304500 -> 0x3414c000
1 0 6 1 71 0 highNoiseHG  # 0x3a304700 -> 0x3410c000
1 0 6 1 72 0 highNoiseHG  # 0x3a304800 -> 0x340ec000
1 0 6 1 73 0 highNoiseHG  # 0x3a304900 -> 0x340cc000
1 0 6 1 74 0 highNoiseHG  # 0x3a304a00 -> 0x340ac000
1 0 6 1 75 0 highNoiseHG  # 0x3a304b00 -> 0x3408c000
1 0 6 1 76 0 highNoiseHG  # 0x3a304c00 -> 0x3406c000
1 0 6 1 77 0 highNoiseHG  # 0x3a304d00 -> 0x3404c000
1 0 6 1 78 0 highNoiseHG  # 0x3a304e00 -> 0x3402c000
1 0 6 1 79 0 highNoiseHG  # 0x3a304f00 -> 0x3400c000
1 0 6 1 81 0 highNoiseHG  # 0x3a305100 -> 0x343cc000
1 0 6 1 83 0 highNoiseHG  # 0x3a305300 -> 0x3438c000
1 0 6 1 85 0 highNoiseHG  # 0x3a305500 -> 0x3434c000
1 0 6 1 87 0 highNoiseHG  # 0x3a305700 -> 0x3430c000
1 0 6 1 89 0 highNoiseHG  # 0x3a305900 -> 0x342cc000
1 0 6 1 91 0 highNoiseHG  # 0x3a305b00 -> 0x3428c000
1 0 6 1 93 0 highNoiseHG  # 0x3a305d00 -> 0x3424c000
1 0 6 1 95 0 highNoiseHG  # 0x3a305f00 -> 0x3420c000
1 0 6 1 103 0 highNoiseHG  # 0x3a306700 -> 0x3450c000
1 0 6 1 105 0 highNoiseHG  # 0x3a306900 -> 0x344cc000
1 0 6 1 107 0 highNoiseHG  # 0x3a306b00 -> 0x3448c000
1 0 6 1 109 0 highNoiseHG  # 0x3a306d00 -> 0x3444c000
1 0 6 1 110 0 highNoiseHG  # 0x3a306e00 -> 0x3442c000
1 0 6 1 111 0 highNoiseHG  # 0x3a306f00 -> 0x3440c000
1 0 6 12 53 0 distorted highNoiseHG  # 0x3a35b500 -> 0x34b5e000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 17 8 124 0 unflaggedByLADIeS  # 0x3a8bfc00 -> 0x2ce013a6
1 0 17 8 126 0 unflaggedByLADIeS  # 0x3a8bfe00 -> 0x2ce013a2
1 0 17 9 107 0 unflaggedByLADIeS  # 0x3a8c6b00 -> 0x2ce021b0
1 0 17 9 110 0 unflaggedByLADIeS  # 0x3a8c6e00 -> 0x2ce023b2
1 0 17 9 111 0 unflaggedByLADIeS  # 0x3a8c6f00 -> 0x2ce023b0
1 0 17 9 112 0 unflaggedByLADIeS  # 0x3a8c7000 -> 0x2ce021ae
1 0 17 9 113 0 unflaggedByLADIeS  # 0x3a8c7100 -> 0x2ce021ac
1 0 17 9 114 0 unflaggedByLADIeS  # 0x3a8c7200 -> 0x2ce021aa
1 0 17 9 115 0 unflaggedByLADIeS  # 0x3a8c7300 -> 0x2ce021a8
1 0 17 9 116 0 unflaggedByLADIeS  # 0x3a8c7400 -> 0x2ce023ae
1 0 17 9 117 0 unflaggedByLADIeS  # 0x3a8c7500 -> 0x2ce023ac
1 0 17 9 118 0 unflaggedByLADIeS  # 0x3a8c7600 -> 0x2ce023aa
1 0 17 9 119 0 unflaggedByLADIeS  # 0x3a8c7700 -> 0x2ce023a8
1 0 17 9 120 0 unflaggedByLADIeS  # 0x3a8c7800 -> 0x2ce021a6
1 0 17 9 121 0 sporadicBurstNoise  # 0x3a8c7900 -> 0x2ce021a4
1 0 17 9 122 0 unflaggedByLADIeS  # 0x3a8c7a00 -> 0x2ce021a2
1 0 17 9 123 0 sporadicBurstNoise  # 0x3a8c7b00 -> 0x2ce021a0
1 0 17 9 124 0 unflaggedByLADIeS  # 0x3a8c7c00 -> 0x2ce023a6
1 0 17 9 125 0 unflaggedByLADIeS  # 0x3a8c7d00 -> 0x2ce023a4
1 0 17 9 126 0 unflaggedByLADIeS  # 0x3a8c7e00 -> 0x2ce023a2
1 0 17 10 107 0 sporadicBurstNoise  # 0x3a8ceb00 -> 0x2cc41ba8
1 0 17 10 110 0 sporadicBurstNoise  # 0x3a8cee00 -> 0x2cc41daa
1 0 17 10 111 0 sporadicBurstNoise  # 0x3a8cef00 -> 0x2cc41da8
1 0 17 10 116 0 sporadicBurstNoise  # 0x3a8cf400 -> 0x2cc419a6
1 0 17 10 117 0 sporadicBurstNoise  # 0x3a8cf500 -> 0x2cc419a4
1 0 17 10 118 0 sporadicBurstNoise  # 0x3a8cf600 -> 0x2cc419a2
1 0 17 10 120 0 sporadicBurstNoise  # 0x3a8cf800 -> 0x2cc41ba6
1 0 17 10 121 0 sporadicBurstNoise  # 0x3a8cf900 -> 0x2cc41ba4
1 0 17 10 122 0 sporadicBurstNoise  # 0x3a8cfa00 -> 0x2cc41ba2
1 0 17 10 123 0 unflaggedByLADIeS  # 0x3a8cfb00 -> 0x2cc41ba0
1 0 17 10 124 0 sporadicBurstNoise  # 0x3a8cfc00 -> 0x2cc41da6
1 0 17 10 125 0 unflaggedByLADIeS  # 0x3a8cfd00 -> 0x2cc41da4
1 0 17 10 126 0 sporadicBurstNoise  # 0x3a8cfe00 -> 0x2cc41da2
1 0 17 11 82 0 unflaggedByLADIeS  # 0x3a8d5200 -> 0x2cc427b2
1 0 17 11 83 0 unflaggedByLADIeS  # 0x3a8d5300 -> 0x2cc427b0
1 0 17 11 86 0 unflaggedByLADIeS  # 0x3a8d5600 -> 0x2cc429b2
1 0 17 11 87 0 sporadicBurstNoise  # 0x3a8d5700 -> 0x2cc429b0
1 0 17 11 90 0 unflaggedByLADIeS  # 0x3a8d5a00 -> 0x2cc42bb2
1 0 17 11 91 0 sporadicBurstNoise  # 0x3a8d5b00 -> 0x2cc42bb0
1 0 17 11 94 0 unflaggedByLADIeS  # 0x3a8d5e00 -> 0x2cc42db2
1 0 17 11 95 0 sporadicBurstNoise  # 0x3a8d5f00 -> 0x2cc42db0
1 0 17 11 96 0 unflaggedByLADIeS  # 0x3a8d6000 -> 0x2cc427ae
1 0 17 11 97 0 sporadicBurstNoise  # 0x3a8d6100 -> 0x2cc427ac
1 0 17 11 98 0 unflaggedByLADIeS  # 0x3a8d6200 -> 0x2cc427aa
1 0 17 11 99 0 sporadicBurstNoise  # 0x3a8d6300 -> 0x2cc427a8
1 0 17 11 100 0 sporadicBurstNoise  # 0x3a8d6400 -> 0x2cc429ae
1 0 17 11 101 0 sporadicBurstNoise  # 0x3a8d6500 -> 0x2cc429ac
1 0 17 11 102 0 sporadicBurstNoise  # 0x3a8d6600 -> 0x2cc429aa
1 0 17 11 103 0 sporadicBurstNoise  # 0x3a8d6700 -> 0x2cc429a8
1 0 17 11 104 0 sporadicBurstNoise  # 0x3a8d6800 -> 0x2cc42bae
1 0 17 11 105 0 sporadicBurstNoise  # 0x3a8d6900 -> 0x2cc42bac
1 0 17 11 106 0 sporadicBurstNoise  # 0x3a8d6a00 -> 0x2cc42baa
1 0 17 11 107 0 sporadicBurstNoise  # 0x3a8d6b00 -> 0x2cc42ba8
1 0 17 11 108 0 sporadicBurstNoise  # 0x3a8d6c00 -> 0x2cc42dae
1 0 17 11 109 0 sporadicBurstNoise  # 0x3a8d6d00 -> 0x2cc42dac
1 0 17 11 110 0 sporadicBurstNoise  # 0x3a8d6e00 -> 0x2cc42daa
1 0 17 11 111 0 sporadicBurstNoise  # 0x3a8d6f00 -> 0x2cc42da8
1 0 17 11 112 0 sporadicBurstNoise  # 0x3a8d7000 -> 0x2cc427a6
1 0 17 11 113 0 sporadicBurstNoise  # 0x3a8d7100 -> 0x2cc427a4
1 0 17 11 114 0 sporadicBurstNoise  # 0x3a8d7200 -> 0x2cc427a2
1 0 17 11 115 0 sporadicBurstNoise  # 0x3a8d7300 -> 0x2cc427a0
1 0 17 11 116 0 sporadicBurstNoise  # 0x3a8d7400 -> 0x2cc429a6
1 0 17 11 117 0 sporadicBurstNoise  # 0x3a8d7500 -> 0x2cc429a4
1 0 17 11 118 0 sporadicBurstNoise  # 0x3a8d7600 -> 0x2cc429a2
1 0 17 11 119 0 sporadicBurstNoise  # 0x3a8d7700 -> 0x2cc429a0
1 0 17 11 120 0 sporadicBurstNoise  # 0x3a8d7800 -> 0x2cc42ba6
1 0 17 11 121 0 unflaggedByLADIeS  # 0x3a8d7900 -> 0x2cc42ba4
1 0 17 11 122 0 sporadicBurstNoise  # 0x3a8d7a00 -> 0x2cc42ba2
1 0 17 11 123 0 unflaggedByLADIeS  # 0x3a8d7b00 -> 0x2cc42ba0
1 0 17 11 124 0 sporadicBurstNoise  # 0x3a8d7c00 -> 0x2cc42da6
1 0 17 11 125 0 unflaggedByLADIeS  # 0x3a8d7d00 -> 0x2cc42da4
1 0 17 11 126 0 sporadicBurstNoise  # 0x3a8d7e00 -> 0x2cc42da2
1 0 17 11 127 0 unflaggedByLADIeS  # 0x3a8d7f00 -> 0x2cc42da0
1 0 17 12 95 0 sporadicBurstNoise  # 0x3a8ddf00 -> 0x2cc43db0
1 0 17 12 97 0 sporadicBurstNoise  # 0x3a8de100 -> 0x2cc437ac
1 0 17 12 98 0 sporadicBurstNoise  # 0x3a8de200 -> 0x2cc437aa
1 0 17 12 99 0 sporadicBurstNoise  # 0x3a8de300 -> 0x2cc437a8
1 0 17 12 101 0 sporadicBurstNoise  # 0x3a8de500 -> 0x2cc439ac
1 0 17 12 102 0 sporadicBurstNoise  # 0x3a8de600 -> 0x2cc439aa
1 0 17 12 103 0 sporadicBurstNoise  # 0x3a8de700 -> 0x2cc439a8
1 0 17 12 105 0 sporadicBurstNoise  # 0x3a8de900 -> 0x2cc43bac
1 0 17 12 106 0 sporadicBurstNoise  # 0x3a8dea00 -> 0x2cc43baa
1 0 17 12 107 0 sporadicBurstNoise  # 0x3a8deb00 -> 0x2cc43ba8
1 0 17 12 108 0 sporadicBurstNoise  # 0x3a8dec00 -> 0x2cc43dae
1 0 17 12 109 0 sporadicBurstNoise  # 0x3a8ded00 -> 0x2cc43dac
1 0 17 12 110 0 sporadicBurstNoise  # 0x3a8dee00 -> 0x2cc43daa
1 0 17 12 111 0 sporadicBurstNoise  # 0x3a8def00 -> 0x2cc43da8
1 0 17 12 112 0 sporadicBurstNoise  # 0x3a8df000 -> 0x2cc437a6
1 0 17 12 113 0 sporadicBurstNoise  # 0x3a8df100 -> 0x2cc437a4
1 0 17 12 114 0 sporadicBurstNoise  # 0x3a8df200 -> 0x2cc437a2
1 0 17 12 115 0 sporadicBurstNoise  # 0x3a8df300 -> 0x2cc437a0
1 0 17 12 116 0 sporadicBurstNoise  # 0x3a8df400 -> 0x2cc439a6
1 0 17 12 117 0 sporadicBurstNoise  # 0x3a8df500 -> 0x2cc439a4
1 0 17 12 118 0 sporadicBurstNoise  # 0x3a8df600 -> 0x2cc439a2
1 0 17 12 119 0 sporadicBurstNoise  # 0x3a8df700 -> 0x2cc439a0
1 0 17 12 120 0 sporadicBurstNoise  # 0x3a8df800 -> 0x2cc43ba6
1 0 17 12 121 0 sporadicBurstNoise  # 0x3a8df900 -> 0x2cc43ba4
1 0 17 12 122 0 sporadicBurstNoise  # 0x3a8dfa00 -> 0x2cc43ba2
1 0 17 12 123 0 sporadicBurstNoise  # 0x3a8dfb00 -> 0x2cc43ba0
1 0 17 12 124 0 sporadicBurstNoise  # 0x3a8dfc00 -> 0x2cc43da6
1 0 17 12 125 0 sporadicBurstNoise  # 0x3a8dfd00 -> 0x2cc43da4
1 0 17 12 126 0 sporadicBurstNoise  # 0x3a8dfe00 -> 0x2cc43da2
1 0 17 12 127 0 sporadicBurstNoise  # 0x3a8dff00 -> 0x2cc43da0
1 0 17 13 64 0 sporadicBurstNoise  # 0x3a8e4000 -> 0x2cc447be
1 0 17 13 82 0 unflaggedByLADIeS  # 0x3a8e5200 -> 0x2cc447b2
1 0 17 13 83 0 unflaggedByLADIeS  # 0x3a8e5300 -> 0x2cc447b0
1 0 17 13 86 0 sporadicBurstNoise  # 0x3a8e5600 -> 0x2cc449b2
1 0 17 13 87 0 sporadicBurstNoise  # 0x3a8e5700 -> 0x2cc449b0
1 0 17 13 90 0 sporadicBurstNoise  # 0x3a8e5a00 -> 0x2cc44bb2
1 0 17 13 91 0 sporadicBurstNoise  # 0x3a8e5b00 -> 0x2cc44bb0
1 0 17 13 94 0 sporadicBurstNoise  # 0x3a8e5e00 -> 0x2cc44db2
1 0 17 13 95 0 unflaggedByLADIeS  # 0x3a8e5f00 -> 0x2cc44db0
1 0 17 13 96 0 sporadicBurstNoise  # 0x3a8e6000 -> 0x2cc447ae
1 0 17 13 97 0 unflaggedByLADIeS  # 0x3a8e6100 -> 0x2cc447ac
1 0 17 13 98 0 sporadicBurstNoise  # 0x3a8e6200 -> 0x2cc447aa
1 0 17 13 99 0 sporadicBurstNoise  # 0x3a8e6300 -> 0x2cc447a8
1 0 17 13 100 0 sporadicBurstNoise  # 0x3a8e6400 -> 0x2cc449ae
1 0 17 13 101 0 sporadicBurstNoise  # 0x3a8e6500 -> 0x2cc449ac
1 0 17 13 102 0 sporadicBurstNoise  # 0x3a8e6600 -> 0x2cc449aa
1 0 17 13 103 0 sporadicBurstNoise  # 0x3a8e6700 -> 0x2cc449a8
1 0 17 13 104 0 sporadicBurstNoise  # 0x3a8e6800 -> 0x2cc44bae
1 0 17 13 105 0 sporadicBurstNoise  # 0x3a8e6900 -> 0x2cc44bac
1 0 17 13 106 0 sporadicBurstNoise  # 0x3a8e6a00 -> 0x2cc44baa
1 0 17 13 107 0 sporadicBurstNoise  # 0x3a8e6b00 -> 0x2cc44ba8
1 0 17 13 108 0 sporadicBurstNoise  # 0x3a8e6c00 -> 0x2cc44dae
1 0 17 13 109 0 sporadicBurstNoise  # 0x3a8e6d00 -> 0x2cc44dac
1 0 17 13 110 0 sporadicBurstNoise  # 0x3a8e6e00 -> 0x2cc44daa
1 0 17 13 111 0 sporadicBurstNoise  # 0x3a8e6f00 -> 0x2cc44da8
1 0 17 13 112 0 sporadicBurstNoise  # 0x3a8e7000 -> 0x2cc447a6
1 0 17 13 113 0 sporadicBurstNoise  # 0x3a8e7100 -> 0x2cc447a4
1 0 17 13 114 0 sporadicBurstNoise  # 0x3a8e7200 -> 0x2cc447a2
1 0 17 13 115 0 sporadicBurstNoise  # 0x3a8e7300 -> 0x2cc447a0
1 0 17 13 116 0 sporadicBurstNoise  # 0x3a8e7400 -> 0x2cc449a6
1 0 17 13 117 0 sporadicBurstNoise  # 0x3a8e7500 -> 0x2cc449a4
1 0 17 13 118 0 sporadicBurstNoise  # 0x3a8e7600 -> 0x2cc449a2
1 0 17 13 119 0 sporadicBurstNoise  # 0x3a8e7700 -> 0x2cc449a0
1 0 17 13 120 0 sporadicBurstNoise  # 0x3a8e7800 -> 0x2cc44ba6
1 0 17 13 121 0 sporadicBurstNoise  # 0x3a8e7900 -> 0x2cc44ba4
1 0 17 13 122 0 sporadicBurstNoise  # 0x3a8e7a00 -> 0x2cc44ba2
1 0 17 13 123 0 sporadicBurstNoise  # 0x3a8e7b00 -> 0x2cc44ba0
1 0 17 13 124 0 sporadicBurstNoise  # 0x3a8e7c00 -> 0x2cc44da6
1 0 17 13 125 0 sporadicBurstNoise  # 0x3a8e7d00 -> 0x2cc44da4
1 0 17 13 126 0 lowNoiseHG sporadicBurstNoise  # 0x3a8e7e00 -> 0x2cc44da2
1 0 17 13 127 0 sporadicBurstNoise  # 0x3a8e7f00 -> 0x2cc44da0
1 0 18 10 127 0 sporadicBurstNoise  # 0x3a94ff00 -> 0x2cc41d80
1 0 18 11 64 0 unflaggedByLADIeS  # 0x3a954000 -> 0x2cc4279e
1 0 18 11 126 0 sporadicBurstNoise  # 0x3a957e00 -> 0x2cc42d82
1 0 18 11 127 0 sporadicBurstNoise  # 0x3a957f00 -> 0x2cc42d80
1 0 18 12 64 0 unflaggedByLADIeS  # 0x3a95c000 -> 0x2cc4379e
1 0 18 12 66 0 unflaggedByLADIeS  # 0x3a95c200 -> 0x2cc4379a
1 0 18 13 64 0 unflaggedByLADIeS  # 0x3a964000 -> 0x2cc4479e
1 0 19 13 64 0 highNoiseHG  # 0x3a9e4000 -> 0x2cc4477e
1 0 20 1 0 0 unflaggedByLADIeS  # 0x3aa00000 -> 0x2c800856
1 0 20 1 1 0 unflaggedByLADIeS  # 0x3aa00100 -> 0x2c800a56
1 0 20 1 2 0 unflaggedByLADIeS  # 0x3aa00200 -> 0x2c800c56
1 0 20 1 3 0 unflaggedByLADIeS  # 0x3aa00300 -> 0x2c800e56
1 0 20 1 4 0 highNoiseHG  # 0x3aa00400 -> 0x2c800854
1 0 20 1 5 0 unflaggedByLADIeS  # 0x3aa00500 -> 0x2c800a54
1 0 20 1 6 0 highNoiseHG  # 0x3aa00600 -> 0x2c800c54
1 0 20 1 7 0 highNoiseHG  # 0x3aa00700 -> 0x2c800e54
1 0 20 1 9 0 unflaggedByLADIeS  # 0x3aa00900 -> 0x2c801256
1 0 20 1 10 0 unflaggedByLADIeS  # 0x3aa00a00 -> 0x2c801456
1 0 20 1 11 0 highNoiseHG  # 0x3aa00b00 -> 0x2c801656
1 0 20 1 12 0 highNoiseHG  # 0x3aa00c00 -> 0x2c801054
1 0 20 1 13 0 highNoiseHG  # 0x3aa00d00 -> 0x2c801254
1 0 20 1 14 0 highNoiseHG  # 0x3aa00e00 -> 0x2c801454
1 0 20 1 15 0 highNoiseHG  # 0x3aa00f00 -> 0x2c801654
1 0 20 1 16 0 unflaggedByLADIeS  # 0x3aa01000 -> 0x2c800852
1 0 20 1 17 0 sporadicBurstNoise  # 0x3aa01100 -> 0x2c800a52
1 0 20 1 18 0 unflaggedByLADIeS  # 0x3aa01200 -> 0x2c800c52
1 0 20 1 19 0 sporadicBurstNoise  # 0x3aa01300 -> 0x2c800e52
1 0 20 1 21 0 unflaggedByLADIeS  # 0x3aa01500 -> 0x2c800a50
1 0 20 1 23 0 unflaggedByLADIeS  # 0x3aa01700 -> 0x2c800e50
1 0 20 1 24 0 unflaggedByLADIeS  # 0x3aa01800 -> 0x2c801052
1 0 20 1 25 0 sporadicBurstNoise  # 0x3aa01900 -> 0x2c801252
1 0 20 1 26 0 unflaggedByLADIeS  # 0x3aa01a00 -> 0x2c801452
1 0 20 1 27 0 sporadicBurstNoise  # 0x3aa01b00 -> 0x2c801652
1 0 20 1 29 0 unflaggedByLADIeS  # 0x3aa01d00 -> 0x2c801250
1 0 20 1 31 0 sporadicBurstNoise  # 0x3aa01f00 -> 0x2c801650
1 0 20 10 19 0 unflaggedByLADIeS  # 0x3aa49300 -> 0x2cc40f50
1 0 20 10 44 0 unflaggedByLADIeS  # 0x3aa4ac00 -> 0x2cc4154e
1 0 20 10 96 0 sporadicBurstNoise  # 0x3aa4e000 -> 0x2cc4174e
1 0 20 10 100 0 sporadicBurstNoise  # 0x3aa4e400 -> 0x2cc4194e
1 0 20 10 104 0 sporadicBurstNoise  # 0x3aa4e800 -> 0x2cc41b4e
1 0 20 10 108 0 sporadicBurstNoise  # 0x3aa4ec00 -> 0x2cc41d4e
1 0 20 11 100 0 unflaggedByLADIeS  # 0x3aa56400 -> 0x2cc4294e
1 0 21 1 16 0 highNoiseHG  # 0x3aa81000 -> 0x2c800056
1 0 21 1 19 0 unflaggedByLADIeS  # 0x3aa81300 -> 0x2c800656
1 0 21 1 20 0 highNoiseHG  # 0x3aa81400 -> 0x2c800054
1 0 21 1 21 0 unflaggedByLADIeS  # 0x3aa81500 -> 0x2c800254
1 0 21 1 22 0 highNoiseHG  # 0x3aa81600 -> 0x2c800454
1 0 21 1 23 0 highNoiseHG  # 0x3aa81700 -> 0x2c800654
1 0 21 1 24 0 unflaggedByLADIeS  # 0x3aa81800 -> 0x2c800052
1 0 21 1 25 0 unflaggedByLADIeS  # 0x3aa81900 -> 0x2c800252
1 0 21 1 26 0 unflaggedByLADIeS  # 0x3aa81a00 -> 0x2c800452
1 0 21 1 27 0 unflaggedByLADIeS  # 0x3aa81b00 -> 0x2c800652
1 0 21 1 28 0 unflaggedByLADIeS  # 0x3aa81c00 -> 0x2c800050
1 0 21 2 0 0 highNoiseHG  # 0x3aa88000 -> 0x2ca0005e
1 0 21 2 4 0 highNoiseHG  # 0x3aa88400 -> 0x2ca0005c
1 0 21 2 8 0 sporadicBurstNoise  # 0x3aa88800 -> 0x2ca0005a
1 0 21 2 12 0 sporadicBurstNoise  # 0x3aa88c00 -> 0x2ca00058
1 0 21 2 16 0 highNoiseHG  # 0x3aa89000 -> 0x2ca00056
1 0 21 2 20 0 sporadicBurstNoise  # 0x3aa89400 -> 0x2ca00054
1 0 21 2 24 0 sporadicBurstNoise  # 0x3aa89800 -> 0x2ca00052
1 0 21 2 25 0 unflaggedByLADIeS  # 0x3aa89900 -> 0x2ca40052
1 0 21 2 26 0 unflaggedByLADIeS  # 0x3aa89a00 -> 0x2ca40252
1 0 21 2 28 0 highNoiseHG  # 0x3aa89c00 -> 0x2ca00050
1 0 21 2 64 0 unflaggedByLADIeS  # 0x3aa8c000 -> 0x2ca0004e
1 0 21 2 68 0 highNoiseHG  # 0x3aa8c400 -> 0x2ca0004c
1 0 21 2 92 0 highNoiseHG  # 0x3aa8dc00 -> 0x2ca00040
1 0 21 3 51 0 unflaggedByLADIeS  # 0x3aa93300 -> 0x2cc00160
1 0 21 3 55 0 unflaggedByLADIeS  # 0x3aa93700 -> 0x2cc40160
1 0 21 3 59 0 unflaggedByLADIeS  # 0x3aa93b00 -> 0x2cc40360
1 0 21 3 63 0 unflaggedByLADIeS  # 0x3aa93f00 -> 0x2cc40560
1 0 21 3 66 0 sporadicBurstNoise unflaggedByLADIeS  # 0x3aa94200 -> 0x2cc0015a
1 0 21 3 83 0 unflaggedByLADIeS  # 0x3aa95300 -> 0x2cc00150
1 0 21 3 87 0 unflaggedByLADIeS  # 0x3aa95700 -> 0x2cc40150
1 0 21 3 91 0 sporadicBurstNoise  # 0x3aa95b00 -> 0x2cc40350
1 0 21 3 95 0 sporadicBurstNoise  # 0x3aa95f00 -> 0x2cc40550
1 0 21 3 101 0 unflaggedByLADIeS  # 0x3aa96500 -> 0x2cc4014c
1 0 21 3 123 0 unflaggedByLADIeS  # 0x3aa97b00 -> 0x2cc40340
1 0 21 3 127 0 unflaggedByLADIeS  # 0x3aa97f00 -> 0x2cc40540
1 0 22 10 40 0 highNoiseHG  # 0x3ab4a800 -> 0x30453000
1 0 23 1 1 0 unflaggedByLADIeS  # 0x3ab80100 -> 0x2c800a4e
1 0 23 1 3 0 unflaggedByLADIeS  # 0x3ab80300 -> 0x2c800e4e
1 0 23 1 11 0 highNoiseHG  # 0x3ab80b00 -> 0x2c80164e
1 1 6 14 62 0 highNoiseHG  # 0x3b36be00 -> 0x37020000
1 1 6 14 63 0 highNoiseHG  # 0x3b36bf00 -> 0x37000000
1 1 6 14 124 0 distorted highNoiseHG  # 0x3b36fc00 -> 0x37068000
1 1 6 14 126 0 highNoiseHG  # 0x3b36fe00 -> 0x37028000
1 1 6 15 60 0 lowNoiseHG highNoiseHG  # 0x3b373c00 -> 0x37070000
1 1 6 15 61 0 distorted highNoiseHG  # 0x3b373d00 -> 0x37050000
1 1 6 15 62 0 highNoiseHG  # 0x3b373e00 -> 0x37030000
1 1 6 15 63 0 highNoiseHG  # 0x3b373f00 -> 0x37010000
1 1 6 15 64 0 highNoiseHG  # 0x3b374000 -> 0x371fe000
1 1 6 15 72 0 lowNoiseHG highNoiseHG  # 0x3b374800 -> 0x370fe000
1 1 6 15 76 0 lowNoiseHG highNoiseHG  # 0x3b374c00 -> 0x3707e000
1 1 6 15 124 0 highNoiseHG  # 0x3b377c00 -> 0x37078000
1 1 6 15 126 0 highNoiseHG  # 0x3b377e00 -> 0x37038000
1 1 6 15 127 0 highNoiseHG  # 0x3b377f00 -> 0x37018000
1 1 9 5 81 0 deadCalib sporadicBurstNoise  # 0x3b4a5100 -> 0x2e282224
1 1 9 5 82 0 sporadicBurstNoise  # 0x3b4a5200 -> 0x2e282424

