Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1114

== Event Veto for run 451557 (Sun, 14 May 2023 11:32:37 +0200) ==
Found Noise or data corruption in run 451557
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451557_Main.db

Found project tag data23_13p6TeV for run 451557
Found 1 Veto Ranges with 2 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451557_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451557
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451557_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 0.50 seconds
Lumi loss due to corruption: 4.20 nb-1 out of 88087.41 nb-1 (0.05 per-mil)
Overall Lumi loss is 4.20287225042145 by 0.500069215 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1114

== Event Veto for run 451557 (Sun, 14 May 2023 11:01:02 +0200) ==
Found Noise or data corruption in run 451557
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451557_Main.db

Found project tag data23_13p6TeV for run 451557
Found 1 Veto Ranges with 2 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451557_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451557
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451557_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 0.50 seconds
Lumi loss due to corruption: 4.20 nb-1 out of 88087.41 nb-1 (0.05 per-mil)
Overall Lumi loss is 4.20287225042145 by 0.500069215 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1114

== Missing EVs for run 451557 (Sat, 6 May 2023 12:16:55 +0200) ==

Found a total of 100 noisy periods, covering a total of 0.11 seconds
Found a total of 614 Mini noise periods, covering a total of 0.61 seconds
Lumi loss due to noise-bursts: 0.83 nb-1 out of 88087.41 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 4.72 nb-1 out of 88087.41 nb-1 (0.05 per-mil)
Overall Lumi loss is 5.554368287513185 by 0.72517102 s of veto length


