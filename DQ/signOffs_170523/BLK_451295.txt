Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1109

== Event Veto for run 451295 (Sat, 13 May 2023 09:26:38 +0200) ==
Found Noise or data corruption in run 451295
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451295_Main.db

Found project tag data23_13p6TeV for run 451295
Found 2 Veto Ranges with 4 events
Found 11 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451295_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451295
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451295_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 8.25 nb-1 out of 204805.21 nb-1 (0.04 per-mil)
Overall Lumi loss is 8.252155484030853 by 1.000725615 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1109

== Event Veto for run 451295 (Sat, 13 May 2023 09:03:10 +0200) ==
Found Noise or data corruption in run 451295
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451295_Main.db

Found project tag data23_13p6TeV for run 451295
Found 2 Veto Ranges with 4 events
Found 11 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451295_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451295
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451295_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 8.25 nb-1 out of 204805.21 nb-1 (0.04 per-mil)
Overall Lumi loss is 8.252155484030853 by 1.000725615 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1109

== Missing EVs for run 451295 (Thu, 4 May 2023 02:20:43 +0200) ==

Found a total of 344 noisy periods, covering a total of 0.35 seconds
Found a total of 1678 Mini noise periods, covering a total of 1.68 seconds
Lumi loss due to noise-bursts: 2.13 nb-1 out of 204805.21 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 12.84 nb-1 out of 204805.21 nb-1 (0.06 per-mil)
Overall Lumi loss is 14.968748906983846 by 2.03550668 s of veto length


