Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1115

== Event Veto for run 451569 (Mon, 15 May 2023 01:32:01 +0200) ==
Found Noise or data corruption in run 451569
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451569_Main.db

Found project tag data23_13p6TeV for run 451569
Found 1 Veto Ranges with 2 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451569_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451569
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451569_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 0.50 seconds
Lumi loss due to corruption: 4.08 nb-1 out of 131940.37 nb-1 (0.03 per-mil)
Overall Lumi loss is 4.083718412535705 by 0.500012125 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1115

== Missing EVs for run 451569 (Sat, 6 May 2023 21:19:58 +0200) ==

Found a total of 754 noisy periods, covering a total of 0.75 seconds
Found a total of 993 Mini noise periods, covering a total of 0.99 seconds
Lumi loss due to noise-bursts: 5.55 nb-1 out of 131940.37 nb-1 (0.04 per-mil)
Lumi loss due to mini-noise-bursts: 7.84 nb-1 out of 131940.37 nb-1 (0.06 per-mil)
Overall Lumi loss is 13.387627203394198 by 1.747754375 s of veto length


