Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1102

== Event Veto for run 451037 (Tue, 9 May 2023 23:42:38 +0200) ==
Found Noise or data corruption in run 451037
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451037_Main.db

Found project tag data23_13p6TeV for run 451037
Found 12 Veto Ranges with 39 events
Found 6 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451037_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451037
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451037_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 6.00 seconds
Lumi loss due to corruption: 17.43 nb-1 out of 76662.25 nb-1 (0.23 per-mil)
Overall Lumi loss is 17.426599154444563 by 6.003103135 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1102

== Noisy cells update for run 451037 (Sat, 13 May 2023 21:31:40 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1345_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 400 events
Flagged cells:109
Flagged in PS:5
Unflagged by DQ shifters:9
SBN:68
SBN in PS:1
HNHG:41
HNHG in PS:4

*****************
The treated cells were:
1 0 6 1 3 0 highNoiseHG reflaggedByLADIeS # 0x3a300300
1 0 6 1 4 0 highNoiseHG reflaggedByLADIeS # 0x3a300400
1 1 6 14 63 0 highNoiseHG reflaggedByLADIeS # 0x3b36bf00
1 0 6 1 65 0 highNoiseHG reflaggedByLADIeS # 0x3a304100
1 0 6 1 3 0 highNoiseHG reflaggedByLADIeS # 0x3a300300
1 0 17 12 106 0 sporadicBurstNoise # 0x3a8dea00
1 0 17 12 95 0 sporadicBurstNoise # 0x3a8ddf00
1 0 17 13 116 0 highNoiseHG # 0x3a8e7400
1 0 17 12 107 0 highNoiseHG # 0x3a8deb00
1 0 17 13 102 0 highNoiseHG # 0x3a8e6600
1 0 17 13 106 0 highNoiseHG # 0x3a8e6a00
1 0 17 12 105 0 highNoiseHG # 0x3a8de900
1 0 17 13 107 0 highNoiseHG # 0x3a8e6b00
1 0 17 12 104 0 sporadicBurstNoise # 0x3a8de800
1 0 17 12 91 0 sporadicBurstNoise # 0x3a8ddb00
1 0 17 13 92 0 sporadicBurstNoise # 0x3a8e5c00
1 0 17 13 93 0 sporadicBurstNoise # 0x3a8e5d00
1 0 17 13 120 0 highNoiseHG # 0x3a8e7800
1 0 17 12 94 0 sporadicBurstNoise # 0x3a8dde00
1 0 17 12 120 0 highNoiseHG # 0x3a8df800
1 0 17 13 104 0 highNoiseHG # 0x3a8e6800
1 0 17 12 102 0 sporadicBurstNoise # 0x3a8de600
1 0 17 12 97 0 sporadicBurstNoise # 0x3a8de100
1 0 17 12 103 0 highNoiseHG # 0x3a8de700
1 0 17 12 93 0 sporadicBurstNoise # 0x3a8ddd00
1 0 17 12 100 0 sporadicBurstNoise # 0x3a8de400
1 0 17 11 106 0 highNoiseHG # 0x3a8d6a00
1 0 17 12 89 0 sporadicBurstNoise # 0x3a8dd900
1 0 17 11 105 0 highNoiseHG # 0x3a8d6900
1 0 17 12 96 0 sporadicBurstNoise # 0x3a8de000
1 0 17 12 121 0 highNoiseHG # 0x3a8df900
1 0 17 11 104 0 highNoiseHG # 0x3a8d6800
1 0 17 11 120 0 highNoiseHG # 0x3a8d7800
1 0 17 12 92 0 sporadicBurstNoise # 0x3a8ddc00
1 0 17 12 87 0 sporadicBurstNoise # 0x3a8dd700
1 0 17 12 90 0 sporadicBurstNoise # 0x3a8dda00
1 0 17 12 85 0 sporadicBurstNoise # 0x3a8dd500
1 0 17 11 91 0 highNoiseHG # 0x3a8d5b00
1 0 6 1 65 0 highNoiseHG # 0x3a304100
1 0 6 1 16 0 highNoiseHG # 0x3a301000
1 0 21 2 20 0 highNoiseHG # 0x3aa89400
1 0 21 2 24 0 highNoiseHG # 0x3aa89800
1 0 20 1 17 0 highNoiseHG # 0x3aa01100
1 0 21 2 28 0 highNoiseHG # 0x3aa89c00
1 0 17 12 101 0 highNoiseHG # 0x3a8de500
1 0 17 12 83 0 sporadicBurstNoise # 0x3a8dd300
1 0 17 12 88 0 sporadicBurstNoise # 0x3a8dd800
1 0 17 12 86 0 sporadicBurstNoise # 0x3a8dd600
1 0 17 10 117 0 sporadicBurstNoise # 0x3a8cf500
1 0 17 10 109 0 sporadicBurstNoise # 0x3a8ced00
1 0 17 10 118 0 sporadicBurstNoise # 0x3a8cf600
1 0 17 10 113 0 sporadicBurstNoise # 0x3a8cf100
1 0 17 10 119 0 sporadicBurstNoise # 0x3a8cf700
1 0 17 10 105 0 sporadicBurstNoise # 0x3a8ce900
1 0 17 10 112 0 sporadicBurstNoise # 0x3a8cf000
1 0 17 11 116 0 highNoiseHG # 0x3a8d7400
1 0 17 10 94 0 sporadicBurstNoise # 0x3a8cde00
1 0 17 10 115 0 sporadicBurstNoise # 0x3a8cf300
1 0 17 10 108 0 sporadicBurstNoise # 0x3a8cec00
1 0 17 10 101 0 sporadicBurstNoise # 0x3a8ce500
1 0 17 11 118 0 highNoiseHG # 0x3a8d7600
1 0 17 10 103 0 sporadicBurstNoise # 0x3a8ce700
1 0 17 11 117 0 highNoiseHG # 0x3a8d7500
1 0 17 10 95 0 sporadicBurstNoise # 0x3a8cdf00
1 0 17 10 114 0 sporadicBurstNoise # 0x3a8cf200
1 0 17 10 106 0 sporadicBurstNoise # 0x3a8cea00
1 0 17 10 120 0 highNoiseHG # 0x3a8cf800
1 0 17 10 99 0 sporadicBurstNoise # 0x3a8ce300
1 0 17 10 90 0 sporadicBurstNoise # 0x3a8cda00
1 0 17 10 102 0 sporadicBurstNoise # 0x3a8ce600
1 0 17 10 104 0 sporadicBurstNoise # 0x3a8ce800
1 0 17 10 97 0 sporadicBurstNoise # 0x3a8ce100
1 0 17 10 98 0 sporadicBurstNoise # 0x3a8ce200
1 0 17 10 91 0 sporadicBurstNoise # 0x3a8cdb00
1 0 17 10 83 0 sporadicBurstNoise # 0x3a8cd300
1 0 17 10 100 0 sporadicBurstNoise # 0x3a8ce400
1 0 17 10 96 0 sporadicBurstNoise # 0x3a8ce000
1 0 17 10 86 0 sporadicBurstNoise # 0x3a8cd600
1 0 17 10 116 0 highNoiseHG # 0x3a8cf400
1 0 17 10 87 0 sporadicBurstNoise # 0x3a8cd700
1 0 17 10 82 0 sporadicBurstNoise # 0x3a8cd200
1 0 17 11 101 0 highNoiseHG # 0x3a8d6500
1 0 17 11 93 0 sporadicBurstNoise # 0x3a8d5d00
1 0 17 11 89 0 sporadicBurstNoise # 0x3a8d5900
1 0 17 11 85 0 sporadicBurstNoise # 0x3a8d5500
1 0 17 11 92 0 sporadicBurstNoise # 0x3a8d5c00
1 0 17 11 81 0 sporadicBurstNoise # 0x3a8d5100
1 0 17 11 79 0 sporadicBurstNoise # 0x3a8d4f00
1 0 17 11 84 0 sporadicBurstNoise # 0x3a8d5400
1 0 17 11 88 0 sporadicBurstNoise # 0x3a8d5800
1 0 17 11 75 0 sporadicBurstNoise # 0x3a8d4b00
1 0 17 11 80 0 sporadicBurstNoise # 0x3a8d5000
1 0 20 1 7 0 highNoiseHG # 0x3aa00700
1 0 20 1 27 0 highNoiseHG # 0x3aa01b00
1 0 20 1 31 0 highNoiseHG # 0x3aa01f00
1 0 20 1 19 0 unflaggedByLADIeS # 0x3aa01300
1 0 20 1 25 0 unflaggedByLADIeS # 0x3aa01900
1 0 20 1 18 0 unflaggedByLADIeS # 0x3aa01200
1 0 20 1 29 0 unflaggedByLADIeS # 0x3aa01d00
1 0 20 1 26 0 unflaggedByLADIeS # 0x3aa01a00
1 0 20 1 15 0 unflaggedByLADIeS # 0x3aa00f00
1 0 21 1 20 0 unflaggedByLADIeS # 0x3aa81400
1 0 20 1 14 0 unflaggedByLADIeS # 0x3aa00e00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 16 0 sporadicBurstNoise # 0x3b619000
1 1 12 4 15 0 highNoiseHG # 0x3b618f00
1 0 20 11 100 0 unflaggedByLADIeS # 0x3aa56400
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
1 1 6 13 85 0 highNoiseHG # 0x3b365500
1 1 6 14 63 0 highNoiseHG # 0x3b36bf00
1 1 6 15 76 0 highNoiseHG # 0x3b374c00
1 0 1 3 127 0 sporadicBurstNoise # 0x3a097f00
1 1 9 14 62 0 sporadicBurstNoise # 0x3b4ebe00
1 1 9 13 27 0 sporadicBurstNoise # 0x3b4e1b00
1 1 9 14 55 0 sporadicBurstNoise # 0x3b4eb700
1 1 9 14 58 0 sporadicBurstNoise # 0x3b4eba00
1 0 18 11 125 0 sporadicBurstNoise # 0x3a957d00
1 0 21 1 1 0 sporadicBurstNoise # 0x3aa80100

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1102

== Missing EVs for run 451037 (Mon, 1 May 2023 06:16:35 +0200) ==

Found a total of 500 noisy periods, covering a total of 0.50 seconds
Found a total of 48 Mini noise periods, covering a total of 0.05 seconds
Lumi loss due to noise-bursts: 1.32 nb-1 out of 76662.25 nb-1 (0.02 per-mil)
Lumi loss due to mini-noise-bursts: 0.13 nb-1 out of 76662.25 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.4510976990154403 by 0.54815763 s of veto length


