Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1102

** Defect LAR_EMBA_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_EMBC_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_HECA_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_HECA_NOISEBURST(INTOLERABLE) **
Affecting 2 LBs: 988, 989 (0 recoverable) 
** Defect LAR_HECC_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 739, 740, 996, 997 (0 recoverable) 
** Defect LAR_EMECA_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST(INTOLERABLE) **
Affecting 2 LBs: 988, 989 (0 recoverable) 
** Defect LAR_EMECC_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 739, 740, 996, 997 (0 recoverable) 
** Defect LAR_FCALA_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST(INTOLERABLE) **
Affecting 2 LBs: 988, 989 (0 recoverable) 
** Defect LAR_FCALC_SEVNOISYCHANNEL **
Affecting 3 LBs: 781, 782, 783 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 739, 740, 996, 997 (0 recoverable) 
