#!/bin/python3
import os, sys
from jira import JIRA
tokenfile = "jiraToken"
with open(tokenfile, "r") as f:       
    token = f.readline().strip()

userMail = "ellis.kay@cern.ch"
#userMail = "ekay"
#print(userMail, token)

from email.mime.text import MIMEText
import smtplib

def sendMail(subject, contents):
    sFrom = os.environ["USER"]+" <"+os.environ["USER"]+"@cern.ch>"
    sTo = "atlas-lar-swdpdq-jira@cern.ch"
    # make the CC field with all of the shifters
    #if shiftermails is not None:
    #    sCC = ", ".join(shiftermails)
    #else:
    #    sCC = ""

    print("Writing mail from", sFrom, "to", sTo, "with", sCC, "in CC, subject is:", subject)
    mail = MIMEText(contents)
    mail['From'] = sFrom 
    mail['To'] = sTo # MUST HAVE THIS OR IT WON'T MAKE THE JIRA
    mail['Subject'] = subject
    #mail['CC'] = sCC # THE WATCHERS
    smtp = smtplib.SMTP()
    smtp.connect()
    smtp.sendmail(sFrom, sTo, mail.as_string())
    ##smtp.sendmail(sFrom, sTo, mail.as_string())
    smtp.close()


def findJira(runNb, jiraTool, verbose=False):
    project = 'ATLLARSWDPQ'
    issues = jiraTool.search_issues('project=' + project + ' AND summary~"DQ Assessment for run '+str(runNb)+'"', maxResults=False, fields = 'comment')
    if verbose: print(issues)    
    if len(issues) == 1:
        return issues[0]
    elif len(issues) > 1:
        if verbose: print("more than one issue found")
        return None
    else:
        if verbose: print("No issue found")
        return None

def getComments(issue):
    if issue is None: return None
    comments = issue.raw['fields']['comment']['comments']
    #for comm in comments:
    #    thiscomm = comm['body'].replace("=\r\n","")
    #    thiscomm = thiscomm.replace("=\n","")
    #    comments[comments.index(comm)]['body'] = thiscomm
    return comments


j=JIRA("https://its.cern.ch/jira", token_auth=(token))
runNb = 436195
project = 'ATLLARSWDPQ'
issues = findJira( runNb, j )



if not isinstance(issues,list):
    issues = [issues]

for issue in issues:
    comments = getComments(issue)
    print("*"*30)
    print(issue)
    for comment in comments:
        print("-"*20)
        print(comment.keys())
        print(comment['body'])
    # Could check if the comment already exists based on its content

    #contents = 'write your comment content here'
    #subject = issue.key
    #SendMail(subject, contents)
