#!/bin/env python
# Script by Clement
import time, calendar, sys
from PyCool import cool

#TAG = 'OflLumi-7TeV-002'
#TAG = 'OflLumi-UPD2-004'

__LBTIMESCache=dict()


def openDB(schema): 
    #fullconn="oracle://ATLAS_COOLPROD;schema=ATLAS_%s;dbname=COMP200" % schema
    fullconn="oracle://ATLAS_COOLPROD;schema=ATLAS_%s;dbname=CONDBR2" % schema
    dbSvc=cool.DatabaseSvcFactory.databaseService()
    db=dbSvc.openDatabase(fullconn)
    return db


def _readDataPerLB(folder,runnumber,channel,valueGetter):

    res=dict()
    #print "run",bin(runnumber)
    limmin=(runnumber << 32) +1 #ATLAS runs always start at LB 1
    #print "min",bin(limmin)
    limmax=((1+runnumber) << 32)-1 #First LB of next run -1
    #print "max",bin(limmax)

    objs=folder.browseObjects(limmin,limmax,cool.ChannelSelection(channel))
    while objs.goToNext():
        obj = objs.currentRef()
        payload = obj.payload()
        ts1=max(obj.since(),limmin)
        ts2=obj.until()
        lb1=ts1 & 0xFFFFFFFF
        lb2=ts2 & 0xFFFFFFFF
        value=valueGetter(payload)
        #print lb1, lb2, value 
        if (ts2<limmax): #Catch the last IOV of this run
            for lb in xrange(lb1,lb2):
                res[lb]=value
                pass
            pass
        else:
            res[lb1]=value #Last IOV of this run 
    objs.close()
    
    return res


#############################################################################
def GetBeamIntensities(runnumber,beam): 
    #Feb 2015: No use-case of this method found under ~larmon/public
    res = []
    #FIXME: Index in res is basically random (IOV sequence)
    attrStr='Beam'+str(beam)+'Intensity'
    (SOR,EOR,NLB) = GetRunStartStopTimes(runnumber)
    trigDB = openDB('COOLONL_TDAQ')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TDAQ\n")
        return None
    try:
        folder = trigDB.getFolder('/TDAQ/OLC/LHC/LBDATA')
        chansel = cool.ChannelSelection(0)
        objs = folder.browseObjects(SOR,EOR,chansel)
        while objs.goToNext():
            obj = objs.currentRef()
            payload = obj.payload()
            res.append(float(payload[attrStr]))
        objs.close()
    except Exception as e:
        sys.stderr.write("ERROR accessing /TDAQ/OLC/LHC/LBDATA\n%s\n" % str(e))
        res=None
    finally:
        trigDB.closeDatabase()

    return res


#############################################################################
def GetLBTimeStamps(runnumber):
    #This method is also used internally in this python module
    if not __LBTIMESCache.has_key(runnumber):
        trigDB=openDB('COOLONL_TRIGGER')
        if (trigDB is None):
            sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n")
            return None
        try:
            folder=trigDB.getFolder('/TRIGGER/LUMI/LBLB')
        except Exception as e:
            sys.stderr.write("Failed to access folder /TRIGGER/LUMI/LBLB in database COOLONL_TRIGGER\n%s\n" % str(e))
            trigDB.closeDatabase()
            return None

        __LBTIMESCache[runnumber] = _readDataPerLB(folder,runnumber,0,lambda payload: (int(payload['StartTime']/1000000000),int(payload['EndTime']/1000000000)))
    
        trigDB.closeDatabase()
        pass #end if not yet cached
    else:
        print( "LBTimes for run %i already cached" % runnumber )
    return __LBTIMESCache[runnumber]


#############################################################################
def GetBCIDstatus(runnumber):
    #This method is also used internally in this python module
    #if not __LBTIMESCache.has_key(runnumber):
    trigDB=openDB('COOLONL_TRIGGER')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n")
        return None
    try:
        folder=trigDB.getFolder('/TRIGGER/LVL1/BunchGroupContent')
    except Exception as e:
        sys.stderr.write("Failed to access folder /TRIGGER/LVL1/BunchGroupContent in database COOLONL_TRIGGER\n%s\n" % str(e))
        trigDB.closeDatabase()
        return None

    res = _readDataPerLB(folder,runnumber,0,lambda payload: bin(payload['BunchCode'][3515]))
    #res=payload['BunchCode']
    trigDB.closeDatabase()
    return res
    #     pass #end if not yet cached
    # else:
    #     print "LBTimes for run %i already cached" % runnumber
    # return __LBTIMESCache[runnumber]
    
#(blob[0] | (blob[1] << 8) | (blob[2]<<16))


def readBunchGroupKeys(run, debug = False):
     
     """ Read bunch group configuration keys from COOL """
 
     db = openDb()
 
     bgKeys = []    
     bgkey_fname='/TRIGGER/LVL1/BunchGroupKey'
     if not db.existsFolder(bgkey_fname) :
         print( "Folder",bgkey_fname,"not found" )
         db.closeDatabase()
         sys.exit(-1)
     folder=db.getFolder(bgkey_fname)
     try:
         itr=folder.browseObjects(run << 32,(run+1) << 32,cool.ChannelSelection.all())
         while itr.goToNext() :
             obj=itr.currentRef()
             payload=obj.payload()
             bgKeys.append(int(payload['Lvl1BunchGroupConfigurationKey']))
         itr.close()
     except Exception as e:
         print( "Reading data from",bgkey_fname,"failed:",e )
 
     if debug : print( "Run",run,"bg keys",bgKeys )
     return bgKeys

#############################################################################
def GetLBDuration(runnumber): 
    #This method is used by WeeklyReport.py
    trigDB=openDB('COOLONL_TRIGGER')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n")
        return None
    
    try:
        folder=trigDB.getFolder('/TRIGGER/LUMI/LBLB')
    except Exception as e:
        sys.stderr.write("Failed to access folder /TRIGGER/LUMI/LBLB in database COOLONL_TRIGGER\n%s\n" % str(e))
        trigDB.closeDatabase()
        return None

    res=  _readDataPerLB(folder,runnumber,0,lambda payload: int(payload['EndTime']/1000000000)-int(payload['StartTime']/1000000000))
    trigDB.closeDatabase()
    return res

####################################################################

def GetRunStartStopTimes(runnumber): 
    #used internally in this module
     #First, try to get start/stop time from /TDAQ/RunCtrl/EOR 
    tdaqDB=openDB('COOLONL_TDAQ')
    if (tdaqDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TDAQ\n")
        return None

    try:
        folder=tdaqDB.getFolder("/TDAQ/RunCtrl/EOR")
    except Exception as e:
        sys.stderr.write("ERROR Failed to access folder /TDAQ/RunCtrl/EOR in database COOLONL_TDAQ\n%s\n" % str(e))
        tdaqDB.closeDatabase()
        return None

    ts=(runnumber << 32) +1
    try:
        obj=folder.findObject(ts,cool.ChannelId(0)) 
    except Exception as e:
        sys.stderr.write("Failed to access data for run %i from folder /TDAQ/RunCtrl/EOR. DAQ crash?\n%s\n" % (runnumber,str(e)))
        obj=None
        pass

    
    if (obj is not None):
        payload=obj.payload()
        NLB=obj.until() & 0xFFFFFFFF
        SORTime=payload["SORTime"]
        EORTime=payload["EORTime"]
        tdaqDB.closeDatabase()
        return (SORTime,EORTime,NLB)
    else:
        #print "Backup solution: Try to access run start/stop from COOLONL_TRIGGER, /TRIGGER/LUMI/LBLB"
        EORTime=0
        SORTime=0xFFFFFFFFFFFFFFFF
        NLB=0
        LBTS=GetLBTimeStamps(runnumber)
        #print "LBTS=",LBTS
        #Search for the highes LB number and the smalles (SOR) and the largest (EOR) timestamps
        for (LB,TS) in LBTS.iteritems():
            if (LB>NLB): NLB=LB
            if TS[0]<SORTime: SORTime=TS[0]
            if TS[1]>EORTime: EORTime=TS[1]
            pass
        
        if (NLB==0):
            sys.stderr.write("ERROR, failed to extract run start/stop from COOLONL_TRIGGER, /TRIGGER/LUMI/LBLB\nGot:%s\n"% str(LBTS))
            return None

        else:
            return (SORTime*1000000000,EORTime*1000000000,NLB)
        pass
    pass

#############################################################################
def GetRunNumberLumiblock(since,until):
    #used by LADIeS/UPDTools.py
    trigDB=openDB('COOLONL_TRIGGER')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n")
        return None
    try:
        folder = trigDB.getFolder('/TRIGGER/LUMI/LBTIME')
        limmin = ConvertUTCtoIOV(since)*1000000000
        limmax = ConvertUTCtoIOV(until)*1000000000

        rLB1=None
        rLB2=None
        objs=folder.browseObjects(limmin,limmax,cool.ChannelSelection(0))
        while objs.goToNext():
            obj=objs.currentRef()
            payload=obj.payload()
            if (rLB1 is None): rLB1=(int(payload['Run']),int(payload['LumiBlock']))
            rLB2=(int(payload['Run']),int(payload['LumiBlock']))
        objs.close()
    except Exception as e:
        sys.stderr.write("ERROR reading required data from /TRIGGER/LUMI/LBTIME\n%s\n" % str(e))
        trigDB.closeDatabase()
        return None
    
    trigDB.closeDatabase()
    return rLB1,rLB2


#############################################################################
def GetTDaqOnlineLumi(runnumber,chan,lumitype):

    res=dict()
    LBTS = GetRunStartStopTimes(runnumber)
    trigDB = openDB('COOLONL_TDAQ')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TDAQ\n")
        return None
    try:
        folder = trigDB.getFolder('/TDAQ/OLC/LUMINOSITY')
        limmin = LBTS[0]
        limmax = LBTS[1]
        chansel = cool.ChannelSelection(chan)
        objs = folder.browseObjects(limmin,limmax,chansel)
        while objs.goToNext():
            obj = objs.currentRef()
            payload = obj.payload()
            lb = payload['RunLB'] & 0xFFFFFFFF
            res[lb] = [payload['LBAvInstLum%s'%lumitype] if payload['Valid']&3 == 0 else 0.][0]
        objs.close()
    except Exception as e:
        sys.stderr.write("ERROR reading required data from /TDAQ/OLC/LUMINOSITY\n%s\n" % str(e))

    trigDB.closeDatabase()
    return res


#############################################################################

#backward compatiblity:
def GetOnlineLumiFromCOOL(runnumber,nlb,channelid):
    return GetOnlineLumiFromCOOL(runnumber,channelid)

def GetOnlineLumiFromCOOL(runnumber,channelid):

    trigDB=openDB('COOLONL_TRIGGER')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n");
        return None

    try:
        folder=trigDB.getFolder('/TRIGGER/LUMI/OnlPrefLumi')
    except Exception as e:
        sys.stderr.write("Failed to access folder /TRIGGER/LUMI/OnlPrefLumi in database COOLONL_TRIGGER\n%s\n" % str(e))
        trigDB.closeDatabase()
        return None

    res= _readDataPerLB(folder,runnumber,0,lambda payload : payload['LBAvInstLumi'])

    trigDB.closeDatabase()
    return res

############################################################################# 
#The same as above just with a different folder name 
#def GetOfflineLumiFromCOOL(runnumber,nlb,channelid):
#    res = [0.*i for i in range(nlb)]
#    trigDB=indirectOpen('COOLOFL_TRIGGER/'+DBINST,oracle=True)
#    if (trigDB is None):
#        print "ERROR: Cannot connect to COOLOFL_TRIGGER/"+DBINST
#        return None
#    try:
#        folder=trigDB.getFolder('/TRIGGER/OFLLUMI/LBLESTOFL')
#        limmin=(runnumber << 32)+1
#        limmax=(runnumber << 32)+10000
#        chansel=cool.ChannelSelection(channelid)
#        objs = folder.browseObjects(limmin,limmax,chansel,TAG)
#        lb = 0
#        while objs.goToNext():
#            obj=objs.currentRef()
#            lb = obj.since() - limmin
#            if lb >= nlb:
#                break
#            payload=obj.payload()
#            res[lb] = payload['LBAvInstLumi']
#    except Exception as e:
#        print "ERROR reading required data from /TRIGGER/OFLLUMI/LBLESTOFL"
#        print e        
#    trigDB.closeDatabase()
#    return res
#############################################################################


def GetOnlineMuFromCOOL(runnumber,nlb,channelid):
    return GetOnlineMuFromCOOL(runnumber,channelid)


def GetOnlineMuFromCOOL(runnumber,channelid):
    #Feb 2015: No use-case found in ~/larmon/public 
    trigDB=openDB('COOLONL_TRIGGER')
    if (trigDB is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TRIGGER\n")
        return None

    try:
        folder=trigDB.getFolder('/TRIGGER/LUMI/OnlPrefLumi')
    except Exception as e:
        sys.stderr.write("Failed to access folder /TRIGGER/LUMI/OnlPrefLumi in database COOLONL_TRIGGER\n%s\n" % str(e))
        trigDB.closeDatabase()
        return None

    res=_readDataPerLB(folder,runnumber,0,lambda payload : payload['LBAvEvtsPerBX'])
    trigDB.closeDatabase()

    return res

#############################################################################
#Same as above just with different folder name

#def GetOfflineMuFromCOOL(runnumber,nlb,channelid):
#    res = [0.*i for i in range(nlb)]
#    trigDB=indirectOpen('COOLOFL_TRIGGER/'+DBINST,oracle=True)
#    if (trigDB is None):
#        print "ERROR: Cannot connect to COOLOFL_TRIGGER/"+DBINST
#        return None
#    try:
#        folder=trigDB.getFolder('/TRIGGER/OFLLUMI/LBLESTOFL')
#        limmin=(runnumber << 32)+1
#        limmax=(runnumber << 32)+10000
#        chansel=cool.ChannelSelection(channelid)
#        objs = folder.browseObjects(limmin,limmax,chansel,TAG)
#        lb = 0
#        while objs.goToNext():
#            obj = objs.currentRef()
#            lb  = obj.since() - limmin
#            if lb >= nlb:
#               break
#            payload = obj.payload()
#            res[lb] = payload['LBAvEvtsPerBX']
#    except Exception as e:
#        print "ERROR accessing /TRIGGER/OFLLUMI/LBLESTOFL"
#        print e        
#    trigDB.closeDatabase()
#    return res
#############################################################################


def GetReadyFlag(runnumber):
    db=openDB('COOLONL_TDAQ')
    if (db is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_TDAQ\n")
        return None

    try:
        folder=db.getFolder("/TDAQ/RunCtrl/DataTakingMode")
    except Exception as e:
        sys.stderr.write("Failed to access folder /TDAQ/RunCtrl/DataTakingMode in database COOLONL_TDAQ\n%s\n" % str(e))
        db.closeDatabase()
        return None

    res= _readDataPerLB(folder,runnumber,0,lambda payload : payload['ReadyForPhysics'])

    db.closeDatabase()
    return res


#############################################################################
"""
def GetFillTimeStamp(start):
    #Feb 2015:No use-case found in the larmon afs area
    res = []
    TIMESTAMP = []
    trigDB = indirectOpen('COOLOFL_DCS/'+DBINST,oracle=True)
    if (trigDB is None):
        print "ERROR: Cannot connect to COOLOFL_DCS/"+DBINST
        return None
    try:
        folder = trigDB.getFolder('/LHC/DCS/FILLSTATE')
        limmin = (start - 86400)*1000000000
        limmax = start*1000000000
        chansel = cool.ChannelSelection.all()
        objs = folder.browseObjects(limmin,limmax,chansel)
        while objs.goToNext():
            obj = objs.currentRef()
            payload = obj.payload()
            fill = int(payload['FillNumber'])
            since = (obj.since())/1000000000
            res.append([fill,since])
        for pair in res:
            if pair[0] == res[-1][0]:
                TIMESTAMP.append(pair[1])
    except Exception as e:
        print "ERROR accessing /LHC/DCS/FILLSTATE"
        print e        
    trigDB.closeDatabase()
    return TIMESTAMP[0]
"""
#############################################################################
def ConvertIOVtoUTC( iovtime ):
    if len(str(iovtime))>10:
        tsec = int(float(iovtime)/1.E9)
    else:
        tsec = int(iovtime)
    utctime = time.asctime( time.gmtime(tsec) )
    return utctime
#############################################################################
def ConvertUTCtoIOV( utctime ):
    try:
        ts   = time.strptime( utctime + '/UTC','%Y-%m-%d:%H:%M:%S/%Z' )
        tsec = int(calendar.timegm(ts))
        return tsec
    except ValueError:
        sys.stderr.write("ERROR in time specification: '%s' (use format: 2007-05-25:14:01:00)\n" % utctime)

#############################################################################
"""
def GetBeamSpotFromCOOL(runnumber,nlb):
    #Feb 2015: No use-case found under ~larmon/
    res = [[-999. for i in range(nlb)],
           [-999. for i in range(nlb)],
           [-999. for i in range(nlb)],
           [-999. for i in range(nlb)],
           [-999. for i in range(nlb)],
           [-999. for i in range(nlb)]]
    trigDB=indirectOpen('COOLOFL_INDET/'+DBINST,oracle=True)
    if (trigDB is None):
        print "ERROR: Cannot connect to COOLONL_TRIGGER/"+DBINST
        return None
    try:
        folder=trigDB.getFolder('/Indet/Beampos')
        limmin=(runnumber << 32)+1
        limmax=(runnumber << 32)+10000
        chansel=cool.ChannelSelection(0)
        objs = folder.browseObjects(limmin,limmax,chansel,'IndetBeampos-ES1-UPD2')
        lb = 0
        while objs.goToNext():
            obj=objs.currentRef()
            lb = obj.since() - limmin + 1
            payload=obj.payload()
            res[0][lb] = payload['posX']
            res[1][lb] = payload['posY']
            res[2][lb] = payload['posZ']
            res[3][lb] = payload['sigmaX']
            res[4][lb] = payload['sigmaY']
            res[5][lb] = payload['sigmaZ']
    except Exception as e:
        print "ERROR accessing /Indet/Beampos"
        print e        
    trigDB.closeDatabase()
    return res
"""

#############################################################################
def GetStableBeams(runnumber):

    EORTime=0
    SORTime=0xFFFFFFFFFFFFF 
    NLB=0
    LBTS=GetLBTimeStamps(runnumber)
    #Search for the highes LB number and the smalles (SOR) and the largest (EOR) timestamps
    for (LB,TS) in LBTS.iteritems():
        if TS[0]<SORTime: SORTime=TS[0]
        if TS[1]>EORTime: EORTime=TS[1]    

    db = openDB('COOLOFL_DCS')
    if (db is None):
        sys.stderr.write("ERROR: Cannot connect to COOLOFL_DCS\n")
        return None

    stableBeamRanges=[]
    try:
        folder = db.getFolder('/LHC/DCS/FILLSTATE')
        chansel = cool.ChannelSelection(1)
        #payloadQuery=cool.FieldSelection('StableBeams',cool.StorageType.Bool,cool.FieldSelection.EQ,True)
        #print payloadQuery
        objs = folder.browseObjects(SORTime*1000000000,EORTime*1000000000,chansel)#,cool.RecordSelection(payloadQuery))
        while objs.goToNext():
            obj = objs.currentRef()
            payload = obj.payload()
            #if (payload['StableBeams']==False): print "ERROR, payload query failed"
            if (payload['StableBeams']):
                if len(stableBeamRanges)>0 and stableBeamRanges[-1][1]==(obj.since()/1000000000):
                    stableBeamRanges[-1][1]=obj.until()/1000000000
                else:
                    stableBeamRanges.append([obj.since()/1000000000,obj.until()/1000000000])
            pass
        objs.close()
    except Exception as e:
        sys.stderr.write("ERROR accessing /LHC/DCS/FILLSTATE\n%s\n" % str(e))
        db.closeDatabase()
        return None

    db.closeDatabase()

    res = dict()
    for (LB,TS) in LBTS.iteritems():
        res[LB]=False
        #print TS
        for r in stableBeamRanges:
            #Check if LB is entirely contained in a stable-beam range
            #eg stable-beam start time before LB start time and stable-beam end time after LB end time
            if (r[0]<TS[0] and r[1]>TS[1]): 
                res[LB]=True

    return res

#############################################################################
def GetNumberOfCollidingBunches(runnumber):

    LBTS=GetRunStartStopTimes(runnumber)
    db = openDB('COOLOFL_DCS')
    if (db is None):
        sys.stderr.write("ERROR: Cannot connect to COOLOFL_DCS\n")
        return None
    
    maxNumberOfBunches=0

    try:
        folder = db.getFolder('/LHC/DCS/FILLSTATE')
        chansel = cool.ChannelSelection(1)
        objs = folder.browseObjects(LBTS[0],LBTS[1],chansel)#,cool.RecordSelection(payloadQuery))
        while objs.goToNext():
            obj = objs.currentRef()
            payload = obj.payload()
            ncb=payload['NumBunchColl']
            if ncb>maxNumberOfBunches: maxNumberOfBunches=ncb
            pass
        objs.close()

    except Exception as e:
        sys.stderr.write("ERROR accessing /LHC/DCS/FILLSTATE\n%s\n" % str(e))
    db.closeDatabase()

    return maxNumberOfBunches;



def GetDefects(runnumber, ListOfChannels, tag="HEAD"):
    db = openDB('COOLOFL_GLOBAL')
    if (db is None):
        sys.stderr.write("ERROR: Cannot connect to COOLONL_GOBAL\n")
        return None

    try:
        folder = db.getFolder("/GLOBAL/DETSTATUS/DEFECTS")
    except Exception as e:
        sys.stderr.write("Failed to access folder /GLOBAL/DETSTATUS/DEFECTS in database COOLONL_GLOBAL\n%s\n" % str(e))
        db.closeDatabase()
        return None

    sel=cool.ChannelSelection(ListOfChannels[0])
    for i in range(1,len(ListOfChannels)):
        sel.addChannel(ListOfChannels[i])
        pass

    result=dict()
    for chan in ListOfChannels:
        result[chan]=(folder.channelName(chan),list())


    limmin=(runnumber << 32) +1 #ATLAS runs always start at LB 1
    limmax=((1+runnumber) << 32)-1 #First LB of next run -1

    objs=folder.browseObjects(limmin,limmax,sel,tag)
    for obj in objs:
        pl=obj.payload()
        present=pl["present"]
        if (present):
            ch=obj.channelId()
            rlb1=obj.since()
            rlb2=obj.until()
            r1=rlb1>>32
            lb1=rlb1 & 0xFFFFFFFF
            r2=rlb2>>32
            lb2=rlb2 & 0xFFFFFFFF
            
            if r1!=runnumber or r2!=runnumber:
                sys.stderr.write("ERROR, defect spanning more than one run. Channel: %i, from %i to %i\n" % (ch,r1,r2))
            else:
                for lb in range(lb1,lb2):
                    result[ch][1].append(lb)
                    pass
                pass
            pass
        pass
    objs.close()
    db.closeDatabase()
    return result

from DQDefects import DefectsDB

def GetIntolerable():
    from DQDefects.db import DEFAULT_CONNECTION_STRING
    db = DefectsDB(DEFAULT_CONNECTION_STRING, tag="HEAD")
    return list(db.get_intolerable_defects())


def getDefect(run):
    # Get defects                                                              
    defectTag=""

    listintoldefect=["HVTRIP","SEVNOISYCHANNEL","SEVMISCALIB"]
    #listintoldefect=["EMBA","EMBC"]
    listLbwithdefect=list()
    db = DefectsDB(tag=defectTag)
    lar_defects = [d for d in (db.defect_names | db.virtual_defect_names) if d.startswith("LAR")]
    #print "lardefect",lar_defects
    defects = db.retrieve((run, 1), (run+1, 0), lar_defects)
    #print "extracted defects",defects
    isintolerable=False
    for defect in defects:
        for defectname in listintoldefect :
            if defectname in defect.channel:
                isintolerable=True
                for lb in range( defect.since.lumi,defect.until.lumi):
                    if not lb in listLbwithdefect:
                        listLbwithdefect.append(lb)
        #if not isintolerable : continue
            
            #print range( defect.since.lumi,defect.until.lumi)
            #for lb in range( defect.since.lumi,defect.until.lumi):
                #if not lb in listLbwithdefect:
                    #listLbwithdefect.append(lb)
            
            #print "defect channel",defect.channel
            #print "defect.since.lumi",defect.since.lumi
            #print "defect.until.lumi",defect.until.lumi
    #print listLbwithdefect
    return listLbwithdefect


def readBunchGroupContent(run, debug = False):
 
     """ Read full bunch group content from COOL """
 
     bgFullContent = {}
 
     db = openDb()
 
     bgcont_fname='/TRIGGER/LVL1/BunchGroupContent'
     if not db.existsFolder(bgcont_fname) :
         print( "Folder",bgcont_fname,"not found" )
         db.closeDatabase()
         sys.exit(-1)
 
     folder=db.getFolder(bgcont_fname)
     try:
         index = 0
         itr=folder.browseObjects(run << 32,(run+1) << 32,cool.ChannelSelection.all())
         while itr.goToNext() :
             obj=itr.currentRef()
             lb= int(obj.since() & 0xffff)
             if debug : print( "LB",lb )
             bgList = []
             for _ in range(8): bgList.append([])
             payload=obj.payload()
             bgcont = payload['BunchCode']
             for bcid in range(3564): 
                 for bgrp in range(8):
                     if(bgcont[bcid]>>bgrp & 1): bgList[bgrp].append(bcid)
                     ++index
                     for grp in range(8):
                         pass
                         # print "BG",grp,"size :",len(bgList[grp])
                         for grp in range(8):
                             pass
                             # print "BG",grp,":",bgList[grp]
             bgFullContent[lb] = bgList
 
         itr.close()
     except Exception as e:
         print( "Reading data from",bgcont_fname,"failed:",e )
 
     return bgFullContent


if __name__=="__main__":

    if len(sys.argv)==3:
        knownCmds=("GetStableBeams","GetReadyFlag")
        cmd=sys.argv[1]

        if not cmd in knownCmds:
            sys.stderr.write("Unknown command %s.\n Known commans are: " % cmd)
            for c in knownCmds:
                sys.stderr.write("%s "% c)
                pass
            sys.stderr.write("\n")
            sys.exit(-1)
            pass
        pass
    
    
            
        try:
            run=int(sys.argv[2])
        except Exception as e:
            sys.stderr.write("Expected and int as runnumber, got %s\n%s\n" % (sys.argv[2],str(e)))
            sys.exit(-1)
    
        result=None
        exec("result=%s(%i)" % (cmd,run))

        if (result is None):
            sys.stderr.write("Execution of cmd %s(%i) failed\n" % (cmd,run))
            sys.exit(-1)
        else:
            print( result )

    else:
        #Self-test:
        start=time.clock()
        print( GetDefects(260345,[44,45,46]) )
        #print getDefects(26034,[4194304,4718592,5242880,6291456])
        #print "Query time=",time.clock()-start
        #sys.exit(0)
        run=280862
        #run=215091
        if len(sys.argv)>1:
            run=int(sys.argv[1])
            pass

        print( "Testing %s with run %i" % (sys.argv[0],run) )

        #print "testing new Defect get"
        #print getDefect(run)
        # print "Testing GetBeamIntensities"
        # print GetBeamIntensities(run,1)

        print( "testing Bunch goupe key" )
        #print GetBCIDstatus(run)
        print( readBunchGroupKeys(run) )
        print( readBunchGroupContent(run) )

        # print "Testing GetLBTimeStamps"
        # print GetLBTimeStamps(run)
    

        #print "Testing GetLBDuration"
        #print GetLBDuration(run)
    
        fromStr="2022-06-19:19:59:59" #Thu Feb 19 19:59:55 2015' (use format: 2007-05-25:14:01:00)
        toStr="2022-06-21:19:55:00"
        print( "Testing GetRunNumberLumiblock",fromStr,toStr)
        print( GetRunNumberLumiblock(fromStr,toStr))

        
        # print "Testing GetTDaqOnlineLumi"
        # print GetTDaqOnlineLumi(run,0,"All")
        
        # print "Testing GetOnlineLumiFromCOOL"
        # print GetOnlineLumiFromCOOL(run,0)

        # print "Testing GetOnlineMuFromCOOL"
        # print GetOnlineMuFromCOOL(run,0)
        
        # print "Testing GetReadyFlag"
        # print GetReadyFlag(run)
        
        # print "Testing StableBeams"
        # print GetStableBeams(run)
        
        # print "Testing GetNumberOfCollidingBunches"
        # print GetNumberOfCollidingBunches(run)
        
