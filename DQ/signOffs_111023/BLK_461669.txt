Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Event Veto for run 461669 (Sat, 7 Oct 2023 23:08:32 +0200) ==
Found Noise or data corruption in run 461669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461669_Main.db

Found project tag data23_hi for run 461669
Found 1 Veto Ranges with 656 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 4.52 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (0.29 per-mil)
Overall Lumi loss is 2.0403222862677293e-06 by 4.51609578 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Event Veto for run 461669 (Sat, 7 Oct 2023 20:20:36 +0200) ==
Found Noise or data corruption in run 461669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461669_Main.db

Found project tag data23_hi for run 461669
Found 1 Veto Ranges with 656 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 4.52 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (0.29 per-mil)
Overall Lumi loss is 2.0403222862677293e-06 by 4.51609578 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Event Veto for run 461669 (Sat, 7 Oct 2023 19:42:55 +0200) ==
Found Noise or data corruption in run 461669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461669_Main.db

Found project tag data23_hi for run 461669
Found 1 Veto Ranges with 656 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 4.52 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (0.29 per-mil)
Overall Lumi loss is 2.0403222862677293e-06 by 4.51609578 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Event Veto for run 461669 (Sat, 7 Oct 2023 19:25:58 +0200) ==
Found Noise or data corruption in run 461669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461669_Main.db

Found project tag data23_hi for run 461669
Found 1 Veto Ranges with 656 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461669_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 4.52 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (0.29 per-mil)
Overall Lumi loss is 2.0403222862677293e-06 by 4.51609578 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Missing EVs for run 461669 (Thu, 28 Sep 2023 07:16:01 +0200) ==

Found a total of 139 noisy periods, covering a total of 0.15 seconds
Found a total of 182 Mini noise periods, covering a total of 0.18 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.02 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.01 per-mil)
Overall Lumi loss is 1.649767571076574e-07 by 0.327518445 s of veto length


