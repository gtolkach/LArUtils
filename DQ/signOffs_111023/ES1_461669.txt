Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Noisy cells update for run 461669 (Fri, 29 Sep 2023 21:04:04 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (x784_h442)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:61
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:36
Changed to HNHG:0
SBN:56
SBN in PS:0
HNHG:5
HNHG in PS:0

*****************
The treated cells were:
1 0 3 6 99 0 highNoiseHG  # 0x3a1ae300 -> 0x31893000
1 0 6 1 0 0 highNoiseHG  # 0x3a300000 -> 0x341ee000
1 0 6 1 1 0 sporadicBurstNoise  # 0x3a300100 -> 0x341ce000
1 0 6 1 3 0 highNoiseHG  # 0x3a300300 -> 0x3418e000
1 0 6 1 17 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301100 -> 0x343ce000
1 0 6 1 19 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301300 -> 0x3438e000
1 0 6 1 21 0 lowNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x3a301500 -> 0x3434e000
1 0 6 1 23 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301700 -> 0x3430e000
1 0 6 1 25 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301900 -> 0x342ce000
1 0 6 1 27 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301b00 -> 0x3428e000
1 0 6 1 29 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301d00 -> 0x3424e000
1 0 6 1 31 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a301f00 -> 0x3420e000
1 0 6 1 35 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a302300 -> 0x3458e000
1 0 6 1 37 0 sporadicBurstNoise  # 0x3a302500 -> 0x3454e000
1 0 6 1 39 0 sporadicBurstNoise  # 0x3a302700 -> 0x3450e000
1 0 6 1 41 0 sporadicBurstNoise  # 0x3a302900 -> 0x344ce000
1 0 6 1 43 0 sporadicBurstNoise  # 0x3a302b00 -> 0x3448e000
1 0 6 1 45 0 sporadicBurstNoise  # 0x3a302d00 -> 0x3444e000
1 0 6 1 47 0 sporadicBurstNoise  # 0x3a302f00 -> 0x3440e000
1 0 6 1 51 0 sporadicBurstNoise  # 0x3a303300 -> 0x3478e000
1 0 6 1 59 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303b00 -> 0x3468e000
1 0 6 1 60 0 sporadicBurstNoise  # 0x3a303c00 -> 0x3466e000
1 0 6 1 61 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303d00 -> 0x3464e000
1 0 6 1 62 0 lowNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x3a303e00 -> 0x3462e000
1 0 6 1 63 0 lowNoiseHG sporadicBurstNoise  # 0x3a303f00 -> 0x3460e000
1 0 6 1 64 0 sporadicBurstNoise  # 0x3a304000 -> 0x341ec000
1 0 6 1 65 0 lowNoiseHG sporadicBurstNoise  # 0x3a304100 -> 0x341cc000
1 0 6 1 66 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a304200 -> 0x341ac000
1 0 6 1 67 0 sporadicBurstNoise  # 0x3a304300 -> 0x3418c000
1 0 6 1 69 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a304500 -> 0x3414c000
1 0 6 1 71 0 highNoiseHG  # 0x3a304700 -> 0x3410c000
1 0 6 1 73 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a304900 -> 0x340cc000
1 0 6 1 75 0 sporadicBurstNoise  # 0x3a304b00 -> 0x3408c000
1 0 6 1 77 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a304d00 -> 0x3404c000
1 0 6 1 79 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a304f00 -> 0x3400c000
1 0 6 1 81 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305100 -> 0x343cc000
1 0 6 1 83 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305300 -> 0x3438c000
1 0 6 1 85 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305500 -> 0x3434c000
1 0 6 1 87 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305700 -> 0x3430c000
1 0 6 1 89 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305900 -> 0x342cc000
1 0 6 1 91 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305b00 -> 0x3428c000
1 0 6 1 93 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305d00 -> 0x3424c000
1 0 6 1 95 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a305f00 -> 0x3420c000
1 0 6 1 97 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306100 -> 0x345cc000
1 0 6 1 99 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306300 -> 0x3458c000
1 0 6 1 101 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306500 -> 0x3454c000
1 0 6 1 103 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306700 -> 0x3450c000
1 0 6 1 105 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306900 -> 0x344cc000
1 0 6 1 107 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306b00 -> 0x3448c000
1 0 6 1 109 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306d00 -> 0x3444c000
1 0 6 1 111 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a306f00 -> 0x3440c000
1 0 6 1 113 0 sporadicBurstNoise  # 0x3a307100 -> 0x347cc000
1 0 6 1 115 0 sporadicBurstNoise  # 0x3a307300 -> 0x3478c000
1 0 6 1 117 0 sporadicBurstNoise  # 0x3a307500 -> 0x3474c000
1 0 6 1 119 0 sporadicBurstNoise  # 0x3a307700 -> 0x3470c000
1 0 6 1 125 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a307d00 -> 0x3464c000
1 0 6 1 127 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a307f00 -> 0x3460c000
1 0 6 2 127 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30ff00 -> 0x34608000
1 0 6 10 44 0 distorted sporadicBurstNoise  # 0x3a34ac00 -> 0x3486e000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 1 6 15 65 0 sporadicBurstNoise  # 0x3b374100 -> 0x371de000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1591

== Event Veto for run 461669 (Thu, 28 Sep 2023 06:26:14 +0200) ==
Found Noise or data corruption in run 461669
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461669.db

Found project tag data23_hi for run 461669
Found 102 Veto Ranges with 3034 events
Found 4 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461669.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-UPD4-13  Run 461669
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461669.db;dbname=CONDBR2
Found a total of 102 corruption periods, covering a total of 270.07 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (3.40 per-mil)
Overall Lumi loss is 2.4094997316171694e-05 by 270.066007565 s of veto length



