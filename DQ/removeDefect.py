import argparse
# TO DO - check that defect exists, to avoid typos

dets = [ "EMB", "EMEC", "HEC", "FCAL" ]
sides = [ "A", "C" ]

def expandDefect(defect):
    defs = []
    detsides = [ d+s for d in dets for s in sides ] 
    return [ defect.replace("*",ds) for ds in detsides ]
    
def getDefectStr(runlist, defects, message):
    final_def = []
    for defect in defects:
        if "*" in defect:
            final_def.extend(expandDefect(defect))
        else:
            final_def.append(defect)
    print(f"Defects are {', '.join(final_def)}")
    outstr = ""
    for run in runlist: 
        outstr += f"@{run}\n"
        for defect in final_def:
            outstr += f"{defect} G # {message}\n"
        outstr += "\n"
    return outstr



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Defect removal script.')
    parser.add_argument('defects', nargs='+', help='Defect(s) to remove, can contain a * in order to expand for all subdetectors')
    parser.add_argument('-r', '--runlist', dest='runlist', help='Run number which you would like to get information for', type=int, nargs='+', required=True)
    parser.add_argument('-o', '--outFile', dest='outFile', default='removedef.txt', help='Output file name for defect list. Default %(default)s.')
    parser.add_argument('-m', '--message', dest='message', default='removing this defect, if it was set', help='Message to add as a comment in the defects database for each removal. Default %(default)s.')
    args = parser.parse_args()

    
    
    defstr = getDefectStr(args.runlist, args.defects, args.message)

    print(f"Writing output to {args.outFile}")
    with open(args.outFile, 'w') as f:
        f.write(defstr)
        
