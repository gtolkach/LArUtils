#!/bin/sh

# install the libraries we need
libs=( jira )
for lib in ${libs[@]}; do
    test=$(pip3 list --disable-pip-version-check --format=columns | grep -F ${lib})
    if [ -z "${test}" ]; then
	echo "pip3 install ${lib} --user"
        pip3 install ${lib} --user > /dev/null 2>&1
    fi
done


# setup tdaq
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-00-00/installed/setup.sh

# Our libraries take presidence
initial="$(echo $USER | head -c 1)"
myPy=/afs/cern.ch/user/${initial}/${USER}/.local/lib/python${pyver}/site-packages
PYTHONPATH=${myPy}:${PYTHONPATH}



