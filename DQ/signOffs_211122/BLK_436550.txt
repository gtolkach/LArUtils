Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Noisy cells update for run 436550 (Thu, 17 Nov 2022 18:25:39 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1294_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:8
Flagged in PS:3
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:7
SBN in PS:2
HNHG:1
HNHG in PS:1

*****************
The treated cells were:
1 0 3 7 46 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a1b2e00
1 0 3 7 46 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a1b2e00
0 0 7 14 37 0 sporadicBurstNoise # 0x383ea500
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Event Veto for run 436550 (Thu, 13 Oct 2022 20:43:42 +0200) ==
Found Noise or data corruption in run 436550
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436550_Main.db

Found project tag data22_13p6TeV for run 436550
Found 4 Veto Ranges with 11 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436550
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.88 nb-1 out of 257589.61 nb-1 (0.14 per-mil)
Overall Lumi loss is 34.88191013701434 by 2.00088376 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Event Veto for run 436550 (Thu, 13 Oct 2022 20:40:00 +0200) ==
Found Noise or data corruption in run 436550
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436550_Main.db

Found project tag data22_13p6TeV for run 436550
Found 4 Veto Ranges with 11 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436550
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436550_Main.db;dbname=CONDBR2
Found a total of 4 corruption periods, covering a total of 2.00 seconds
Lumi loss due to corruption: 34.88 nb-1 out of 257589.61 nb-1 (0.14 per-mil)
Overall Lumi loss is 34.88191013701434 by 2.00088376 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-670

== Missing EVs for run 436550 (Tue, 11 Oct 2022 04:16:28 +0200) ==

Found a total of 5 noisy periods, covering a total of 0.01 seconds
Found a total of 361 Mini noise periods, covering a total of 0.36 seconds
Lumi loss due to noise-bursts: 0.09 nb-1 out of 257589.61 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 6.42 nb-1 out of 257589.61 nb-1 (0.02 per-mil)
Overall Lumi loss is 6.509500045481723 by 0.36601776 s of veto length


