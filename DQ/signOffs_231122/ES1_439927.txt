Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-801

== Noisy cells update for run 439927 (Fri, 18 Nov 2022 14:43:36 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x711_h408)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:113
Flagged in PS:38
Unflagged by DQ shifters:5
Changed to SBN:1
Changed to HNHG:0
SBN:98
SBN in PS:29
HNHG:15
HNHG in PS:9

*****************
The treated cells were:
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 1 23 8 108 0 sporadicBurstNoise # 0x39bbec00
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 highNoiseHG # 0x38480500
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 16 0 sporadicBurstNoise # 0x3b619000
1 1 2 11 2 0 sporadicBurstNoise # 0x3b150200
1 1 2 4 14 0 highNoiseHG # 0x3b118e00
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200
0 0 15 2 90 0 sporadicBurstNoise # 0x3878da00
0 0 15 2 89 0 sporadicBurstNoise # 0x3878d900
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
1 1 2 11 6 0 sporadicBurstNoise # 0x3b150600
0 0 5 1 6 0 sporadicBurstNoise unflaggedByLADIeS # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 1 9 1 5 0 sporadicBurstNoise # 0x39480500
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
1 0 3 7 46 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a1b2e00
0 0 26 10 36 0 sporadicBurstNoise # 0x38d4a400
0 0 26 10 37 0 highNoiseHG # 0x38d4a500
0 0 22 12 54 0 sporadicBurstNoise # 0x38b5b600
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 5 1 0 0 highNoiseHG # 0x38280000
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 1 8 10 48 0 sporadicBurstNoise # 0x3944b000
0 1 8 10 49 0 sporadicBurstNoise # 0x3944b100
0 1 8 10 114 0 sporadicBurstNoise # 0x3944f200
0 1 8 10 115 0 sporadicBurstNoise # 0x3944f300
1 1 10 6 59 0 highNoiseHG # 0x3b52bb00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 12 13 103 0 sporadicBurstNoise # 0x38666700
0 0 30 1 7 0 sporadicBurstNoise # 0x38f00700
0 1 9 1 21 0 unflaggedByLADIeS # 0x39481500
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 0 16 13 14 0 sporadicBurstNoise # 0x38860e00
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 25 0 sporadicBurstNoise # 0x38681900
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 highNoiseHG # 0x39802b00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 0 10 12 127 0 sporadicBurstNoise # 0x3855ff00
0 0 4 1 13 0 sporadicBurstNoise # 0x38200d00
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 0 26 1 81 0 sporadicBurstNoise # 0x38d05100
0 0 11 4 126 0 sporadicBurstNoise # 0x3859fe00
0 0 11 4 127 0 sporadicBurstNoise # 0x3859ff00
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 30 1 15 0 sporadicBurstNoise # 0x38f00f00
0 0 22 6 117 0 sporadicBurstNoise # 0x38b2f500
0 0 22 6 119 0 sporadicBurstNoise # 0x38b2f700
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
0 1 21 1 3 0 sporadicBurstNoise # 0x39a80300
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 13 1 22 0 sporadicBurstNoise # 0x38681600
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 24 13 73 0 sporadicBurstNoise # 0x38c64900
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 1 4 1 86 0 sporadicBurstNoise # 0x39205600
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 17 6 31 0 unflaggedByLADIeS # 0x398a9f00
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 17 6 32 0 unflaggedByLADIeS # 0x398aa000
1 1 9 4 76 0 sporadicBurstNoise # 0x3b49cc00
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 5 13 63 0 sporadicBurstNoise # 0x382e3f00
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 0 30 1 98 0 sporadicBurstNoise # 0x38f06200
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 0 28 1 14 0 highNoiseHG # 0x38e00e00
0 0 28 1 15 0 sporadicBurstNoise # 0x38e00f00
1 1 21 4 70 0 sporadicBurstNoise # 0x3ba9c600
0 0 21 11 65 0 unflaggedByLADIeS # 0x38ad4100
0 0 20 1 5 0 unflaggedByLADIeS # 0x38a00500
0 0 9 1 71 0 sporadicBurstNoise # 0x38484700
0 0 23 7 11 0 sporadicBurstNoise # 0x38bb0b00
0 0 23 13 95 0 sporadicBurstNoise # 0x38be5f00
0 0 23 1 63 0 sporadicBurstNoise # 0x38b83f00

