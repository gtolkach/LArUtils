Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-796

== Noisy cells update for run 439859 (Tue, 22 Nov 2022 18:59:21 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:3 (f1310_h408)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:21
Flagged in PS:4
Unflagged by DQ shifters:0
Changed to SBN:3
Changed to HNHG:0
SBN:20
SBN in PS:3
HNHG:1
HNHG in PS:1

*****************
The treated cells were:
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 1 8 1 76 0 sporadicBurstNoise # 0x39404c00
1 0 21 15 119 0 sporadicBurstNoise # 0x3aaf7700
1 0 21 15 123 0 sporadicBurstNoise # 0x3aaf7b00
1 0 21 13 127 0 sporadicBurstNoise # 0x3aae7f00
1 0 7 10 23 0 sporadicBurstNoise # 0x3a3c9700
1 0 21 15 127 0 sporadicBurstNoise # 0x3aaf7f00
1 0 21 13 122 0 sporadicBurstNoise # 0x3aae7a00
1 1 21 4 70 0 sporadicBurstNoise reflaggedByLADIeS # 0x3ba9c600
1 1 21 2 80 0 sporadicBurstNoise reflaggedByLADIeS # 0x3ba8d000
1 1 21 4 66 0 sporadicBurstNoise reflaggedByLADIeS # 0x3ba9c200
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
1 1 2 11 4 0 sporadicBurstNoise # 0x3b150400
1 1 2 11 0 0 sporadicBurstNoise # 0x3b150000
1 1 2 11 1 0 sporadicBurstNoise # 0x3b150100
1 1 2 11 3 0 sporadicBurstNoise # 0x3b150300
1 1 2 9 66 0 sporadicBurstNoise # 0x3b144200
0 0 20 1 5 0 sporadicBurstNoise # 0x38a00500
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-796

== Event Veto for run 439859 (Tue, 22 Nov 2022 00:01:04 +0100) ==
Found Noise or data corruption in run 439859
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto439859_Main.db

Found project tag data22_13p6TeV for run 439859
Found 9 Veto Ranges with 7954 events
Found 39 isolated events
Reading event veto info from db sqlite://;schema=EventVeto439859_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 439859
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto439859_Main.db;dbname=CONDBR2
Found a total of 9 corruption periods, covering a total of 9.29 seconds
Lumi loss due to corruption: 174.52 nb-1 out of 502988.19 nb-1 (0.35 per-mil)
Overall Lumi loss is 174.5203679781018 by 9.290052505 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-796

== Event Veto for run 439859 (Mon, 21 Nov 2022 23:07:26 +0100) ==
Found Noise or data corruption in run 439859
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto439859_Main.db

Found project tag data22_13p6TeV for run 439859
Found 9 Veto Ranges with 7954 events
Found 39 isolated events
Reading event veto info from db sqlite://;schema=EventVeto439859_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 439859
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto439859_Main.db;dbname=CONDBR2
Found a total of 9 corruption periods, covering a total of 9.29 seconds
Lumi loss due to corruption: 174.52 nb-1 out of 502988.19 nb-1 (0.35 per-mil)
Overall Lumi loss is 174.5203679781018 by 9.290052505 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-796

== Missing EVs for run 439859 (Wed, 16 Nov 2022 22:15:50 +0100) ==

Found a total of 20 noisy periods, covering a total of 0.02 seconds
Found a total of 110 Mini noise periods, covering a total of 0.11 seconds
Lumi loss due to noise-bursts: 0.37 nb-1 out of 502988.19 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 2.04 nb-1 out of 502988.19 nb-1 (0.00 per-mil)
Overall Lumi loss is 2.414914375739872 by 0.133130505 s of veto length


