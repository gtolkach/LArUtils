Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-786

== Noisy cells update for run 439607 (Mon, 14 Nov 2022 17:56:31 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x708_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:44
Flagged in PS:18
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:2
SBN:40
SBN in PS:16
HNHG:4
HNHG in PS:2

*****************
The treated cells were:
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 1 28 2 102 0 sporadicBurstNoise # 0x39e0e600
0 1 28 2 101 0 sporadicBurstNoise # 0x39e0e500
0 1 28 2 103 0 sporadicBurstNoise # 0x39e0e700
0 0 15 13 5 0 sporadicBurstNoise # 0x387e0500
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 0 1 2 41 0 sporadicBurstNoise # 0x3808a900
0 0 1 2 42 0 sporadicBurstNoise # 0x3808aa00
0 0 30 1 7 0 sporadicBurstNoise # 0x38f00700
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 9 10 24 0 sporadicBurstNoise # 0x384c9800
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 13 1 15 0 sporadicBurstNoise # 0x38680f00
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
1 0 3 7 62 0 highNoiseHG reflaggedByLADIeS # 0x3a1b3e00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 11 1 15 0 sporadicBurstNoise # 0x38580f00
0 1 28 11 84 0 sporadicBurstNoise # 0x39e55400
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 13 1 22 0 sporadicBurstNoise # 0x38681600
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600

