Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1554

== Missing EVs for run 460913 (Sun, 17 Sep 2023 12:17:57 +0200) ==

Found a total of 18 noisy periods, covering a total of 0.02 seconds
Found a total of 939 Mini noise periods, covering a total of 0.94 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.01 per-mil)
Overall Lumi loss is 2.727484468733799e-07 by 0.959473585 s of veto length


