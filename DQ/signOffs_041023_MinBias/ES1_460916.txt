Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1555

== Noisy cells update for run 460916 (Mon, 18 Sep 2023 21:04:59 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x764_h438)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:40
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:36
SBN in PS:0
HNHG:4
HNHG in PS:0

*****************
The treated cells were:
1 0 1 2 123 0 sporadicBurstNoise  # 0x3a08fb00 -> 0x2ca87630
1 0 1 2 125 0 sporadicBurstNoise  # 0x3a08fd00 -> 0x2ca87a30
1 0 1 2 127 0 sporadicBurstNoise  # 0x3a08ff00 -> 0x2ca87e30
1 0 1 3 125 0 sporadicBurstNoise  # 0x3a097d00 -> 0x2ca8ba30
1 0 1 3 127 0 sporadicBurstNoise  # 0x3a097f00 -> 0x2ca8be30
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 6 1 0 0 sporadicBurstNoise  # 0x3a300000 -> 0x341ee000
1 0 6 1 1 0 sporadicBurstNoise  # 0x3a300100 -> 0x341ce000
1 0 6 1 29 0 sporadicBurstNoise  # 0x3a301d00 -> 0x3424e000
1 0 6 1 31 0 sporadicBurstNoise  # 0x3a301f00 -> 0x3420e000
1 0 6 1 37 0 sporadicBurstNoise  # 0x3a302500 -> 0x3454e000
1 0 6 1 39 0 sporadicBurstNoise  # 0x3a302700 -> 0x3450e000
1 0 6 1 41 0 sporadicBurstNoise  # 0x3a302900 -> 0x344ce000
1 0 6 1 43 0 sporadicBurstNoise  # 0x3a302b00 -> 0x3448e000
1 0 6 1 45 0 sporadicBurstNoise  # 0x3a302d00 -> 0x3444e000
1 0 6 1 47 0 sporadicBurstNoise  # 0x3a302f00 -> 0x3440e000
1 0 6 1 59 0 sporadicBurstNoise  # 0x3a303b00 -> 0x3468e000
1 0 6 1 64 0 sporadicBurstNoise  # 0x3a304000 -> 0x341ec000
1 0 6 1 65 0 lowNoiseHG sporadicBurstNoise  # 0x3a304100 -> 0x341cc000
1 0 6 1 66 0 sporadicBurstNoise  # 0x3a304200 -> 0x341ac000
1 0 6 1 67 0 sporadicBurstNoise  # 0x3a304300 -> 0x3418c000
1 0 6 1 69 0 sporadicBurstNoise  # 0x3a304500 -> 0x3414c000
1 0 6 1 71 0 sporadicBurstNoise  # 0x3a304700 -> 0x3410c000
1 0 6 1 73 0 sporadicBurstNoise  # 0x3a304900 -> 0x340cc000
1 0 6 1 75 0 sporadicBurstNoise  # 0x3a304b00 -> 0x3408c000
1 0 6 1 77 0 sporadicBurstNoise  # 0x3a304d00 -> 0x3404c000
1 0 6 1 79 0 sporadicBurstNoise  # 0x3a304f00 -> 0x3400c000
1 0 6 1 81 0 sporadicBurstNoise  # 0x3a305100 -> 0x343cc000
1 0 6 1 103 0 sporadicBurstNoise  # 0x3a306700 -> 0x3450c000
1 0 6 1 105 0 sporadicBurstNoise  # 0x3a306900 -> 0x344cc000
1 0 6 1 107 0 sporadicBurstNoise  # 0x3a306b00 -> 0x3448c000
1 0 6 1 109 0 sporadicBurstNoise  # 0x3a306d00 -> 0x3444c000
1 0 6 1 111 0 sporadicBurstNoise  # 0x3a306f00 -> 0x3440c000
1 0 6 1 113 0 sporadicBurstNoise  # 0x3a307100 -> 0x347cc000
1 0 6 1 115 0 sporadicBurstNoise  # 0x3a307300 -> 0x3478c000
1 0 6 1 117 0 sporadicBurstNoise  # 0x3a307500 -> 0x3474c000
1 0 6 1 119 0 sporadicBurstNoise  # 0x3a307700 -> 0x3470c000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 9 63 0 highNoiseHG  # 0x3a543f00 -> 0x31cc6000
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000

