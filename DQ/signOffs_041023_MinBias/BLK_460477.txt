Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1539

== Noisy cells update for run 460477 (Tue, 3 Oct 2023 10:49:06 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:23
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:20
SBN in PS:0
HNHG:3
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 0 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300000 -> 0x341ee000
1 0 6 1 1 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300100 -> 0x341ce000
1 0 6 1 35 0 sporadicBurstNoise  # 0x3a302300 -> 0x3458e000
1 0 6 1 37 0 sporadicBurstNoise  # 0x3a302500 -> 0x3454e000
1 0 6 1 39 0 sporadicBurstNoise  # 0x3a302700 -> 0x3450e000
1 0 6 1 41 0 sporadicBurstNoise  # 0x3a302900 -> 0x344ce000
1 0 6 1 43 0 sporadicBurstNoise  # 0x3a302b00 -> 0x3448e000
1 0 6 1 67 0 sporadicBurstNoise  # 0x3a304300 -> 0x3418c000
1 0 6 1 69 0 sporadicBurstNoise  # 0x3a304500 -> 0x3414c000
1 0 6 1 73 0 sporadicBurstNoise  # 0x3a304900 -> 0x340cc000
1 0 6 1 75 0 sporadicBurstNoise  # 0x3a304b00 -> 0x3408c000
1 0 6 1 77 0 sporadicBurstNoise  # 0x3a304d00 -> 0x3404c000
1 0 6 1 79 0 sporadicBurstNoise  # 0x3a304f00 -> 0x3400c000
1 0 6 1 97 0 sporadicBurstNoise  # 0x3a306100 -> 0x345cc000
1 0 6 1 99 0 sporadicBurstNoise  # 0x3a306300 -> 0x3458c000
1 0 6 1 103 0 sporadicBurstNoise  # 0x3a306700 -> 0x3450c000
1 0 6 1 105 0 sporadicBurstNoise  # 0x3a306900 -> 0x344cc000
1 0 6 1 107 0 sporadicBurstNoise  # 0x3a306b00 -> 0x3448c000
1 0 6 1 109 0 sporadicBurstNoise  # 0x3a306d00 -> 0x3444c000
1 0 6 1 111 0 sporadicBurstNoise  # 0x3a306f00 -> 0x3440c000
1 0 10 5 64 0 highNoiseHG  # 0x3a524000 -> 0x3000b000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 10 9 63 0 highNoiseHG  # 0x3a543f00 -> 0x31cc6000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1539

== Missing EVs for run 460477 (Mon, 11 Sep 2023 09:15:45 +0200) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 99 Mini noise periods, covering a total of 0.10 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Overall Lumi loss is 2.9536002217524362e-08 by 0.100197895 s of veto length


