Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1536

== Noisy cells update for run 460447 (Tue, 3 Oct 2023 10:29:44 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:3
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:3
HNHG in PS:0

*****************
The treated cells were:
1 0 10 5 64 0 highNoiseHG  # 0x3a524000 -> 0x3000b000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 10 9 63 0 highNoiseHG  # 0x3a543f00 -> 0x31cc6000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1536

== Missing EVs for run 460447 (Sun, 10 Sep 2023 19:15:25 +0200) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 62 Mini noise periods, covering a total of 0.06 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.04 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.3362861308379047e-07 by 0.06339534 s of veto length


