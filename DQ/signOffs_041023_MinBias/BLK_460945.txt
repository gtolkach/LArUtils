Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1558

== Missing EVs for run 460945 (Mon, 18 Sep 2023 11:15:25 +0200) ==

Found a total of 3 noisy periods, covering a total of 0.00 seconds
Found a total of 2 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.02 nb-1 (0.00 per-mil)
Overall Lumi loss is 4.459242685697973e-09 by 0.005 s of veto length


