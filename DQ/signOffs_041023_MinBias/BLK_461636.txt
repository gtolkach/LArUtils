Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1572

== Noisy cells update for run 461636 (Wed, 4 Oct 2023 13:46:54 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1393_h444)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:33
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:14
Changed to HNHG:8
SBN:19
SBN in PS:0
HNHG:14
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 5 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300500 -> 0x3414e000
1 0 6 1 7 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300700 -> 0x3410e000
1 0 6 1 11 0 highNoiseHG  # 0x3a300b00 -> 0x3408e000
1 0 6 1 13 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300d00 -> 0x3404e000
1 0 6 1 15 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300f00 -> 0x3400e000
1 0 6 1 42 0 highNoiseHG reflaggedByLADIeS  # 0x3a302a00 -> 0x344ae000
1 0 6 1 49 0 sporadicBurstNoise  # 0x3a303100 -> 0x347ce000
1 0 6 1 58 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303a00 -> 0x346ae000
1 0 6 1 65 0 lowNoiseHG highNoiseHG sporadicBurstNoise  # 0x3a304100 -> 0x341cc000
1 0 6 1 123 0 sporadicBurstNoise  # 0x3a307b00 -> 0x3468c000
1 0 6 2 60 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30bc00 -> 0x3466a000
1 0 6 2 63 0 highNoiseHG  # 0x3a30bf00 -> 0x3460a000
1 0 6 2 125 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30fd00 -> 0x34648000
1 0 6 3 63 0 sporadicBurstNoise  # 0x3a313f00 -> 0x34606000
1 0 6 3 92 0 highNoiseHG reflaggedByLADIeS  # 0x3a315c00 -> 0x34264000
1 0 6 3 105 0 highNoiseHG reflaggedByLADIeS  # 0x3a316900 -> 0x344c4000
1 0 6 4 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a31be00 -> 0x34622000
1 0 6 4 123 0 highNoiseHG reflaggedByLADIeS  # 0x3a31fb00 -> 0x34680000
1 0 6 5 62 0 highNoiseHG reflaggedByLADIeS  # 0x3a323e00 -> 0x3463e000
1 0 6 5 65 0 highNoiseHG reflaggedByLADIeS  # 0x3a324100 -> 0x341dc000
1 0 6 5 125 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a327d00 -> 0x3465c000
1 0 6 6 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a32be00 -> 0x3463a000
1 0 6 6 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a32bf00 -> 0x3461a000
1 1 6 12 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3b35bf00 -> 0x36a10000
1 1 6 15 60 0 lowNoiseHG highNoiseHG reflaggedByLADIeS  # 0x3b373c00 -> 0x37070000
1 1 6 15 62 0 sporadicBurstNoise  # 0x3b373e00 -> 0x37030000
1 1 6 15 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3b373f00 -> 0x37010000
1 1 6 15 64 0 sporadicBurstNoise  # 0x3b374000 -> 0x371fe000
1 1 6 15 126 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3b377e00 -> 0x37038000
1 1 6 15 127 0 highNoiseHG reflaggedByLADIeS  # 0x3b377f00 -> 0x37018000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1572

== Missing EVs for run 461636 (Wed, 27 Sep 2023 15:15:54 +0200) ==

Found a total of 131 noisy periods, covering a total of 0.13 seconds
Found a total of 113 Mini noise periods, covering a total of 0.11 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.02 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.02 per-mil)
Overall Lumi loss is 4.342705183690603e-08 by 0.24622786 s of veto length


