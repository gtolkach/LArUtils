Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1278

== Missing EVs for run 455623 (Fri, 30 Jun 2023 17:16:16 +0200) ==

Found a total of 71 noisy periods, covering a total of 0.07 seconds
Found a total of 202 Mini noise periods, covering a total of 0.20 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 786.36 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 786.36 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.0023226845143031107 by 0.27300292 s of veto length


