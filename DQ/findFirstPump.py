import ROOT as R
import glob
R.gROOT.SetBatch(True)

stream = "physics_CosmicCalo"
camp = "data23_13p6TeV"
#camp = "data22_13p6TeV"
rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/*/"+camp+".*."+stream+".merge.HIST.x*/"+camp+".*."+stream+".merge.HIST.x*.1"
print("Wildcard:",rootpath)
rootfiles = glob.glob(rootpath)
runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
runs = sorted(list(set(runs)),reverse=True)
print(len(runs), "runs")
infiles = {}
for run in runs:
    print(run)
    rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
    if len(rfile)>1:
        #print(len(rfile), run, ":", rfile)
        amis = sorted(list([ r.split(".")[-3] for r in rfile]))
        newest = [ r for r in rfile if "."+amis[-1]+"." in r]
        infiles[run] = newest[0]
    else:
        infiles[run] = rfile[0]

    infile = R.TFile(infiles[run], "R")
    histpath = "run_"+str(run)
    nthresh = 1
    histpaths = [ "CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_EMEC2C_5Sigma_CSCveto", "CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_EMEC2C_hiEth_CSCveto", "CaloMonitoring/LArCellMon_NoTrigSel/2d_PoorQualityFraction/fractionOverQthVsEtaPhi_EMEC2C_hiEth_CSCveto", "CaloMonitoring/LArCellMon_NoTrigSel/2d_AvgEnergy/CellAvgEnergyVsEtaPhi_EMEC2C_5Sigma_CSCveto" ]
    
    nperside=4
    canv = R.TCanvas("canv_"+str(run), "canv_"+str(run), 400*nperside, 800)
    canv.Divide(nperside,2)
    R.gStyle.SetPalette(R.kTemperatureMap)
    hists = {}
    print(len(histpaths))
    null = 0
    for hp in histpaths:
        fullpath = histpath+"/"+hp
        hists[hp] = infile.Get(fullpath)
        #print(histpaths.index(hp)+1,hists[hp])
        canv.cd(histpaths.index(hp)+1)
        if hists[hp]:
            hists[hp].SetTitle(str(run)+": "+hists[hp].GetTitle())
            if "fraction" in hists[hp].GetTitle():
                zmin = 0
                zmax = 1
            elif "AvgEnergy" in hp:
                zmin = 0
                zmax = 10000
            else:
                zmin = 0
                zmax = 1000
            hists[hp].GetZaxis().SetRangeUser(zmin,zmax)
            hists[hp].Draw("COLZ")
        else:
            null+=1
        canv.cd()
    if null < len(histpaths)/2:
        canv.Print("checkPumpNoise_Thresh"+str(nthresh)+"_"+camp+"_"+stream+"_"+str(run)+".pdf")
    #input("Press Enter to continue...")

        
    infile.Close()
