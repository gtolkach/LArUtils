== Noisy cells update for run 430542 ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:1
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:1
SBN in PS:1
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 5 1 5 0 sporadicBurstNoise # 0x38280500

== Event Veto for run 430542 ==
Found Noise or data corruption in run 430542
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto430542_=
Main.db

Found project tag data22_13p6TeV for run 430542
Found 3 Veto Ranges with 1086 events
Found 16 isolated events
Reading event veto info from db sqlite://;schema=3DEventVeto430542_Main.db;=
dbname=3DCONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 430542
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=3DEve=
ntVeto430542_Main.db;dbname=3DCONDBR2
Found a total of 3 corruption periods, covering a total of 2.70 seconds
Lumi loss due to corruption: 35.15 nb-1 out of 231800.49 nb-1 (0.15 per-mil=
)
Overall Lumi loss is 35.14725179563912 by 2.70321142 s of veto length



== Missing EVs for run 430542 ==

Found a total of 2 noisy periods, covering a total of 0.00 seconds
Found a total of 6 Mini noise periods, covering a total of 0.01 seconds
Lumi loss due to noise-bursts: 0.02 nb-1 out of 231800.49 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.08 nb-1 out of 231800.49 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.09863928986605958 by 0.00800097 s of veto length


