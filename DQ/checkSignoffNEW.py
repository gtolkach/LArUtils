#!/usr/bin/python3
import sys
sys.path.append("/afs/cern.ch/user/l/larmon/public/prod/LArPage1/")
import GetNumberFromDB as DB
import xmlrpc.client

dqmsite = "atlasdqm.cern.ch"
dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
dqmpass = None
with open(dqmpassfile, "r") as f:
    dqmpass = f.readlines()[0].strip()
    if ":" not in dqmpass:
        print("Problem reading dqmpass")
        sys.exit()
    dqmapi = xmlrpc.client.ServerProxy("https://"+dqmpass+"@"+dqmsite)

list_of_defects = ["LAR_FIRSTDEFECTS_UNCHECKED", "LAR_UNCHECKED","LAR_BULK_UNCHECKED","LAR_UNCHECKED_FINAL","GLOBAL_NOTCONSIDERED"]    
run_spec = {'stream': 'physics_CosmicCalo', 'source': 'tier0'}

last_run = dqmapi.get_latest_run(True, False) # arguments are atlas_partition, data_test, where latter omits da
defdelta=1000
data = {}
data['firstrun'] = str(int(data['lastrun'])-defdelta)
data['lastrun'] = last_run    
defectsDBname=DB.GetDefectsDBName(data['firstrun'],data['lastrun'])

defects_info = dqmapi.get_defects_lb(run_spec, list_of_defects, 'HEAD', False, False, defectsDBname)    

print("OI", defects_info)
