Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-826

== Noisy cells update for run 440543 (Fri, 9 Dec 2022 09:59:27 +0000) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1321_h410)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:36
Flagged in PS:16
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:33
SBN in PS:14
HNHG:3
HNHG in PS:2

*****************
The treated cells were:
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
1 0 21 15 119 0 sporadicBurstNoise # 0x3aaf7700
1 0 21 15 123 0 sporadicBurstNoise # 0x3aaf7b00
1 0 21 13 127 0 sporadicBurstNoise # 0x3aae7f00
1 0 21 15 127 0 sporadicBurstNoise # 0x3aaf7f00
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
1 0 21 13 122 0 sporadicBurstNoise # 0x3aae7a00
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 1 8 1 78 0 sporadicBurstNoise # 0x39404e00
0 1 16 6 103 0 sporadicBurstNoise # 0x3982e700
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 1 17 6 30 0 sporadicBurstNoise # 0x398a9e00
0 1 5 1 74 0 sporadicBurstNoise # 0x39284a00
1 0 9 2 12 0 sporadicBurstNoise # 0x3a488c00
0 0 27 14 45 0 sporadicBurstNoise # 0x38dead00
0 0 23 1 6 0 sporadicBurstNoise # 0x38b80600
0 1 4 1 72 0 sporadicBurstNoise # 0x39204800
0 0 15 1 81 0 sporadicBurstNoise # 0x38785100
0 0 15 14 18 0 sporadicBurstNoise # 0x387e9200
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 0 5 1 14 0 sporadicBurstNoise # 0x38280e00
0 0 16 1 79 0 sporadicBurstNoise # 0x38804f00
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 13 1 22 0 sporadicBurstNoise # 0x38681600
0 0 11 1 105 0 sporadicBurstNoise # 0x38586900
0 1 31 12 30 0 sporadicBurstNoise # 0x39fd9e00
0 1 14 11 86 0 sporadicBurstNoise # 0x39755600
0 0 19 1 28 0 highNoiseHG # 0x38981c00
0 0 20 3 5 0 sporadicBurstNoise # 0x38a10500
0 0 20 3 6 0 sporadicBurstNoise # 0x38a10600
0 1 4 11 20 0 sporadicBurstNoise # 0x39251400
0 0 15 1 7 0 sporadicBurstNoise # 0x38780700

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-826

== Event Veto for run 440543 (Tue, 6 Dec 2022 16:34:28 +0100) ==
Found Noise or data corruption in run 440543
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440543_Main.db

Found project tag data22_13p6TeV for run 440543
Found 23 Veto Ranges with 2764 events
Found 51 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440543_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440543
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440543_Main.db;dbname=CONDBR2
Found a total of 23 corruption periods, covering a total of 13.14 seconds
Lumi loss due to corruption: 209.73 nb-1 out of 837768.01 nb-1 (0.25 per-mil)
Overall Lumi loss is 209.73335254169686 by 13.140019575 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-826

== Event Veto for run 440543 (Tue, 6 Dec 2022 16:10:15 +0100) ==
Found Noise or data corruption in run 440543
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440543_Main.db

Found project tag data22_13p6TeV for run 440543
Found 23 Veto Ranges with 2764 events
Found 51 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440543_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440543
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440543_Main.db;dbname=CONDBR2
Found a total of 23 corruption periods, covering a total of 13.14 seconds
Lumi loss due to corruption: 209.73 nb-1 out of 837768.01 nb-1 (0.25 per-mil)
Overall Lumi loss is 209.73335254169686 by 13.140019575 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-826

== Missing EVs for run 440543 (Sun, 27 Nov 2022 10:16:06 +0100) ==

Found a total of 13 noisy periods, covering a total of 0.01 seconds
Found a total of 22 Mini noise periods, covering a total of 0.02 seconds
Lumi loss due to noise-bursts: 0.22 nb-1 out of 837768.01 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.36 nb-1 out of 837768.01 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.5786039156468458 by 0.036276125 s of veto length


