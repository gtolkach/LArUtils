Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-832

== Noisy cells update for run 440613 (Wed, 7 Dec 2022 18:35:39 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1321_h410)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:15
Flagged in PS:5
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:14
SBN in PS:5
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
1 0 21 15 119 0 sporadicBurstNoise # 0x3aaf7700
1 0 21 15 123 0 sporadicBurstNoise # 0x3aaf7b00
1 0 21 13 127 0 sporadicBurstNoise # 0x3aae7f00
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
1 0 21 15 127 0 sporadicBurstNoise # 0x3aaf7f00
1 0 21 13 122 0 sporadicBurstNoise # 0x3aae7a00
1 1 15 2 12 0 sporadicBurstNoise # 0x3b788c00
1 1 15 3 54 0 sporadicBurstNoise # 0x3b793600
0 1 3 1 66 0 sporadicBurstNoise # 0x39184200
0 0 26 1 85 0 sporadicBurstNoise # 0x38d05500

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-832

== Event Veto for run 440613 (Tue, 6 Dec 2022 06:20:43 +0100) ==
Found Noise or data corruption in run 440613
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440613_Main.db

Found project tag data22_13p6TeV for run 440613
Found 17 Veto Ranges with 11003 events
Found 31 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440613_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440613
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440613_Main.db;dbname=CONDBR2
Found a total of 17 corruption periods, covering a total of 15.17 seconds
Lumi loss due to corruption: 270.11 nb-1 out of 429800.55 nb-1 (0.63 per-mil)
Overall Lumi loss is 270.10847643095326 by 15.168081345 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-832

== Missing EVs for run 440613 (Mon, 28 Nov 2022 16:16:19 +0100) ==

Found a total of 9 noisy periods, covering a total of 0.01 seconds
Found a total of 136 Mini noise periods, covering a total of 0.14 seconds
Lumi loss due to noise-bursts: 0.15 nb-1 out of 429800.55 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 2.47 nb-1 out of 429800.55 nb-1 (0.01 per-mil)
Overall Lumi loss is 2.6241492967429663 by 0.146365885 s of veto length


