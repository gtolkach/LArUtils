Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-820

** Defect LAR_FCALC_NOISEBURST(INTOLERABLE) **
Affecting 6 LBs: 103, 104, 157, 158, 170, 171 (0 recoverable) 
** Defect LAR_HECA_NOISEBURST(INTOLERABLE) **
Affecting 17 LBs: 103, 104, 137, 138, 169, 170, 171, 172, 173, 174, 175, 180, 181, 190, 191, 268, 269 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST(INTOLERABLE) **
Affecting 6 LBs: 103, 104, 157, 158, 170, 171 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST(INTOLERABLE) **
Affecting 17 LBs: 103, 104, 137, 138, 169, 170, 171, 172, 173, 174, 175, 180, 181, 190, 191, 268, 269 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST(INTOLERABLE) **
Affecting 6 LBs: 103, 104, 157, 158, 170, 171 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST(INTOLERABLE) **
Affecting 17 LBs: 103, 104, 137, 138, 169, 170, 171, 172, 173, 174, 175, 180, 181, 190, 191, 268, 269 (0 recoverable) 
