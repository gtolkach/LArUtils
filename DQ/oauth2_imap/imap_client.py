"""Process incoming emails."""
import time
import base64
from imap_tools import MailBox
from oauth2_imap.config import Config
from oauth2_imap.oauth2_flow import Oauth2Flow
import imaplib

class ImapClient:
    """Process incoming emails."""

    def __init__(self, conf: Config) -> None:
        """Initialize the Imap client."""
        self.conf = conf
        self.oauth = Oauth2Flow(conf)

    def process_mailbox(self) -> None:
        """Process mailbox imap access"""
        while True:
            print("Fetching emails...")
            self.__process_bulk()
            time.sleep(30)

    def __process_bulk(self) -> None:
        """Process bulk with imap-tools library"""
        # Authenticate to account using OAuth 2.0 mechanism
        access_token, username = self.oauth.get_access_token()
        print("username", username)
        with MailBox(self.conf.IMAP_SERVER).xoauth2(
            username,
            access_token, # sasl xoauth2 token is built in the library
            "INBOX",
        ) as mailbox:
            for msg in mailbox.fetch(limit=5):
                print(msg.date_str, msg.subject)

    def __process_standard(self) -> None:
        """Process normal imaplib authentication"""
        # Authenticate to account using OAuth 2.0 mechanism
        access_token, username = self.oauth.get_access_token()
        # Apparently 365 wants clear data, not base64, so set to False
        auth_string = self.sasl_xoauth2(username, access_token, False)
        print("username", username)
        imap_conn = imaplib.IMAP4_SSL(self.conf.IMAP_SERVER)
        imap_conn.debug = 4
        imap_conn.authenticate('XOAUTH2', lambda x: auth_string)
        imap_conn.select('INBOX', readonly=True)
        imap_conn.close()
        
    def sasl_xoauth2(self, username, access_token, base64_encode=False) -> str:
        """Convert the access_token into XOAUTH2 format"""
        auth_string = "user=%s\1auth=Bearer %s\1\1" % (username, access_token)
        if base64_encode:
            auth_string = base64.b64encode(auth_string.encode("ascii")).decode("ascii")
        return auth_string
