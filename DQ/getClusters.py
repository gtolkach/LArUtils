import xmlrpc.client
from threading import Thread
import re, time
import itertools

passfile = open("/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt")
passwd = passfile.read().strip(); passfile.close()  
run_spec =  {'stream': 'physics_CosmicCalo', 'source': 'tier0'}
def getPassURL(usedev):
  if usedev is True:
    passurl = 'https://%s@atlasdqm-dev.cern.ch' % passwd
  else:
    passurl = 'https://%s@atlasdqm.cern.ch' % passwd
  return passurl


ThrDictEM = { 0: 0, 1: 4, 2: 10, 3: 15, 4: 25 }
ThrDictTopo= { 0: 0, 1: 10, 2: 15, 3: 25, 4: 50 }
poss_thr = list(itertools.product(ThrDictEM.values(), ThrDictTopo.values()))
poss_thr_str = [ f"Et > {x}/{y}GeV" for x,y in poss_thr ]

def getEMthr(thrstr):
  return int(thrstr.split("> ")[1].split("/")[0])
def getTopothr(thrstr):
  return int(thrstr.split("/")[1].split("GeV")[0])

def get_thrnum_EM(thrstr):
  EMthr = getEMthr(thrstr)
  return list(ThrDictEM.keys())[list(ThrDictEM.values()).index(EMthr)]

def get_thrnum_Topo(thrstr):
  Topothr = getTopothr(thrstr)
  return list(ThrDictTopo.keys())[list(ThrDictTopo.values()).index(Topothr)]


def getClusters(therun=455814, run_spec=run_spec, ami="x", thr='Et > 4/10GeV'):
  run_spec['run_list'] = [therun]
  run_spec['low_run'] = therun
  run_spec['high_run'] = therun
  run_spec['useAtlasdqm_dev'] = False
  #run_spec['pass_letter'] ='f'  # bulk
  #run_spec['stream_coll'] = "physics_Main"                
  run_spec['pass_letter'] =ami  # express
  run_spec['stream_coll'] = "express_express"
  DETs = list(range(4))
  Sides = ["ECC", "BAR", "ECA"]
  Clusters_stream = {f"stream:{thr}":[] for thr in poss_thr_str}
  Clusters_collisions = {f"collisions:{thr}":[] for thr in poss_thr_str}
  Clusters = {**Clusters_stream, **Clusters_collisions}
  advOptions = {}
  advOptions['cluster_threshold'] = 10
  advOptions['cluster_E'] = thr
  passurl = getPassURL(run_spec['useAtlasdqm_dev'])
  server = xmlrpc.client.ServerProxy(passurl)
  tmp_ProcVer = []
  tmp_AmiTag = {}
  tmp_StreamName = ""
  ProcVer = []
  runAmiTag = {}
  ProcVer_coll = []

  mapping = server.get_procpass_amitag_mapping(run_spec)
  if len(list(mapping.keys())) == 0: 
    print("no", therun)
    return Clusters

  for item in mapping["%d"%run_spec['run_list'][0]]:
    proc=item[0]
    stream=item[1]
    amitag=item[2]
    # In some runs, a manual reprocessing may have an "unknown" tag instead of the proper ami tag 
    if (stream == run_spec['stream'] and (amitag.startswith(run_spec['pass_letter']) or amitag.startswith("Unknown"))):
      ProcVer.append(proc)
      runAmiTag["%d"%(proc)] = amitag
    if (stream == run_spec['stream_coll'] and (amitag.startswith(run_spec['pass_letter']) or amitag.startswith("Unknown"))):
      ProcVer_coll.append(proc)
    if (stream == "tmp_%s"%(run_spec['stream']) and (amitag.startswith(run_spec['pass_letter']) or amitag.startswith("Unknown"))):
      tmp_ProcVer.append(proc)
      tmp_StreamName = stream
      tmp_AmiTag["%d"%(proc)] = amitag

    pass

  if len(ProcVer) > 0: # take the highest proc version of finished processing
    run_spec['proc_ver'] = max(ProcVer)
    run_spec['amiTag'] = runAmiTag["%d"%(run_spec['proc_ver'])]
  else:
    if len(tmp_ProcVer) > 0: # take the highest proc version of not yet finished processing
      run_spec['proc_ver'] = max(tmp_ProcVer)
      run_spec['stream'] = tmp_StreamName
      run_spec['amiTag'] = tmp_AmiTag["%d"%(run_spec['proc_ver'])]
      tmp_processing = True

  Duplicated = []
  #run_lumi=server.get_run_beamluminfo(run_spec)
  #Max beam energy during run (float)
  #Stable beam flag enabled during run (boolean)
  #ATLAS ready flag enabled during run (boolean)
  #Total integrated luminosity (/nb) - online best estimate (float)
  #ATLAS ready luminosity (/nb) - online best estimate (float)
  #if run_lumi[4]<1000:
  #    advOptions['cluster_threshold'] = 15
  #elif run_lumi[4]<25000:
  #    advOptions['cluster_threshold'] = 50
  #elif run_lumi[4]<150000:
  #    advOptions['cluster_threshold'] = 100
  #elif run_lumi[4]<300000:
  #    advOptions['cluster_threshold'] = 150
  #else:
  #    advOptions['cluster_threshold'] = 200
  clus = GetClusters(run_spec,Sides,Clusters["stream:%s"%advOptions['cluster_E']],Duplicated,advOptions['cluster_threshold'],advOptions['cluster_E']); clus.start()
  #print("-Clusters",Clusters)
  clus.join()

  return Clusters



class GetClusters(Thread):
  def __init__(self,run_spec,Sides,Clusters,Duplicated,cluster_threshold,cluster_E):
    Thread.__init__(self)

    self.run_spec = run_spec
    self.Sides = Sides
    self.Clusters = Clusters
    self.Duplicated = Duplicated
    self.cluster_threshold = cluster_threshold
    self.cluster_E = cluster_E
    passurl = getPassURL(run_spec['useAtlasdqm_dev'])
    self.server = xmlrpc.client.ServerProxy(passurl)
    self.multicall = xmlrpc.client.MultiCall( self.server )
    self.thrnum_EM = get_thrnum_EM(cluster_E)
    self.thrnum_Topo = get_thrnum_Topo(cluster_E)


  def run(self):
    ClusterTypes = ['EMTopoClusters','CaloTopoClusters']
    # Distance cells/clusters; 
    DR_match = 0.2;
    # Decoding the DQ bin output
    RE = re.compile(r'(?P<cat>\S+)-\(eta,phi\)\[OSRatio\]=\((?P<eta>\S+),(?P<phi>\S+)\)\[(?P<sigma>\S+)\]')
    N = "%d"%(self.run_spec['run_list'][0])

    for side in self.Sides:
      for i in ClusterTypes:
        if i=='EMTopoClusters':
          self.multicall.get_dqmf_all_results(self.run_spec,f'CaloTopoClusters/CalEM{side}/EMThresh{self.thrnum_EM}{side}Occ')
          
        if i=='CaloTopoClusters':
          self.multicall.get_dqmf_all_results(self.run_spec,f'CaloTopoClusters/Cal{side}/Thresh{self.thrnum_Topo}{side}Occ')
    
    #print( "DEBUG - clusters :",len(self.multicall._MultiCall__call_list),time.time(),"<br>")
    #print(self.multicall._MultiCall__call_list)
    R = tuple(self.multicall())
    #print( "@@DEBUG", R,"<br>")
    for i,I in enumerate(self.multicall._MultiCall__call_list):
      r = R[i][N]
      K = list(r.keys())
      #print( "DEBUG",r,K, "<br>" )
      for k in K:
        if k[0] == "Y" or k[0] == "R":
          # select clusters above threshold
          #print( "DEBUG",k,r[k],self.cluster_threshold,"<br>")
          if int(r[k])<self.cluster_threshold: continue
          m = RE.search(k).groupdict()
          # kDuplciated clusters are a priori obsolete - To be confirmed...
          for cl in self.Clusters:
            #print("@@cl:",cl)
            if k.split("=")[1] == cl[4].split("=")[1]:
              self.Duplicated.append( [float(m['eta']),float(m['phi']),DR_match,I[1][-1],k,int(r[k])] )
              self.Duplicated.append( cl )
          # Duplicated clusters are stored both
          self.Clusters.append( [float(m['eta']),float(m['phi']),DR_match,I[1][-1],k,int(r[k])] ) 
    
    #print("@@self.Clusters",self.Clusters)
    #print("Threshold000", self.cluster_E)




