Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1517

== Noisy cells update for run 459511 (Wed, 6 Sep 2023 09:10:06 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x758_h436)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:5
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:0
SBN in PS:0
HNHG:5
HNHG in PS:0

*****************
The treated cells were:
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 5 13 0 highNoiseHG  # 0x3a520d00 -> 0x3080c000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000

