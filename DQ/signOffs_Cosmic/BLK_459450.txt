Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1520

== Noisy cells update for run 459450 (Mon, 11 Sep 2023 22:44:28 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1379_h436)
Cluster matching: based on Et > 4/10GeV plots requiring at least 400 events
Flagged cells:180
Flagged in PS:64
Unflagged by DQ shifters:0
Changed to SBN:12
Changed to HNHG:0
SBN:91
SBN in PS:10
HNHG:89
HNHG in PS:54

*****************
The treated cells were:
0 1 4 10 84 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x3924d400 -> 0x2de02a48
0 1 7 1 31 0 sporadicBurstNoise reflaggedByLADIeS  # 0x39381f00 -> 0x2d801e1e
1 0 6 1 56 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a303800 -> 0x346ee000
1 0 20 1 7 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x3aa00700 -> 0x2c800e54

