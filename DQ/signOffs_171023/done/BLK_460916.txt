Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1555

== Noisy cells update for run 460916 (Tue, 3 Oct 2023 17:04:04 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:3
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:1
Changed to HNHG:0
SBN:3
SBN in PS:0
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 83 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a305300
1 0 6 1 33 0 sporadicBurstNoise # 0x3a302100
1 0 6 1 57 0 sporadicBurstNoise # 0x3a303900

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1555

== Missing EVs for run 460916 (Sun, 17 Sep 2023 12:17:57 +0200) ==

Found a total of 1 noisy periods, covering a total of 0.00 seconds
Found a total of 127 Mini noise periods, covering a total of 0.13 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.01 per-mil)
Overall Lumi loss is 4.0254504152383047e-08 by 0.128514665 s of veto length


