Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1570

== Missing EVs for run 461635 (Wed, 27 Sep 2023 04:15:22 +0200) ==

Found a total of 3 noisy periods, covering a total of 0.00 seconds
Found a total of 3 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.583991287634126e-09 by 0.0061779999999999995 s of veto length


