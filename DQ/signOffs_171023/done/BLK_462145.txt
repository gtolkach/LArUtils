Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1612

== Missing EVs for run 462145 (Thu, 5 Oct 2023 10:15:26 +0200) ==

Found a total of 7 noisy periods, covering a total of 0.01 seconds
Found a total of 3 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.00 nb-1 (0.00 per-mil)
Overall Lumi loss is 8.901421875780216e-09 by 0.01053175 s of veto length


