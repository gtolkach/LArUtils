Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Event Veto for run 461655 (Sat, 7 Oct 2023 23:09:52 +0200) ==
Found Noise or data corruption in run 461655
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461655_Main.db

Found project tag data23_hi for run 461655
Found 12 Veto Ranges with 3796 events
Found 23 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461655
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 20.03 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (1.73 per-mil)
Overall Lumi loss is 1.0385873777077544e-05 by 20.02507938 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Event Veto for run 461655 (Sat, 7 Oct 2023 20:21:26 +0200) ==
Found Noise or data corruption in run 461655
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461655_Main.db

Found project tag data23_hi for run 461655
Found 12 Veto Ranges with 3796 events
Found 23 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461655
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 20.03 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (1.73 per-mil)
Overall Lumi loss is 1.0385873777077544e-05 by 20.02507938 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Event Veto for run 461655 (Sat, 7 Oct 2023 19:45:06 +0200) ==
Found Noise or data corruption in run 461655
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461655_Main.db

Found project tag data23_hi for run 461655
Found 12 Veto Ranges with 3796 events
Found 23 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461655
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 20.03 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (1.73 per-mil)
Overall Lumi loss is 1.0385873777077544e-05 by 20.02507938 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Event Veto for run 461655 (Sat, 7 Oct 2023 19:26:53 +0200) ==
Found Noise or data corruption in run 461655
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto461655_Main.db

Found project tag data23_hi for run 461655
Found 12 Veto Ranges with 3796 events
Found 23 isolated events
Reading event veto info from db sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 461655
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto461655_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 20.03 seconds
Lumi loss due to corruption: 0.00 nb-1 out of 0.01 nb-1 (1.73 per-mil)
Overall Lumi loss is 1.0385873777077544e-05 by 20.02507938 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Noisy cells update for run 461655 (Wed, 4 Oct 2023 13:59:29 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1393_h444)
Cluster matching: based on Et > 4/10GeV plots requiring at least 15 events
Flagged cells:27
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:10
Changed to HNHG:6
SBN:13
SBN in PS:0
HNHG:14
HNHG in PS:0

*****************
The treated cells were:
1 0 6 1 5 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300500 -> 0x3414e000
1 0 6 1 13 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a300d00 -> 0x3404e000
1 0 6 1 44 0 highNoiseHG reflaggedByLADIeS  # 0x3a302c00 -> 0x3446e000
1 0 6 1 49 0 highNoiseHG  # 0x3a303100 -> 0x347ce000
1 0 6 1 56 0 sporadicBurstNoise  # 0x3a303800 -> 0x346ee000
1 0 6 1 58 0 highNoiseHG  # 0x3a303a00 -> 0x346ae000
1 0 6 1 65 0 lowNoiseHG highNoiseHG sporadicBurstNoise  # 0x3a304100 -> 0x341cc000
1 0 6 2 63 0 highNoiseHG  # 0x3a30bf00 -> 0x3460a000
1 0 6 2 125 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a30fd00 -> 0x34648000
1 0 6 3 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a313f00 -> 0x34606000
1 0 6 3 92 0 highNoiseHG reflaggedByLADIeS  # 0x3a315c00 -> 0x34264000
1 0 6 3 105 0 highNoiseHG reflaggedByLADIeS  # 0x3a316900 -> 0x344c4000
1 0 6 3 125 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a317d00 -> 0x34644000
1 0 6 4 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a31be00 -> 0x34622000
1 0 6 5 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a323f00 -> 0x3461e000
1 0 6 5 65 0 highNoiseHG reflaggedByLADIeS  # 0x3a324100 -> 0x341dc000
1 0 6 5 125 0 highNoiseHG reflaggedByLADIeS  # 0x3a327d00 -> 0x3465c000
1 0 6 6 62 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3a32be00 -> 0x3463a000
1 1 6 15 40 0 highNoiseHG reflaggedByLADIeS  # 0x3b372800 -> 0x370f2000
1 1 6 15 62 0 sporadicBurstNoise  # 0x3b373e00 -> 0x37030000
1 1 6 15 63 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3b373f00 -> 0x37010000
1 1 6 15 64 0 sporadicBurstNoise  # 0x3b374000 -> 0x371fe000
1 1 6 15 126 0 sporadicBurstNoise reflaggedByLADIeS  # 0x3b377e00 -> 0x37038000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1590

== Missing EVs for run 461655 (Wed, 27 Sep 2023 23:16:08 +0200) ==

Found a total of 196 noisy periods, covering a total of 0.21 seconds
Found a total of 205 Mini noise periods, covering a total of 0.21 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.03 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 0.01 nb-1 (0.01 per-mil)
Overall Lumi loss is 2.504504537594523e-07 by 0.412521865 s of veto length


