Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1374

** Defect LAR_HECA_NOISEBURST **
Affecting 2 LBs: 129, 130 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 2 LBs: 125, 126 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 2 LBs: 129, 130 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 2 LBs: 125, 126 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 2 LBs: 129, 130 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 2 LBs: 125, 126 (0 recoverable) 
