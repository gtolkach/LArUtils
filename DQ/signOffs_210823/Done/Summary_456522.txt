Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1383

** Defect LAR_HECA_NOISEBURST **
Affecting 8 LBs: 126, 127, 203, 204, 216, 217, 229, 230 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 31 LBs: 126, 127, 136, 137, 140, 141, 147, 148, 149, 150, 197, 198, 203, 204, 205, 208, 209, 212, 213, 214, 215, 216, 217, 338, 339, 374, 375, 383, 384, 386, 387 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 8 LBs: 126, 127, 203, 204, 216, 217, 229, 230 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 31 LBs: 126, 127, 136, 137, 140, 141, 147, 148, 149, 150, 197, 198, 203, 204, 205, 208, 209, 212, 213, 214, 215, 216, 217, 338, 339, 374, 375, 383, 384, 386, 387 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 8 LBs: 126, 127, 203, 204, 216, 217, 229, 230 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 31 LBs: 126, 127, 136, 137, 140, 141, 147, 148, 149, 150, 197, 198, 203, 204, 205, 208, 209, 212, 213, 214, 215, 216, 217, 338, 339, 374, 375, 383, 384, 386, 387 (0 recoverable) 
