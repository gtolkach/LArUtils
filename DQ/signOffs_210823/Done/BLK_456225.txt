Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1369

== Noisy cells update for run 456225 (Thu, 3 Aug 2023 10:47:50 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1367_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:9
Flagged in PS:4
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:9
SBN in PS:4
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 5 1 14 0 sporadicBurstNoise  # 0x38280e00 -> 0x2d000c28
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 7 10 40 0 sporadicBurstNoise  # 0x383ca800 -> 0x2d60348e
0 0 7 14 45 0 sporadicBurstNoise  # 0x383ead00 -> 0x2d406e8c
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
1 0 16 1 4 0 sporadicBurstNoise  # 0x3a800400 -> 0x2c20007a
1 0 16 1 5 0 sporadicBurstNoise  # 0x3a800500 -> 0x2c200078

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1369

== Event Veto for run 456225 (Sat, 15 Jul 2023 10:39:58 +0200) ==
Found Noise or data corruption in run 456225
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456225_Main.db

Found project tag data23_13p6TeV for run 456225
Found 12 Veto Ranges with 26 events
Found 33 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456225_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456225
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456225_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 6.46 seconds
Lumi loss due to corruption: 129.34 nb-1 out of 388659.13 nb-1 (0.33 per-mil)
Overall Lumi loss is 129.3353362848832 by 6.46077702 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1369

== Event Veto for run 456225 (Sat, 15 Jul 2023 10:18:10 +0200) ==
Found Noise or data corruption in run 456225
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456225_Main.db

Found project tag data23_13p6TeV for run 456225
Found 12 Veto Ranges with 26 events
Found 33 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456225_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456225
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456225_Main.db;dbname=CONDBR2
Found a total of 12 corruption periods, covering a total of 6.46 seconds
Lumi loss due to corruption: 129.34 nb-1 out of 388659.13 nb-1 (0.33 per-mil)
Overall Lumi loss is 129.3353362848832 by 6.46077702 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1369

== Missing EVs for run 456225 (Tue, 11 Jul 2023 11:21:09 +0200) ==

Found a total of 118 noisy periods, covering a total of 0.15 seconds
Found a total of 944 Mini noise periods, covering a total of 1.03 seconds
Lumi loss due to noise-bursts: 2.94 nb-1 out of 388659.13 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 20.40 nb-1 out of 388659.13 nb-1 (0.05 per-mil)
Overall Lumi loss is 23.332647455129884 by 1.180762015 s of veto length


