Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1387

** Defect LAR_HECA_NOISEBURST **
Affecting 26 LBs: 696, 697, 742, 743, 744, 745, 748, 749, 750, 756, 757, 758, 759, 760, 761, 769, 770, 773, 774, 777, 778, 783, 784, 806, 807, 808 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST **
Affecting 4 LBs: 770, 771, 793, 794 (0 recoverable) 
** Defect LAR_EMECA_NOISEBURST **
Affecting 26 LBs: 696, 697, 742, 743, 744, 745, 748, 749, 750, 756, 757, 758, 759, 760, 761, 769, 770, 773, 774, 777, 778, 783, 784, 806, 807, 808 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST **
Affecting 4 LBs: 770, 771, 793, 794 (0 recoverable) 
** Defect LAR_FCALA_NOISEBURST **
Affecting 26 LBs: 696, 697, 742, 743, 744, 745, 748, 749, 750, 756, 757, 758, 759, 760, 761, 769, 770, 773, 774, 777, 778, 783, 784, 806, 807, 808 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST **
Affecting 4 LBs: 770, 771, 793, 794 (0 recoverable) 
