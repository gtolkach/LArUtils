Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1374

== Noisy cells update for run 456314 (Wed, 12 Jul 2023 18:36:25 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x754_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:3
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:1
SBN in PS:1
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000

