Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Noisy cells update for run 456316 (Thu, 3 Aug 2023 15:19:43 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1369_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:6
Flagged in PS:3
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:6
SBN in PS:3
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 7 10 104 0 sporadicBurstNoise  # 0x383ce800 -> 0x2d603486
0 1 8 1 71 0 sporadicBurstNoise  # 0x39404700 -> 0x2d804622
1 0 16 1 4 0 sporadicBurstNoise  # 0x3a800400 -> 0x2c20007a
1 0 16 1 5 0 sporadicBurstNoise  # 0x3a800500 -> 0x2c200078

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Event Veto for run 456316 (Sun, 16 Jul 2023 12:39:40 +0200) ==
Found Noise or data corruption in run 456316
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456316_Main.db

Found project tag data23_13p6TeV for run 456316
Found 35 Veto Ranges with 12572 events
Found 48 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456316
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2
Found a total of 35 corruption periods, covering a total of 24.15 seconds
Lumi loss due to corruption: 467.61 nb-1 out of 569846.09 nb-1 (0.82 per-mil)
Overall Lumi loss is 467.6071549169549 by 24.15294587 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Event Veto for run 456316 (Sun, 16 Jul 2023 12:16:32 +0200) ==
Found Noise or data corruption in run 456316
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456316_Main.db

Found project tag data23_13p6TeV for run 456316
Found 35 Veto Ranges with 12572 events
Found 48 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456316
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456316_Main.db;dbname=CONDBR2
Found a total of 35 corruption periods, covering a total of 24.15 seconds
Lumi loss due to corruption: 467.61 nb-1 out of 569846.09 nb-1 (0.82 per-mil)
Overall Lumi loss is 467.6071549169549 by 24.15294587 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1375

== Missing EVs for run 456316 (Wed, 12 Jul 2023 13:28:02 +0200) ==

Found a total of 149 noisy periods, covering a total of 0.17 seconds
Found a total of 1178 Mini noise periods, covering a total of 1.29 seconds
Lumi loss due to noise-bursts: 3.25 nb-1 out of 569846.09 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 24.46 nb-1 out of 569846.09 nb-1 (0.04 per-mil)
Overall Lumi loss is 27.708875953729088 by 1.46438873 s of veto length


