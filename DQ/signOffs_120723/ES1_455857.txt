Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1289

== Noisy cells update for run 455857 (Wed, 5 Jul 2023 11:39:53 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 300 events
Flagged cells:192
Flagged in PS:49
Unflagged by DQ shifters:17
Changed to SBN:39
Changed to HNHG:0
SBN:173
SBN in PS:37
HNHG:19
HNHG in PS:12

*****************
The treated cells were:
DQshift
    including the input from the LAr DQ JIRA
    https://its.cern.ch/jira/projects/ATLLARSWDPQ
    - When done, clean the LAr DQ notebook

