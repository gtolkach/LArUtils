Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1292

== Noisy cells update for run 455899 (Thu, 6 Jul 2023 10:29:18 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x752_h431)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:100
Flagged in PS:27
Unflagged by DQ shifters:3
Changed to SBN:0
Changed to HNHG:0
SBN:86
SBN in PS:18
HNHG:14
HNHG in PS:9

*****************
The treated cells were:
0 0 0 1 90 0 sporadicBurstNoise  # 0x38005a00 -> 0x2d005c3e
0 0 4 4 45 0 sporadicBurstNoise  # 0x3821ad00 -> 0x2d215a2e
0 0 4 4 46 0 sporadicBurstNoise  # 0x3821ae00 -> 0x2d215c2e
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 6 0 highNoiseHG  # 0x38280600 -> 0x2d000428
0 0 7 11 53 0 sporadicBurstNoise  # 0x383d3500 -> 0x2d401a8c
0 0 7 14 20 0 sporadicBurstNoise  # 0x383e9400 -> 0x2d406286
0 0 7 14 22 0 sporadicBurstNoise  # 0x383e9600 -> 0x2d406282
0 0 7 14 23 0 sporadicBurstNoise  # 0x383e9700 -> 0x2d406280
0 0 7 14 24 0 sporadicBurstNoise  # 0x383e9800 -> 0x2d406486
0 0 7 14 25 0 sporadicBurstNoise  # 0x383e9900 -> 0x2d406484
0 0 7 14 26 0 sporadicBurstNoise  # 0x383e9a00 -> 0x2d406482
0 0 7 14 62 0 sporadicBurstNoise  # 0x383ebe00 -> 0x2d406e82
0 0 7 14 63 0 sporadicBurstNoise  # 0x383ebf00 -> 0x2d406e80
0 0 8 6 31 0 sporadicBurstNoise  # 0x38429f00 -> 0x2d223e1e
0 0 8 6 32 0 sporadicBurstNoise  # 0x3842a000 -> 0x2d22401e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 23 0 highNoiseHG  # 0x38581700 -> 0x2d001610
0 0 13 1 25 0 sporadicBurstNoise  # 0x38681900 -> 0x2d001a0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 14 1 106 0 sporadicBurstNoise  # 0x38706a00 -> 0x2d006c06
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 13 13 0 sporadicBurstNoise  # 0x387e0d00 -> 0x2d40460c
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 18 1 5 0 highNoiseHG  # 0x38900500 -> 0x2d000274
0 0 22 6 117 0 sporadicBurstNoise  # 0x38b2f500 -> 0x2d226a64
0 0 22 6 118 0 sporadicBurstNoise  # 0x38b2f600 -> 0x2d226c64
0 0 22 6 119 0 sporadicBurstNoise  # 0x38b2f700 -> 0x2d226e64
0 0 23 1 5 0 sporadicBurstNoise  # 0x38b80500 -> 0x2d000260
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 10 105 0 sporadicBurstNoise  # 0x38bce900 -> 0x2d603584
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 unflaggedByLADIeS  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 7 0 sporadicBurstNoise  # 0x38f00700 -> 0x2d000644
0 1 1 8 64 0 sporadicBurstNoise  # 0x390bc000 -> 0x2da30006
0 1 1 8 65 0 sporadicBurstNoise  # 0x390bc100 -> 0x2da30206
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 7 13 66 0 sporadicBurstNoise  # 0x393e4200 -> 0x2dc0407c
0 1 14 1 77 0 sporadicBurstNoise  # 0x39704d00 -> 0x2d804a3a
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 12 103 0 sporadicBurstNoise  # 0x3985e700 -> 0x2dc0330e
0 1 18 5 44 0 sporadicBurstNoise  # 0x39922c00 -> 0x2da1d848
0 1 21 3 9 0 unflaggedByLADIeS  # 0x39a90900 -> 0x2da09254
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 11 3 0 sporadicBurstNoise  # 0x39ad0300 -> 0x2dc00156
0 1 22 7 109 0 sporadicBurstNoise  # 0x39b36d00 -> 0x2da2da5a
0 1 22 7 110 0 sporadicBurstNoise  # 0x39b36e00 -> 0x2da2dc5a
0 1 22 7 111 0 sporadicBurstNoise  # 0x39b36f00 -> 0x2da2de5a
0 1 22 7 114 0 sporadicBurstNoise  # 0x39b37200 -> 0x2da2e45a
0 1 22 7 115 0 sporadicBurstNoise  # 0x39b37300 -> 0x2da2e65a
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 23 14 7 0 sporadicBurstNoise  # 0x39be8700 -> 0x2dc06376
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 28 2 102 0 sporadicBurstNoise  # 0x39e0e600 -> 0x2da04c72
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 3 6 115 0 highNoiseHG  # 0x3a1af300 -> 0x318d3000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 15 13 17 0 sporadicBurstNoise  # 0x3a7e1100 -> 0x2ce025ec
1 0 15 13 20 0 sporadicBurstNoise  # 0x3a7e1400 -> 0x2ce027ee
1 0 15 13 21 0 sporadicBurstNoise  # 0x3a7e1500 -> 0x2ce027ec
1 0 15 13 22 0 sporadicBurstNoise  # 0x3a7e1600 -> 0x2ce027ea
1 0 15 14 36 0 sporadicBurstNoise  # 0x3a7ea400 -> 0x2cc451ee
1 0 15 14 37 0 sporadicBurstNoise  # 0x3a7ea500 -> 0x2cc451ec
1 0 15 14 38 0 sporadicBurstNoise  # 0x3a7ea600 -> 0x2cc451ea
1 0 15 14 40 0 sporadicBurstNoise  # 0x3a7ea800 -> 0x2cc453ee
1 0 15 14 41 0 sporadicBurstNoise  # 0x3a7ea900 -> 0x2cc453ec
1 0 15 14 42 0 sporadicBurstNoise  # 0x3a7eaa00 -> 0x2cc453ea
1 0 15 14 44 0 sporadicBurstNoise  # 0x3a7eac00 -> 0x2cc455ee
1 0 15 14 45 0 sporadicBurstNoise  # 0x3a7ead00 -> 0x2cc455ec
1 0 15 14 46 0 sporadicBurstNoise  # 0x3a7eae00 -> 0x2cc455ea
1 0 15 14 47 0 sporadicBurstNoise  # 0x3a7eaf00 -> 0x2cc455e8
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 unflaggedByLADIeS  # 0x3aae7b00 -> 0x2ce02500
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1292

== Event Veto for run 455899 (Wed, 5 Jul 2023 08:28:07 +0200) ==
Found Noise or data corruption in run 455899
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto455899.db

Found project tag data23_13p6TeV for run 455899
Found 1 Veto Ranges with 94 events
Found 1 isolated events
Reading event veto info from db sqlite://;schema=EventVeto455899.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-UPD4-13  Run 455899
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto455899.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 1.98 seconds
Lumi loss due to corruption: 26.42 nb-1 out of 551381.72 nb-1 (0.05 per-mil)
Overall Lumi loss is 26.4244039330434 by 1.981131095 s of veto length



