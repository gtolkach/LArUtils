Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1089

** Defect LAR_HECC_HVTRIP **
Affecting 2 LBs: 697, 698 (0 recoverable) 
** Defect LAR_HECC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 425, 426, 763, 764 (0 recoverable) 
** Defect LAR_EMECC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 425, 426, 763, 764 (0 recoverable) 
** Defect LAR_FCALC_NOISEBURST(INTOLERABLE) **
Affecting 4 LBs: 425, 426, 763, 764 (0 recoverable) 
