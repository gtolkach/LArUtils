== Noisy cells update for run 438411 (Tue, 8 Nov 2022 09:35:11 +0000) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1307_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:3
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:2
SBN in PS:1
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00

== Event Veto for run 438411 (Sat, 5 Nov 2022 16:02:36 +0100) ==
Found Noise or data corruption in run 438411
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438411_Main.db

Found project tag data22_13p6TeV for run 438411
Found 8 Veto Ranges with 17 events
Found 14 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438411
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2
Found a total of 8 corruption periods, covering a total of 4.00 seconds
Lumi loss due to corruption: 73.65 nb-1 out of 198225.64 nb-1 (0.37 per-mil)
Overall Lumi loss is 73.65095692093756 by 4.00111925 s of veto length



== Event Veto for run 438411 (Sat, 5 Nov 2022 15:23:47 +0100) ==
Found Noise or data corruption in run 438411
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438411_Main.db

Found project tag data22_13p6TeV for run 438411
Found 8 Veto Ranges with 17 events
Found 14 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438411
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2
Found a total of 8 corruption periods, covering a total of 4.00 seconds
Lumi loss due to corruption: 73.65 nb-1 out of 198225.64 nb-1 (0.37 per-mil)
Overall Lumi loss is 73.65095692093756 by 4.00111925 s of veto length



== Event Veto for run 438411 (Sat, 5 Nov 2022 14:51:28 +0100) ==
Found Noise or data corruption in run 438411
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438411_Main.db

Found project tag data22_13p6TeV for run 438411
Found 8 Veto Ranges with 17 events
Found 14 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438411
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438411_Main.db;dbname=CONDBR2
Found a total of 8 corruption periods, covering a total of 4.00 seconds
Lumi loss due to corruption: 73.65 nb-1 out of 198225.64 nb-1 (0.37 per-mil)
Overall Lumi loss is 73.65095692093756 by 4.00111925 s of veto length



== Missing EVs for run 438411 (Tue, 1 Nov 2022 09:15:56 +0100) ==

Found a total of 3 noisy periods, covering a total of 0.00 seconds
Found a total of 91 Mini noise periods, covering a total of 0.09 seconds
Lumi loss due to noise-bursts: 0.06 nb-1 out of 198225.64 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 1.66 nb-1 out of 198225.64 nb-1 (0.01 per-mil)
Overall Lumi loss is 1.721249066111846 by 0.094125485 s of veto length


