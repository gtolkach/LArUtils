Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1149

== Noisy cells update for run 452463 (Fri, 19 May 2023 15:38:58 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x742_h420)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:98
Flagged in PS:34
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:83
SBN in PS:27
HNHG:15
HNHG in PS:7

*****************
The treated cells were:
0 1 16 1 43 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x39802b00 -> 0x2d802e40

