== Noisy cells update for run 438234 (Sun, 6 Nov 2022 18:44:52 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1305_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:36
Flagged in PS:6
Unflagged by DQ shifters:7
Changed to SBN:0
Changed to HNHG:3
SBN:31
SBN in PS:6
HNHG:5
HNHG in PS:0

*****************
The treated cells were:
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
1 0 3 7 62 0 highNoiseHG reflaggedByLADIeS # 0x3a1b3e00
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
0 1 8 1 77 0 sporadicBurstNoise # 0x39404d00
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 8 1 76 0 sporadicBurstNoise # 0x39404c00
0 0 7 9 16 0 sporadicBurstNoise # 0x383c1000
0 0 7 11 127 0 sporadicBurstNoise # 0x383d7f00
0 0 15 11 91 0 sporadicBurstNoise # 0x387d5b00
0 1 15 13 15 0 sporadicBurstNoise # 0x397e0f00
0 0 7 11 48 0 sporadicBurstNoise # 0x383d3000
0 0 7 9 19 0 sporadicBurstNoise # 0x383c1300
0 0 7 9 80 0 sporadicBurstNoise # 0x383c5000
0 0 7 11 115 0 unflaggedByLADIeS # 0x383d7300
0 0 7 11 123 0 unflaggedByLADIeS # 0x383d7b00
0 0 7 11 119 0 unflaggedByLADIeS # 0x383d7700
0 0 7 11 101 0 unflaggedByLADIeS # 0x383d6500
0 0 7 11 97 0 unflaggedByLADIeS # 0x383d6100
0 0 7 9 82 0 sporadicBurstNoise # 0x383c5200
0 0 7 11 109 0 unflaggedByLADIeS # 0x383d6d00
0 0 7 9 86 0 sporadicBurstNoise # 0x383c5600
0 0 7 11 105 0 unflaggedByLADIeS # 0x383d6900
0 0 13 1 13 0 sporadicBurstNoise # 0x38680d00
0 0 23 4 1 0 sporadicBurstNoise # 0x38b98100
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
1 0 13 10 83 0 sporadicBurstNoise # 0x3a6cd300
0 1 3 13 123 0 sporadicBurstNoise # 0x391e7b00
0 1 3 10 94 0 sporadicBurstNoise # 0x391cde00
0 1 3 10 95 0 highNoiseHG # 0x391cdf00
0 1 3 10 97 0 sporadicBurstNoise # 0x391ce100
0 1 3 10 93 0 sporadicBurstNoise # 0x391cdd00
0 1 3 13 119 0 sporadicBurstNoise # 0x391e7700
0 0 7 11 63 0 sporadicBurstNoise # 0x383d3f00
0 0 7 11 60 0 sporadicBurstNoise # 0x383d3c00
0 0 7 11 38 0 sporadicBurstNoise # 0x383d2600
0 0 7 11 42 0 sporadicBurstNoise # 0x383d2a00
0 0 15 5 58 0 sporadicBurstNoise # 0x387a3a00

== Event Veto for run 438234 (Thu, 3 Nov 2022 20:04:34 +0100) ==
Found Noise or data corruption in run 438234
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438234_Main.db

Found project tag data22_13p6TeV for run 438234
Found 14 Veto Ranges with 5402 events
Found 42 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438234_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438234
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438234_Main.db;dbname=CONDBR2
Found a total of 14 corruption periods, covering a total of 10.30 seconds
Lumi loss due to corruption: 181.66 nb-1 out of 729781.22 nb-1 (0.25 per-mil)
Overall Lumi loss is 181.65932444582944 by 10.29910568 s of veto length



== Event Veto for run 438234 (Thu, 3 Nov 2022 19:41:58 +0100) ==
Found Noise or data corruption in run 438234
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438234_Main.db

Found project tag data22_13p6TeV for run 438234
Found 14 Veto Ranges with 5402 events
Found 42 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438234_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438234
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438234_Main.db;dbname=CONDBR2
Found a total of 14 corruption periods, covering a total of 10.30 seconds
Lumi loss due to corruption: 181.66 nb-1 out of 729781.22 nb-1 (0.25 per-mil)
Overall Lumi loss is 181.65932444582944 by 10.29910568 s of veto length



== Missing EVs for run 438234 (Sat, 29 Oct 2022 11:16:57 +0200) ==

Found a total of 11 noisy periods, covering a total of 0.01 seconds
Found a total of 470 Mini noise periods, covering a total of 0.47 seconds
Lumi loss due to noise-bursts: 0.18 nb-1 out of 729781.22 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 8.28 nb-1 out of 729781.22 nb-1 (0.01 per-mil)
Overall Lumi loss is 8.469003130961083 by 0.48103299 s of veto length


