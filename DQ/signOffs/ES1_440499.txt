Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-824

== Noisy cells update for run 440499 (Sat, 26 Nov 2022 13:29:32 +0000) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x716_h409)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:77
Flagged in PS:26
Unflagged by DQ shifters:1
Changed to SBN:1
Changed to HNHG:0
SBN:73
SBN in PS:23
HNHG:4
HNHG in PS:3

*****************
The treated cells were:
1 0 9 9 83 0 sporadicBurstNoise unflaggedByLADIeS # 0x3a4c5300
1 0 9 11 39 0 sporadicBurstNoise # 0x3a4d2700
1 0 9 11 35 0 sporadicBurstNoise unflaggedByLADIeS # 0x3a4d2300
0 0 15 13 115 0 sporadicBurstNoise # 0x387e7300
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 0 9 1 4 0 unflaggedByLADIeS # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise unflaggedByLADIeS # 0x38480500
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 16 0 sporadicBurstNoise # 0x3b619000
1 1 2 11 2 0 sporadicBurstNoise # 0x3b150200
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 0 30 1 7 0 sporadicBurstNoise # 0x38f00700
1 0 3 7 46 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a1b2e00
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 1 9 1 5 0 sporadicBurstNoise # 0x39480500
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 24 13 73 0 sporadicBurstNoise # 0x38c64900
0 1 8 1 67 0 sporadicBurstNoise # 0x39404300
0 0 6 1 12 0 sporadicBurstNoise # 0x38300c00
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 1 23 8 108 0 sporadicBurstNoise # 0x39bbec00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise unflaggedByLADIeS # 0x39802b00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 0 13 1 25 0 sporadicBurstNoise # 0x38681900
0 0 15 11 23 0 sporadicBurstNoise # 0x387d1700
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
1 0 9 11 43 0 sporadicBurstNoise # 0x3a4d2b00
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 1 8 10 48 0 sporadicBurstNoise # 0x3944b000
0 1 8 10 49 0 sporadicBurstNoise # 0x3944b100
0 1 8 10 114 0 sporadicBurstNoise # 0x3944f200
0 1 8 10 115 0 sporadicBurstNoise # 0x3944f300
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 12 1 18 0 sporadicBurstNoise # 0x38601200
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 12 1 15 0 sporadicBurstNoise # 0x38600f00
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 0 21 1 91 0 sporadicBurstNoise # 0x38a85b00

