Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1116

== Noisy cells update for run 451587 (Wed, 24 May 2023 11:59:43 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1350_h420)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:47
Flagged in PS:6
Unflagged by DQ shifters:2
Changed to SBN:6
Changed to HNHG:4
SBN:29
SBN in PS:6
HNHG:18
HNHG in PS:0

*****************
The treated cells were:
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 13 1 5 0 sporadicBurstNoise  # 0x38680500 -> 0x2d000208
0 0 15 5 60 0 sporadicBurstNoise  # 0x387a3c00 -> 0x2d21f802
0 0 15 5 61 0 sporadicBurstNoise  # 0x387a3d00 -> 0x2d21fa02
0 0 15 13 31 0 sporadicBurstNoise  # 0x387e1f00 -> 0x2d404e08
0 0 15 13 84 0 highNoiseHG  # 0x387e5400 -> 0x2d404a06
0 0 15 13 85 0 sporadicBurstNoise  # 0x387e5500 -> 0x2d404a04
0 0 15 13 88 0 sporadicBurstNoise  # 0x387e5800 -> 0x2d404c06
0 0 15 13 89 0 highNoiseHG  # 0x387e5900 -> 0x2d404c04
0 0 15 13 92 0 highNoiseHG  # 0x387e5c00 -> 0x2d404e06
0 0 15 13 93 0 sporadicBurstNoise  # 0x387e5d00 -> 0x2d404e04
0 1 5 1 67 0 sporadicBurstNoise  # 0x39284300 -> 0x2d804614
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
0 1 13 12 65 0 sporadicBurstNoise  # 0x396dc100 -> 0x2dc020da
0 1 13 12 69 0 sporadicBurstNoise  # 0x396dc500 -> 0x2dc022da
0 1 29 11 65 0 sporadicBurstNoise  # 0x39ed4100 -> 0x2dc001da
1 0 6 1 3 0 highNoiseHG  # 0x3a300300 -> 0x3418e000
1 0 6 1 4 0 highNoiseHG  # 0x3a300400 -> 0x3416e000
1 0 6 1 5 0 highNoiseHG  # 0x3a300500 -> 0x3414e000
1 0 6 1 6 0 highNoiseHG  # 0x3a300600 -> 0x3412e000
1 0 6 1 23 0 highNoiseHG  # 0x3a301700 -> 0x3430e000
1 0 6 1 31 0 highNoiseHG  # 0x3a301f00 -> 0x3420e000
1 0 6 1 65 0 lowNoiseHG highNoiseHG  # 0x3a304100 -> 0x341cc000
1 0 10 9 63 0 sporadicBurstNoise  # 0x3a543f00 -> 0x31cc6000
1 0 20 1 17 0 unflaggedByLADIeS  # 0x3aa01100 -> 0x2c800a52
1 0 20 1 21 0 sporadicBurstNoise  # 0x3aa01500 -> 0x2c800a50
1 0 21 2 4 0 highNoiseHG  # 0x3aa88400 -> 0x2ca0005c
1 0 21 2 8 0 highNoiseHG  # 0x3aa88800 -> 0x2ca0005a
1 0 21 2 28 0 highNoiseHG  # 0x3aa89c00 -> 0x2ca00050
1 0 21 2 68 0 highNoiseHG  # 0x3aa8c400 -> 0x2ca0004c
1 0 21 2 84 0 unflaggedByLADIeS  # 0x3aa8d400 -> 0x2ca00044
1 0 21 2 92 0 highNoiseHG  # 0x3aa8dc00 -> 0x2ca00040
1 0 21 4 64 0 sporadicBurstNoise  # 0x3aa9c000 -> 0x2cc0011e
1 0 21 4 68 0 sporadicBurstNoise  # 0x3aa9c400 -> 0x2cc4011e
1 0 21 13 47 0 sporadicBurstNoise  # 0x3aae2f00 -> 0x2ce02750
1 0 21 13 121 0 sporadicBurstNoise  # 0x3aae7900 -> 0x2ce02504
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 126 0 sporadicBurstNoise  # 0x3aae7e00 -> 0x2ce02702
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 115 0 highNoiseHG  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 118 0 sporadicBurstNoise  # 0x3aaf7600 -> 0x2cc45102
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 0 21 15 122 0 sporadicBurstNoise  # 0x3aaf7a00 -> 0x2cc45302
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 126 0 sporadicBurstNoise  # 0x3aaf7e00 -> 0x2cc45502
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500
1 1 6 15 70 0 lowNoiseHG highNoiseHG  # 0x3b374600 -> 0x3713e000
1 1 6 15 124 0 highNoiseHG  # 0x3b377c00 -> 0x37078000

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1116

== Missing EVs for run 451587 (Sun, 7 May 2023 03:21:39 +0200) ==

Found a total of 2125 noisy periods, covering a total of 2.17 seconds
Found a total of 434 Mini noise periods, covering a total of 0.44 seconds
Lumi loss due to noise-bursts: 20.05 nb-1 out of 119776.69 nb-1 (0.17 per-mil)
Lumi loss due to mini-noise-bursts: 4.16 nb-1 out of 119776.69 nb-1 (0.03 per-mil)
Overall Lumi loss is 24.20792526248559 by 2.6035539950000004 s of veto length


