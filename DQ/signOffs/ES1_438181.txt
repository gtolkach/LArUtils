== Noisy cells update for run 438181 (Fri, 28 Oct 2022 19:42:19 +0200) ==
Tool version:WebDisplayExtractor-07-01-01 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x707_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:204
Flagged in PS:64
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:183
SBN in PS:50
HNHG:21
HNHG in PS:14

*****************
The treated cells were:
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
0 1 25 5 115 0 sporadicBurstNoise # 0x39ca7300
0 1 25 5 113 0 sporadicBurstNoise # 0x39ca7100
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise unflaggedByLADIeS # 0x38480500
0 1 22 13 123 0 sporadicBurstNoise # 0x39b67b00
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 14 2 29 0 sporadicBurstNoise # 0x38709d00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 1 3 8 30 0 sporadicBurstNoise # 0x391b9e00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 1 11 13 69 0 sporadicBurstNoise # 0x395e4500
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 13 0 sporadicBurstNoise # 0x3b618d00
0 1 12 12 111 0 sporadicBurstNoise # 0x3965ef00
0 0 5 1 0 0 sporadicBurstNoise unflaggedByLADIeS # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise unflaggedByLADIeS # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 1 9 12 93 0 sporadicBurstNoise # 0x394ddd00
0 0 11 1 23 0 highNoiseHG # 0x38581700
0 0 23 13 95 0 sporadicBurstNoise # 0x38be5f00
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 1 10 1 93 0 sporadicBurstNoise # 0x39505d00
0 0 8 1 17 0 highNoiseHG # 0x38401100
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 sporadicBurstNoise # 0x383c5400
0 0 7 9 21 0 sporadicBurstNoise # 0x383c1500
0 0 7 9 22 0 sporadicBurstNoise # 0x383c1600
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
1 1 2 11 34 0 sporadicBurstNoise # 0x3b152200
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 11 12 99 0 sporadicBurstNoise # 0x385de300
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 24 13 73 0 sporadicBurstNoise # 0x38c64900
0 0 7 9 25 0 sporadicBurstNoise # 0x383c1900
0 0 7 9 20 0 sporadicBurstNoise # 0x383c1400
0 0 6 1 12 0 sporadicBurstNoise # 0x38300c00
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 25 1 29 0 sporadicBurstNoise # 0x38c81d00
0 0 17 7 15 0 sporadicBurstNoise # 0x388b0f00
0 0 17 7 14 0 sporadicBurstNoise # 0x388b0e00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise unflaggedByLADIeS # 0x39802b00
0 0 28 1 82 0 sporadicBurstNoise # 0x38e05200
0 1 12 1 4 0 sporadicBurstNoise # 0x39600400
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 1 8 1 70 0 highNoiseHG # 0x39404600
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 0 27 1 53 0 highNoiseHG # 0x38d83500
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 12 11 75 0 sporadicBurstNoise # 0x38654b00
0 0 13 11 4 0 highNoiseHG # 0x386d0400
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
1 1 9 3 104 0 sporadicBurstNoise # 0x3b496800
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 22 0 highNoiseHG # 0x38681600
0 0 13 1 29 0 sporadicBurstNoise # 0x38681d00
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 1 5 12 124 0 sporadicBurstNoise # 0x392dfc00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 0 14 1 66 0 sporadicBurstNoise # 0x38704200
1 0 9 10 19 0 sporadicBurstNoise # 0x3a4c9300
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 0 7 1 103 0 sporadicBurstNoise # 0x38386700
0 0 1 2 41 0 sporadicBurstNoise # 0x3808a900
0 0 1 2 42 0 sporadicBurstNoise # 0x3808aa00
0 0 26 1 81 0 highNoiseHG # 0x38d05100
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 18 12 2 0 sporadicBurstNoise # 0x38958200
0 0 20 1 4 0 highNoiseHG # 0x38a00400
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 1 15 13 13 0 sporadicBurstNoise # 0x397e0d00
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 1 19 1 10 0 sporadicBurstNoise # 0x39980a00
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 1 31 10 21 0 sporadicBurstNoise # 0x39fc9500
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 7 12 68 0 sporadicBurstNoise # 0x393dc400
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 8 1 16 0 highNoiseHG # 0x38401000
0 0 7 9 26 0 highNoiseHG # 0x383c1a00
0 0 8 1 18 0 sporadicBurstNoise # 0x38401200
0 0 28 1 14 0 highNoiseHG # 0x38e00e00
0 0 28 1 15 0 sporadicBurstNoise unflaggedByLADIeS # 0x38e00f00
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 0 11 1 105 0 sporadicBurstNoise # 0x38586900
0 0 30 1 103 0 sporadicBurstNoise # 0x38f06700
1 0 2 4 50 0 sporadicBurstNoise # 0x3a11b200
1 0 2 2 76 0 sporadicBurstNoise # 0x3a10cc00
0 1 15 13 14 0 sporadicBurstNoise # 0x397e0e00
0 1 15 13 10 0 sporadicBurstNoise # 0x397e0a00
0 1 15 13 11 0 sporadicBurstNoise # 0x397e0b00
0 1 15 13 15 0 sporadicBurstNoise # 0x397e0f00
0 1 28 11 84 0 sporadicBurstNoise # 0x39e55400
0 1 6 12 64 0 sporadicBurstNoise # 0x3935c000
0 1 24 10 69 0 sporadicBurstNoise # 0x39c4c500
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 0 7 9 24 0 sporadicBurstNoise # 0x383c1800
0 0 7 9 27 0 sporadicBurstNoise # 0x383c1b00
0 0 7 9 88 0 highNoiseHG # 0x383c5800
0 0 7 11 106 0 sporadicBurstNoise # 0x383d6a00
0 0 7 11 102 0 sporadicBurstNoise # 0x383d6600
0 1 28 12 6 0 sporadicBurstNoise # 0x39e58600
0 0 15 2 90 0 sporadicBurstNoise # 0x3878da00
0 0 15 2 89 0 sporadicBurstNoise # 0x3878d900
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 0 21 11 65 0 sporadicBurstNoise # 0x38ad4100
0 0 22 1 1 0 sporadicBurstNoise # 0x38b00100
0 0 22 12 54 0 sporadicBurstNoise # 0x38b5b600
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 0 4 1 14 0 sporadicBurstNoise # 0x38200e00
0 1 13 12 65 0 sporadicBurstNoise # 0x396dc100
0 1 14 14 61 0 sporadicBurstNoise # 0x3976bd00
0 1 11 12 60 0 sporadicBurstNoise # 0x395dbc00
0 0 1 13 121 0 sporadicBurstNoise # 0x380e7900
1 0 13 11 115 0 sporadicBurstNoise # 0x3a6d7300
1 0 13 11 63 0 sporadicBurstNoise # 0x3a6d3f00
1 0 2 4 54 0 sporadicBurstNoise # 0x3a11b600
1 0 2 4 55 0 sporadicBurstNoise # 0x3a11b700
1 0 2 4 58 0 sporadicBurstNoise # 0x3a11ba00
1 0 2 4 51 0 sporadicBurstNoise # 0x3a11b300
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 1 21 1 3 0 sporadicBurstNoise # 0x39a80300
0 1 23 13 33 0 sporadicBurstNoise # 0x39be2100
0 1 13 12 69 0 sporadicBurstNoise # 0x396dc500
1 0 21 3 41 0 sporadicBurstNoise # 0x3aa92900
0 1 1 1 14 0 sporadicBurstNoise # 0x39080e00
1 1 2 10 104 0 sporadicBurstNoise # 0x3b14e800
0 0 26 1 63 0 sporadicBurstNoise # 0x38d03f00
0 1 5 13 3 0 sporadicBurstNoise # 0x392e0300
0 1 31 13 63 0 sporadicBurstNoise # 0x39fe3f00
0 0 16 8 40 0 sporadicBurstNoise # 0x3883a800
0 0 16 8 41 0 sporadicBurstNoise # 0x3883a900
0 0 16 1 78 0 sporadicBurstNoise # 0x38804e00
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 1 23 8 108 0 sporadicBurstNoise # 0x39bbec00
0 1 14 14 57 0 sporadicBurstNoise # 0x3976b900
0 0 27 12 50 0 sporadicBurstNoise # 0x38ddb200
0 0 23 4 0 0 sporadicBurstNoise # 0x38b98000
0 0 23 3 63 0 sporadicBurstNoise # 0x38b93f00
0 0 23 4 1 0 sporadicBurstNoise # 0x38b98100
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 13 1 30 0 sporadicBurstNoise # 0x38681e00
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
1 1 21 4 70 0 sporadicBurstNoise # 0x3ba9c600
1 1 21 2 80 0 sporadicBurstNoise # 0x3ba8d000
1 1 21 4 66 0 sporadicBurstNoise # 0x3ba9c200
1 1 21 4 69 0 sporadicBurstNoise # 0x3ba9c500
1 0 16 10 106 0 highNoiseHG # 0x3a84ea00
0 1 26 11 36 0 sporadicBurstNoise # 0x39d52400
0 1 0 12 77 0 sporadicBurstNoise # 0x3905cd00
0 0 15 1 81 0 sporadicBurstNoise # 0x38785100
0 0 19 12 93 0 sporadicBurstNoise # 0x389ddd00
1 1 9 4 76 0 sporadicBurstNoise # 0x3b49cc00
0 0 7 13 63 0 sporadicBurstNoise # 0x383e3f00
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200
1 0 5 3 32 0 sporadicBurstNoise # 0x3a292000
1 0 5 3 33 0 sporadicBurstNoise # 0x3a292100
1 0 5 3 34 0 sporadicBurstNoise # 0x3a292200
0 0 21 1 83 0 sporadicBurstNoise # 0x38a85300
0 1 24 13 127 0 sporadicBurstNoise # 0x39c67f00
0 1 25 13 55 0 sporadicBurstNoise # 0x39ce3700
0 0 0 1 43 0 sporadicBurstNoise # 0x38002b00
0 0 15 13 111 0 sporadicBurstNoise # 0x387e6f00
1 0 10 7 10 0 highNoiseHG # 0x3a530a00

== Event Veto for run 438181 (Fri, 28 Oct 2022 18:56:58 +0200) ==
Found Noise or data corruption in run 438181
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438181.db

Found project tag data22_13p6TeV for run 438181
Found 1 Veto Ranges with 213 events
Found 8 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438181.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-UPD4-11  Run 438181
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438181.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 4.89 seconds
Lumi loss due to corruption: 76.16 nb-1 out of 882747.40 nb-1 (0.09 per-mil)
Overall Lumi loss is 76.15642904561294 by 4.885400855 s of veto length



