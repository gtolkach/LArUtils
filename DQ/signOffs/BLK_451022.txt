Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1101

== Missing EVs for run 451022 (Sun, 30 Apr 2023 15:16:24 +0200) ==

Found a total of 5 noisy periods, covering a total of 0.01 seconds
Found a total of 493 Mini noise periods, covering a total of 0.49 seconds
Lumi loss due to noise-bursts: 0.01 nb-1 out of 84096.80 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 1.35 nb-1 out of 84096.80 nb-1 (0.02 per-mil)
Overall Lumi loss is 1.3612231661887597 by 0.49812553 s of veto length


