Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1001

== Noisy cells update for run 448637 (Mon, 10 Apr 2023 11:20:27 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x725_h414)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 15 events
Flagged cells:9
Flagged in PS:0
Unflagged by DQ shifters:2
SBN:1
SBN in PS:0
HNHG:8
HNHG in PS:0

*****************
The treated cells were:
1 1 22 7 86 0 unflaggedByLADIeS # 0x3bb35600
1 0 3 6 8 0 highNoiseHG # 0x3a1a8800
1 0 10 5 64 0 highNoiseHG # 0x3a524000
1 1 16 6 108 0 unflaggedByLADIeS # 0x3b82ec00
1 1 10 6 42 0 highNoiseHG # 0x3b52aa00
1 0 3 6 17 0 highNoiseHG # 0x3a1a9100
1 0 3 6 115 0 highNoiseHG # 0x3a1af300
1 1 22 8 102 0 sporadicBurstNoise # 0x3bb3e600
1 0 16 10 106 0 highNoiseHG # 0x3a84ea00
0 1 23 5 82 0 highNoiseHG # 0x39ba5200
1 0 14 4 65 0 highNoiseHG # 0x3a71c100

