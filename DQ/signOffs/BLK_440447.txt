Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-823

== Noisy cells update for run 440447 (Wed, 7 Dec 2022 18:27:38 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1321_h410)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:22
Flagged in PS:8
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:20
SBN in PS:8
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
1 0 21 15 119 0 sporadicBurstNoise # 0x3aaf7700
1 0 21 15 123 0 sporadicBurstNoise # 0x3aaf7b00
1 0 21 13 127 0 sporadicBurstNoise # 0x3aae7f00
1 0 21 15 127 0 sporadicBurstNoise # 0x3aaf7f00
0 0 4 1 14 0 sporadicBurstNoise # 0x38200e00
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 1 23 13 46 0 sporadicBurstNoise # 0x39be2e00
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 8 1 76 0 sporadicBurstNoise # 0x39404c00
1 0 21 13 122 0 sporadicBurstNoise # 0x3aae7a00
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 0 11 14 45 0 sporadicBurstNoise # 0x385ead00
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 0 14 2 120 0 sporadicBurstNoise # 0x3870f800
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 0 20 1 5 0 sporadicBurstNoise # 0x38a00500
0 1 26 9 66 0 sporadicBurstNoise # 0x39d44200
0 0 13 1 5 0 sporadicBurstNoise # 0x38680500
0 0 4 1 12 0 sporadicBurstNoise # 0x38200c00

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-823

== Event Veto for run 440447 (Sun, 4 Dec 2022 20:59:30 +0100) ==
Found Noise or data corruption in run 440447
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440447_Main.db

Found project tag data22_13p6TeV for run 440447
Found 20 Veto Ranges with 8262 events
Found 43 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440447_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440447
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440447_Main.db;dbname=CONDBR2
Found a total of 20 corruption periods, covering a total of 14.73 seconds
Lumi loss due to corruption: 274.36 nb-1 out of 503596.20 nb-1 (0.54 per-mil)
Overall Lumi loss is 274.35919157124573 by 14.73219651 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-823

== Event Veto for run 440447 (Sun, 4 Dec 2022 16:44:48 +0100) ==
Found Noise or data corruption in run 440447
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto440447_Main.db

Found project tag data22_13p6TeV for run 440447
Found 20 Veto Ranges with 8262 events
Found 43 isolated events
Reading event veto info from db sqlite://;schema=EventVeto440447_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 440447
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto440447_Main.db;dbname=CONDBR2
Found a total of 20 corruption periods, covering a total of 14.73 seconds
Lumi loss due to corruption: 274.36 nb-1 out of 503596.20 nb-1 (0.54 per-mil)
Overall Lumi loss is 274.35919157124573 by 14.73219651 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-823

== Missing EVs for run 440447 (Fri, 25 Nov 2022 04:17:13 +0100) ==

Found a total of 19 noisy periods, covering a total of 0.02 seconds
Found a total of 372 Mini noise periods, covering a total of 0.37 seconds
Lumi loss due to noise-bursts: 0.36 nb-1 out of 503596.20 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 6.89 nb-1 out of 503596.20 nb-1 (0.01 per-mil)
Overall Lumi loss is 7.246730216764199 by 0.39362532 s of veto length


