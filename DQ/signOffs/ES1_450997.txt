Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1100

== Noisy cells update for run 450997 (Mon, 1 May 2023 15:08:58 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:larcommeos / Processing version:1 (Unknown)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:211
Flagged in PS:25
Unflagged by DQ shifters:0
Changed to SBN:7
Changed to HNHG:0
SBN:184
SBN in PS:19
HNHG:27
HNHG in PS:6

*****************
The treated cells were:
0 0 1 8 99 0 sporadicBurstNoise  # 0x380be300 -> 0x2d234638
0 0 1 8 100 0 sporadicBurstNoise  # 0x380be400 -> 0x2d234838
0 0 4 1 13 0 sporadicBurstNoise  # 0x38200d00 -> 0x2d000a2c
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 10 102 0 sporadicBurstNoise  # 0x382ce600 -> 0x2d6032a2
0 0 5 10 103 0 highNoiseHG  # 0x382ce700 -> 0x2d6032a0
0 0 7 10 38 0 sporadicBurstNoise  # 0x383ca600 -> 0x2d60328a
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 7 10 40 0 sporadicBurstNoise  # 0x383ca800 -> 0x2d60348e
0 0 7 10 43 0 sporadicBurstNoise  # 0x383cab00 -> 0x2d603488
0 0 7 10 100 0 highNoiseHG  # 0x383ce400 -> 0x2d603286
0 0 7 10 104 0 highNoiseHG  # 0x383ce800 -> 0x2d603486
0 0 7 14 36 0 sporadicBurstNoise  # 0x383ea400 -> 0x2d406a8e
0 0 7 14 37 0 sporadicBurstNoise  # 0x383ea500 -> 0x2d406a8c
0 0 7 14 40 0 sporadicBurstNoise  # 0x383ea800 -> 0x2d406c8e
0 0 7 14 41 0 sporadicBurstNoise  # 0x383ea900 -> 0x2d406c8c
0 0 7 14 44 0 highNoiseHG  # 0x383eac00 -> 0x2d406e8e
0 0 7 14 45 0 sporadicBurstNoise  # 0x383ead00 -> 0x2d406e8c
0 0 7 14 54 0 sporadicBurstNoise  # 0x383eb600 -> 0x2d406a82
0 0 7 14 55 0 sporadicBurstNoise  # 0x383eb700 -> 0x2d406a80
0 0 7 14 58 0 sporadicBurstNoise  # 0x383eba00 -> 0x2d406c82
0 0 7 14 59 0 sporadicBurstNoise  # 0x383ebb00 -> 0x2d406c80
0 0 7 14 61 0 sporadicBurstNoise  # 0x383ebd00 -> 0x2d406e84
0 0 7 14 62 0 sporadicBurstNoise  # 0x383ebe00 -> 0x2d406e82
0 0 7 14 63 0 highNoiseHG  # 0x383ebf00 -> 0x2d406e80
0 0 8 14 1 0 sporadicBurstNoise  # 0x38468100 -> 0x2d40607c
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 11 1 23 0 highNoiseHG  # 0x38581700 -> 0x2d001610
0 0 13 2 68 0 sporadicBurstNoise  # 0x3868c400 -> 0x2d200808
0 0 13 2 69 0 sporadicBurstNoise  # 0x3868c500 -> 0x2d200a08
0 0 15 4 94 0 sporadicBurstNoise  # 0x3879de00 -> 0x2d213c00
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 12 72 0 sporadicBurstNoise  # 0x387dc800 -> 0x2d402406
0 0 15 13 80 0 highNoiseHG  # 0x387e5000 -> 0x2d404806
0 0 15 13 84 0 sporadicBurstNoise  # 0x387e5400 -> 0x2d404a06
0 0 15 13 86 0 sporadicBurstNoise  # 0x387e5600 -> 0x2d404a02
0 0 15 13 87 0 highNoiseHG  # 0x387e5700 -> 0x2d404a00
0 0 15 13 89 0 sporadicBurstNoise  # 0x387e5900 -> 0x2d404c04
0 0 15 13 90 0 sporadicBurstNoise  # 0x387e5a00 -> 0x2d404c02
0 0 15 13 91 0 highNoiseHG  # 0x387e5b00 -> 0x2d404c00
0 0 15 13 92 0 sporadicBurstNoise  # 0x387e5c00 -> 0x2d404e06
0 0 15 14 18 0 sporadicBurstNoise  # 0x387e9200 -> 0x2d406002
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 15 14 44 0 sporadicBurstNoise  # 0x387eac00 -> 0x2d406e0e
0 0 16 5 18 0 sporadicBurstNoise  # 0x38821200 -> 0x2d21a47e
0 0 16 5 19 0 sporadicBurstNoise  # 0x38821300 -> 0x2d21a67e
0 0 16 5 20 0 sporadicBurstNoise  # 0x38821400 -> 0x2d21a87e
0 0 16 8 40 0 sporadicBurstNoise  # 0x3883a800 -> 0x2d23507e
0 0 16 8 41 0 sporadicBurstNoise  # 0x3883a900 -> 0x2d23527e
0 0 17 7 54 0 sporadicBurstNoise  # 0x388b3600 -> 0x2d22ec7a
0 0 17 7 55 0 sporadicBurstNoise  # 0x388b3700 -> 0x2d22ee7a
0 0 17 10 20 0 sporadicBurstNoise  # 0x388c9400 -> 0x2d602bee
0 0 17 10 21 0 highNoiseHG  # 0x388c9500 -> 0x2d602bec
0 0 17 13 67 0 sporadicBurstNoise  # 0x388e4300 -> 0x2d4041e0
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 21 11 65 0 sporadicBurstNoise  # 0x38ad4100 -> 0x2d4001a4
0 0 22 1 1 0 sporadicBurstNoise  # 0x38b00100 -> 0x2d000266
0 0 23 1 63 0 sporadicBurstNoise  # 0x38b83f00 -> 0x2d003e60
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 13 15 0 sporadicBurstNoise  # 0x38be0f00 -> 0x2d404788
0 0 23 13 95 0 sporadicBurstNoise  # 0x38be5f00 -> 0x2d404f80
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 19 0 sporadicBurstNoise  # 0x38d01300 -> 0x2d001656
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 13 55 0 sporadicBurstNoise  # 0x38d63700 -> 0x2d405b58
0 0 27 12 50 0 sporadicBurstNoise  # 0x38ddb200 -> 0x2d40394a
0 0 27 13 103 0 sporadicBurstNoise  # 0x38de6700 -> 0x2d405340
0 0 30 1 13 0 sporadicBurstNoise  # 0x38f00d00 -> 0x2d000a44
0 1 1 8 64 0 sporadicBurstNoise  # 0x390bc000 -> 0x2da30006
0 1 1 8 65 0 sporadicBurstNoise  # 0x390bc100 -> 0x2da30206
0 1 1 8 66 0 sporadicBurstNoise  # 0x390bc200 -> 0x2da30406
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 3 8 27 0 sporadicBurstNoise  # 0x391b9b00 -> 0x2da3360c
0 1 3 8 28 0 sporadicBurstNoise  # 0x391b9c00 -> 0x2da3380c
0 1 3 8 29 0 sporadicBurstNoise  # 0x391b9d00 -> 0x2da33a0c
0 1 3 8 30 0 sporadicBurstNoise  # 0x391b9e00 -> 0x2da33c0c
0 1 4 1 71 0 sporadicBurstNoise  # 0x39204700 -> 0x2d804612
0 1 5 1 88 0 sporadicBurstNoise  # 0x39285800 -> 0x2d805814
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 6 12 64 0 sporadicBurstNoise  # 0x3935c000 -> 0x2dc02068
0 1 8 1 70 0 highNoiseHG  # 0x39404600 -> 0x2d804422
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 21 0 sporadicBurstNoise  # 0x39481500 -> 0x2d801226
0 1 13 12 65 0 sporadicBurstNoise  # 0x396dc100 -> 0x2dc020da
0 1 13 12 69 0 sporadicBurstNoise  # 0x396dc500 -> 0x2dc022da
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 14 1 77 0 sporadicBurstNoise  # 0x39704d00 -> 0x2d804a3a
0 1 16 1 43 0 sporadicBurstNoise  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 16 11 118 0 sporadicBurstNoise  # 0x39857600 -> 0x2dc01b0c
0 1 17 1 40 0 sporadicBurstNoise  # 0x39882800 -> 0x2d802844
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 23 1 87 0 sporadicBurstNoise  # 0x39b85700 -> 0x2d80565e
0 1 23 5 82 0 highNoiseHG  # 0x39ba5200 -> 0x2da1a45e
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 24 12 76 0 sporadicBurstNoise  # 0x39c5cc00 -> 0x2dc02788
0 1 26 11 36 0 sporadicBurstNoise  # 0x39d52400 -> 0x2dc013a0
0 1 28 12 6 0 sporadicBurstNoise  # 0x39e58600 -> 0x2dc023c4
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 2 2 88 0 sporadicBurstNoise  # 0x3a10d800 -> 0x2ca00022
1 0 2 2 89 0 sporadicBurstNoise  # 0x3a10d900 -> 0x2ca40022
1 0 2 2 90 0 sporadicBurstNoise  # 0x3a10da00 -> 0x2ca40222
1 0 2 2 91 0 sporadicBurstNoise  # 0x3a10db00 -> 0x2ca40422
1 0 2 3 75 0 sporadicBurstNoise  # 0x3a114b00 -> 0x2cc402d8
1 0 2 4 86 0 distorted highNoiseHG  # 0x3a11d600 -> 0x2cc40092
1 0 2 4 91 0 distorted highNoiseHG  # 0x3a11db00 -> 0x2cc40290
1 0 2 4 92 0 highNoiseHG  # 0x3a11dc00 -> 0x2cc40496
1 0 2 4 94 0 sporadicBurstNoise  # 0x3a11de00 -> 0x2cc40492
1 0 2 4 95 0 sporadicBurstNoise  # 0x3a11df00 -> 0x2cc40490
1 0 2 4 96 0 sporadicBurstNoise  # 0x3a11e000 -> 0x2cc0008e
1 0 2 4 97 0 sporadicBurstNoise  # 0x3a11e100 -> 0x2cc0008c
1 0 2 4 98 0 highNoiseHG  # 0x3a11e200 -> 0x2cc0008a
1 0 2 4 99 0 sporadicBurstNoise  # 0x3a11e300 -> 0x2cc00088
1 0 2 4 100 0 sporadicBurstNoise  # 0x3a11e400 -> 0x2cc4008e
1 0 2 4 101 0 sporadicBurstNoise  # 0x3a11e500 -> 0x2cc4008c
1 0 2 4 102 0 sporadicBurstNoise  # 0x3a11e600 -> 0x2cc4008a
1 0 2 4 103 0 sporadicBurstNoise  # 0x3a11e700 -> 0x2cc40088
1 0 2 4 104 0 sporadicBurstNoise  # 0x3a11e800 -> 0x2cc4028e
1 0 2 4 105 0 sporadicBurstNoise  # 0x3a11e900 -> 0x2cc4028c
1 0 2 4 106 0 sporadicBurstNoise  # 0x3a11ea00 -> 0x2cc4028a
1 0 2 4 107 0 sporadicBurstNoise  # 0x3a11eb00 -> 0x2cc40288
1 0 2 4 109 0 sporadicBurstNoise  # 0x3a11ed00 -> 0x2cc4048c
1 0 2 4 110 0 sporadicBurstNoise  # 0x3a11ee00 -> 0x2cc4048a
1 0 2 4 111 0 sporadicBurstNoise  # 0x3a11ef00 -> 0x2cc40488
1 0 2 13 24 0 sporadicBurstNoise  # 0x3a161800 -> 0x2ce024e6
1 0 2 13 25 0 sporadicBurstNoise  # 0x3a161900 -> 0x2ce024e4
1 0 2 13 28 0 sporadicBurstNoise  # 0x3a161c00 -> 0x2ce026e6
1 0 2 13 29 0 sporadicBurstNoise  # 0x3a161d00 -> 0x2ce026e4
1 0 2 14 52 0 sporadicBurstNoise  # 0x3a16b400 -> 0x2cc450e6
1 0 2 14 53 0 sporadicBurstNoise  # 0x3a16b500 -> 0x2cc450e4
1 0 2 14 56 0 sporadicBurstNoise  # 0x3a16b800 -> 0x2cc452e6
1 0 2 14 57 0 sporadicBurstNoise  # 0x3a16b900 -> 0x2cc452e4
1 0 2 14 58 0 sporadicBurstNoise  # 0x3a16ba00 -> 0x2cc452e2
1 0 2 14 60 0 sporadicBurstNoise  # 0x3a16bc00 -> 0x2cc454e6
1 0 2 14 61 0 unstable lowNoiseHG sporadicBurstNoise  # 0x3a16bd00 -> 0x2cc454e4
1 0 3 6 111 0 highNoiseHG  # 0x3a1aef00 -> 0x31890000
1 0 4 8 2 0 sporadicBurstNoise  # 0x3a238200 -> 0x2ce004ba
1 0 5 2 33 0 sporadicBurstNoise  # 0x3a28a100 -> 0x2ca84224
1 0 5 2 34 0 sporadicBurstNoise  # 0x3a28a200 -> 0x2ca84424
1 0 5 2 35 0 sporadicBurstNoise  # 0x3a28a300 -> 0x2ca84624
1 0 9 12 28 0 sporadicBurstNoise  # 0x3a4d9c00 -> 0x2cb40010
1 0 9 12 29 0 distorted sporadicBurstNoise  # 0x3a4d9d00 -> 0x2cb40210
1 0 9 12 30 0 sporadicBurstNoise  # 0x3a4d9e00 -> 0x2cb40410
1 0 9 12 31 0 sporadicBurstNoise  # 0x3a4d9f00 -> 0x2cb40610
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 sporadicBurstNoise  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 13 11 63 0 sporadicBurstNoise  # 0x3a6d3f00 -> 0x2cc425e0
1 0 13 11 115 0 sporadicBurstNoise  # 0x3a6d7300 -> 0x2cc427e0
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 3 41 0 sporadicBurstNoise  # 0x3aa92900 -> 0x2cc4036c
1 1 9 4 76 0 sporadicBurstNoise  # 0x3b49cc00 -> 0x2e4404e0
1 1 9 5 80 0 sporadicBurstNoise  # 0x3b4a5000 -> 0x2e282024
1 1 9 5 81 0 deadCalib sporadicBurstNoise  # 0x3b4a5100 -> 0x2e282224
1 1 9 5 82 0 sporadicBurstNoise  # 0x3b4a5200 -> 0x2e282424
1 1 9 13 26 0 sporadicBurstNoise  # 0x3b4e1a00 -> 0x2e60249c
1 1 9 13 27 0 sporadicBurstNoise  # 0x3b4e1b00 -> 0x2e60249e
1 1 9 13 30 0 sporadicBurstNoise  # 0x3b4e1e00 -> 0x2e60269c
1 1 9 13 31 0 sporadicBurstNoise  # 0x3b4e1f00 -> 0x2e60269e
1 1 9 14 54 0 sporadicBurstNoise  # 0x3b4eb600 -> 0x2e44509c
1 1 9 14 55 0 sporadicBurstNoise  # 0x3b4eb700 -> 0x2e44509e
1 1 9 14 58 0 highNoiseHG  # 0x3b4eba00 -> 0x2e44529c
1 1 9 14 59 0 sporadicBurstNoise  # 0x3b4ebb00 -> 0x2e44529e
1 1 9 14 62 0 highNoiseHG  # 0x3b4ebe00 -> 0x2e44549c
1 1 9 14 63 0 sporadicBurstNoise  # 0x3b4ebf00 -> 0x2e44549e
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 11 10 36 0 sporadicBurstNoise  # 0x3b5ca400 -> 0x2e4410d0
1 1 13 9 17 0 sporadicBurstNoise  # 0x3b6c1100 -> 0x2e601512
1 1 13 11 109 0 sporadicBurstNoise  # 0x3b6d6d00 -> 0x2e442d12
1 1 13 12 32 0 sporadicBurstNoise  # 0x3b6da000 -> 0x2e442f10
1 1 13 12 33 0 sporadicBurstNoise  # 0x3b6da100 -> 0x2e442f12
1 1 13 12 34 0 sporadicBurstNoise  # 0x3b6da200 -> 0x2e442f14
1 1 13 12 35 0 sporadicBurstNoise  # 0x3b6da300 -> 0x2e442f16
1 1 13 12 37 0 sporadicBurstNoise  # 0x3b6da500 -> 0x2e443112
1 1 13 12 41 0 sporadicBurstNoise  # 0x3b6da900 -> 0x2e443312
1 1 15 13 115 0 sporadicBurstNoise  # 0x3b7e7300 -> 0x2e602576
1 1 15 13 119 0 sporadicBurstNoise  # 0x3b7e7700 -> 0x2e602776
1 1 15 15 103 0 sporadicBurstNoise  # 0x3b7f6700 -> 0x2e445176
1 1 15 15 107 0 sporadicBurstNoise  # 0x3b7f6b00 -> 0x2e445376
1 1 15 15 111 0 sporadicBurstNoise  # 0x3b7f6f00 -> 0x2e445576
1 1 15 15 124 0 sporadicBurstNoise  # 0x3b7f7c00 -> 0x2e445578
1 1 21 2 68 0 sporadicBurstNoise  # 0x3ba8c400 -> 0x2e200072
1 1 21 2 70 0 sporadicBurstNoise  # 0x3ba8c600 -> 0x2e240272
1 1 21 3 66 0 sporadicBurstNoise  # 0x3ba94200 -> 0x2e4001a4
1 1 21 3 70 0 sporadicBurstNoise  # 0x3ba94600 -> 0x2e4401a4
1 1 21 4 18 0 sporadicBurstNoise  # 0x3ba99200 -> 0x2e4001cc
1 1 21 4 22 0 sporadicBurstNoise  # 0x3ba99600 -> 0x2e4401cc
1 1 21 4 26 0 sporadicBurstNoise  # 0x3ba99a00 -> 0x2e4403cc
1 1 21 4 116 0 sporadicBurstNoise  # 0x3ba9f400 -> 0x2e4401f8
1 1 21 11 67 0 sporadicBurstNoise  # 0x3bad4300 -> 0x2e4407e6

