== Noisy cells update for run 438323 (Mon, 7 Nov 2022 17:27:36 +0000) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1307_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:1
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:1
SBN in PS:1
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600

== Event Veto for run 438323 (Sat, 5 Nov 2022 03:02:17 +0100) ==
Found Noise or data corruption in run 438323
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438323_Main.db

Found project tag data22_13p6TeV for run 438323
Found 5 Veto Ranges with 10858 events
Found 9 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438323_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438323
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438323_Main.db;dbname=CONDBR2
Found a total of 5 corruption periods, covering a total of 9.21 seconds
Lumi loss due to corruption: 170.22 nb-1 out of 133689.80 nb-1 (1.27 per-mil)
Overall Lumi loss is 170.21895124381638 by 9.2076107 s of veto length



== Event Veto for run 438323 (Sat, 5 Nov 2022 02:32:40 +0100) ==
Found Noise or data corruption in run 438323
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto438323_Main.db

Found project tag data22_13p6TeV for run 438323
Found 5 Veto Ranges with 10858 events
Found 9 isolated events
Reading event veto info from db sqlite://;schema=EventVeto438323_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 438323
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto438323_Main.db;dbname=CONDBR2
Found a total of 5 corruption periods, covering a total of 9.21 seconds
Lumi loss due to corruption: 170.22 nb-1 out of 133689.80 nb-1 (1.27 per-mil)
Overall Lumi loss is 170.21895124381638 by 9.2076107 s of veto length



== Missing EVs for run 438323 (Mon, 31 Oct 2022 12:16:02 +0100) ==

Found a total of 8 noisy periods, covering a total of 0.01 seconds
Found a total of 7 Mini noise periods, covering a total of 0.01 seconds
Lumi loss due to noise-bursts: 0.15 nb-1 out of 133689.80 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.13 nb-1 out of 133689.80 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.28229732188402346 by 0.01536123 s of veto length


