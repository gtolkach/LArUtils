Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1087

== Noisy cells update for run 450360 (Tue, 25 Apr 2023 09:46:26 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:15
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:11
SBN in PS:0
HNHG:4
HNHG in PS:0

*****************
The treated cells were:
0 0 4 14 0 0 sporadicBurstNoise  # 0x38268000 -> 0x2d4060be
0 0 4 14 2 0 sporadicBurstNoise  # 0x38268200 -> 0x2d4060ba
0 0 4 14 3 0 sporadicBurstNoise  # 0x38268300 -> 0x2d4060b8
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 14 23 0 sporadicBurstNoise  # 0x38c69700 -> 0x2d406370
0 1 14 13 110 0 sporadicBurstNoise  # 0x39766e00 -> 0x2dc056ec
0 1 14 13 114 0 sporadicBurstNoise  # 0x39767200 -> 0x2dc058ec
1 0 3 5 64 0 highNoiseHG  # 0x3a1a4000 -> 0x3001b000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 sporadicBurstNoise  # 0x3a53fa00 -> 0x311c1000
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 19 10 109 0 sporadicBurstNoise  # 0x3a9ced00 -> 0x2cc41d6c
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000

