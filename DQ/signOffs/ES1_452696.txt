Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1158

== Noisy cells update for run 452696 (Tue, 23 May 2023 10:31:04 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x743_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:7
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:5
SBN in PS:0
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 21 2 72 0 sporadicBurstNoise  # 0x3aa8c800 -> 0x2ca0004a
1 0 21 4 35 0 sporadicBurstNoise  # 0x3aa9a300 -> 0x2cc00128
1 0 21 4 39 0 sporadicBurstNoise  # 0x3aa9a700 -> 0x2cc40128

