Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1002

== Noisy cells update for run 448715 (Mon, 10 Apr 2023 12:44:03 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x725_h414)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 15 events</p>
Flagged cells:8
Flagged in PS:0
Unflagged by DQ shifters:0
SBN:1
SBN in PS:0
HNHG:7
HNHG in PS:0

*****************
The treated cells were:
1 0 10 5 64 0 highNoiseHG # 0x3a524000
1 0 3 6 8 0 highNoiseHG # 0x3a1a8800
1 1 10 6 42 0 highNoiseHG # 0x3b52aa00
1 0 3 6 17 0 highNoiseHG # 0x3a1a9100
1 0 3 6 115 0 highNoiseHG # 0x3a1af300
1 0 2 11 1 0 sporadicBurstNoise # 0x3a150100
1 0 16 10 106 0 highNoiseHG # 0x3a84ea00
0 1 23 5 82 0 highNoiseHG # 0x39ba5200

