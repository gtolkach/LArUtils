Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1086

== Noisy cells update for run 450427 (Tue, 25 Apr 2023 13:55:32 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:larcommeos / Processing version:1551 (Unknown)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:3
Flagged in PS:0
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:1
SBN in PS:0
HNHG:2
HNHG in PS:0

*****************
The treated cells were:
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 94 0 distorted highNoiseHG  # 0x3a53de00 -> 0x31140000
1 0 10 8 122 0 sporadicBurstNoise  # 0x3a53fa00 -> 0x311c1000

