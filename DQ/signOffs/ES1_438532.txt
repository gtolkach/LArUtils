== Noisy cells update for run 438532 (Fri, 4 Nov 2022 15:15:59 +0000) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:134
Flagged in PS:40
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:117
SBN in PS:31
HNHG:17
HNHG in PS:9

*****************
The treated cells were:
0 0 30 9 119 0 sporadicBurstNoise # 0x38f47700
0 0 30 12 111 0 sporadicBurstNoise # 0x38f5ef00
0 0 30 12 107 0 highNoiseHG # 0x38f5eb00
0 0 30 9 118 0 highNoiseHG # 0x38f47600
0 0 30 9 115 0 sporadicBurstNoise # 0x38f47300
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 14 2 29 0 highNoiseHG # 0x38709d00
0 1 31 10 21 0 sporadicBurstNoise # 0x39fc9500
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 highNoiseHG # 0x38480500
1 1 9 4 10 0 sporadicBurstNoise # 0x3b498a00
0 0 7 10 40 0 sporadicBurstNoise # 0x383ca800
0 0 7 14 36 0 highNoiseHG # 0x383ea400
0 0 7 14 32 0 highNoiseHG # 0x383ea000
0 0 7 10 38 0 sporadicBurstNoise # 0x383ca600
0 0 25 2 5 0 sporadicBurstNoise # 0x38c88500
0 0 25 2 6 0 sporadicBurstNoise # 0x38c88600
0 1 24 13 27 0 sporadicBurstNoise # 0x39c61b00
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 1 8 13 12 0 sporadicBurstNoise # 0x39460c00
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
0 1 25 5 113 0 sporadicBurstNoise # 0x39ca7100
0 1 25 5 115 0 sporadicBurstNoise # 0x39ca7300
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 1 2 13 83 0 sporadicBurstNoise # 0x39165300
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
1 0 9 3 88 0 sporadicBurstNoise # 0x3a495800
0 0 10 9 78 0 sporadicBurstNoise # 0x38544e00
0 1 7 13 66 0 sporadicBurstNoise # 0x393e4200
0 1 8 10 60 0 sporadicBurstNoise # 0x3944bc00
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 22 0 highNoiseHG # 0x38681600
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 0 11 12 99 0 sporadicBurstNoise # 0x385de300
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 28 2 102 0 sporadicBurstNoise # 0x39e0e600
0 1 19 11 56 0 sporadicBurstNoise # 0x399d3800
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 1 27 12 41 0 sporadicBurstNoise # 0x39dda900
0 1 11 12 78 0 sporadicBurstNoise # 0x395dce00
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 30 1 15 0 sporadicBurstNoise # 0x38f00f00
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 1 8 1 77 0 sporadicBurstNoise # 0x39404d00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 0 15 13 115 0 sporadicBurstNoise # 0x387e7300
0 0 24 13 73 0 sporadicBurstNoise # 0x38c64900
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 0 7 12 89 0 sporadicBurstNoise # 0x383dd900
0 1 28 2 101 0 sporadicBurstNoise # 0x39e0e500
0 1 28 2 103 0 sporadicBurstNoise # 0x39e0e700
0 1 28 3 69 0 sporadicBurstNoise # 0x39e14500
0 1 28 3 70 0 sporadicBurstNoise # 0x39e14600
1 0 10 9 22 0 highNoiseHG # 0x3a541600
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700
0 0 10 11 90 0 sporadicBurstNoise # 0x38555a00
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 0 11 1 28 0 sporadicBurstNoise # 0x38581c00
0 0 11 1 15 0 sporadicBurstNoise # 0x38580f00
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 19 11 52 0 sporadicBurstNoise # 0x399d3400
0 1 19 11 60 0 sporadicBurstNoise # 0x399d3c00
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 28 3 68 0 sporadicBurstNoise # 0x39e14400
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 highNoiseHG # 0x39802b00
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 7 12 68 0 sporadicBurstNoise # 0x393dc400
0 0 20 1 4 0 highNoiseHG # 0x38a00400
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 1 1 1 14 0 sporadicBurstNoise # 0x39080e00
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00
1 0 10 7 10 0 highNoiseHG # 0x3a530a00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 10 11 91 0 sporadicBurstNoise # 0x38555b00
0 0 30 1 98 0 sporadicBurstNoise # 0x38f06200
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 0 5 10 103 0 sporadicBurstNoise # 0x382ce700
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 0 26 1 81 0 sporadicBurstNoise # 0x38d05100
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 0 28 1 14 0 highNoiseHG # 0x38e00e00
0 0 28 1 15 0 sporadicBurstNoise # 0x38e00f00
0 0 5 14 18 0 sporadicBurstNoise # 0x382e9200
0 1 2 13 72 0 sporadicBurstNoise # 0x39164800
0 0 4 1 14 0 sporadicBurstNoise # 0x38200e00
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
1 0 4 9 7 0 sporadicBurstNoise # 0x3a240700
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 1 20 14 38 0 sporadicBurstNoise # 0x39a6a600
0 1 20 10 42 0 sporadicBurstNoise # 0x39a4aa00
0 0 16 8 40 0 sporadicBurstNoise # 0x3883a800

