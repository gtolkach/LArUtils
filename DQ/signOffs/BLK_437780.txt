== Noisy cells update for run 437780 (Thu, 3 Nov 2022 16:49:51 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:test / Processing version:31122 (Unknown)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:26
Flagged in PS:7
Unflagged by DQ shifters:2
Changed to SBN:1
Changed to HNHG:3
SBN:19
SBN in PS:5
HNHG:7
HNHG in PS:2

*****************
The treated cells were:
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
1 0 3 7 62 0 highNoiseHG reflaggedByLADIeS # 0x3a1b3e00
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 0 6 14 44 0 highNoiseHG # 0x3836ac00
0 0 6 14 63 0 sporadicBurstNoise # 0x3836bf00
0 0 6 14 45 0 sporadicBurstNoise # 0x3836ad00
0 0 6 14 62 0 sporadicBurstNoise # 0x3836be00
0 1 0 7 47 0 sporadicBurstNoise # 0x39032f00
0 0 6 14 55 0 sporadicBurstNoise # 0x3836b700
0 0 6 14 40 0 sporadicBurstNoise # 0x3836a800
0 0 6 14 59 0 sporadicBurstNoise # 0x3836bb00
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise reflaggedByLADIeS # 0x38280600
0 0 6 14 51 0 unflaggedByLADIeS # 0x3836b300
0 0 6 14 36 0 unflaggedByLADIeS # 0x3836a400
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 0 21 11 65 0 sporadicBurstNoise # 0x38ad4100
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 5 1 6 0 highNoiseHG # 0x38280600
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 0 7 14 41 0 sporadicBurstNoise # 0x383ea900
0 1 17 6 32 0 sporadicBurstNoise # 0x398aa000
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 8 1 76 0 sporadicBurstNoise # 0x39404c00
0 1 8 1 71 0 sporadicBurstNoise # 0x39404700
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00

== Event Veto for run 437780 (Mon, 31 Oct 2022 23:37:29 +0100) ==
Found Noise or data corruption in run 437780
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437780_Main.db

Found project tag data22_13p6TeV for run 437780
Found 9 Veto Ranges with 2668 events
Found 32 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437780_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437780
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437780_Main.db;dbname=CONDBR2
Found a total of 9 corruption periods, covering a total of 6.10 seconds
Lumi loss due to corruption: 102.45 nb-1 out of 485127.43 nb-1 (0.21 per-mil)
Overall Lumi loss is 102.4452303965976 by 6.102175365 s of veto length



== Event Veto for run 437780 (Mon, 31 Oct 2022 22:16:36 +0100) ==
Found Noise or data corruption in run 437780
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437780_Main.db

Found project tag data22_13p6TeV for run 437780
Found 9 Veto Ranges with 2668 events
Found 32 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437780_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437780
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437780_Main.db;dbname=CONDBR2
Found a total of 9 corruption periods, covering a total of 6.10 seconds
Lumi loss due to corruption: 102.45 nb-1 out of 485127.43 nb-1 (0.21 per-mil)
Overall Lumi loss is 102.4452303965976 by 6.102175365 s of veto length



== Missing EVs for run 437780 (Tue, 25 Oct 2022 02:17:52 +0200) ==

Found a total of 4 noisy periods, covering a total of 0.00 seconds
Found a total of 134 Mini noise periods, covering a total of 0.13 seconds
Lumi loss due to noise-bursts: 0.06 nb-1 out of 485127.43 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 1.85 nb-1 out of 485127.43 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.905646806508081 by 0.13848818 s of veto length


