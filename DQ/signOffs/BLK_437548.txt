== Noisy cells update for run 437548 (Tue, 1 Nov 2022 19:27:20 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:4 (f1302_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:49
Flagged in PS:9
Unflagged by DQ shifters:37
Changed to SBN:0
Changed to HNHG:3
SBN:34
SBN in PS:6
HNHG:15
HNHG in PS:3

*****************
The treated cells were:
Cells requiring masking in run: BEC Side FT SL CH CL flag # onlid
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
1 0 3 7 62 0 highNoiseHG reflaggedByLADIeS # 0x3a1b3e00
1 0 9 4 11 0 sporadicBurstNoise # 0x3a498b00
1 0 3 7 46 0 highNoiseHG reflaggedByLADIeS # 0x3a1b2e00
1 0 21 2 89 0 highNoiseHG # 0x3aa8d900
1 0 21 2 90 0 highNoiseHG # 0x3aa8da00
1 0 21 2 94 0 highNoiseHG # 0x3aa8de00
1 0 21 4 114 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa9f200
1 0 21 4 97 0 unflaggedByLADIeS # 0x3aa9e100
1 0 21 4 98 0 unflaggedByLADIeS # 0x3aa9e200
1 0 21 4 113 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa9f100
1 0 21 2 88 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8d800
1 0 21 2 93 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8dd00
1 0 21 4 121 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa9f900
1 0 21 4 115 0 unflaggedByLADIeS # 0x3aa9f300
1 0 21 4 117 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa9f500
1 0 21 4 104 0 unflaggedByLADIeS # 0x3aa9e800
1 0 21 4 101 0 unflaggedByLADIeS # 0x3aa9e500
1 0 21 4 102 0 unflaggedByLADIeS # 0x3aa9e600
1 0 21 4 105 0 unflaggedByLADIeS # 0x3aa9e900
1 0 21 4 119 0 unflaggedByLADIeS # 0x3aa9f700
1 0 21 4 96 0 unflaggedByLADIeS # 0x3aa9e000
1 0 21 4 106 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa9ea00
1 0 21 4 112 0 unflaggedByLADIeS # 0x3aa9f000
1 0 21 4 100 0 unflaggedByLADIeS # 0x3aa9e400
1 0 21 4 107 0 unflaggedByLADIeS # 0x3aa9eb00
1 0 21 4 118 0 unflaggedByLADIeS # 0x3aa9f600
1 0 21 4 123 0 unflaggedByLADIeS # 0x3aa9fb00
1 0 21 4 103 0 unflaggedByLADIeS # 0x3aa9e700
1 0 21 4 120 0 unflaggedByLADIeS # 0x3aa9f800
1 0 21 4 116 0 unflaggedByLADIeS # 0x3aa9f400
1 0 21 2 92 0 unflaggedByLADIeS # 0x3aa8dc00
1 0 21 4 122 0 unflaggedByLADIeS # 0x3aa9fa00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
1 0 21 2 84 0 sporadicBurstNoise # 0x3aa8d400
1 0 21 2 86 0 unflaggedByLADIeS # 0x3aa8d600
1 0 21 2 80 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8d000
1 0 21 2 85 0 unflaggedByLADIeS # 0x3aa8d500
1 0 21 2 76 0 unflaggedByLADIeS # 0x3aa8cc00
1 0 21 2 72 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8c800
1 0 21 2 82 0 highNoiseHG # 0x3aa8d200
1 0 21 2 81 0 highNoiseHG # 0x3aa8d100
1 0 21 2 73 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8c900
1 0 21 2 77 0 unflaggedByLADIeS # 0x3aa8cd00
1 0 21 2 74 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8ca00
1 0 21 2 78 0 unflaggedByLADIeS # 0x3aa8ce00
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 highNoiseHG # 0x38280600
1 0 21 2 70 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8c600
1 0 21 2 69 0 unflaggedByLADIeS # 0x3aa8c500
1 0 21 2 68 0 unflaggedByLADIeS # 0x3aa8c400
1 0 21 4 99 0 unflaggedByLADIeS # 0x3aa9e300
1 0 21 4 111 0 unflaggedByLADIeS # 0x3aa9ef00
1 0 21 4 109 0 unflaggedByLADIeS # 0x3aa9ed00
1 0 21 4 108 0 unflaggedByLADIeS # 0x3aa9ec00
1 0 21 4 110 0 unflaggedByLADIeS # 0x3aa9ee00
1 0 21 4 124 0 unflaggedByLADIeS # 0x3aa9fc00
1 0 21 4 83 0 unflaggedByLADIeS # 0x3aa9d300
1 0 21 4 91 0 unflaggedByLADIeS # 0x3aa9db00
1 0 21 4 87 0 unflaggedByLADIeS # 0x3aa9d700
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
1 0 9 4 15 0 highNoiseHG # 0x3a498f00
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 0 5 1 4 0 sporadicBurstNoise # 0x38280400
0 0 20 1 4 0 highNoiseHG # 0x38a00400
1 0 21 2 64 0 highNoiseHG # 0x3aa8c000
1 0 21 2 66 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8c200
1 0 21 2 65 0 sporadicBurstNoise unflaggedByLADIeS # 0x3aa8c100
0 0 16 1 78 0 sporadicBurstNoise # 0x38804e00
1 0 9 4 7 0 highNoiseHG # 0x3a498700
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
1 0 21 4 95 0 unflaggedByLADIeS # 0x3aa9df00
1 0 21 4 89 0 unflaggedByLADIeS # 0x3aa9d900
1 0 21 3 41 0 sporadicBurstNoise # 0x3aa92900
0 0 26 1 82 0 sporadicBurstNoise # 0x38d05200
0 1 3 1 65 0 sporadicBurstNoise # 0x39184100
0 1 10 13 30 0 sporadicBurstNoise # 0x39561e00
0 0 25 2 5 0 sporadicBurstNoise # 0x38c88500
0 0 25 2 6 0 sporadicBurstNoise # 0x38c88600
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
1 0 16 10 106 0 highNoiseHG # 0x3a84ea00
0 0 16 8 41 0 sporadicBurstNoise # 0x3883a900
0 0 15 11 23 0 sporadicBurstNoise # 0x387d1700
1 0 2 4 48 0 sporadicBurstNoise # 0x3a11b000
0 0 0 14 43 0 sporadicBurstNoise # 0x3806ab00

== Event Veto for run 437548 (Fri, 28 Oct 2022 08:22:16 +0200) ==
Found Noise or data corruption in run 437548
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437548_Main.db

Found project tag data22_13p6TeV for run 437548
Found 18 Veto Ranges with 1755 events
Found 38 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437548_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437548
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437548_Main.db;dbname=CONDBR2
Found a total of 18 corruption periods, covering a total of 10.10 seconds
Lumi loss due to corruption: 171.90 nb-1 out of 608238.52 nb-1 (0.28 per-mil)
Overall Lumi loss is 171.903590528503 by 10.096918165 s of veto length



== Event Veto for run 437548 (Fri, 28 Oct 2022 06:05:13 +0200) ==
Found Noise or data corruption in run 437548
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto437548_Main.db

Found project tag data22_13p6TeV for run 437548
Found 18 Veto Ranges with 1755 events
Found 38 isolated events
Reading event veto info from db sqlite://;schema=EventVeto437548_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 437548
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto437548_Main.db;dbname=CONDBR2
Found a total of 18 corruption periods, covering a total of 10.10 seconds
Lumi loss due to corruption: 171.90 nb-1 out of 608238.52 nb-1 (0.28 per-mil)
Overall Lumi loss is 171.903590528503 by 10.096918165 s of veto length



== Missing EVs for run 437548 (Sat, 22 Oct 2022 13:18:25 +0200) ==

Found a total of 17 noisy periods, covering a total of 0.02 seconds
Found a total of 474 Mini noise periods, covering a total of 0.47 seconds
Lumi loss due to noise-bursts: 0.27 nb-1 out of 608238.52 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 7.01 nb-1 out of 608238.52 nb-1 (0.01 per-mil)
Overall Lumi loss is 7.283251940858508 by 0.49149276000000003 s of veto length


