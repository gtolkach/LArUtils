== Noisy cells update for run 437944 (Thu, 27 Oct 2022 09:45:43 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x706_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:27
Flagged in PS:9
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:23
SBN in PS:7
HNHG:4
HNHG in PS:2

*****************
The treated cells were:
0 0 0 14 43 0 sporadicBurstNoise # 0x3806ab00
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 0 27 1 64 0 sporadicBurstNoise # 0x38d84000
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 8 1 17 0 sporadicBurstNoise unflaggedByLADIeS # 0x38401100
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 highNoiseHG # 0x383c5400
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
1 1 2 10 104 0 sporadicBurstNoise # 0x3b14e800
0 0 5 1 0 0 sporadicBurstNoise # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 1 1 0 0 sporadicBurstNoise # 0x39080000
0 1 1 1 1 0 highNoiseHG # 0x39080100
1 0 15 10 38 0 sporadicBurstNoise # 0x3a7ca600
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700

