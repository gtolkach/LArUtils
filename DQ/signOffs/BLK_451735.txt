Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1125

== Noisy cells update for run 451735 (Fri, 26 May 2023 11:02:28 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1352_h421)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:24
Flagged in PS:7
Unflagged by DQ shifters:0
Changed to SBN:8
Changed to HNHG:0
SBN:22
SBN in PS:6
HNHG:2
HNHG in PS:1

*****************
The treated cells were:
0 0 7 14 4 0 sporadicBurstNoise  # 0x383e8400 -> 0x2d40628e
0 0 12 11 51 0 sporadicBurstNoise  # 0x38653300 -> 0x2d401838
0 0 13 1 5 0 sporadicBurstNoise  # 0x38680500 -> 0x2d000208
0 0 13 2 65 0 sporadicBurstNoise  # 0x3868c100 -> 0x2d200208
0 0 13 2 68 0 sporadicBurstNoise  # 0x3868c400 -> 0x2d200808
0 0 13 2 69 0 sporadicBurstNoise  # 0x3868c500 -> 0x2d200a08
0 0 13 2 70 0 sporadicBurstNoise  # 0x3868c600 -> 0x2d200c08
0 0 13 2 71 0 sporadicBurstNoise  # 0x3868c700 -> 0x2d200e08
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 5 1 72 0 sporadicBurstNoise  # 0x39284800 -> 0x2d804814
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500
1 1 12 4 15 0 highNoiseHG sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1125

== Event Veto for run 451735 (Sun, 21 May 2023 19:05:46 +0200) ==
Found Noise or data corruption in run 451735
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451735_Main.db

Found project tag data23_13p6TeV for run 451735
Found 1 Veto Ranges with 3 events
Found 16 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451735_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451735
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451735_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 0.50 seconds
Lumi loss due to corruption: 3.83 nb-1 out of 344337.93 nb-1 (0.01 per-mil)
Overall Lumi loss is 3.8253699586246346 by 0.50030143 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1125

== Event Veto for run 451735 (Sun, 21 May 2023 17:56:32 +0200) ==
Found Noise or data corruption in run 451735
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451735_Main.db

Found project tag data23_13p6TeV for run 451735
Found 1 Veto Ranges with 3 events
Found 16 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451735_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451735
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451735_Main.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 0.50 seconds
Lumi loss due to corruption: 3.83 nb-1 out of 344337.93 nb-1 (0.01 per-mil)
Overall Lumi loss is 3.8253699586246346 by 0.50030143 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1125

== Missing EVs for run 451735 (Wed, 10 May 2023 06:18:50 +0200) ==

Found a total of 241 noisy periods, covering a total of 0.29 seconds
Found a total of 880 Mini noise periods, covering a total of 0.84 seconds
Lumi loss due to noise-bursts: 2.05 nb-1 out of 344337.93 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 6.30 nb-1 out of 344337.93 nb-1 (0.02 per-mil)
Overall Lumi loss is 8.35085501979204 by 1.12864554 s of veto length


