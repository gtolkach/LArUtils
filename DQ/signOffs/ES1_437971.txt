== Noisy cells update for run 437971 (Thu, 27 Oct 2022 12:14:20 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x706_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:143
Flagged in PS:43
Unflagged by DQ shifters:2
Changed to SBN:0
Changed to HNHG:0
SBN:122
SBN in PS:31
HNHG:21
HNHG in PS:12

*****************
The treated cells were:
0 0 0 14 43 0 sporadicBurstNoise # 0x3806ab00
0 1 12 1 4 0 sporadicBurstNoise # 0x39600400
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
0 0 27 1 64 0 sporadicBurstNoise # 0x38d84000
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 0 22 6 117 0 sporadicBurstNoise # 0x38b2f500
0 0 22 6 119 0 sporadicBurstNoise # 0x38b2f700
0 1 10 13 33 0 sporadicBurstNoise # 0x39562100
0 1 10 13 37 0 highNoiseHG # 0x39562500
0 1 10 13 32 0 sporadicBurstNoise # 0x39562000
0 1 10 13 36 0 sporadicBurstNoise # 0x39562400
0 1 10 13 35 0 sporadicBurstNoise # 0x39562300
0 1 10 13 34 0 unflaggedByLADIeS # 0x39562200
0 1 10 13 38 0 unflaggedByLADIeS # 0x39562600
0 1 10 13 42 0 sporadicBurstNoise # 0x39562a00
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 sporadicBurstNoise # 0x388c9500
0 0 10 9 78 0 sporadicBurstNoise # 0x38544e00
0 0 10 11 90 0 sporadicBurstNoise # 0x38555a00
1 1 2 10 104 0 sporadicBurstNoise # 0x3b14e800
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 1 3 8 30 0 sporadicBurstNoise # 0x391b9e00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 8 1 17 0 sporadicBurstNoise unflaggedByLADIeS # 0x38401100
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 highNoiseHG # 0x383c5400
0 0 7 9 21 0 sporadicBurstNoise # 0x383c1500
0 0 7 9 25 0 sporadicBurstNoise # 0x383c1900
0 0 7 9 26 0 highNoiseHG # 0x383c1a00
0 0 7 9 22 0 sporadicBurstNoise # 0x383c1600
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 1 1 0 0 sporadicBurstNoise # 0x39080000
0 1 1 1 1 0 highNoiseHG # 0x39080100
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 11 1 23 0 highNoiseHG # 0x38581700
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 14 11 122 0 sporadicBurstNoise unflaggedByLADIeS # 0x39757a00
0 1 14 11 120 0 sporadicBurstNoise unflaggedByLADIeS # 0x39757800
0 1 14 11 121 0 highNoiseHG # 0x39757900
0 1 14 11 123 0 sporadicBurstNoise # 0x39757b00
0 1 10 10 22 0 highNoiseHG # 0x39549600
0 1 10 13 39 0 sporadicBurstNoise # 0x39562700
0 1 10 1 93 0 sporadicBurstNoise # 0x39505d00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 1 23 8 108 0 sporadicBurstNoise # 0x39bbec00
0 0 28 1 14 0 highNoiseHG # 0x38e00e00
0 0 28 1 15 0 sporadicBurstNoise # 0x38e00f00
0 0 29 11 10 0 sporadicBurstNoise # 0x38ed0a00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 1 8 1 70 0 highNoiseHG # 0x39404600
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 0 17 13 67 0 sporadicBurstNoise # 0x388e4300
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 1 31 10 21 0 sporadicBurstNoise # 0x39fc9500
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 0 7 9 20 0 sporadicBurstNoise # 0x383c1400
0 0 6 1 12 0 sporadicBurstNoise # 0x38300c00
0 1 28 11 84 0 sporadicBurstNoise # 0x39e55400
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise unflaggedByLADIeS # 0x39802b00
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 14 1 66 0 sporadicBurstNoise # 0x38704200
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 0 22 10 86 0 sporadicBurstNoise # 0x38b4d600
0 0 22 10 87 0 highNoiseHG # 0x38b4d700
0 0 23 7 11 0 sporadicBurstNoise # 0x38bb0b00
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 17 6 31 0 sporadicBurstNoise # 0x398a9f00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 5 13 114 0 highNoiseHG # 0x382e7200
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
0 0 26 1 81 0 highNoiseHG # 0x38d05100
0 0 9 10 24 0 sporadicBurstNoise # 0x384c9800
0 0 30 1 103 0 sporadicBurstNoise # 0x38f06700
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 6 12 64 0 sporadicBurstNoise # 0x3935c000
0 1 7 9 24 0 sporadicBurstNoise # 0x393c1800
0 1 7 9 25 0 highNoiseHG # 0x393c1900
0 0 20 1 4 0 highNoiseHG # 0x38a00400
0 0 21 11 65 0 sporadicBurstNoise # 0x38ad4100
0 1 28 12 6 0 sporadicBurstNoise # 0x39e58600
1 0 13 11 115 0 sporadicBurstNoise # 0x3a6d7300
1 0 13 11 63 0 sporadicBurstNoise # 0x3a6d3f00
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 1 11 12 60 0 sporadicBurstNoise # 0x395dbc00
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 0 22 1 1 0 sporadicBurstNoise # 0x38b00100
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 0 25 1 29 0 sporadicBurstNoise # 0x38c81d00
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
1 0 21 3 41 0 sporadicBurstNoise # 0x3aa92900
0 1 21 12 2 0 sporadicBurstNoise # 0x39ad8200
0 0 31 1 101 0 sporadicBurstNoise # 0x38f86500
0 0 16 8 40 0 sporadicBurstNoise # 0x3883a800
0 0 16 8 41 0 sporadicBurstNoise # 0x3883a900
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
0 0 27 1 53 0 sporadicBurstNoise # 0x38d83500
0 0 27 12 50 0 sporadicBurstNoise # 0x38ddb200
0 1 13 12 65 0 sporadicBurstNoise # 0x396dc100
0 1 13 12 69 0 sporadicBurstNoise # 0x396dc500
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 0 15 13 111 0 sporadicBurstNoise # 0x387e6f00
0 1 23 13 33 0 sporadicBurstNoise # 0x39be2100
0 0 27 1 66 0 highNoiseHG # 0x38d84200
0 1 26 11 36 0 sporadicBurstNoise # 0x39d52400
0 0 15 12 7 0 sporadicBurstNoise # 0x387d8700
0 0 5 14 18 0 sporadicBurstNoise # 0x382e9200

