Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1131

== Noisy cells update for run 451866 (Sat, 27 May 2023 17:47:16 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:25
Flagged in PS:8
Unflagged by DQ shifters:0
Changed to SBN:15
Changed to HNHG:0
SBN:25
SBN in PS:8
HNHG:0
HNHG in PS:0

*****************
The treated cells were:
0 0 5 1 5 0 highNoiseHG sporadicBurstNoise  # 0x38280500 -> 0x2d000228
0 0 15 10 98 0 sporadicBurstNoise  # 0x387ce200 -> 0x2d603002
0 0 15 13 111 0 sporadicBurstNoise  # 0x387e6f00 -> 0x2d405600
0 0 15 13 115 0 sporadicBurstNoise  # 0x387e7300 -> 0x2d405800
0 0 15 14 18 0 sporadicBurstNoise  # 0x387e9200 -> 0x2d406002
0 1 8 1 77 0 sporadicBurstNoise  # 0x39404d00 -> 0x2d804a22
0 1 8 1 78 0 sporadicBurstNoise  # 0x39404e00 -> 0x2d804c22
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
1 0 21 13 121 0 sporadicBurstNoise  # 0x3aae7900 -> 0x2ce02504
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 125 0 sporadicBurstNoise  # 0x3aae7d00 -> 0x2ce02704
1 0 21 13 126 0 sporadicBurstNoise  # 0x3aae7e00 -> 0x2ce02702
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 118 0 sporadicBurstNoise  # 0x3aaf7600 -> 0x2cc45102
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 0 21 15 122 0 sporadicBurstNoise  # 0x3aaf7a00 -> 0x2cc45302
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 126 0 sporadicBurstNoise  # 0x3aaf7e00 -> 0x2cc45502
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1131

== Event Veto for run 451866 (Sun, 21 May 2023 14:39:00 +0200) ==
Found Noise or data corruption in run 451866
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451866_Main.db

Found project tag data23_13p6TeV for run 451866
Found 8 Veto Ranges with 5533 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451866_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451866
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451866_Main.db;dbname=CONDBR2
Found a total of 8 corruption periods, covering a total of 8.56 seconds
Lumi loss due to corruption: 113.14 nb-1 out of 327262.39 nb-1 (0.35 per-mil)
Overall Lumi loss is 113.14241842972808 by 8.555940515 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1131

== Event Veto for run 451866 (Sun, 21 May 2023 13:00:12 +0200) ==
Found Noise or data corruption in run 451866
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto451866_Main.db

Found project tag data23_13p6TeV for run 451866
Found 8 Veto Ranges with 5533 events
Found 18 isolated events
Reading event veto info from db sqlite://;schema=EventVeto451866_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 451866
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto451866_Main.db;dbname=CONDBR2
Found a total of 8 corruption periods, covering a total of 8.56 seconds
Lumi loss due to corruption: 113.14 nb-1 out of 327262.39 nb-1 (0.35 per-mil)
Overall Lumi loss is 113.14241842972808 by 8.555940515 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1131

== Missing EVs for run 451866 (Thu, 11 May 2023 20:19:52 +0200) ==

Found a total of 231 noisy periods, covering a total of 0.23 seconds
Found a total of 648 Mini noise periods, covering a total of 0.65 seconds
Lumi loss due to noise-bursts: 2.16 nb-1 out of 327262.39 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 7.66 nb-1 out of 327262.39 nb-1 (0.02 per-mil)
Overall Lumi loss is 9.825195166454781 by 0.87696025 s of veto length


