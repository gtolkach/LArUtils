== Noisy cells update for run 438446 (Fri, 4 Nov 2022 07:40:14 +0000) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x707_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:148
Flagged in PS:55
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:130
SBN in PS:44
HNHG:18
HNHG in PS:11

*****************
The treated cells were:
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 14 2 29 0 sporadicBurstNoise # 0x38709d00
0 0 7 14 50 0 sporadicBurstNoise # 0x383eb200
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 13 0 sporadicBurstNoise # 0x3b618d00
0 0 16 13 14 0 sporadicBurstNoise # 0x38860e00
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 highNoiseHG # 0x38480500
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 0 7 9 23 0 sporadicBurstNoise # 0x383c1700
0 0 7 9 84 0 highNoiseHG # 0x383c5400
0 0 15 13 115 0 sporadicBurstNoise # 0x387e7300
0 1 7 13 66 0 sporadicBurstNoise # 0x393e4200
0 1 7 12 68 0 sporadicBurstNoise # 0x393dc400
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 1 3 8 30 0 sporadicBurstNoise # 0x391b9e00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 highNoiseHG # 0x38280600
0 0 5 1 5 0 highNoiseHG # 0x38280500
0 1 1 8 65 0 sporadicBurstNoise # 0x390bc100
0 1 1 8 64 0 sporadicBurstNoise # 0x390bc000
0 1 1 8 66 0 sporadicBurstNoise # 0x390bc200
0 1 31 13 63 0 sporadicBurstNoise # 0x39fe3f00
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 8 1 77 0 sporadicBurstNoise # 0x39404d00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 8 10 60 0 sporadicBurstNoise # 0x3944bc00
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 9 1 5 0 sporadicBurstNoise # 0x39480500
0 1 1 1 14 0 sporadicBurstNoise # 0x39080e00
0 1 23 13 33 0 sporadicBurstNoise # 0x39be2100
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 25 1 29 0 sporadicBurstNoise # 0x38c81d00
0 0 30 1 15 0 sporadicBurstNoise # 0x38f00f00
0 1 14 11 120 0 sporadicBurstNoise # 0x39757800
0 1 14 11 122 0 sporadicBurstNoise # 0x39757a00
0 1 14 11 121 0 sporadicBurstNoise # 0x39757900
0 1 14 11 123 0 sporadicBurstNoise # 0x39757b00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 15 13 5 0 sporadicBurstNoise # 0x387e0500
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 0 17 1 29 0 sporadicBurstNoise # 0x38881d00
0 0 7 9 21 0 sporadicBurstNoise # 0x383c1500
0 0 7 9 22 0 sporadicBurstNoise # 0x383c1600
0 0 7 9 25 0 sporadicBurstNoise # 0x383c1900
0 0 6 1 13 0 sporadicBurstNoise # 0x38300d00
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 highNoiseHG # 0x39802b00
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 0 30 1 4 0 sporadicBurstNoise # 0x38f00400
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 30 1 5 0 sporadicBurstNoise # 0x38f00500
0 0 11 1 23 0 highNoiseHG # 0x38581700
0 0 11 1 15 0 sporadicBurstNoise # 0x38580f00
0 0 11 1 28 0 sporadicBurstNoise # 0x38581c00
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 21 10 39 0 sporadicBurstNoise # 0x38aca700
0 0 21 10 100 0 highNoiseHG # 0x38ace400
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
1 1 9 4 76 0 sporadicBurstNoise # 0x3b49cc00
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 1 28 3 69 0 sporadicBurstNoise # 0x39e14500
0 1 28 3 70 0 sporadicBurstNoise # 0x39e14600
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 25 12 40 0 sporadicBurstNoise # 0x38cda800
0 0 16 1 78 0 sporadicBurstNoise # 0x38804e00
0 1 4 1 93 0 sporadicBurstNoise # 0x39205d00
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 0 26 1 81 0 sporadicBurstNoise # 0x38d05100
0 0 20 1 4 0 highNoiseHG # 0x38a00400
0 0 20 1 5 0 sporadicBurstNoise # 0x38a00500
0 1 11 13 69 0 sporadicBurstNoise # 0x395e4500
0 1 8 9 67 0 sporadicBurstNoise # 0x39444300
0 0 28 1 82 0 sporadicBurstNoise # 0x38e05200
0 0 15 14 44 0 sporadicBurstNoise # 0x387eac00
0 0 22 12 54 0 sporadicBurstNoise # 0x38b5b600
0 1 23 8 109 0 sporadicBurstNoise # 0x39bbed00
0 0 7 10 40 0 sporadicBurstNoise # 0x383ca800
0 0 7 14 36 0 sporadicBurstNoise # 0x383ea400
0 0 7 14 32 0 sporadicBurstNoise # 0x383ea000
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 13 1 25 0 sporadicBurstNoise # 0x38681900
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 1 28 12 6 0 sporadicBurstNoise # 0x39e58600
1 0 10 7 10 0 highNoiseHG # 0x3a530a00
0 0 13 1 13 0 sporadicBurstNoise # 0x38680d00
0 0 13 1 22 0 sporadicBurstNoise # 0x38681600
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 0 23 11 0 0 sporadicBurstNoise # 0x38bd0000
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 7 12 72 0 sporadicBurstNoise # 0x393dc800
0 1 6 12 64 0 sporadicBurstNoise # 0x3935c000
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
0 0 30 1 98 0 sporadicBurstNoise # 0x38f06200
0 0 30 9 119 0 sporadicBurstNoise # 0x38f47700
0 1 16 11 93 0 sporadicBurstNoise # 0x39855d00
0 0 14 1 66 0 sporadicBurstNoise # 0x38704200
0 0 5 1 3 0 sporadicBurstNoise # 0x38280300
0 0 5 1 1 0 sporadicBurstNoise # 0x38280100
0 1 8 12 9 0 sporadicBurstNoise # 0x39458900
0 0 9 13 96 0 highNoiseHG # 0x384e6000
0 0 9 13 93 0 sporadicBurstNoise # 0x384e5d00
0 1 13 1 116 0 sporadicBurstNoise # 0x39687400
1 0 21 3 41 0 sporadicBurstNoise # 0x3aa92900
0 0 16 8 40 0 sporadicBurstNoise # 0x3883a800
0 1 13 12 65 0 sporadicBurstNoise # 0x396dc100
0 1 31 1 11 0 sporadicBurstNoise # 0x39f80b00
0 1 13 12 69 0 sporadicBurstNoise # 0x396dc500

