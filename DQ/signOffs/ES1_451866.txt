Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1131

== Noisy cells update for run 451866 (Sat, 13 May 2023 17:54:29 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x738_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 200 events</p>
Flagged cells:125
Flagged in PS:25
Unflagged by DQ shifters:1
SBN:107
SBN in PS:21
HNHG:18
HNHG in PS:4

*****************
The treated cells were:
1 0 10 8 94 0 highNoiseHG # 0x3a53de00
1 0 10 8 78 0 highNoiseHG # 0x3a53ce00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 12 4 14 0 sporadicBurstNoise # 0x3b618e00
1 1 12 4 16 0 sporadicBurstNoise # 0x3b619000
0 1 25 5 114 0 sporadicBurstNoise # 0x39ca7200
0 1 25 5 113 0 sporadicBurstNoise # 0x39ca7100
0 1 25 5 115 0 sporadicBurstNoise # 0x39ca7300
0 0 9 10 80 0 sporadicBurstNoise # 0x384cd000
0 0 9 13 96 0 highNoiseHG # 0x384e6000
0 1 11 13 69 0 sporadicBurstNoise # 0x395e4500
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00
0 0 17 2 13 0 sporadicBurstNoise # 0x38888d00
0 0 17 2 11 0 sporadicBurstNoise # 0x38888b00
0 1 7 12 19 0 sporadicBurstNoise # 0x393d9300
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 22 6 118 0 sporadicBurstNoise # 0x38b2f600
0 0 22 6 117 0 sporadicBurstNoise # 0x38b2f500
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
1 0 5 7 103 0 sporadicBurstNoise # 0x3a2b6700
0 0 11 1 23 0 sporadicBurstNoise # 0x38581700
0 0 11 1 21 0 sporadicBurstNoise # 0x38581500
1 0 10 5 94 0 highNoiseHG # 0x3a525e00
1 0 10 8 122 0 highNoiseHG # 0x3a53fa00
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
0 1 14 12 53 0 sporadicBurstNoise # 0x3975b500
0 0 21 10 39 0 sporadicBurstNoise # 0x38aca700
0 0 21 10 100 0 highNoiseHG # 0x38ace400
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
1 0 2 2 84 0 highNoiseHG # 0x3a10d400
0 0 23 2 6 0 sporadicBurstNoise # 0x38b88600
0 0 23 2 5 0 sporadicBurstNoise # 0x38b88500
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 20 6 78 0 sporadicBurstNoise # 0x39a2ce00
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 6 0 sporadicBurstNoise # 0x38280600
0 0 22 6 119 0 sporadicBurstNoise # 0x38b2f700
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 3 0 sporadicBurstNoise # 0x38b30300
0 0 26 14 60 0 sporadicBurstNoise # 0x38d6bc00
0 0 26 14 61 0 sporadicBurstNoise # 0x38d6bd00
0 0 26 14 62 0 sporadicBurstNoise # 0x38d6be00
0 0 26 14 63 0 sporadicBurstNoise # 0x38d6bf00
0 0 26 14 56 0 sporadicBurstNoise # 0x38d6b800
0 0 26 14 52 0 sporadicBurstNoise # 0x38d6b400
0 0 26 14 57 0 highNoiseHG # 0x38d6b900
0 0 26 14 53 0 &nbsp;unflaggedByLADIeS # 0x38d6b500
0 0 26 10 96 0 highNoiseHG # 0x38d4e000
0 0 26 10 100 0 sporadicBurstNoise # 0x38d4e400
0 0 26 14 59 0 sporadicBurstNoise # 0x38d6bb00
0 0 26 14 55 0 sporadicBurstNoise # 0x38d6b700
0 0 26 10 104 0 sporadicBurstNoise # 0x38d4e800
0 0 26 10 101 0 highNoiseHG # 0x38d4e500
0 0 26 14 24 0 highNoiseHG # 0x38d69800
0 0 26 10 97 0 sporadicBurstNoise reflaggedByLADIeS # 0x38d4e100
0 0 26 14 28 0 sporadicBurstNoise # 0x38d69c00
0 0 26 14 48 0 sporadicBurstNoise # 0x38d6b000
0 0 26 14 20 0 sporadicBurstNoise # 0x38d69400
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 18 1 14 0 sporadicBurstNoise # 0x38900e00
0 0 12 11 75 0 sporadicBurstNoise # 0x38654b00
0 0 13 11 4 0 highNoiseHG # 0x386d0400
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 sporadicBurstNoise reflaggedByLADIeS # 0x39802b00
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
1 1 9 4 16 0 sporadicBurstNoise # 0x3b499000
0 0 15 13 84 0 highNoiseHG # 0x387e5400
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 1 8 1 70 0 sporadicBurstNoise # 0x39404600
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 0 7 9 115 0 sporadicBurstNoise # 0x383c7300
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 24 0 sporadicBurstNoise # 0x38681800
0 0 17 10 20 0 sporadicBurstNoise # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
1 0 9 12 31 0 sporadicBurstNoise # 0x3a4d9f00
1 0 9 12 30 0 sporadicBurstNoise # 0x3a4d9e00
1 0 9 12 28 0 sporadicBurstNoise # 0x3a4d9c00
1 0 9 12 29 0 sporadicBurstNoise # 0x3a4d9d00
0 0 10 13 67 0 sporadicBurstNoise # 0x38564300
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 0 13 2 68 0 sporadicBurstNoise # 0x3868c400
0 0 13 2 69 0 sporadicBurstNoise # 0x3868c500
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
1 1 9 2 68 0 sporadicBurstNoise # 0x3b48c400
1 1 9 4 17 0 sporadicBurstNoise # 0x3b499100
1 1 9 4 20 0 sporadicBurstNoise # 0x3b499400
0 0 15 13 89 0 sporadicBurstNoise # 0x387e5900
0 0 15 13 88 0 sporadicBurstNoise # 0x387e5800
0 0 15 13 90 0 sporadicBurstNoise # 0x387e5a00
0 0 15 13 92 0 sporadicBurstNoise # 0x387e5c00
0 0 15 13 86 0 sporadicBurstNoise # 0x387e5600
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 9 1 23 0 sporadicBurstNoise # 0x39481700
1 1 9 4 76 0 sporadicBurstNoise # 0x3b49cc00

