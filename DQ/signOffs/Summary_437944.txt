** Defect LAR_HECA_NOISEBURST** 
In LB 48 - 50 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 55 - 56 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 95 - 96 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_FCALA_NOISEBURST** 
In LB 48 - 50 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 55 - 56 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 95 - 96 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
** Defect LAR_EMECA_NOISEBURST** 
In LB 48 - 50 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 55 - 56 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
In LB 95 - 96 with comment: Between 1 and 1000 events flagged as LArNoisyRO_Std in EndCapA/physics_Main - Cleaned by time veto. NOT RECOVERABLE.
