import sys
import glob


def getRootFile(run, stream, camp, express=True):
    if express:
        amistr = "x"
    else:
        amistr = "f"
    rootpath = f"/eos/atlas/atlastier0/rucio/{camp}/{stream}/00{run}/{camp}.*.{stream}.merge.HIST.{amistr}*/{camp}.*.{stream}.merge.HIST.{amistr}*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    if len(rootfiles) == 0:
        print("NO ROOTFILES FOUND")
        sys.exit()
    amis = sorted(list([ r.split(".")[-3] for r in rootfiles]))
    newest = [ r for r in rootfiles if "."+amis[-1]+"." in r]
    ami = amis[-1]
    if len(rootfiles)>1:
        rootfiles = newest[0]
    else:
        rootfiles = rootfiles[0]
    return rootfiles, ami

def getLBRootFiles(run, stream, camp, amistr="x"):
    print("OI", amistr)
    rootpath = f"/eos/atlas/atlastier0/tzero/prod/{camp}/{stream}/00{run}/{camp}.*.{stream}.recon.HIST.{amistr}*/{camp}.*.{stream}.recon.HIST.{amistr}*SFO-ALL*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    if len(rootfiles) == 0:
        print("NO LB ROOTFILES FOUND")
        sys.exit()
    return rootfiles


if __name__ == "__main__":
    run = 463213

    rootfile, ami = getRootFile(run, "physics_CosmicCalo", "data23_hi", True)
    if "_" in ami: 
        ami = ami.split("_")[0]
    lbrootfiles = getLBRootFiles(run, "physics_CosmicCalo", "data23_hi", ami)


    
