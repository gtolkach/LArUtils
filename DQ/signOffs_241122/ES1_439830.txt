Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-794

== Noisy cells update for run 439830 (Thu, 17 Nov 2022 16:11:33 +0100) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:test / Processing version:171122 (Unknown)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:161
Flagged in PS:49
Unflagged by DQ shifters:1
Changed to SBN:1
Changed to HNHG:0
SBN:146
SBN in PS:40
HNHG:15
HNHG in PS:9

*****************
The treated cells were:
0 0 24 8 11 0 sporadicBurstNoise # 0x38c38b00
0 0 24 8 12 0 sporadicBurstNoise # 0x38c38c00
0 0 24 8 13 0 sporadicBurstNoise # 0x38c38d00
0 0 24 8 9 0 sporadicBurstNoise # 0x38c38900
0 0 24 8 10 0 sporadicBurstNoise # 0x38c38a00
0 0 24 8 8 0 sporadicBurstNoise # 0x38c38800
0 1 21 5 102 0 sporadicBurstNoise # 0x39aa6600
0 1 21 5 101 0 sporadicBurstNoise # 0x39aa6500
0 1 21 5 103 0 sporadicBurstNoise # 0x39aa6700
0 0 30 10 34 0 sporadicBurstNoise # 0x38f4a200
0 0 25 2 5 0 sporadicBurstNoise # 0x38c88500
0 0 25 2 6 0 sporadicBurstNoise # 0x38c88600
0 0 5 1 3 0 sporadicBurstNoise # 0x38280300
0 0 4 1 13 0 sporadicBurstNoise # 0x38200d00
1 0 3 7 46 0 sporadicBurstNoise reflaggedByLADIeS # 0x3a1b2e00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 sporadicBurstNoise # 0x391b9b00
0 1 3 8 30 0 sporadicBurstNoise # 0x391b9e00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
0 0 15 14 36 0 sporadicBurstNoise # 0x387ea400
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 7 10 40 0 sporadicBurstNoise # 0x383ca800
0 0 7 14 36 0 highNoiseHG # 0x383ea400
0 0 7 14 32 0 highNoiseHG # 0x383ea000
0 1 20 6 78 0 sporadicBurstNoise # 0x39a2ce00
0 1 20 6 77 0 sporadicBurstNoise # 0x39a2cd00
0 0 11 1 4 0 sporadicBurstNoise # 0x38580400
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 0 1 2 41 0 sporadicBurstNoise # 0x3808a900
0 0 1 2 42 0 sporadicBurstNoise # 0x3808aa00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 sporadicBurstNoise # 0x39a90c00
0 0 9 10 24 0 sporadicBurstNoise # 0x384c9800
0 1 8 1 70 0 highNoiseHG # 0x39404600
0 0 16 13 14 0 sporadicBurstNoise # 0x38860e00
0 1 21 13 59 0 sporadicBurstNoise # 0x39ae3b00
0 0 25 1 24 0 sporadicBurstNoise # 0x38c81800
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 24 12 67 0 sporadicBurstNoise # 0x38c5c300
0 0 6 1 12 0 sporadicBurstNoise # 0x38300c00
0 0 25 1 22 0 sporadicBurstNoise # 0x38c81600
0 1 18 5 44 0 sporadicBurstNoise # 0x39922c00
0 1 18 5 43 0 sporadicBurstNoise # 0x39922b00
0 1 1 8 64 0 sporadicBurstNoise # 0x390bc000
0 1 1 8 65 0 sporadicBurstNoise # 0x390bc100
0 1 1 8 66 0 sporadicBurstNoise # 0x390bc200
0 0 13 1 28 0 sporadicBurstNoise # 0x38681c00
0 0 13 1 23 0 sporadicBurstNoise # 0x38681700
0 0 13 1 25 0 sporadicBurstNoise # 0x38681900
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
0 0 30 1 6 0 highNoiseHG # 0x38f00600
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 0 15 5 63 0 sporadicBurstNoise # 0x387a3f00
0 0 25 1 31 0 sporadicBurstNoise # 0x38c81f00
0 0 11 1 23 0 highNoiseHG # 0x38581700
0 0 11 1 21 0 sporadicBurstNoise # 0x38581500
0 0 11 1 28 0 sporadicBurstNoise # 0x38581c00
1 0 10 9 63 0 highNoiseHG # 0x3a543f00
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 1 16 6 99 0 sporadicBurstNoise # 0x3982e300
0 1 16 6 97 0 sporadicBurstNoise # 0x3982e100
1 1 12 4 15 0 sporadicBurstNoise # 0x3b618f00
1 1 13 5 87 0 sporadicBurstNoise # 0x3b6a5700
1 1 13 5 86 0 sporadicBurstNoise # 0x3b6a5600
0 0 23 7 9 0 sporadicBurstNoise # 0x38bb0900
0 0 23 7 10 0 sporadicBurstNoise # 0x38bb0a00
0 0 23 7 8 0 sporadicBurstNoise # 0x38bb0800
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 highNoiseHG # 0x39802b00
0 0 17 7 15 0 sporadicBurstNoise # 0x388b0f00
0 0 17 7 14 0 sporadicBurstNoise # 0x388b0e00
0 0 9 1 71 0 sporadicBurstNoise # 0x38484700
0 1 28 2 102 0 sporadicBurstNoise # 0x39e0e600
0 0 8 14 63 0 highNoiseHG # 0x3846bf00
0 0 8 14 62 0 sporadicBurstNoise # 0x3846be00
0 0 8 14 44 0 sporadicBurstNoise # 0x3846ac00
0 0 8 14 45 0 highNoiseHG # 0x3846ad00
0 0 8 10 98 0 sporadicBurstNoise # 0x3844e200
0 0 8 10 99 0 sporadicBurstNoise # 0x3844e300
1 0 11 8 12 0 sporadicBurstNoise # 0x3a5b8c00
0 0 11 1 6 0 sporadicBurstNoise # 0x38580600
0 1 4 11 77 0 sporadicBurstNoise # 0x39254d00
0 0 26 1 81 0 sporadicBurstNoise # 0x38d05100
1 1 9 4 76 0 sporadicBurstNoise # 0x3b49cc00
1 1 9 4 0 0 sporadicBurstNoise # 0x3b498000
0 1 11 1 15 0 sporadicBurstNoise # 0x39580f00
0 1 1 1 14 0 sporadicBurstNoise # 0x39080e00
0 0 15 5 61 0 sporadicBurstNoise # 0x387a3d00
0 0 15 5 60 0 sporadicBurstNoise # 0x387a3c00
0 0 15 13 86 0 sporadicBurstNoise # 0x387e5600
0 1 21 3 9 0 sporadicBurstNoise # 0x39a90900
0 1 5 1 78 0 sporadicBurstNoise # 0x39284e00
1 1 21 4 65 0 sporadicBurstNoise # 0x3ba9c100
0 0 23 1 84 0 sporadicBurstNoise # 0x38b85400
0 0 23 7 11 0 sporadicBurstNoise # 0x38bb0b00
0 1 27 1 1 0 sporadicBurstNoise # 0x39d80100
0 0 25 1 64 0 highNoiseHG # 0x38c84000
0 0 24 13 73 0 sporadicBurstNoise # 0x38c64900
0 1 19 14 43 0 sporadicBurstNoise # 0x399eab00
0 0 13 1 22 0 sporadicBurstNoise # 0x38681600
0 0 13 1 27 0 sporadicBurstNoise # 0x38681b00
0 0 26 1 59 0 sporadicBurstNoise # 0x38d03b00
0 1 13 13 102 0 sporadicBurstNoise # 0x396e6600
0 0 17 1 23 0 sporadicBurstNoise # 0x38881700
0 0 18 1 14 0 sporadicBurstNoise # 0x38900e00
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 21 1 18 0 sporadicBurstNoise # 0x38a81200
0 0 21 1 17 0 highNoiseHG # 0x38a81100
0 0 21 1 25 0 sporadicBurstNoise # 0x38a81900
0 1 8 11 89 0 sporadicBurstNoise # 0x39455900
1 1 21 4 116 0 sporadicBurstNoise # 0x3ba9f400
0 0 12 13 103 0 sporadicBurstNoise # 0x38666700
0 0 3 12 127 0 sporadicBurstNoise # 0x381dff00
0 0 3 12 126 0 sporadicBurstNoise # 0x381dfe00
0 0 3 12 125 0 sporadicBurstNoise # 0x381dfd00
0 0 6 14 44 0 sporadicBurstNoise # 0x3836ac00
0 0 6 14 45 0 sporadicBurstNoise # 0x3836ad00
0 0 5 10 102 0 sporadicBurstNoise # 0x382ce600
0 0 5 10 103 0 highNoiseHG # 0x382ce700
0 1 9 1 21 0 sporadicBurstNoise # 0x39481500
0 1 16 6 101 0 sporadicBurstNoise # 0x3982e500
0 1 16 6 96 0 sporadicBurstNoise # 0x3982e000
0 1 16 6 100 0 sporadicBurstNoise # 0x3982e400
0 1 16 6 102 0 sporadicBurstNoise # 0x3982e600
0 1 16 6 103 0 unflaggedByLADIeS # 0x3982e700
0 1 21 11 10 0 sporadicBurstNoise # 0x39ad0a00
0 1 12 13 93 0 sporadicBurstNoise # 0x39665d00
0 0 20 1 5 0 sporadicBurstNoise # 0x38a00500
0 0 11 1 15 0 sporadicBurstNoise # 0x38580f00
1 0 13 11 115 0 sporadicBurstNoise # 0x3a6d7300
1 0 13 11 63 0 sporadicBurstNoise # 0x3a6d3f00
0 0 23 1 5 0 sporadicBurstNoise # 0x38b80500
0 0 15 14 44 0 sporadicBurstNoise # 0x387eac00
0 1 7 1 43 0 sporadicBurstNoise # 0x39382b00
0 1 6 12 64 0 sporadicBurstNoise # 0x3935c000
0 0 26 1 19 0 sporadicBurstNoise # 0x38d01300
0 0 17 2 12 0 sporadicBurstNoise # 0x38888c00
0 1 30 1 2 0 sporadicBurstNoise # 0x39f00200
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 28 12 6 0 sporadicBurstNoise # 0x39e58600
0 0 28 1 14 0 highNoiseHG # 0x38e00e00
0 0 28 1 15 0 sporadicBurstNoise # 0x38e00f00
0 1 8 1 79 0 sporadicBurstNoise # 0x39404f00
0 0 21 11 65 0 sporadicBurstNoise # 0x38ad4100
0 0 22 1 1 0 sporadicBurstNoise # 0x38b00100
0 1 16 12 16 0 sporadicBurstNoise # 0x39859000
0 0 23 3 63 0 sporadicBurstNoise # 0x38b93f00
0 0 23 4 0 0 sporadicBurstNoise # 0x38b98000
0 0 23 4 1 0 sporadicBurstNoise # 0x38b98100
1 0 21 3 41 0 sporadicBurstNoise # 0x3aa92900
1 0 2 10 48 0 sporadicBurstNoise # 0x3a14b000
0 1 13 12 65 0 sporadicBurstNoise # 0x396dc100
0 1 13 12 69 0 sporadicBurstNoise # 0x396dc500
0 0 27 12 50 0 sporadicBurstNoise # 0x38ddb200

