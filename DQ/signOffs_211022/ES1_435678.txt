== Noisy cells update for run 435678 (Sat, 1 Oct 2022 16:06:32 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x695_h400)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:62
Flagged in PS:11
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:1
SBN:28
SBN in PS:3
HNHG:34
HNHG in PS:8

*****************
The treated cells were:
20
Unmatched cells treatment: No flag proposal in Inner wheel/FCal
MNB treatment: No flag proposed (except >4 nbclus) - Tier0 list: 5 FEBs ( EMBC_2_10 EMBC_6_10 EMBC_2_14 EMBC_6_14 EMBA_31_14 )

Total number of channels flagged: 62 (11 in presampler)

Total number of cells unflagged by LADIeS: 0
Total number of cells modified (highNoiseHG -> sporadicBurstNoise) by LADIeS: 0

Total number of cells modified (Nothing -> sporadicBurstNoise) by LADIeS: 0
Total number of cells modified (sporadicBurstNoise/nothing -> highNoiseHG ) by LADIeS: 1

Total number of sporadicBurstNoise: 28 (3 in presampler)
Total number of highNoiseHG: 34 (8 in presampler)

FEBs with more than 6 channels flagged as highNoiseHG.
FEB EMECC HN-FT 4/Slot 1 -> 7 channels proposed
FEB HECC HN-FT 3/Slot 6 -> 7 channels proposed
Please double check that it may not create holes...


Middle EM FEBs with more than 8 channels flagged as sporadicBurstNoise.
None :-)

