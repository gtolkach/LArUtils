== Noisy cells update for run 437010 (Sat, 15 Oct 2022 11:51:54 +0100) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x705_h404)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:20
Flagged in PS:8
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:18
SBN in PS:7
HNHG:2
HNHG in PS:1

*****************
The treated cells were:
0 1 11 12 78 0 sporadicBurstNoise # 0x395dce00
1 0 14 7 4 0 sporadicBurstNoise # 0x3a730400
1 0 14 7 5 0 sporadicBurstNoise # 0x3a730500
1 0 14 7 3 0 sporadicBurstNoise # 0x3a730300
1 0 14 7 6 0 sporadicBurstNoise # 0x3a730600
1 0 3 6 103 0 highNoiseHG # 0x3a1ae700
0 0 9 1 4 0 highNoiseHG # 0x38480400
0 0 9 1 5 0 sporadicBurstNoise # 0x38480500
0 0 18 1 10 0 sporadicBurstNoise # 0x38900a00
0 0 18 1 5 0 sporadicBurstNoise # 0x38900500
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 13 1 35 0 sporadicBurstNoise # 0x38682300
0 0 13 1 29 0 sporadicBurstNoise # 0x38681d00
0 0 13 1 30 0 sporadicBurstNoise # 0x38681e00
0 0 23 5 12 0 sporadicBurstNoise # 0x38ba0c00
0 0 23 5 13 0 sporadicBurstNoise # 0x38ba0d00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 0 14 2 28 0 sporadicBurstNoise # 0x38709c00
0 1 23 1 96 0 sporadicBurstNoise # 0x39b86000

