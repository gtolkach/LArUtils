== Noisy cells update for run 436169 (Thu, 13 Oct 2022 10:02:40 +0200) ==
No cells were flagged as noisy by the LAr DQ shifters for this run


== Event Veto for run 436169 (Sun, 9 Oct 2022 08:44:32 +0200) ==
Found Noise or data corruption in run 436169
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436169_Main.db

Found project tag data22_13p6TeV for run 436169
Found 3 Veto Ranges with 6 events
Found 6 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436169_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436169
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436169_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 1.50 seconds
Lumi loss due to corruption: 28.15 nb-1 out of 127580.21 nb-1 (0.22 per-mil)
Overall Lumi loss is 28.15204547688438 by 1.50075486 s of veto length



== Event Veto for run 436169 (Sun, 9 Oct 2022 07:49:55 +0200) ==
Found Noise or data corruption in run 436169
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto436169_Main.db

Found project tag data22_13p6TeV for run 436169
Found 3 Veto Ranges with 6 events
Found 6 isolated events
Reading event veto info from db sqlite://;schema=EventVeto436169_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 436169
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto436169_Main.db;dbname=CONDBR2
Found a total of 3 corruption periods, covering a total of 1.50 seconds
Lumi loss due to corruption: 28.15 nb-1 out of 127580.21 nb-1 (0.22 per-mil)
Overall Lumi loss is 28.15204547688438 by 1.50075486 s of veto length



== Missing EVs for run 436169 (Thu, 6 Oct 2022 08:15:31 +0200) ==

Found a total of 5 noisy periods, covering a total of 0.01 seconds
Found a total of 52 Mini noise periods, covering a total of 0.05 seconds
Lumi loss due to noise-bursts: 0.09 nb-1 out of 127580.21 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.94 nb-1 out of 127580.21 nb-1 (0.01 per-mil)
Overall Lumi loss is 1.0352547990619043 by 0.057004269999999996 s of veto length


