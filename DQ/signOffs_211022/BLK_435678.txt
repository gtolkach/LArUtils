== Noisy cells update for run 435678 (Thu, 20 Oct 2022 11:09:44 +0200) ==
Tool version:WebDisplayExtractor-07-00-00 / Stream:physics_CosmicCalo / Source:tier0 / 
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:4
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:1
Changed to HNHG:0
SBN:3
SBN in PS:1
HNHG:1
HNHG in PS:0

*****************
The treated cells were:
0 1 19 2 71 0 sporadicBurstNoise # 0x3998c700
0 1 19 2 72 0 sporadicBurstNoise reflaggedByLADIeS # 0x3998c800
0 1 23 2 2 0 highNoiseHG # 0x39b88200
0 1 21 1 3 0 sporadicBurstNoise # 0x39a80300

== Missing EVs for run 435678 (Fri, 30 Sep 2022 10:15:34 +0200) ==

Found a total of 1 Mini noise periods, covering a total of 0.00 seconds
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 6900.85 nb-1 (0.00 per-mil)
Overall Lumi loss is 1.6467632984519003e-07 by 0.00126952 s of veto length


