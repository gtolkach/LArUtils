Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1197

** Defect LAR_HECA_SEVNOISEBURST(INTOLERABLE) **
Affecting 8 LBs: 678, 679, 682, 683, 977, 978, 1286, 1287 (0 recoverable) 
** Defect LAR_EMECA_SEVNOISEBURST(INTOLERABLE) **
Affecting 8 LBs: 678, 679, 682, 683, 977, 978, 1286, 1287 (0 recoverable) 
** Defect LAR_FCALA_SEVNOISEBURST(INTOLERABLE) **
Affecting 8 LBs: 678, 679, 682, 683, 977, 978, 1286, 1287 (0 recoverable) 
