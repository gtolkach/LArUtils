Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1194

== Event Veto for run 453657 (Wed, 7 Jun 2023 07:54:27 +0200) ==
Found Noise or data corruption in run 453657
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto453657_Main.db

Found project tag data23_13p6TeV for run 453657
Found 2 Veto Ranges with 5 events
Found 2 isolated events
Reading event veto info from db sqlite://;schema=EventVeto453657_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 453657
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto453657_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 13.27 nb-1 out of 71413.57 nb-1 (0.19 per-mil)
Overall Lumi loss is 13.268352322857686 by 1.000215475 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1194

== Event Veto for run 453657 (Wed, 7 Jun 2023 07:37:40 +0200) ==
Found Noise or data corruption in run 453657
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto453657_Main.db

Found project tag data23_13p6TeV for run 453657
Found 2 Veto Ranges with 5 events
Found 2 isolated events
Reading event veto info from db sqlite://;schema=EventVeto453657_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 453657
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto453657_Main.db;dbname=CONDBR2
Found a total of 2 corruption periods, covering a total of 1.00 seconds
Lumi loss due to corruption: 13.27 nb-1 out of 71413.57 nb-1 (0.19 per-mil)
Overall Lumi loss is 13.268352322857686 by 1.000215475 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1194

== Missing EVs for run 453657 (Sun, 4 Jun 2023 03:15:47 +0200) ==

Found a total of 16 noisy periods, covering a total of 0.02 seconds
Found a total of 140 Mini noise periods, covering a total of 0.16 seconds
Lumi loss due to noise-bursts: 0.25 nb-1 out of 71413.57 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 2.26 nb-1 out of 71413.57 nb-1 (0.03 per-mil)
Overall Lumi loss is 2.50517754249106 by 0.17449739 s of veto length


