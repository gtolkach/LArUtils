Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1202

== Event Veto for run 453795 (Sat, 10 Jun 2023 06:34:26 +0200) ==
Found Noise or data corruption in run 453795
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto453795_Main.db

Found project tag data23_13p6TeV for run 453795
Found 7 Veto Ranges with 761 events
Found 34 isolated events
Reading event veto info from db sqlite://;schema=EventVeto453795_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 453795
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto453795_Main.db;dbname=CONDBR2
Found a total of 7 corruption periods, covering a total of 4.19 seconds
Lumi loss due to corruption: 65.97 nb-1 out of 555772.64 nb-1 (0.12 per-mil)
Overall Lumi loss is 65.96826606299393 by 4.186470275 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1202

== Missing EVs for run 453795 (Tue, 6 Jun 2023 16:23:52 +0200) ==

Found a total of 591 noisy periods, covering a total of 0.57 seconds
Found a total of 1230 Mini noise periods, covering a total of 1.14 seconds
Lumi loss due to noise-bursts: 9.79 nb-1 out of 555772.64 nb-1 (0.02 per-mil)
Lumi loss due to mini-noise-bursts: 19.46 nb-1 out of 555772.64 nb-1 (0.04 per-mil)
Overall Lumi loss is 29.25007632914868 by 1.714715805 s of veto length


