Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1569

** Defect LAR_HECA_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
** Defect LAR_HECC_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
** Defect LAR_EMECA_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
** Defect LAR_EMECC_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
** Defect LAR_FCALA_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
** Defect LAR_FCALC_SEVNOISEBURST(INTOLERABLE) **
Affecting 23 LBs: 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189 (23 recoverable) 
