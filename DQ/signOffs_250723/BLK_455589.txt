Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1277

== Missing EVs for run 455589 (Fri, 30 Jun 2023 11:17:25 +0200) ==

Found a total of 19 noisy periods, covering a total of 0.02 seconds
Found a total of 78 Mini noise periods, covering a total of 0.08 seconds
Lumi loss due to noise-bursts: 0.00 nb-1 out of 127.92 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 0.00 nb-1 out of 127.92 nb-1 (0.00 per-mil)
Overall Lumi loss is 0.00046461020971991733 by 0.097005565 s of veto length


