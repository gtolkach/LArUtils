Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1362

== Noisy cells update for run 456118 (Thu, 20 Jul 2023 19:30:26 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1367_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:12
Flagged in PS:3
Unflagged by DQ shifters:0
Changed to SBN:5
Changed to HNHG:1
SBN:10
SBN in PS:2
HNHG:2
HNHG in PS:1

*****************
The treated cells were:
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 10 11 87 0 sporadicBurstNoise  # 0x38555700 -> 0x2d400a50
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 1 16 6 103 0 sporadicBurstNoise  # 0x3982e700 -> 0x2da24e42
1 0 15 13 16 0 sporadicBurstNoise  # 0x3a7e1000 -> 0x2ce025ee
1 0 15 13 18 0 sporadicBurstNoise  # 0x3a7e1200 -> 0x2ce025ea
1 0 15 14 33 0 sporadicBurstNoise  # 0x3a7ea100 -> 0x2cc44fec
1 0 15 14 43 0 sporadicBurstNoise  # 0x3a7eab00 -> 0x2cc453e8
1 0 16 1 4 0 highNoiseHG  # 0x3a800400 -> 0x2c20007a
1 0 16 1 5 0 sporadicBurstNoise  # 0x3a800500 -> 0x2c200078

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1362

== Event Veto for run 456118 (Thu, 13 Jul 2023 20:47:51 +0200) ==
Found Noise or data corruption in run 456118
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456118_Main.db

Found project tag data23_13p6TeV for run 456118
Found 31 Veto Ranges with 87 events
Found 78 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456118_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456118
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456118_Main.db;dbname=CONDBR2
Found a total of 31 corruption periods, covering a total of 15.51 seconds
Lumi loss due to corruption: 300.81 nb-1 out of 810278.93 nb-1 (0.37 per-mil)
Overall Lumi loss is 300.80800285424095 by 15.507580985 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1362

== Event Veto for run 456118 (Thu, 13 Jul 2023 19:46:15 +0200) ==
Found Noise or data corruption in run 456118
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456118_Main.db

Found project tag data23_13p6TeV for run 456118
Found 31 Veto Ranges with 87 events
Found 78 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456118_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456118
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456118_Main.db;dbname=CONDBR2
Found a total of 31 corruption periods, covering a total of 15.51 seconds
Lumi loss due to corruption: 300.81 nb-1 out of 810278.93 nb-1 (0.37 per-mil)
Overall Lumi loss is 300.80800285424095 by 15.507580985 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1362

== Missing EVs for run 456118 (Sun, 9 Jul 2023 11:25:38 +0200) ==

Found a total of 213 noisy periods, covering a total of 0.26 seconds
Found a total of 1690 Mini noise periods, covering a total of 1.83 seconds
Lumi loss due to noise-bursts: 4.49 nb-1 out of 810278.93 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 32.12 nb-1 out of 810278.93 nb-1 (0.04 per-mil)
Overall Lumi loss is 36.60596596395306 by 2.085345965 s of veto length


