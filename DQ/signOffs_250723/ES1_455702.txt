Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1285

== Noisy cells update for run 455702 (Wed, 5 Jul 2023 08:46:12 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x752_h431)
Cluster matching: based on Et > 4/10GeV plots requiring at least 50 events
Flagged cells:15
Flagged in PS:1
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:9
SBN in PS:1
HNHG:6
HNHG in PS:0

*****************
The treated cells were:
0 0 6 13 62 0 sporadicBurstNoise  # 0x38363e00 -> 0x2d405e9a
0 0 15 11 10 0 sporadicBurstNoise  # 0x387d0a00 -> 0x2d40040a
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 24 14 23 0 sporadicBurstNoise  # 0x38c69700 -> 0x2d406370
0 0 31 6 3 0 sporadicBurstNoise  # 0x38fa8300 -> 0x2d220642
0 0 31 6 4 0 highNoiseHG  # 0x38fa8400 -> 0x2d220842
0 0 31 6 5 0 sporadicBurstNoise  # 0x38fa8500 -> 0x2d220a42
0 1 4 1 107 0 sporadicBurstNoise  # 0x39206b00 -> 0x2d806e10
0 1 22 12 114 0 sporadicBurstNoise  # 0x39b5f200 -> 0x2dc0396c
0 1 28 3 69 0 sporadicBurstNoise  # 0x39e14500 -> 0x2da08a72
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000

