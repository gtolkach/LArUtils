#!/bin/python3
import sys, glob
import argparse
import ROOT as R
import xmlrpc.client
import ctypes
#0x3a305b00

dqmpassfile="/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"

onIDhistFile = R.TFile("/afs/cern.ch/user/l/larmon/public/development/WebDisplayExtractor/makeOnIDHists/onIDs.root","READ")

def getFEBinfo( onId ):
    if isinstance(onId,str):
        if onId.startswith("0x"):
            onId = int(onId, 16)
        else:
            onId = int(onId)
    pn = int( ((onId)>>24)&0x1 )
    benc = int( ((onId)>> 25)&0x1 )
    ft = int( ((onId)>>19)&0x1F )
    sl = int( (((onId)>>15)&0xF)+1 )
    return onId, pn, benc, ft, sl

def getFebStr(benc, pn, ft, sl):
    side, part, fullpart = None, None, None
    if pn == 0 :
        side = "C"
    elif pn == 1:
        side = "A"
    else:
        print( "getFEBinfo -> weird result for pn = "+str(pn) )
        side = "??"

    if benc == 0:
        fullpart = "EMB"
        part = "Barrel"
    elif benc == 1:
        part = "Endcap"
        fullpart="EMEC"
        if ( ft == 3 or ft == 10 or ft == 16 or ft == 22 ) and sl > 4:
            fullpart = "HEC"
        elif ft == 6:
            fullpart = "FCAL"
        else:
            fullpart = "EMEC"
    # return part+side+str(ft).zfill(2)+str(sl).zfill(2)                                                                                                                     
    return part+side+"FT"+str(ft).zfill(2)+"Slot"+str(sl).zfill(2), fullpart


def getRootFiles(run, camp, stream, amistr="x"):
    rootpath = "/eos/atlas/atlastier0/tzero/prod/"+camp+"/"+stream+"/*"+str(run)+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"\
*SFO*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    #return rootfiles

def findHistFilesLB(run, camp, stream, amistr="x"):
    rootpath = "/eos/atlas/atlastier0/tzero/prod/"+camp+"/"+stream+"/*"+str(run)+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*/"+camp+".*."+stream+".recon.HIST."+amistr+"*SFO*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    amis = sorted(list(set([r.split("/")[-2].split(".")[-1] for r in rootfiles])))
    if len(amis) > 1:
        newest = [ r for r in rfile if "."+amis[-1]+"." in r]
        return newest
    return rootfiles

def findHistFile(run, camp, stream, ami="x"):
    rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/*/"+camp+".*."+stream+".merge.HIST."+ami+"*/"+camp+".*."+stream+".merge.HIST."+ami+"*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    infiles = []
    rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
    amis = sorted(list([ r.split(".")[-3] for r in rfile]))
    newest = [ r for r in rfile if "."+amis[-1]+"." in r]
    if len(rfile) > 1:
        return newest[0]
    elif len(rfile) == 1:
        return rfile[0]
    else:
        print("Did not find any files from requested wildcard")
        sys.exit()


def basicInfo(onlID,  isFEB=False):
    onlID, pn, benc, ft, sl = getFEBinfo(onlID)
    FEB, part = getFebStr(benc, pn, ft, sl)
    side = "A"
    if pn == 0: side="C"

    posshists = [k.GetName() for k in onIDhistFile.GetListOfKeys() if k.GetName().startswith("h_"+part) and k.GetName().endswith(side)]
    binN = 0
    theHist = None
    if not isFEB:
        for hname in posshists:
            h = onIDhistFile.Get(hname)
            for x in range(1,h.GetNbinsX() + 1):
                for y in range(1,h.GetNbinsY() + 1):
                    if h.GetBinContent(x,y) == onlID:
                        binN = h.FindBin(h.GetXaxis().GetBinCenter(x), h.GetYaxis().GetBinCenter(y))
                        theHist = hname.split("h_")[1]
                        break

        return onlID, FEB, part, side, theHist, binN
    else:
        return onlID, FEB, part, side, ft, sl

def setupAPI():
    global dqmapi
    passfile = open(dqmpassfile)
    passwd = passfile.read().strip(); passfile.close()
    passurl = 'https://%s@atlasdqm.cern.ch'%passwd
    dqmapi = xmlrpc.client.ServerProxy(passurl)

def printFEBInfo(FEB_ID, run, stream="physics_CosmicCalo", amiTag="x", doPerLB=False):
    FEB_ID, FEB, part, side, ft, sl  = basicInfo(FEB_ID, True)
    print(f"{FEB_ID} is in {part}{side} - FEB {FEB}")

    if "dqmapi" not in globals(): setupAPI()
    run_spec = {'stream': stream, 'source': 'tier0', 'low_run': run, 'high_run':run}
    run_info= dqmapi.get_run_information(run_spec)
    run_info = run_info[str(run)]
    tag = run_info[1]

    histFilePath = findHistFile(run, tag, stream=stream, ami=amiTag)
    histFile = R.TFile(histFilePath, "READ")
    print(f"Reading from {histFilePath} bin {ft},{sl}")
    histsToCheck = []
    histsToCheck.append(f"LAr/FEBMon/{part}/NbOfSweet1PerFEB_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/NbOfSweet2PerFEB_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/LArFEBMonErrorsAbsolute_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/checkSum_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/knownFaultyFEB_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/missingTriggerType_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/nbOfEvts_{part}{side}")
    histsToCheck.append(f"LAr/FEBMon/{part}/LArFEBMonErrors_{part}{side}")

    histsToCheck.append(f"LAr/NoisyRO/{part}/MNBKnownFEB_{part}{side}")
    histsToCheck.append(f"LAr/NoisyRO/{part}/CandidateMNBLooseFEBPerEvt_{part}{side}")
    histsToCheck.append(f"LAr/NoisyRO/{part}/CandidateMNBTightFEBPerEvt_{part}{side}")
    histsToCheck.append(f"LAr/NoisyRO/{part}/CandidateMNBTight_PSVetoFEBPerEvt_{part}{side}")
    histsToCheck.append(f"LAr/NoisyRO/{part}/MNBLooseFEBPerEvt_{part}{side}")
    histsToCheck.append(f"LAr/NoisyRO/{part}/NoisyFEBPerEvt_{part}{side}")
    histsToCheck.append(f"CandidateMNBLooseFEBFracPerEvt_{part}{side}")
    histsToCheck.append(f"CandidateMNBTightFEBFracPerEvt_{part}{side}")
    histsToCheck.append(f"CandidateMNBTight_PsVetoFEBFracPerEvt_{part}{side}")
    histsToCheck.append(f"MNBLooseFEBFracPerEvt_{part}{side}")
    histsToCheck.append(f"NoisyFEBFracPerEvt_{part}{side}")

    for htc in histsToCheck:
        htc = f"run_{run}/{htc}"
        hist = histFile.Get(htc)
        hname = htc.split("/")[-1]
        if not isinstance(hist, R.TH2): 
            continue
        print(f"{hname} : {round(hist.GetBinContent(sl,ft+1),2)} (mean = {round(hist.GetMean(),2)})")
        if args.showNeighbours:
            for nsl in range(sl-1,sl+2):
                for nft in range(ft-1, ft+2):
                    if nft == ft and nsl == sl: continue
                    print(f"-- FT{nft}Slot{nsl}: {round(hist.GetBinContent(nsl,nft+1),2)}")
    histFile.Close()

    if doPerLB:
        LBfiles = findHistFilesLB(run, tag, stream=stream, amistr=amiTag)
        counts = {}
        for htc in histsToCheck:
            counts[htc] = []
        for lbf in LBfiles:
            LB = lbf.split(".")[-4].split("_lb")[1]
            histFile = R.TFile(lbf, "READ")
            for htc in histsToCheck:
                this_htc = htc
                this_htc = f"run_{run}/{this_htc}"
                hist = histFile.Get(this_htc)
                if not isinstance(hist, R.TH2): 
                    this_htc = this_htc.replace(f"run_{run}/",f"run_{run}/RAW_")
                if not isinstance(hist, R.TH2): 
                    continue

                hname = this_htc.split("/")[-1]
                #print(f"LB {int(LB)} {hname} : {round(hist.GetBinContent(binN),2)}")
                counts[htc].append(hist.GetBinContent(sl,ft))
            histFile.Close()
        print("*"*20)
        for htc in counts.keys():
            if len(counts[htc]) != 0: print(f'{htc.split("/")[-1]}: {counts[htc]}')
            #if "fraction" in htc or "Percent" in htc:
            #    den = sum(counts["CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_5Sigma_CSCveto"])
            #    num = sum(counts[htc])
            #    print(f"= {100*round(num/den,2)}%")


def printCellInfo(cell_ID, run, stream="physics_CosmicCalo", amiTag="x", doPerLB=False):
    cell_ID, FEB, part, side, theHist, binN = basicInfo(cell_ID)
    print(f"{cell_ID} is in {theHist} - FEB {FEB}")
    binx, biny, binz = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
    if "dqmapi" not in globals(): setupAPI()
    run_spec = {'stream': stream, 'source': 'tier0', 'low_run': run, 'high_run':run}
    run_info= dqmapi.get_run_information(run_spec)
    run_info = run_info[str(run)]
    tag = run_info[1]

    histFilePath = findHistFile(run, tag, stream=stream, ami=amiTag)
    histFile = R.TFile(histFilePath, "READ")
    print(f"Reading from {histFilePath} bin {binN}")
    histsToCheck = []
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/2d_AvgEnergy/CellAvgEnergyVsEtaPhi_"+theHist+"_5Sigma_CSCveto")
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_5Sigma_CSCveto")
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_hiEth_CSCveto")
    histsToCheck.append("CaloMonitoring/LArClusterCellMon/ClusterCell/PercentClusteredCells_"+theHist+"")
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/2d_PoorQualityFraction/fractionOverQthVsEtaPhi_"+theHist+"_hiEth_CSCveto")
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/DatabaseNoise/DatabaseNoiseVsEtaPhi_"+theHist)
    histsToCheck.append("CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_noEth_rndm_CSCveto")
    hID = onIDhistFile.Get("h_"+theHist)
    for htc in histsToCheck:
        htc = f"run_{run}/{htc}"
        hist = histFile.Get(htc)
        hname = htc.split("/")[-1]
        if not isinstance(hist, R.TH1): 
            continue
        print(f"{hname} : {round(hist.GetBinContent(binN),2)}")

        if args.showNeighbours:
            if isinstance(hist, R.TH2): 
                hist.GetBinXYZ(binN, binx, biny, binz)
                for nx in range(binx.value-1, binx.value+2):
                    for ny in range(biny.value-1,biny.value+2):
                        if nx == binx.value and ny == biny.value: continue
                        thisID = int(hID.GetBinContent(nx,ny))
                        print(f"-- {thisID}: {round(hist.GetBinContent(nx,ny),2)}")
    histFile.Close()

    if doPerLB:
        LBfiles = findHistFilesLB(run, tag, stream=stream, amistr=amiTag)
        counts = {}
        for htc in histsToCheck:
            counts[htc] = []
        for lbf in LBfiles:
            LB = lbf.split(".")[-4].split("_lb")[1]
            histFile = R.TFile(lbf, "READ")
            for htc in histsToCheck:
                this_htc = htc
                if "fraction" in this_htc: this_htc = this_htc.replace("/fraction","/RAW_fraction")
                if "PercentClusteredCells" in this_htc: this_htc = this_htc.replace("PercentClusteredCells", "NClusteredCells")
                this_htc = f"run_{run}/{this_htc}"
                hist = histFile.Get(this_htc)
                if not isinstance(hist, R.TH2): 
                    continue
                hname = this_htc.split("/")[-1]
                #print(f"LB {int(LB)} {hname} : {round(hist.GetBinContent(binN),2)}")
                counts[htc].append(hist.GetBinContent(binN))

            histFile.Close()
        print("*"*20)
        for htc in counts.keys():
            print(f'{htc.split("/")[-1]}: {counts[htc]}')
            
            if "fraction" in htc or "Percent" in htc:
                den = sum(counts["CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_hiEth_CSCveto"])
                num = sum(counts[htc])
                if den != 0:
                    print(f"= {100*round(num/den,2)}%")

            if "Percent" in htc:
                den = sum(counts["CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_noEth_rndm_CSCveto"])
                num = sum(counts[htc])
                if den != 0:
                    print(f"= {100*round(num/den,2)}%")
                test =  [ n/d if d != 0 else 0 for n,d in zip(counts[htc],counts["CaloMonitoring/LArCellMon_NoTrigSel/2d_Occupancy/CellOccupancyVsEtaPhi_"+theHist+"_noEth_rndm_CSCveto"]) ]
                print("****", test)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dump information about requested cell(s) in the desired run/stream.')
    parser.add_argument('-r','--run',type=int,dest='runNumber',required=True,help="Run number",action='store')
    parser.add_argument('-c','--cell',type=str,dest='cellIDs',nargs='+', help="Cell ID(s) to check - can be in integer or hex form")
    parser.add_argument('-f','--FEB',type=str,dest='FEBIDs',nargs='+', help="FEB ID(s) to check - can be in integer or hex form")
    parser.add_argument('-s','--stream',type=str,dest='stream', default='physics_CosmicCalo', help="Stream in which to find data. Default %(default)s")
    parser.add_argument('-a','--amiTag',type=str,dest='amiTag',default="x",help="AMI tag to use for hist file search. Can be the full tag or simply x/f (for express/bulk). Default %(default)s")
    parser.add_argument('--perLB', action='store_true',dest='doPerLB', help="Also print stats per LB?")
    parser.add_argument('--showNeighbours', action='store_true',dest='showNeighbours', help="Also print bin contents of neighbouring cells/FEBs for merged hist file")
    args = parser.parse_args()

    if args.cellIDs is None and args.FEBIDs is None:
        print("Nothing to check - you must provide either cell ID(s) or FEB  ID(s)")

    if args.cellIDs is not None:
        for cellID in args.cellIDs:
            print("*"*10, cellID, "*"*10)
            printCellInfo(cellID, args.runNumber, args.stream, args.amiTag, args.doPerLB)
    if args.FEBIDs is not None:
        for FEBID in args.FEBIDs:
            print("*"*10, FEBID, "*"*10)
            printFEBInfo(FEBID, args.runNumber, args.stream, args.amiTag, args.doPerLB)
