Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1113

== Noisy cells update for run 451543 (Thu, 11 May 2023 16:31:16 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (x739_h418)
Cluster matching: based on Et &gt; 4/10GeV plots requiring at least 200 events
Flagged cells:69
Flagged in PS:16
Unflagged by DQ shifters:22
SBN:47
SBN in PS:2
HNHG:22
HNHG in PS:14

*****************
The treated cells were:
0 0 24 14 23 0 highNoiseHG # 0x38c69700
1 0 10 8 94 0 highNoiseHG # 0x3a53de00
1 0 10 8 78 0 highNoiseHG # 0x3a53ce00
0 0 17 10 20 0 highNoiseHG # 0x388c9400
0 0 17 10 21 0 highNoiseHG # 0x388c9500
0 1 24 1 90 0 highNoiseHG # 0x39c05a00
0 1 24 1 91 0 sporadicBurstNoise # 0x39c05b00
0 1 24 7 55 0 sporadicBurstNoise # 0x39c33700
0 1 2 13 72 0 sporadicBurstNoise # 0x39164800
0 0 10 13 67 0 sporadicBurstNoise # 0x38564300
0 1 23 2 1 0 sporadicBurstNoise # 0x39b88100
0 1 23 2 2 0 sporadicBurstNoise # 0x39b88200
0 0 18 1 5 0 highNoiseHG # 0x38900500
1 1 9 4 16 0 sporadicBurstNoise # 0x3b499000
0 0 30 1 13 0 highNoiseHG # 0x38f00d00
0 1 3 8 28 0 sporadicBurstNoise # 0x391b9c00
0 1 3 8 29 0 sporadicBurstNoise # 0x391b9d00
0 1 3 8 27 0 unflaggedByLADIeS # 0x391b9b00
0 0 15 14 40 0 sporadicBurstNoise # 0x387ea800
1 0 2 2 84 0 highNoiseHG # 0x3a10d400
0 1 29 11 67 0 sporadicBurstNoise # 0x39ed4300
0 0 22 7 2 0 sporadicBurstNoise # 0x38b30200
0 0 22 7 1 0 sporadicBurstNoise # 0x38b30100
0 0 22 7 3 0 unflaggedByLADIeS # 0x38b30300
1 0 10 8 122 0 highNoiseHG # 0x3a53fa00
0 0 5 13 63 0 sporadicBurstNoise # 0x382e3f00
0 0 25 12 15 0 sporadicBurstNoise # 0x38cd8f00
0 0 13 1 35 0 highNoiseHG # 0x38682300
0 0 13 1 28 0 unflaggedByLADIeS # 0x38681c00
0 1 21 3 10 0 sporadicBurstNoise # 0x39a90a00
0 1 21 3 11 0 sporadicBurstNoise # 0x39a90b00
0 1 21 3 12 0 unflaggedByLADIeS # 0x39a90c00
0 1 8 1 70 0 highNoiseHG # 0x39404600
1 1 9 4 20 0 sporadicBurstNoise # 0x3b499400
1 1 9 4 17 0 sporadicBurstNoise # 0x3b499100
1 1 9 2 68 0 unflaggedByLADIeS # 0x3b48c400
0 0 1 13 121 0 sporadicBurstNoise # 0x380e7900
0 0 23 14 35 0 sporadicBurstNoise # 0x38bea300
0 1 16 6 98 0 sporadicBurstNoise # 0x3982e200
0 0 13 1 5 0 highNoiseHG # 0x38680500
0 0 7 12 22 0 sporadicBurstNoise # 0x383d9600
0 0 4 4 46 0 sporadicBurstNoise # 0x3821ae00
0 0 4 4 47 0 sporadicBurstNoise # 0x3821af00
0 0 23 9 29 0 sporadicBurstNoise # 0x38bc1d00
1 0 21 13 123 0 sporadicBurstNoise # 0x3aae7b00
1 0 21 15 115 0 sporadicBurstNoise # 0x3aaf7300
0 1 5 14 27 0 sporadicBurstNoise # 0x392e9b00
0 0 23 1 63 0 sporadicBurstNoise # 0x38b83f00
0 1 24 11 55 0 sporadicBurstNoise # 0x39c53700
1 0 19 10 105 0 sporadicBurstNoise # 0x3a9ce900
0 0 26 1 19 0 highNoiseHG # 0x38d01300
0 0 18 1 14 0 highNoiseHG # 0x38900e00
0 0 11 1 23 0 highNoiseHG # 0x38581700
1 0 21 2 16 0 sporadicBurstNoise # 0x3aa89000
1 0 21 2 28 0 sporadicBurstNoise # 0x3aa89c00
0 0 26 14 52 0 sporadicBurstNoise # 0x38d6b400
1 0 21 2 24 0 sporadicBurstNoise # 0x3aa89800
0 0 26 14 56 0 sporadicBurstNoise # 0x38d6b800
0 0 26 14 61 0 sporadicBurstNoise # 0x38d6bd00
0 0 26 14 62 0 sporadicBurstNoise # 0x38d6be00
0 0 26 14 63 0 unflaggedByLADIeS # 0x38d6bf00
0 0 26 10 96 0 unflaggedByLADIeS # 0x38d4e000
0 0 26 10 100 0 unflaggedByLADIeS # 0x38d4e400
0 0 26 14 53 0 highNoiseHG # 0x38d6b500
0 0 26 14 57 0 unflaggedByLADIeS # 0x38d6b900
0 0 26 14 60 0 unflaggedByLADIeS # 0x38d6bc00
0 0 26 14 59 0 unflaggedByLADIeS # 0x38d6bb00
0 0 26 10 101 0 unflaggedByLADIeS # 0x38d4e500
0 0 26 14 55 0 unflaggedByLADIeS # 0x38d6b700
0 0 26 10 97 0 unflaggedByLADIeS # 0x38d4e100
0 0 26 10 104 0 unflaggedByLADIeS # 0x38d4e800
0 0 26 14 48 0 unflaggedByLADIeS # 0x38d6b000
0 0 26 14 24 0 unflaggedByLADIeS # 0x38d69800
0 0 26 14 25 0 unflaggedByLADIeS # 0x38d69900
0 0 26 14 54 0 unflaggedByLADIeS # 0x38d6b600
0 0 26 14 29 0 unflaggedByLADIeS # 0x38d69d00
0 0 26 14 28 0 unflaggedByLADIeS # 0x38d69c00
0 0 15 5 62 0 sporadicBurstNoise # 0x387a3e00
0 1 18 12 76 0 sporadicBurstNoise # 0x3995cc00
0 1 1 8 64 0 sporadicBurstNoise # 0x390bc000
0 1 1 8 65 0 sporadicBurstNoise # 0x390bc100
0 1 1 8 66 0 unflaggedByLADIeS # 0x390bc200
0 1 9 1 21 0 highNoiseHG # 0x39481500
0 0 5 1 0 0 highNoiseHG # 0x38280000
0 0 5 1 1 0 highNoiseHG # 0x38280100
1 1 9 13 31 0 sporadicBurstNoise # 0x3b4e1f00
1 1 9 14 63 0 sporadicBurstNoise # 0x3b4ebf00
1 1 9 13 30 0 sporadicBurstNoise # 0x3b4e1e00
1 1 9 14 59 0 sporadicBurstNoise # 0x3b4ebb00
0 1 16 1 48 0 highNoiseHG # 0x39803000
0 1 16 1 43 0 highNoiseHG # 0x39802b00

