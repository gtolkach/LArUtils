Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1101

== Noisy cells update for run 451022 (Thu, 11 May 2023 14:36:59 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1344_h417)
Cluster matching: based on Et > 4/10GeV plots requiring at least 100 events
Flagged cells:44
Flagged in PS:3
Unflagged by DQ shifters:0
Changed to SBN:2
Changed to HNHG:0
SBN:30
SBN in PS:1
HNHG:14
HNHG in PS:2

*****************
The treated cells were:
0 0 0 14 38 0 sporadicBurstNoise  # 0x3806a600 -> 0x2d406afa
0 0 0 14 39 0 sporadicBurstNoise  # 0x3806a700 -> 0x2d406af8
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 2 70 0 sporadicBurstNoise  # 0x3868c600 -> 0x2d200c08
0 0 13 2 71 0 sporadicBurstNoise  # 0x3868c700 -> 0x2d200e08
0 0 15 5 60 0 sporadicBurstNoise  # 0x387a3c00 -> 0x2d21f802
0 0 15 5 61 0 sporadicBurstNoise  # 0x387a3d00 -> 0x2d21fa02
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 13 23 0 sporadicBurstNoise  # 0x387e1700 -> 0x2d404a08
0 0 15 13 27 0 sporadicBurstNoise  # 0x387e1b00 -> 0x2d404c08
0 0 15 13 31 0 sporadicBurstNoise  # 0x387e1f00 -> 0x2d404e08
0 0 15 13 84 0 highNoiseHG sporadicBurstNoise  # 0x387e5400 -> 0x2d404a06
0 0 15 13 85 0 sporadicBurstNoise  # 0x387e5500 -> 0x2d404a04
0 0 15 13 88 0 sporadicBurstNoise  # 0x387e5800 -> 0x2d404c06
0 0 15 13 89 0 highNoiseHG sporadicBurstNoise  # 0x387e5900 -> 0x2d404c04
0 0 15 13 90 0 highNoiseHG sporadicBurstNoise  # 0x387e5a00 -> 0x2d404c02
0 0 15 13 92 0 highNoiseHG sporadicBurstNoise  # 0x387e5c00 -> 0x2d404e06
0 0 15 13 93 0 sporadicBurstNoise  # 0x387e5d00 -> 0x2d404e04
0 0 30 1 13 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x38f00d00 -> 0x2d000a44
0 1 0 12 77 0 sporadicBurstNoise  # 0x3905cd00 -> 0x2dc0260a
0 1 16 1 43 0 highNoiseHG sporadicBurstNoise reflaggedByLADIeS  # 0x39802b00 -> 0x2d802e40
0 1 16 12 44 0 sporadicBurstNoise  # 0x3985ac00 -> 0x2dc03700
1 0 2 13 19 0 sporadicBurstNoise  # 0x3a161300 -> 0x2ce024e8
1 0 2 13 23 0 sporadicBurstNoise  # 0x3a161700 -> 0x2ce026e8
1 0 2 13 25 0 sporadicBurstNoise  # 0x3a161900 -> 0x2ce024e4
1 0 2 13 28 0 highNoiseHG sporadicBurstNoise  # 0x3a161c00 -> 0x2ce026e6
1 0 2 13 29 0 sporadicBurstNoise  # 0x3a161d00 -> 0x2ce026e4
1 0 2 14 39 0 sporadicBurstNoise  # 0x3a16a700 -> 0x2cc450e8
1 0 2 14 43 0 sporadicBurstNoise  # 0x3a16ab00 -> 0x2cc452e8
1 0 2 14 47 0 sporadicBurstNoise  # 0x3a16af00 -> 0x2cc454e8
1 0 2 14 48 0 sporadicBurstNoise  # 0x3a16b000 -> 0x2cc44ee6
1 0 2 14 49 0 highNoiseHG  # 0x3a16b100 -> 0x2cc44ee4
1 0 2 14 52 0 highNoiseHG sporadicBurstNoise  # 0x3a16b400 -> 0x2cc450e6
1 0 2 14 53 0 highNoiseHG sporadicBurstNoise  # 0x3a16b500 -> 0x2cc450e4
1 0 2 14 54 0 sporadicBurstNoise  # 0x3a16b600 -> 0x2cc450e2
1 0 2 14 56 0 highNoiseHG sporadicBurstNoise  # 0x3a16b800 -> 0x2cc452e6
1 0 2 14 57 0 highNoiseHG sporadicBurstNoise  # 0x3a16b900 -> 0x2cc452e4
1 0 2 14 58 0 sporadicBurstNoise  # 0x3a16ba00 -> 0x2cc452e2
1 0 2 14 60 0 highNoiseHG sporadicBurstNoise  # 0x3a16bc00 -> 0x2cc454e6
1 0 2 14 61 0 unstable lowNoiseHG highNoiseHG sporadicBurstNoise  # 0x3a16bd00 -> 0x2cc454e4
1 0 2 14 62 0 sporadicBurstNoise  # 0x3a16be00 -> 0x2cc454e2
1 1 21 3 104 0 sporadicBurstNoise  # 0x3ba96800 -> 0x2e4403b0

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1101

== Missing EVs for run 451022 (Sun, 30 Apr 2023 15:16:24 +0200) ==

Found a total of 5 noisy periods, covering a total of 0.01 seconds
Found a total of 493 Mini noise periods, covering a total of 0.49 seconds
Lumi loss due to noise-bursts: 0.01 nb-1 out of 84096.80 nb-1 (0.00 per-mil)
Lumi loss due to mini-noise-bursts: 1.35 nb-1 out of 84096.80 nb-1 (0.02 per-mil)
Overall Lumi loss is 1.3612231661887597 by 0.49812553 s of veto length


