import sys
import ROOT as R
import numpy as np
import pandas as pd
import ctypes
PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"


sys.path.append("/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.42/InstallArea/x86_64-centos7-gcc11-opt/python/CaloMonitoring")
from LArCellBinning import lArCellBinningScheme
sys.path.append(PrintMappingDir)
import printMapping as pm
from LArCellBinning import lArCellBinningScheme


plots = {}
for subdet in ["EMB", "EMEC", "HEC", "FCAL" ]:
    plots[subdet] = {}
    for side in ["A", "C"]:
        plots[subdet][side] = {}
        for layer in [0, 1, 2, 3]:
            #plots[subdet][side][layer] = {}
            if layer == 0 and subdet.startswith("EM"):
                thislayer = "P"
            else:
                thislayer = str(layer)

            part = subdet+thislayer+side
            if part not in lArCellBinningScheme.etaRange.keys():
                plots[subdet][side][layer] = None
            else:
                xbins = np.array(lArCellBinningScheme.etaRange[part], dtype='d')
                ybins = np.array(lArCellBinningScheme.phiRange[part], dtype='d')
                plots[subdet][side][layer] = R.TH2D("h_"+part, "h_"+part, len(xbins)-1, xbins, len(ybins)-1, ybins)

isSC = False 

if isSC is False:
    onid = "ONL_ID"
    eta = "ETA"
    phi = "PHI"
    ieta = "IETA"
    iphi = "IPHI"
else:
    onid = "SC_ONL_ID"
    eta = "SCETA"
    phi = "SCPHI"
    ieta = "SCIETA"
    iphi = "SCIPHI"

want = [ onid, "DETNAME", "SAM", eta, phi, ieta, iphi]
print("Query printmapping")
pmout = pm.query(want = want, showdecimal = True)
pmdf = pd.DataFrame(pmout, columns = want)

for col in [ onid, ieta, iphi, "SAM" ]:
    pmdf = pmdf.astype({col: int})
for col in [ onid, eta, phi ]:
    pmdf = pmdf.astype({col: float})

SAM = pmdf['SAM'].values
DETNAME = pmdf['DETNAME'].values
ETA = sorted(pmdf[eta].values)
PHI = pmdf[phi].values


#function returning Series
def findBin(detname, layer, eta, phi):
    #detname = x.DETNAME
    det = detname[:-1]
    side = detname[-1:]
    thisplot = plots[det][side][layer]
    try:
        nBin = thisplot.FindBin(eta, phi)
    except:
        print(type(thisplot), det, side,layer)
        nBin = None
    return nBin




pmdf['nBin'] = pmdf.apply(lambda row : findBin(row["DETNAME"], row["SAM"], row[eta], row[phi]), axis = 1)
for layerName in lArCellBinningScheme.etaRange.keys():
    det = layerName[:-2]
    layer = layerName[-2:][0]
    side = layerName[-2:][1]
    detname = det+side
    if layer == "P": layer = 0
    test = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer) ) ]
    
    ncell = len(test.index.values)

    nbins = len(list(set(test['nBin'].values)))
    thisplot = plots[det][side][int(layer)]
    if ncell != nbins:
        print(layerName, ncell, nbins)
        counts = test.value_counts("nBin",sort=True)
        repeatBins = list(set(counts[counts > 1].index))
        for nb in repeatBins:

            def getLoHi(nb):
                x, y, z = ctypes.c_int(1), ctypes.c_int(1), ctypes.c_int(1)
                thisplot.GetBinXYZ(nb, x, y, z)
                xlo = thisplot.GetXaxis().GetBinLowEdge(x.value)
                xhi = thisplot.GetXaxis().GetBinUpEdge(x.value)
                ylo = thisplot.GetYaxis().GetBinLowEdge(y.value)
                yhi = thisplot.GetYaxis().GetBinUpEdge(y.value)
                return xlo, xhi, ylo, yhi
            for ni in range(nb-4, nb+4):
                xlo, xhi, ylo, yhi = getLoHi(ni)
                print(layerName, xlo, xhi, ylo, yhi)
                test = pmdf[ (pmdf[eta] >= xlo) & (pmdf[eta] <= xhi) & (pmdf[phi] >= ylo) & (pmdf[phi] <= yhi) ]

                if len(test.index.values) == 1:
                    thisonlid = test[onid].values[0]
                    print("OIOIOI", nb, ni, thisonlid)
                    pmdf.loc[pmdf[onid] == thisonlid, "nBin"] = ni

    print("*"*40)
    test = pmdf.loc[ ( pmdf["DETNAME"] == detname ) & ( pmdf["SAM"] == int(layer) ) ]
    
    ncell = len(test.index.values)
    nbins = len(list(set(test['nBin'].values)))

    if ncell != nbins:
        print(layerName, ncell, nbins)
        counts = test.value_counts("nBin",sort=True)
        repeatBins = list(set(counts[counts > 1].index))
        newtest = test[ test['nBin'].isin(repeatBins) ]
        print(newtest)
