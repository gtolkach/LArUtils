Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1202

== Noisy cells update for run 453795 (Wed, 7 Jun 2023 16:19:29 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x747_h424)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:115
Flagged in PS:48
Unflagged by DQ shifters:0
Changed to SBN:10
Changed to HNHG:0
SBN:99
SBN in PS:38
HNHG:16
HNHG in PS:10

*****************
The treated cells were:
0 0 4 1 14 0 sporadicBurstNoise  # 0x38200e00 -> 0x2d000c2c
0 0 4 13 37 0 sporadicBurstNoise  # 0x38262500 -> 0x2d4052bc
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 7 1 103 0 sporadicBurstNoise  # 0x38386700 -> 0x2d006620
0 0 7 9 20 0 sporadicBurstNoise  # 0x383c1400 -> 0x2d600a8e
0 0 7 9 21 0 sporadicBurstNoise  # 0x383c1500 -> 0x2d600a8c
0 0 7 9 22 0 sporadicBurstNoise  # 0x383c1600 -> 0x2d600a8a
0 0 7 9 23 0 sporadicBurstNoise  # 0x383c1700 -> 0x2d600a88
0 0 7 9 25 0 sporadicBurstNoise  # 0x383c1900 -> 0x2d600c8c
0 0 7 14 4 0 sporadicBurstNoise  # 0x383e8400 -> 0x2d40628e
0 0 8 1 17 0 highNoiseHG  # 0x38401100 -> 0x2d00121e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 13 67 0 sporadicBurstNoise  # 0x38564300 -> 0x2d404050
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 15 0 sporadicBurstNoise  # 0x38580f00 -> 0x2d000e10
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 11 1 28 0 sporadicBurstNoise  # 0x38581c00 -> 0x2d001810
0 0 13 1 5 0 sporadicBurstNoise  # 0x38680500 -> 0x2d000208
0 0 13 1 22 0 highNoiseHG  # 0x38681600 -> 0x2d001408
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 0 13 1 25 0 sporadicBurstNoise  # 0x38681900 -> 0x2d001a0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 14 2 28 0 sporadicBurstNoise  # 0x38709c00 -> 0x2d203806
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 11 87 0 sporadicBurstNoise  # 0x387d5700 -> 0x2d400a00
0 0 15 13 5 0 sporadicBurstNoise  # 0x387e0500 -> 0x2d40420c
0 0 16 8 40 0 sporadicBurstNoise  # 0x3883a800 -> 0x2d23507e
0 0 16 8 41 0 sporadicBurstNoise  # 0x3883a900 -> 0x2d23527e
0 0 16 13 14 0 sporadicBurstNoise  # 0x38860e00 -> 0x2d4047fa
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 22 6 117 0 sporadicBurstNoise  # 0x38b2f500 -> 0x2d226a64
0 0 22 6 118 0 sporadicBurstNoise  # 0x38b2f600 -> 0x2d226c64
0 0 22 6 119 0 sporadicBurstNoise  # 0x38b2f700 -> 0x2d226e64
0 0 23 1 5 0 highNoiseHG  # 0x38b80500 -> 0x2d000260
0 0 23 1 6 0 sporadicBurstNoise  # 0x38b80600 -> 0x2d000460
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 2 14 0 sporadicBurstNoise  # 0x38b88e00 -> 0x2d201c62
0 0 23 2 15 0 sporadicBurstNoise  # 0x38b88f00 -> 0x2d201e62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 12 67 0 sporadicBurstNoise  # 0x38c5c300 -> 0x2d402170
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 32 0 sporadicBurstNoise  # 0x38c82000 -> 0x2d00205a
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 1 63 0 sporadicBurstNoise  # 0x38d03f00 -> 0x2d003e54
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 7 0 sporadicBurstNoise  # 0x38f00700 -> 0x2d000644
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 0 31 6 4 0 sporadicBurstNoise  # 0x38fa8400 -> 0x2d220842
0 1 1 8 64 0 sporadicBurstNoise  # 0x390bc000 -> 0x2da30006
0 1 1 8 65 0 sporadicBurstNoise  # 0x390bc100 -> 0x2da30206
0 1 2 9 66 0 sporadicBurstNoise  # 0x39144200 -> 0x2de0002c
0 1 4 1 66 0 sporadicBurstNoise  # 0x39204200 -> 0x2d804410
0 1 4 1 70 0 sporadicBurstNoise  # 0x39204600 -> 0x2d804412
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 7 1 43 0 sporadicBurstNoise  # 0x39382b00 -> 0x2d802e1c
0 1 7 12 19 0 sporadicBurstNoise  # 0x393d9300 -> 0x2dc02876
0 1 8 1 67 0 sporadicBurstNoise  # 0x39404300 -> 0x2d804620
0 1 8 1 70 0 sporadicBurstNoise  # 0x39404600 -> 0x2d804422
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 11 13 106 0 sporadicBurstNoise  # 0x395e6a00 -> 0x2dc054bc
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 9 0 sporadicBurstNoise  # 0x39a90900 -> 0x2da09254
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 5 101 0 sporadicBurstNoise  # 0x39aa6500 -> 0x2da1ca56
0 1 21 5 102 0 sporadicBurstNoise  # 0x39aa6600 -> 0x2da1cc56
0 1 22 7 114 0 sporadicBurstNoise  # 0x39b37200 -> 0x2da2e45a
0 1 22 7 115 0 sporadicBurstNoise  # 0x39b37300 -> 0x2da2e65a
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 24 13 27 0 sporadicBurstNoise  # 0x39c61b00 -> 0x2dc04d86
0 1 28 1 71 0 sporadicBurstNoise  # 0x39e04700 -> 0x2d804672
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 1 5 10 40 0 sporadicBurstNoise  # 0x3b2ca800 -> 0x2e441270
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 12 4 14 0 sporadicBurstNoise  # 0x3b618e00 -> 0x2e2c1c38
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38
1 1 12 4 16 0 sporadicBurstNoise  # 0x3b619000 -> 0x2e2c2038
1 1 22 2 88 0 highNoiseHG  # 0x3bb0d800 -> 0x2ec00c70

