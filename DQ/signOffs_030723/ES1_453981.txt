Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1208

== Noisy cells update for run 453981 (Fri, 9 Jun 2023 15:59:19 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x747_h424)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:136
Flagged in PS:63
Unflagged by DQ shifters:0
Changed to SBN:9
Changed to HNHG:0
SBN:119
SBN in PS:52
HNHG:17
HNHG in PS:11

*****************
The treated cells were:
0 0 1 13 121 0 sporadicBurstNoise  # 0x380e7900 -> 0x2d405ce4
0 0 4 1 14 0 sporadicBurstNoise  # 0x38200e00 -> 0x2d000c2c
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 sporadicBurstNoise  # 0x38280500 -> 0x2d000228
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 5 1 53 0 sporadicBurstNoise  # 0x38283500 -> 0x2d003228
0 0 5 1 60 0 sporadicBurstNoise  # 0x38283c00 -> 0x2d003828
0 0 5 10 102 0 sporadicBurstNoise  # 0x382ce600 -> 0x2d6032a2
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 7 10 41 0 sporadicBurstNoise  # 0x383ca900 -> 0x2d60348c
0 0 7 11 53 0 sporadicBurstNoise  # 0x383d3500 -> 0x2d401a8c
0 0 7 14 16 0 sporadicBurstNoise  # 0x383e9000 -> 0x2d406086
0 0 7 14 20 0 highNoiseHG  # 0x383e9400 -> 0x2d406286
0 0 7 14 22 0 sporadicBurstNoise  # 0x383e9600 -> 0x2d406282
0 0 7 14 24 0 sporadicBurstNoise  # 0x383e9800 -> 0x2d406486
0 0 7 14 26 0 sporadicBurstNoise  # 0x383e9a00 -> 0x2d406482
0 0 7 14 44 0 sporadicBurstNoise  # 0x383eac00 -> 0x2d406e8e
0 0 7 14 62 0 sporadicBurstNoise  # 0x383ebe00 -> 0x2d406e82
0 0 7 14 63 0 sporadicBurstNoise  # 0x383ebf00 -> 0x2d406e80
0 0 8 12 56 0 sporadicBurstNoise  # 0x3845b800 -> 0x2d403c7e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 9 11 64 0 sporadicBurstNoise  # 0x384d4000 -> 0x2d400066
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 15 0 sporadicBurstNoise  # 0x38580f00 -> 0x2d000e10
0 0 11 1 23 0 highNoiseHG  # 0x38581700 -> 0x2d001610
0 0 11 1 24 0 sporadicBurstNoise  # 0x38581800 -> 0x2d001812
0 0 11 1 28 0 sporadicBurstNoise  # 0x38581c00 -> 0x2d001810
0 0 12 1 0 0 sporadicBurstNoise  # 0x38600000 -> 0x2d00000e
0 0 12 1 1 0 sporadicBurstNoise  # 0x38600100 -> 0x2d00020e
0 0 13 1 5 0 sporadicBurstNoise  # 0x38680500 -> 0x2d000208
0 0 13 1 22 0 sporadicBurstNoise  # 0x38681600 -> 0x2d001408
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 25 0 sporadicBurstNoise  # 0x38681900 -> 0x2d001a0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 14 1 0 0 sporadicBurstNoise  # 0x38700000 -> 0x2d000006
0 0 14 1 4 0 sporadicBurstNoise  # 0x38700400 -> 0x2d000004
0 0 14 1 5 0 sporadicBurstNoise  # 0x38700500 -> 0x2d000204
0 0 14 1 66 0 sporadicBurstNoise  # 0x38704200 -> 0x2d004406
0 0 14 2 28 0 sporadicBurstNoise  # 0x38709c00 -> 0x2d203806
0 0 15 5 61 0 sporadicBurstNoise  # 0x387a3d00 -> 0x2d21fa02
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 12 72 0 sporadicBurstNoise  # 0x387dc800 -> 0x2d402406
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 17 10 20 0 sporadicBurstNoise  # 0x388c9400 -> 0x2d602bee
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 20 1 5 0 sporadicBurstNoise  # 0x38a00500 -> 0x2d00026c
0 0 20 1 12 0 highNoiseHG  # 0x38a00c00 -> 0x2d00086c
0 0 21 11 65 0 sporadicBurstNoise  # 0x38ad4100 -> 0x2d4001a4
0 0 23 1 5 0 highNoiseHG  # 0x38b80500 -> 0x2d000260
0 0 23 1 6 0 sporadicBurstNoise  # 0x38b80600 -> 0x2d000460
0 0 23 1 63 0 sporadicBurstNoise  # 0x38b83f00 -> 0x2d003e60
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 13 23 0 sporadicBurstNoise  # 0x38be1700 -> 0x2d404b88
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 26 1 59 0 sporadicBurstNoise  # 0x38d03b00 -> 0x2d003e56
0 0 26 9 72 0 deadCalib sporadicBurstNoise  # 0x38d44800 -> 0x2d600556
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 0 31 6 4 0 sporadicBurstNoise  # 0x38fa8400 -> 0x2d220842
0 1 3 1 65 0 sporadicBurstNoise  # 0x39184100 -> 0x2d80420c
0 1 4 1 66 0 sporadicBurstNoise  # 0x39204200 -> 0x2d804410
0 1 4 1 93 0 sporadicBurstNoise  # 0x39205d00 -> 0x2d805a12
0 1 5 13 54 0 sporadicBurstNoise  # 0x392e3600 -> 0x2dc05a54
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 6 12 64 0 sporadicBurstNoise  # 0x3935c000 -> 0x2dc02068
0 1 7 1 43 0 sporadicBurstNoise  # 0x39382b00 -> 0x2d802e1c
0 1 7 2 3 0 sporadicBurstNoise  # 0x39388300 -> 0x2da0061c
0 1 7 2 4 0 sporadicBurstNoise  # 0x39388400 -> 0x2da0081c
0 1 7 2 5 0 sporadicBurstNoise  # 0x39388500 -> 0x2da00a1c
0 1 8 1 70 0 highNoiseHG  # 0x39404600 -> 0x2d804422
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
0 1 8 11 66 0 sporadicBurstNoise  # 0x39454200 -> 0x2dc0008c
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 5 0 sporadicBurstNoise  # 0x39480500 -> 0x2d800226
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 11 13 106 0 sporadicBurstNoise  # 0x395e6a00 -> 0x2dc054bc
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 15 1 32 0 sporadicBurstNoise  # 0x39782000 -> 0x2d80203c
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 17 1 36 0 sporadicBurstNoise  # 0x39882400 -> 0x2d802046
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 17 6 32 0 sporadicBurstNoise  # 0x398aa000 -> 0x2da24044
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 19 1 25 0 sporadicBurstNoise  # 0x39981900 -> 0x2d801a4c
0 1 21 3 9 0 sporadicBurstNoise  # 0x39a90900 -> 0x2da09254
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 21 10 30 0 sporadicBurstNoise  # 0x39ac9e00 -> 0x2de02f54
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 26 11 65 0 sporadicBurstNoise  # 0x39d54100 -> 0x2dc001aa
0 1 28 1 79 0 sporadicBurstNoise  # 0x39e04f00 -> 0x2d804e72
0 1 28 12 6 0 sporadicBurstNoise  # 0x39e58600 -> 0x2dc023c4
0 1 30 1 2 0 sporadicBurstNoise  # 0x39f00200 -> 0x2d800478
0 1 31 1 90 0 sporadicBurstNoise  # 0x39f85a00 -> 0x2d805c7c
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 1 10 6 42 0 highNoiseHG  # 0x3b52aa00 -> 0x3309a000
1 1 21 5 32 0 sporadicBurstNoise  # 0x3baa2000 -> 0x2e280062
1 1 21 5 33 0 sporadicBurstNoise  # 0x3baa2100 -> 0x2e280262
1 1 21 5 34 0 sporadicBurstNoise  # 0x3baa2200 -> 0x2e280462

