Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1218

== Noisy cells update for run 454163 (Tue, 13 Jun 2023 11:17:08 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x749_h425)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:149
Flagged in PS:42
Unflagged by DQ shifters:0
Changed to SBN:33
Changed to HNHG:0
SBN:134
SBN in PS:32
HNHG:15
HNHG in PS:10

*****************
The treated cells were:
0 0 3 10 63 0 sporadicBurstNoise  # 0x381cbf00 -> 0x2d4400ce
0 0 3 14 40 0 sporadicBurstNoise  # 0x381ea800 -> 0x2d406cce
0 0 3 14 44 0 sporadicBurstNoise  # 0x381eac00 -> 0x2d406ece
0 0 4 1 14 0 sporadicBurstNoise  # 0x38200e00 -> 0x2d000c2c
0 0 5 1 0 0 highNoiseHG  # 0x38280000 -> 0x2d00002a
0 0 5 1 1 0 sporadicBurstNoise  # 0x38280100 -> 0x2d00022a
0 0 5 1 3 0 sporadicBurstNoise  # 0x38280300 -> 0x2d00062a
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 6 1 13 0 sporadicBurstNoise  # 0x38300d00 -> 0x2d000a24
0 0 6 13 62 0 sporadicBurstNoise  # 0x38363e00 -> 0x2d405e9a
0 0 7 9 21 0 sporadicBurstNoise  # 0x383c1500 -> 0x2d600a8c
0 0 7 9 22 0 sporadicBurstNoise  # 0x383c1600 -> 0x2d600a8a
0 0 7 9 23 0 sporadicBurstNoise  # 0x383c1700 -> 0x2d600a88
0 0 7 9 25 0 sporadicBurstNoise  # 0x383c1900 -> 0x2d600c8c
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 7 10 41 0 sporadicBurstNoise  # 0x383ca900 -> 0x2d60348c
0 0 8 1 16 0 highNoiseHG  # 0x38401000 -> 0x2d00101e
0 0 8 1 17 0 highNoiseHG  # 0x38401100 -> 0x2d00121e
0 0 8 1 18 0 sporadicBurstNoise  # 0x38401200 -> 0x2d00141e
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 13 67 0 sporadicBurstNoise  # 0x38564300 -> 0x2d404050
0 0 11 1 4 0 sporadicBurstNoise  # 0x38580400 -> 0x2d000010
0 0 11 1 23 0 sporadicBurstNoise  # 0x38581700 -> 0x2d001610
0 0 13 1 22 0 highNoiseHG  # 0x38681600 -> 0x2d001408
0 0 13 1 23 0 sporadicBurstNoise  # 0x38681700 -> 0x2d001608
0 0 13 1 25 0 sporadicBurstNoise  # 0x38681900 -> 0x2d001a0a
0 0 13 1 28 0 sporadicBurstNoise  # 0x38681c00 -> 0x2d001808
0 0 13 1 29 0 sporadicBurstNoise  # 0x38681d00 -> 0x2d001a08
0 0 14 10 40 0 sporadicBurstNoise  # 0x3874a800 -> 0x2d60341e
0 0 15 5 60 0 sporadicBurstNoise  # 0x387a3c00 -> 0x2d21f802
0 0 15 5 61 0 sporadicBurstNoise  # 0x387a3d00 -> 0x2d21fa02
0 0 15 5 62 0 sporadicBurstNoise  # 0x387a3e00 -> 0x2d21fc02
0 0 15 5 63 0 sporadicBurstNoise  # 0x387a3f00 -> 0x2d21fe02
0 0 15 11 23 0 sporadicBurstNoise  # 0x387d1700 -> 0x2d400a08
0 0 15 14 36 0 sporadicBurstNoise  # 0x387ea400 -> 0x2d406a0e
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 16 8 40 0 sporadicBurstNoise  # 0x3883a800 -> 0x2d23507e
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 18 1 14 0 sporadicBurstNoise  # 0x38900e00 -> 0x2d000c74
0 0 22 6 117 0 sporadicBurstNoise  # 0x38b2f500 -> 0x2d226a64
0 0 22 6 118 0 sporadicBurstNoise  # 0x38b2f600 -> 0x2d226c64
0 0 22 6 119 0 sporadicBurstNoise  # 0x38b2f700 -> 0x2d226e64
0 0 23 2 5 0 sporadicBurstNoise  # 0x38b88500 -> 0x2d200a62
0 0 23 2 6 0 sporadicBurstNoise  # 0x38b88600 -> 0x2d200c62
0 0 23 14 35 0 sporadicBurstNoise  # 0x38bea300 -> 0x2d406988
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 25 1 22 0 sporadicBurstNoise  # 0x38c81600 -> 0x2d001458
0 0 25 1 24 0 sporadicBurstNoise  # 0x38c81800 -> 0x2d00185a
0 0 25 1 29 0 sporadicBurstNoise  # 0x38c81d00 -> 0x2d001a58
0 0 25 1 31 0 sporadicBurstNoise  # 0x38c81f00 -> 0x2d001e58
0 0 25 1 64 0 highNoiseHG  # 0x38c84000 -> 0x2d00405a
0 0 25 2 5 0 sporadicBurstNoise  # 0x38c88500 -> 0x2d200a5a
0 0 25 2 6 0 sporadicBurstNoise  # 0x38c88600 -> 0x2d200c5a
0 0 25 12 15 0 sporadicBurstNoise  # 0x38cd8f00 -> 0x2d402768
0 0 27 12 5 0 sporadicBurstNoise  # 0x38dd8500 -> 0x2d40234c
0 0 28 1 14 0 highNoiseHG  # 0x38e00e00 -> 0x2d000c4c
0 0 28 1 15 0 sporadicBurstNoise  # 0x38e00f00 -> 0x2d000e4c
0 0 30 1 5 0 sporadicBurstNoise  # 0x38f00500 -> 0x2d000244
0 0 30 1 6 0 highNoiseHG  # 0x38f00600 -> 0x2d000444
0 0 30 1 7 0 sporadicBurstNoise  # 0x38f00700 -> 0x2d000644
0 1 1 8 64 0 sporadicBurstNoise  # 0x390bc000 -> 0x2da30006
0 1 1 8 65 0 sporadicBurstNoise  # 0x390bc100 -> 0x2da30206
0 1 5 14 27 0 sporadicBurstNoise  # 0x392e9b00 -> 0x2dc0645e
0 1 7 10 80 0 sporadicBurstNoise  # 0x393cd000 -> 0x2de02878
0 1 7 13 66 0 sporadicBurstNoise  # 0x393e4200 -> 0x2dc0407c
0 1 8 1 67 0 sporadicBurstNoise  # 0x39404300 -> 0x2d804620
0 1 8 1 70 0 sporadicBurstNoise  # 0x39404600 -> 0x2d804422
0 1 8 9 66 0 sporadicBurstNoise  # 0x39444200 -> 0x2de0008c
0 1 8 11 89 0 sporadicBurstNoise  # 0x39455900 -> 0x2dc00c8a
0 1 9 1 28 0 sporadicBurstNoise  # 0x39481c00 -> 0x2d801826
0 1 10 1 36 0 sporadicBurstNoise  # 0x39502400 -> 0x2d80202a
0 1 10 10 20 0 sporadicBurstNoise  # 0x39549400 -> 0x2de02aa0
0 1 10 10 22 0 sporadicBurstNoise  # 0x39549600 -> 0x2de02aa4
0 1 10 13 28 0 sporadicBurstNoise  # 0x39561c00 -> 0x2dc04ea0
0 1 10 13 31 0 sporadicBurstNoise  # 0x39561f00 -> 0x2dc04ea6
0 1 10 13 32 0 sporadicBurstNoise  # 0x39562000 -> 0x2dc050a0
0 1 10 13 33 0 sporadicBurstNoise  # 0x39562100 -> 0x2dc050a2
0 1 10 13 34 0 sporadicBurstNoise  # 0x39562200 -> 0x2dc050a4
0 1 10 13 35 0 sporadicBurstNoise  # 0x39562300 -> 0x2dc050a6
0 1 10 13 36 0 sporadicBurstNoise  # 0x39562400 -> 0x2dc052a0
0 1 10 13 37 0 sporadicBurstNoise  # 0x39562500 -> 0x2dc052a2
0 1 10 13 38 0 sporadicBurstNoise  # 0x39562600 -> 0x2dc052a4
0 1 10 13 40 0 sporadicBurstNoise  # 0x39562800 -> 0x2dc054a0
0 1 10 13 41 0 sporadicBurstNoise  # 0x39562900 -> 0x2dc054a2
0 1 10 13 46 0 sporadicBurstNoise  # 0x39562e00 -> 0x2dc056a4
0 1 11 1 2 0 sporadicBurstNoise  # 0x39580200 -> 0x2d80042c
0 1 11 1 15 0 sporadicBurstNoise  # 0x39580f00 -> 0x2d800e2e
0 1 13 13 102 0 sporadicBurstNoise  # 0x396e6600 -> 0x2dc052dc
0 1 16 1 43 0 highNoiseHG  # 0x39802b00 -> 0x2d802e40
0 1 16 1 48 0 highNoiseHG  # 0x39803000 -> 0x2d803040
0 1 16 6 96 0 sporadicBurstNoise  # 0x3982e000 -> 0x2da24042
0 1 16 6 97 0 sporadicBurstNoise  # 0x3982e100 -> 0x2da24242
0 1 16 6 98 0 sporadicBurstNoise  # 0x3982e200 -> 0x2da24442
0 1 16 6 99 0 sporadicBurstNoise  # 0x3982e300 -> 0x2da24642
0 1 16 6 101 0 sporadicBurstNoise  # 0x3982e500 -> 0x2da24a42
0 1 17 1 36 0 sporadicBurstNoise  # 0x39882400 -> 0x2d802046
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 18 12 76 0 sporadicBurstNoise  # 0x3995cc00 -> 0x2dc02728
0 1 21 3 9 0 sporadicBurstNoise  # 0x39a90900 -> 0x2da09254
0 1 21 3 10 0 sporadicBurstNoise  # 0x39a90a00 -> 0x2da09454
0 1 21 3 11 0 sporadicBurstNoise  # 0x39a90b00 -> 0x2da09654
0 1 21 3 12 0 sporadicBurstNoise  # 0x39a90c00 -> 0x2da09854
0 1 22 7 114 0 sporadicBurstNoise  # 0x39b37200 -> 0x2da2e45a
0 1 22 7 115 0 sporadicBurstNoise  # 0x39b37300 -> 0x2da2e65a
0 1 23 2 1 0 sporadicBurstNoise  # 0x39b88100 -> 0x2da0025c
0 1 23 2 2 0 sporadicBurstNoise  # 0x39b88200 -> 0x2da0045c
0 1 23 14 7 0 sporadicBurstNoise  # 0x39be8700 -> 0x2dc06376
0 1 23 14 11 0 sporadicBurstNoise  # 0x39be8b00 -> 0x2dc06576
0 1 24 1 13 0 sporadicBurstNoise  # 0x39c00d00 -> 0x2d800a62
0 1 24 11 55 0 sporadicBurstNoise  # 0x39c53700 -> 0x2dc01b86
0 1 27 1 14 0 sporadicBurstNoise  # 0x39d80e00 -> 0x2d800c6e
0 1 28 1 76 0 sporadicBurstNoise  # 0x39e04c00 -> 0x2d804872
0 1 28 3 68 0 sporadicBurstNoise  # 0x39e14400 -> 0x2da08872
0 1 28 3 69 0 sporadicBurstNoise  # 0x39e14500 -> 0x2da08a72
0 1 28 3 70 0 sporadicBurstNoise  # 0x39e14600 -> 0x2da08c72
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 11 8 12 0 sporadicBurstNoise  # 0x3a5b8c00 -> 0x2ce00636
1 0 19 10 105 0 sporadicBurstNoise  # 0x3a9ce900 -> 0x2cc41b6c
1 0 21 13 123 0 sporadicBurstNoise  # 0x3aae7b00 -> 0x2ce02500
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 119 0 sporadicBurstNoise  # 0x3aaf7700 -> 0x2cc45100
1 1 9 2 68 0 sporadicBurstNoise  # 0x3b48c400 -> 0x2e200032
1 1 9 2 72 0 sporadicBurstNoise  # 0x3b48c800 -> 0x2e200034
1 1 9 2 80 0 highNoiseHG  # 0x3b48d000 -> 0x2e200038
1 1 9 4 19 0 sporadicBurstNoise  # 0x3b499300 -> 0x2e4000ce
1 1 9 4 32 0 sporadicBurstNoise  # 0x3b49a000 -> 0x2e4000d0
1 1 9 4 33 0 sporadicBurstNoise  # 0x3b49a100 -> 0x2e4000d2
1 1 9 4 34 0 sporadicBurstNoise  # 0x3b49a200 -> 0x2e4000d4
1 1 9 4 35 0 sporadicBurstNoise  # 0x3b49a300 -> 0x2e4000d6
1 1 9 4 36 0 sporadicBurstNoise  # 0x3b49a400 -> 0x2e4400d0
1 1 9 4 37 0 sporadicBurstNoise  # 0x3b49a500 -> 0x2e4400d2
1 1 9 4 38 0 sporadicBurstNoise  # 0x3b49a600 -> 0x2e4400d4
1 1 9 4 39 0 sporadicBurstNoise  # 0x3b49a700 -> 0x2e4400d6
1 1 9 4 40 0 sporadicBurstNoise  # 0x3b49a800 -> 0x2e4402d0
1 1 9 4 41 0 sporadicBurstNoise  # 0x3b49a900 -> 0x2e4402d2
1 1 9 4 42 0 sporadicBurstNoise  # 0x3b49aa00 -> 0x2e4402d4
1 1 9 4 43 0 sporadicBurstNoise  # 0x3b49ab00 -> 0x2e4402d6
1 1 9 4 44 0 sporadicBurstNoise  # 0x3b49ac00 -> 0x2e4404d0
1 1 12 4 14 0 sporadicBurstNoise  # 0x3b618e00 -> 0x2e2c1c38
1 1 12 4 15 0 sporadicBurstNoise  # 0x3b618f00 -> 0x2e2c1e38
1 1 12 4 16 0 sporadicBurstNoise  # 0x3b619000 -> 0x2e2c2038
1 1 21 5 32 0 sporadicBurstNoise  # 0x3baa2000 -> 0x2e280062
1 1 21 5 33 0 sporadicBurstNoise  # 0x3baa2100 -> 0x2e280262

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1218

== Event Veto for run 454163 (Mon, 12 Jun 2023 20:27:47 +0200) ==
Found Noise or data corruption in run 454163
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto454163.db

Found project tag data23_13p6TeV for run 454163
Found 1 Veto Ranges with 201 events
Found 3 isolated events
Reading event veto info from db sqlite://;schema=EventVeto454163.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-UPD4-13  Run 454163
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto454163.db;dbname=CONDBR2
Found a total of 1 corruption periods, covering a total of 3.32 seconds
Lumi loss due to corruption: 63.69 nb-1 out of 568663.15 nb-1 (0.11 per-mil)
Overall Lumi loss is 63.68779296476422 by 3.320211255 s of veto length



