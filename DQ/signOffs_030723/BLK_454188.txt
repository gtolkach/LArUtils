Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1220

== Noisy cells update for run 454188 (Sat, 24 Jun 2023 16:46:57 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:2 (f1360_h425)
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:19
Flagged in PS:6
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:15
SBN in PS:5
HNHG:4
HNHG in PS:1

*****************
The treated cells were:
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 5 0 highNoiseHG  # 0x38280500 -> 0x2d000228
0 0 7 9 19 0 highNoiseHG sporadicBurstNoise  # 0x383c1300 -> 0x2d600888
0 0 7 9 22 0 highNoiseHG sporadicBurstNoise  # 0x383c1600 -> 0x2d600a8a
0 0 11 1 6 0 sporadicBurstNoise  # 0x38580600 -> 0x2d000410
0 0 11 1 15 0 sporadicBurstNoise  # 0x38580f00 -> 0x2d000e10
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 1 8 1 79 0 sporadicBurstNoise  # 0x39404f00 -> 0x2d804e22
0 1 10 10 22 0 sporadicBurstNoise  # 0x39549600 -> 0x2de02aa4
0 1 10 13 37 0 highNoiseHG sporadicBurstNoise  # 0x39562500 -> 0x2dc052a2
0 1 10 13 39 0 sporadicBurstNoise  # 0x39562700 -> 0x2dc052a6
0 1 10 13 46 0 sporadicBurstNoise  # 0x39562e00 -> 0x2dc056a4
0 1 17 6 31 0 sporadicBurstNoise  # 0x398a9f00 -> 0x2da23e44
0 1 21 11 3 0 sporadicBurstNoise  # 0x39ad0300 -> 0x2dc00156
1 0 21 13 122 0 sporadicBurstNoise  # 0x3aae7a00 -> 0x2ce02502
1 0 21 13 127 0 sporadicBurstNoise  # 0x3aae7f00 -> 0x2ce02700
1 0 21 15 115 0 sporadicBurstNoise  # 0x3aaf7300 -> 0x2cc44f00
1 0 21 15 123 0 sporadicBurstNoise  # 0x3aaf7b00 -> 0x2cc45300
1 0 21 15 127 0 sporadicBurstNoise  # 0x3aaf7f00 -> 0x2cc45500

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1220

== Event Veto for run 454188 (Fri, 16 Jun 2023 07:45:10 +0200) ==
Found Noise or data corruption in run 454188
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto454188_Main.db

Found project tag data23_13p6TeV for run 454188
Found 15 Veto Ranges with 35 events
Found 48 isolated events
Reading event veto info from db sqlite://;schema=EventVeto454188_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 454188
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto454188_Main.db;dbname=CONDBR2
Found a total of 15 corruption periods, covering a total of 7.50 seconds
Lumi loss due to corruption: 140.87 nb-1 out of 533697.68 nb-1 (0.26 per-mil)
Overall Lumi loss is 140.86891372591631 by 7.502945135 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1220

== Missing EVs for run 454188 (Mon, 12 Jun 2023 12:21:07 +0200) ==

Found a total of 130 noisy periods, covering a total of 0.15 seconds
Found a total of 1057 Mini noise periods, covering a total of 1.16 seconds
Lumi loss due to noise-bursts: 2.78 nb-1 out of 533697.68 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 21.78 nb-1 out of 533697.68 nb-1 (0.04 per-mil)
Overall Lumi loss is 24.565590853866922 by 1.31114876 s of veto length


