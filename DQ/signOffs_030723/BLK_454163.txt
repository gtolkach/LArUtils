Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1218

== Event Veto for run 454163 (Thu, 15 Jun 2023 23:41:02 +0200) ==
Found Noise or data corruption in run 454163
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto454163_Main.db

Found project tag data23_13p6TeV for run 454163
Found 18 Veto Ranges with 4855 events
Found 40 isolated events
Reading event veto info from db sqlite://;schema=EventVeto454163_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 454163
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto454163_Main.db;dbname=CONDBR2
Found a total of 18 corruption periods, covering a total of 12.13 seconds
Lumi loss due to corruption: 228.61 nb-1 out of 568663.15 nb-1 (0.40 per-mil)
Overall Lumi loss is 228.6132174620619 by 12.125946975 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1218

== Event Veto for run 454163 (Thu, 15 Jun 2023 22:51:19 +0200) ==
Found Noise or data corruption in run 454163
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto454163_Main.db

Found project tag data23_13p6TeV for run 454163
Found 18 Veto Ranges with 4855 events
Found 40 isolated events
Reading event veto info from db sqlite://;schema=EventVeto454163_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 454163
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto454163_Main.db;dbname=CONDBR2
Found a total of 18 corruption periods, covering a total of 12.13 seconds
Lumi loss due to corruption: 228.61 nb-1 out of 568663.15 nb-1 (0.40 per-mil)
Overall Lumi loss is 228.6132174620619 by 12.125946975 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1218

== Missing EVs for run 454163 (Mon, 12 Jun 2023 21:21:22 +0200) ==

Found a total of 142 noisy periods, covering a total of 0.17 seconds
Found a total of 963 Mini noise periods, covering a total of 1.04 seconds
Lumi loss due to noise-bursts: 3.09 nb-1 out of 568663.15 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 19.29 nb-1 out of 568663.15 nb-1 (0.03 per-mil)
Overall Lumi loss is 22.373136173693716 by 1.2115705 s of veto length


