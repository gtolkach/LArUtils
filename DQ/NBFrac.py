from argparse import ArgumentParser
import ROOT as R
import glob
import numpy as np
import sys, os, errno
from getStreamRate import getRate

R.gROOT.SetBatch(True)

# Suggested TO DO list if this script proves useful:
# Make it more configurable: output directory, list of runs, some selection on runs? 
# Option to either read root files or from WDs. e.g. with get_hist_objects(run_spec, histogram) form the ATLAS DQM APIs. Returns dictionary with keys being run numbers (as strings) and values being an XML representation of the histogram (as a string). This representation can be turned into a usable ROOT histogram with the TBufferXML::ConvertFromXML
# Other combos, e.g. EMEC or EMB or EM or A-side or C-side together, as well as just "ALL"



def getRootFiles(stream, camp, express=True):
    if express:
        amistr = "x"
    else:
        amistr = "f"
    rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/*/"+camp+".*."+stream+".merge.HIST."+amistr+"*/"+camp+".*."+stream+".merge.HIST."+amistr+"*.1"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    return rootfiles


def main(args):
    rootfiles = getRootFiles(args.stream, args.camp, args.isExpress)
    runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
    runs = sorted(list(set(runs)))
    print(len(runs), "runs found for requested stream, campaign and tag")

    rates_CC = {}
    print("OI", runs)
    for run in runs:
        print("OI", run)
        rate_cc, rate_tot = getRate(run, "physics_CosmicCalo")
        try:
            rates_CC[run] = rate_cc/rate_tot
        except Exception as e:
            print(e)
            print("rate_cc",rate_cc)
            print("rate_tot",rate_tot)
            sys.exit()

    if args.runList is not None:
        runs = [r for r in runs if r in args.runList]
        print(len(runs), "runs after removing those which were not requested (requested"+str(len(args.runList))+")")
    else:
        args.runList = runs

    infiles = {}
    plotRuns = []

    flaggedStdRNB = {}
    flaggedStdRNB_notVeto = {}
    flaggedSatRNB = {}
    flaggedSatRNB_notVeto = {}
    flaggedStdRNB_eff = {}
    flaggedSatRNB_eff = {}

    flaggedTightMNB = {}
    flaggedTightMNB_notVeto = {}
    flaggedTight_PsVetoMNB = {}
    flaggedTight_PsVetoMNB_notVeto = {}
    flaggedLooseMNB = {}
    flaggedLooseMNB_notVeto = {}
    flaggedTightMNB_eff = {}
    flaggedTight_PsVetoMNB_eff = {}
    flaggedLooseMNB_eff = {}

    parts = [ "EMB", "EMEC", "HEC", "FCAL" ]
    sides = [ "A", "C" ]
    allparts = [ p+s for s in sides for p in parts ]
    combos = []


    outstr = args.camp+" "+args.stream+" "
    if args.isExpress:
        outstr += "express"
    else:
        outstr += "bulk"

    outnamestr = outstr.replace(" ","_")

    for run in runs:
        rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
        if len(rfile)>1:
            #print(len(rfile), run, ":", rfile)
            amis = sorted(list([ r.split(".")[-3] for r in rfile]))
            newest = [ r for r in rfile if "."+amis[-1]+"." in r]
            infiles[run] = newest[0]
        else:
            infiles[run] = rfile[0]
        infile = R.TFile(infiles[run], "R")
        print("Reading from file:",infiles[run])
        histpath = "run_"+str(run)

        topdirEMB = histpath+"/LAr/NoisyRO/EMB"

        topdirEMEC = histpath+"/LAr/NoisyRO/EMEC"

        flaggedStdRNB[run] = {}
        flaggedStdRNB_notVeto[run] = {}
        flaggedSatRNB[run] = {}
        flaggedSatRNB_notVeto[run] = {}
        flaggedStdRNB_eff[run] = {}
        flaggedSatRNB_eff[run] = {}

        flaggedTightMNB[run] = {}
        flaggedTightMNB_notVeto[run] = {}
        flaggedTight_PsVetoMNB[run] = {}
        flaggedTight_PsVetoMNB_notVeto[run] = {}
        flaggedLooseMNB[run] = {}
        flaggedLooseMNB_notVeto[run] = {}
        flaggedTightMNB_eff[run] = {}
        flaggedTight_PsVetoMNB_eff[run] = {}
        flaggedLooseMNB_eff[run] = {}

        def getHistSum(histpath):
            try:
                hsum = infile.Get(histpath).GetSumOfWeights()
            except:
                hsum = 0
            return hsum
        def getHistStdDev(histpath):
            try:
                stddev = infile.Get(histpath).GetStdDev()
            except:
                stddev = 0
            return stddev

        def getEff(flagged, notVeto):
            if flagged == 0 :
                return -10
            else:
                return 100 * (( flagged - notVeto ) / flagged)
        def getErr(hist):
            try:
                return hist.GetMeanError()
            except:
                return 0

        for part in allparts:
            if part.startswith("EMB"):
                topdir = topdirEMB
            else:
                topdir = topdirEMEC

            flaggedStdRNB[run][part] = getHistSum(topdir+"/NoisyEvent_"+part)
            flaggedStdRNB_notVeto[run][part] = getHistSum(topdir+"/NoisyEvent_TimeVeto_"+part)
            flaggedSatRNB[run][part] = getHistSum(topdir+"/SaturatedNoisyEvent_"+part)
            flaggedSatRNB_notVeto[run][part] = getHistSum(topdir+"/SaturatedNoisyEvent_TimeVeto_"+part)

            flaggedStdRNB_eff[run][part] = getEff(flaggedStdRNB[run][part], flaggedStdRNB_notVeto[run][part])
            flaggedSatRNB_eff[run][part] = getEff(flaggedSatRNB[run][part], flaggedSatRNB_notVeto[run][part])

            flaggedTightMNB[run][part] = getHistSum(topdir+"/MNBTightEvent_"+part)
            flaggedTightMNB_notVeto[run][part] = getHistSum(topdir+"/MNBTightEvent_TimeVeto_"+part)
            flaggedTight_PsVetoMNB[run][part] = getHistSum(topdir+"/MNBTight_PsVetoEvent_"+part)
            flaggedTight_PsVetoMNB_notVeto[run][part] = getHistSum(topdir+"/MNBTight_PsVetoEvent_TimeVeto_"+part)
            flaggedLooseMNB[run][part] = getHistSum(topdir+"/MNBLooseEvent_"+part)
            flaggedLooseMNB_notVeto[run][part] = getHistSum(topdir+"/MNBLooseEvent_TimeVeto_"+part)

            flaggedTightMNB_eff[run][part] = getEff(flaggedTightMNB[run][part], flaggedTightMNB_notVeto[run][part])
            flaggedTight_PsVetoMNB_eff[run][part] = getEff(flaggedTight_PsVetoMNB[run][part], flaggedTight_PsVetoMNB_notVeto[run][part])
            flaggedLooseMNB_eff[run][part] = getEff(flaggedLooseMNB[run][part], flaggedLooseMNB_notVeto[run][part])

        # Sum the values from each run and calculate total efficiencies
        flaggedStdRNB[run]["ALL"] = sum( flaggedStdRNB[run].values() )
        flaggedStdRNB_notVeto[run]["ALL"] = sum( flaggedStdRNB_notVeto[run].values() )
        flaggedSatRNB[run]["ALL"] = sum( flaggedSatRNB[run].values() )
        flaggedSatRNB_notVeto[run]["ALL"] = sum( flaggedSatRNB_notVeto[run].values() )
        flaggedStdRNB_eff[run]["ALL"] = getEff(flaggedStdRNB[run]["ALL"], flaggedStdRNB_notVeto[run]["ALL"])
        flaggedSatRNB_eff[run]["ALL"] = getEff(flaggedSatRNB[run]["ALL"], flaggedSatRNB_notVeto[run]["ALL"])
        flaggedTightMNB[run]["ALL"] = sum( flaggedTightMNB[run].values() )
        flaggedTightMNB_notVeto[run]["ALL"] = sum( flaggedTightMNB_notVeto[run].values() )
        flaggedTight_PsVetoMNB[run]["ALL"] = sum( flaggedTight_PsVetoMNB[run].values() )
        flaggedTight_PsVetoMNB_notVeto[run]["ALL"] = sum( flaggedTight_PsVetoMNB_notVeto[run].values() )
        flaggedLooseMNB[run]["ALL"] = sum( flaggedLooseMNB[run].values() )
        flaggedLooseMNB_notVeto[run]["ALL"] = sum( flaggedLooseMNB_notVeto[run].values() )
        flaggedTightMNB_eff[run]["ALL"] = getEff(flaggedTightMNB[run]["ALL"], flaggedTightMNB_notVeto[run]["ALL"])
        flaggedTight_PsVetoMNB_eff[run]["ALL"] = getEff(flaggedTight_PsVetoMNB[run]["ALL"], flaggedTight_PsVetoMNB_notVeto[run]["ALL"])
        flaggedLooseMNB_eff[run]["ALL"] = getEff(flaggedLooseMNB[run]["ALL"], flaggedLooseMNB_notVeto[run]["ALL"])
        if "ALL" not in combos:
            combos.append("ALL")


        if flaggedStdRNB[run]["ALL"] == 0 and flaggedSatRNB[run]["ALL"] == 0 and flaggedTightMNB[run]["ALL"] == 0 and flaggedTight_PsVetoMNB[run]["ALL"] == 0 and flaggedLooseMNB[run]["ALL"] == 0: 
            continue

        print("*",run,"*")
        def printvals(eff, flagged, notVeto, tag="?", low=None, high=None, flagged_err=None, notVeto_err=None):
            if low is not None:
                if eff <= low:
                    return
            if high is not None:
                if eff >= high:
                    return
            if flagged_err is not None and notVeto_err is not None:
                print(tag+" efficiency =", str(round(eff,2))+"% :", flagged, "flagged", "("+str(round(flagged_err,2))+")", notVeto, "not removed by veto", "("+str(round(notVeto_err,2))+")") 
            else:
                print(tag+" efficiency =", str(round(eff,2))+"% :", flagged, "flagged", notVeto, "not removed by veto") 

        low = -0.0001  # 0
        high = None  # 90

        for part in allparts+combos:
            printvals(flaggedStdRNB_eff[run][part], flaggedStdRNB[run][part], flaggedStdRNB_notVeto[run][part], "RNB Std "+part, low, high )#, flaggedStdRNB_EMECC_stddev, flaggedStdRNB_notVeto_EMECC_stddev)
            printvals(flaggedSatRNB_eff[run][part], flaggedSatRNB[run][part], flaggedSatRNB_notVeto[run][part], "RNB Sat "+part, low, high )#, flaggedSatRNB_EMECC_stddev, flaggedSatRNB_notVeto_EMECC_stddev)
            printvals(flaggedTightMNB_eff[run][part], flaggedTightMNB[run][part], flaggedTightMNB_notVeto[run][part], "MNB Tight "+part, low, high )#, flaggedTightMNB_EMECC_stddev, flaggedTightMNB_notVeto_EMECC_stddev)
            printvals(flaggedTight_PsVetoMNB_eff[run][part], flaggedTight_PsVetoMNB[run][part], flaggedTight_PsVetoMNB_notVeto[run][part], "MNB Tight_PsVeto "+part, low, high )#, flaggedTight_PsVetoMNB_EMECC_stddev, flaggedTight_PsVetoMNB_notVeto_EMECC_stddev)
            printvals(flaggedLooseMNB_eff[run][part], flaggedLooseMNB[run][part], flaggedLooseMNB_notVeto[run][part], "MNB Loose "+part, low, high )#, flaggedLooseMNB_EMECC_stddev, flaggedLooseMNB_notVeto_EMECC_stddev)

        plotRuns.append(run)

    canv = R.TCanvas()
    

    def drawPlot(yvals, outname, title="", ytitle="", xtitle="Run", verbose=False):
        canv.objs = []
        canv.Clear()
        p1 = R.TPad("p1", "", 0, 0, 1, 1)
        p2 = R.TPad("p2", "", 0, 0, 1, 1)
        p1.SetFillStyle(4000) # transparent
        p2.SetFillStyle(4000) # transparent
        if all(ele == -10 for ele in yvals): return  
        if verbose:
            print("*"*30)
            print(outname, {plotRuns[i]: yvals[i] for i in range(len(plotRuns))})
        p1.SetTitle("")
        p1.Draw()
        p1.cd()

        gr = R.TGraph(len(yvals), np.array(plotRuns, dtype='d'), np.array(yvals, dtype='d'))
        gr.GetHistogram().SetMinimum(-1.)
        gr.SetMarkerStyle(20)
        gr.SetTitle(title)
        gr.Draw("AP")
        gr.GetXaxis().SetMaxDigits(6)
        gr.GetXaxis().SetTitle(xtitle)
        gr.GetYaxis().SetTitle(ytitle)
        #xmin = R.TMath.MinElement(gr.GetN(), gr.GetX())
        xmin = gr.GetXaxis().GetBinLowEdge(1)
        #xmax = R.TMath.MaxElement(gr.GetN(), gr.GetX())
        xmax = gr.GetXaxis().GetBinUpEdge(gr.GetXaxis().GetNbins())
        ymin = gr.GetHistogram().GetMinimum()
        ymax = gr.GetHistogram().GetMaximum()
        def getLineY(yval, col, style=9):
            line = R.TLine(xmin,yval,xmax,yval) 
            line.SetLineColor(col)
            line.SetLineStyle(style)
            return line
        def getLineX(xval, col, style=9):
            line = R.TLine(xval,ymin,xval,ymax) 
            line.SetLineColor(col)
            line.SetLineStyle(style)
            return line
        line100 = getLineY(100, R.kRed)
        canv.objs.append(line100)
        line100.Draw("sameL")
        line90 = getLineY(90, R.kGray)
        canv.objs.append(line90)
        line90.Draw("sameL")
        line95 = getLineY(95, R.kGray)
        canv.objs.append(line95)
        line95.Draw("sameL")


        if args.runLines is not None:
            txt = R.TLatex()
            #txt.SetNDC()
            txt.SetTextColor(R.kBlue)
            txt.SetTextAngle(90)
            txt.SetTextAlign(10)
            txt.SetTextSize(0.025)
            line_run = {}
            for rl in args.runLines:
                line_run[rl] = getLineX(rl, R.kBlue)
                canv.objs.append(line_run[rl])
                line_run[rl].Draw("sameL")
                txt.DrawLatex(rl-2, 0.21, str(rl))
            
        gr.Draw("same,P")

        p1.Update()
        canv.objs.append(p1)

        ratevals = np.array([ rates_CC[r] for r in plotRuns], dtype='d')
        gr_rates = R.TGraph(len(ratevals), np.array(plotRuns, dtype='d'), ratevals)


        xmin = p1.GetUxmin()
        xmax = p1.GetUxmax()
        dx = (xmax - xmin) / 0.8 # 10 percent margins left and right
        ymin = gr_rates.GetHistogram().GetMinimum()
        ymax = gr_rates.GetHistogram().GetMaximum()
        dy = (ymax - ymin) / 0.8 # 10 percent margins top and bottom

        p2.Range(xmin-0.1*dx, ymin-0.1*dy, xmax+0.1*dx, ymax+0.1*dy);
        p2.SetTitle("")
        p2.Draw()
        canv.objs.append(p2)
        p2.cd()
        
        gr_rates.GetYaxis().SetTitle('# events CosmicCalo / # events total')
        gr_rates.SetLineColor(R.kGreen+2)
        gr_rates.SetMarkerColor(R.kGreen+2)
        gr_rates.SetTitle("")
        gr_rates.Draw("Pl")
        p2.Update()

        axis = R.TGaxis(xmax, ymin, xmax, ymax, ymin, ymax, 510, "+L");
        axis.SetLineColor(R.kGreen+2)
        axis.SetLabelColor(R.kGreen+2)
        axis.SetTitleColor(R.kGreen+2)
        axis.SetTitle(gr_rates.GetYaxis().GetTitle())
        axis.SetTitleOffset(1.3)
        axis.Draw()
        p2.Update()
        
        canv.Print(args.outdir+"/"+outname+".png")


    print(len(plotRuns), "runs")

    def cleanDict(thisdict, keylist):
        return { key: thisdict[key] for key in keylist }

    def listFromDict(thisdict, keylist, part):
        thisdict = cleanDict(thisdict, keylist)
        thislist = [ thisdict[key][part] if part in thisdict[key].keys() else -10 for key in keylist ]
        return thislist

    for part in allparts + combos:
        drawPlot( listFromDict(flaggedSatRNB_eff, plotRuns, part), "RNB_Sat_eff_"+part+"_"+outnamestr, "Efficiency of Saturated RNB Flag for "+part+" "+outstr, "RNB Saturated Veto Efficiency")
        drawPlot( listFromDict(flaggedStdRNB_eff, plotRuns, part), "RNB_Std_eff_"+part+"_"+outnamestr, "Efficiency of Standard RNB Flag for "+part+" "+outstr, "RNB Standard Veto Efficiency")
        drawPlot( listFromDict(flaggedLooseMNB_eff, plotRuns, part), "MNB_Loose_eff_"+part+"_"+outnamestr, "Efficiency of Loose MNB Flag for "+part+" "+outstr, "MNB Loose Veto Efficiency")
        drawPlot( listFromDict(flaggedTightMNB_eff, plotRuns, part), "MNB_Tight_eff_"+part+"_"+outnamestr, "Efficiency of Tight MNB Flag for "+part+" "+outstr, "MNB Tight Veto Efficiency")
        drawPlot( listFromDict(flaggedTight_PsVetoMNB_eff, plotRuns, part), "MNB_Tight_PsVeto_eff_"+part+"_"+outnamestr, "Efficiency of Tight_PsVeto MNB Flag for "+part+" "+outstr, "MNB Tight_PsVeto Veto Efficiency")


if __name__ == "__main__":
    parser = ArgumentParser()
    # parser.add_argument('-n', '--nruns', dest='nruns', default=None, type=int, help='If a run range is not defined - search for this number of runs above high run / below low run')
    parser.add_argument('-s', '--stream', dest='stream', default="physics_CosmicCalo", type=str, help='Stream to look at, e.g. physics_CosmicCalo, express_express, physics_Main')
    parser.add_argument('-c', '--camp', dest='camp', default="data23_13p6TeV", type=str, help='Campaign to look at, e.g. data23_13p6TeV, data_cos')
    parser.add_argument('--useBulk', dest='isBulk', default=False, action='store_true', help='Look for bulk output?')                         
    parser.add_argument('-o', '--outdir', dest='outdir', default=".", type=str, help='Output directory for plots')
    #parser.add_argument('-s', '--lowrun', dest='lowrun', default=None, type=int, help='Search for runs starting from this run number')
    #parser.add_argument('-e', '--highrun', dest='highrun', default=None, type=int, help='Search for runs with a maximum of this run number')
    parser.add_argument('-r', '--runlist', dest='runList', help='Run number which you would like to get information for', type=int, nargs='+', default=None,required=False)
    parser.add_argument('-l', '--runlines', dest='runLines', help='Run numbers which should be highlighted on the plots with a vertical, annotated line', type=int, nargs='+', default=None,required=False)
    #parser.add_argument('--onlyLAr', dest='ignoreNoLAr', action='store_true', help='Only show runs where LAr is enabled')
    #parser.add_argument('--onlySB', dest='onlySB', action='store_true', help='Only look at stable beam runs')
    args = parser.parse_args()
    args.isExpress = not args.isBulk

    args.outdir+="/effPlots"
    try:
        os.makedirs(args.outdir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    main(args)

