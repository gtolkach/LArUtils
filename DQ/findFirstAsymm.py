import ROOT as R
import glob
R.gROOT.SetBatch(True)

stream = "physics_MinBias"
camp = "data23_13p6TeV"
#camp = "data22_13p6TeV"
procver="f"
rootpath = "/eos/atlas/atlastier0/rucio/"+camp+"/"+stream+"/*/"+camp+".*."+stream+".merge.HIST."+procver+"*/"+camp+".*."+stream+".merge.HIST."+procver+"*.1"
print("Wildcard:",rootpath)
rootfiles = glob.glob(rootpath)
runs = sorted(list([int(r.split("/")[7]) for r in rootfiles]))
runs = sorted(list(set(runs)),reverse=True)
print(len(runs), "runs")
infiles = {}
for run in runs:
    print(run)
    rfile = [r for r in rootfiles if "/00"+str(run)+"/" in r]
    if len(rfile)>1:
        #print(len(rfile), run, ":", rfile)
        amis = sorted(list([ r.split(".")[-3] for r in rfile]))
        newest = [ r for r in rfile if "."+amis[-1]+"." in r]
        infiles[run] = newest[0]
    else:
        infiles[run] = rfile[0]

    infile = R.TFile(infiles[run], "R")
    histpath = "run_"+str(run)
    nthresh = 1
    histpaths = [  "CaloTopoClusters/CalECC/Thresh"+str(nthresh)+"ECCOcc", "CaloTopoClusters/CalBAR/Thresh"+str(nthresh)+"BAROcc", "CaloTopoClusters/CalECA/Thresh"+str(nthresh)+"ECAOcc" ]

    histpaths += [ "CaloTopoClusters/CalEMECC/EMThresh"+str(nthresh)+"ECCOcc", "CaloTopoClusters/CalEMBAR/EMThresh"+str(nthresh)+"BAROcc", "CaloTopoClusters/CalEMECA/EMThresh"+str(nthresh)+"ECAOcc" ]

    nperside=3
    canv = R.TCanvas("canv_"+str(run), "canv_"+str(run), 400*nperside, 800)
    canv.Divide(nperside,2)
    R.gStyle.SetPalette(R.kTemperatureMap)
    R.gStyle.SetOptStat(0)

    hists = {}
    print(len(histpaths))
    null = 0
    for hp in histpaths:
        fullpath = histpath+"/"+hp
        hists[hp] = infile.Get(fullpath)
        #print(histpaths.index(hp)+1,hists[hp])
        canv.cd(histpaths.index(hp)+1)
        R.gPad.SetRightMargin(0.2)
        if hists[hp]:
            hists[hp].SetTitle(str(run)+": "+hists[hp].GetTitle())
            zmin = 0
            zmax = 1000
            #hists[hp].GetZaxis().SetRangeUser(zmin,zmax)
            hists[hp].Draw("COLZ")
        else:
            null+=1
        canv.cd()

    if null < len(histpaths)/2:
        canv.Print("checkClusters_Thresh"+str(nthresh)+"_"+camp+"_"+stream+"_"+str(run)+".pdf")
    #input("Press Enter to continue...")

        
    infile.Close()
