Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1393

== Missing EVs for run 456749 (Mon, 17 Jul 2023 08:15:28 +0200) ==

Found a total of 7 noisy periods, covering a total of 0.01 seconds
Found a total of 11 Mini noise periods, covering a total of 0.01 seconds
Lumi loss due to noise-bursts: 0.05 nb-1 out of 10241.74 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 0.10 nb-1 out of 10241.74 nb-1 (0.01 per-mil)
Overall Lumi loss is 0.15023737037970694 by 0.02218249 s of veto length


