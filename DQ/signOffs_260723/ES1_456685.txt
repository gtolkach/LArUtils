Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1389

== Noisy cells update for run 456685 (Sun, 16 Jul 2023 22:12:19 +0200) ==
Tool version:WebDisplayExtractor-07-01-02 / Stream:physics_CosmicCalo / Source:tier0 / Processing version:1 (x754_h432)
Cluster matching: based on Et > 4/10GeV plots requiring at least 150 events
Flagged cells:41
Flagged in PS:10
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:34
SBN in PS:8
HNHG:7
HNHG in PS:2

*****************
The treated cells were:
0 0 2 12 85 0 sporadicBurstNoise  # 0x3815d500 -> 0x2d402ad4
0 0 5 1 0 0 sporadicBurstNoise  # 0x38280000 -> 0x2d00002a
0 0 5 1 4 0 sporadicBurstNoise  # 0x38280400 -> 0x2d000028
0 0 5 1 6 0 sporadicBurstNoise  # 0x38280600 -> 0x2d000428
0 0 9 1 4 0 highNoiseHG  # 0x38480400 -> 0x2d000018
0 0 9 1 5 0 sporadicBurstNoise  # 0x38480500 -> 0x2d000218
0 0 10 1 27 0 sporadicBurstNoise  # 0x38501b00 -> 0x2d001e16
0 0 15 14 40 0 sporadicBurstNoise  # 0x387ea800 -> 0x2d406c0e
0 0 18 1 5 0 sporadicBurstNoise  # 0x38900500 -> 0x2d000274
0 0 18 1 14 0 sporadicBurstNoise  # 0x38900e00 -> 0x2d000c74
0 0 22 10 86 0 sporadicBurstNoise  # 0x38b4d600 -> 0x2d602b92
0 0 23 1 5 0 highNoiseHG  # 0x38b80500 -> 0x2d000260
0 0 23 1 6 0 sporadicBurstNoise  # 0x38b80600 -> 0x2d000460
0 0 24 8 11 0 sporadicBurstNoise  # 0x38c38b00 -> 0x2d23165e
0 0 24 8 12 0 sporadicBurstNoise  # 0x38c38c00 -> 0x2d23185e
0 0 24 8 13 0 sporadicBurstNoise  # 0x38c38d00 -> 0x2d231a5e
0 0 30 10 34 0 sporadicBurstNoise  # 0x38f4a200 -> 0x2d60311a
0 1 21 5 101 0 sporadicBurstNoise  # 0x39aa6500 -> 0x2da1ca56
0 1 21 5 102 0 sporadicBurstNoise  # 0x39aa6600 -> 0x2da1cc56
0 1 22 7 109 0 sporadicBurstNoise  # 0x39b36d00 -> 0x2da2da5a
0 1 22 7 110 0 sporadicBurstNoise  # 0x39b36e00 -> 0x2da2dc5a
0 1 22 7 111 0 sporadicBurstNoise  # 0x39b36f00 -> 0x2da2de5a
0 1 26 11 65 0 sporadicBurstNoise  # 0x39d54100 -> 0x2dc001aa
1 0 2 2 84 0 highNoiseHG  # 0x3a10d400 -> 0x2ca00024
1 0 3 6 17 0 highNoiseHG  # 0x3a1a9100 -> 0x30857000
1 0 3 6 115 0 highNoiseHG  # 0x3a1af300 -> 0x318d3000
1 0 10 8 78 0 highNoiseHG  # 0x3a53ce00 -> 0x31100000
1 0 10 8 122 0 highNoiseHG  # 0x3a53fa00 -> 0x311c1000
1 0 15 13 17 0 sporadicBurstNoise  # 0x3a7e1100 -> 0x2ce025ec
1 0 15 13 20 0 sporadicBurstNoise  # 0x3a7e1400 -> 0x2ce027ee
1 0 15 13 21 0 sporadicBurstNoise  # 0x3a7e1500 -> 0x2ce027ec
1 0 15 14 36 0 sporadicBurstNoise  # 0x3a7ea400 -> 0x2cc451ee
1 0 15 14 37 0 sporadicBurstNoise  # 0x3a7ea500 -> 0x2cc451ec
1 0 15 14 40 0 sporadicBurstNoise  # 0x3a7ea800 -> 0x2cc453ee
1 0 15 14 41 0 sporadicBurstNoise  # 0x3a7ea900 -> 0x2cc453ec
1 0 15 14 44 0 sporadicBurstNoise  # 0x3a7eac00 -> 0x2cc455ee
1 0 15 14 45 0 sporadicBurstNoise  # 0x3a7ead00 -> 0x2cc455ec
1 0 15 14 46 0 sporadicBurstNoise  # 0x3a7eae00 -> 0x2cc455ea
1 1 9 2 68 0 sporadicBurstNoise  # 0x3b48c400 -> 0x2e200032
1 1 9 4 16 0 sporadicBurstNoise  # 0x3b499000 -> 0x2e4000c8
1 1 9 4 20 0 sporadicBurstNoise  # 0x3b499400 -> 0x2e4400c8

