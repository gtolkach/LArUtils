Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1390

== Noisy cells update for run 456714 (Mon, 24 Jul 2023 16:40:51 +0200) ==
Cluster matching: based on Et > 4/10GeV plots requiring at least 200 events
Flagged cells:10
Flagged in PS:8
Unflagged by DQ shifters:0
Changed to SBN:0
Changed to HNHG:0
SBN:7
SBN in PS:5
HNHG:3
HNHG in PS:3

*****************
The treated cells were:
0 0 7 10 39 0 sporadicBurstNoise  # 0x383ca700 -> 0x2d603288
0 0 10 1 27 0 highNoiseHG sporadicBurstNoise  # 0x38501b00 -> 0x2d001e16
0 0 13 1 24 0 sporadicBurstNoise  # 0x38681800 -> 0x2d00180a
0 0 13 1 27 0 sporadicBurstNoise  # 0x38681b00 -> 0x2d001e0a
0 0 20 1 4 0 highNoiseHG  # 0x38a00400 -> 0x2d00006c
0 0 20 1 5 0 sporadicBurstNoise  # 0x38a00500 -> 0x2d00026c
0 0 23 1 14 0 highNoiseHG  # 0x38b80e00 -> 0x2d000c60
0 0 23 1 15 0 sporadicBurstNoise  # 0x38b80f00 -> 0x2d000e60
0 0 30 1 15 0 sporadicBurstNoise  # 0x38f00f00 -> 0x2d000e44
0 1 28 11 84 0 sporadicBurstNoise  # 0x39e55400 -> 0x2dc00bc8

Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1390

== Event Veto for run 456714 (Thu, 20 Jul 2023 05:40:27 +0200) ==
Found Noise or data corruption in run 456714
Sqlite file:  /afs/cern.ch/work/l/larmon/public/EventVeto//EventVeto456714_Main.db

Found project tag data23_13p6TeV for run 456714
Found 21 Veto Ranges with 54 events
Found 58 isolated events
Reading event veto info from db sqlite://;schema=EventVeto456714_Main.db;dbname=CONDBR2, tag LARBadChannelsOflEventVeto-RUN2-Bulk-00  Run 456714
Reading folder /LAR/BadChannelsOfl/EventVeto from DB sqlite://;schema=EventVeto456714_Main.db;dbname=CONDBR2
Found a total of 21 corruption periods, covering a total of 10.50 seconds
Lumi loss due to corruption: 209.81 nb-1 out of 752803.51 nb-1 (0.28 per-mil)
Overall Lumi loss is 209.8098242448736 by 10.504214615 s of veto length



Discussed in https://its.cern.ch/jira/browse/ATLLARSWDPQ-1390

== Missing EVs for run 456714 (Mon, 17 Jul 2023 03:24:01 +0200) ==

Found a total of 197 noisy periods, covering a total of 0.23 seconds
Found a total of 1498 Mini noise periods, covering a total of 1.65 seconds
Lumi loss due to noise-bursts: 4.20 nb-1 out of 752803.51 nb-1 (0.01 per-mil)
Lumi loss due to mini-noise-bursts: 31.16 nb-1 out of 752803.51 nb-1 (0.04 per-mil)
Overall Lumi loss is 35.36191455634777 by 1.8768273800000002 s of veto length


