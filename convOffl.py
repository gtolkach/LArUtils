import sys
import printMapping as pm

infile = open("LatomeDB_masked_osum_run444421.csv","r")
lines = infile.readlines()
offIDs = [ line.strip() for line in lines if line.strip() != '']

want = ["SC_OFFL_ID","SC_ONL_ID","DETNAME","SAM"]
inputs={"want":want}
print("querying printMapping for",want)
pmquery = pm.query(**inputs)
pmout = { str(int(p[0],16)):p[1] for p in pmquery }
pmref = { str(int(p[0],16)):p[2:] for p in pmquery }
#pmdf = pd.DataFrame(pmquery, columns = want)
#pmdf = pmdf.set_index("SC_OFFL_ID")

checkFile = open("badSC_444421_RUN3-UPD4-00.txt", "r")
checkOnIDs = checkFile.readlines()
checkOnIDs = { line.split("#")[1].split("->")[0].strip():line.split("#")[0] for line in checkOnIDs }
print(checkOnIDs)
outf = open("TomSC_wrongFlag.txt","w+")
SCin=0
SCout=0
for offID in offIDs:
    #print(offID)
    if offID not in pmout.keys():
        print("WARNING - OFFLINE ID",offID,"NOT IN LARIDTRANSLATOR")
    else:
        if pmout[offID] in checkOnIDs.keys():
            print("SC "+offID+" ("+pmout[offID]+") is in the athena bad cells list:", checkOnIDs[pmout[offID]])
            SCin+=1
        else:
            print("This SC is NOT in the athena bad cells list")
            
            print("SC "+offID+" ("+pmout[offID]+", "+(" ").join(pmref[offID])+") is NOT in the athena bad cells list")
            outf.write(offID+"\n")
            SCout+=1
print("*"*30)
print(SCin, SCout)
