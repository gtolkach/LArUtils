//-----------------------------------------------------------------------
// File and Version Information:
// 
// PlotPolar.hh
//
// Description: 2D Histo Plotter in eta-phi space
//
//   
// Environment:
//      Software developed for the ATLAS Detector at the CERN LHC
//
// Author List:
//      Sven Menke
//
//-----------------------------------------------------------------------

#ifndef PLOTPOLAR_HH
#define PLOTPOLAR_HH

#include "TObject.h"
#include "TString.h"
#include "Gtypes.h"

class TH2F;

class PlotPolar: public TObject{

private:

  TString *_name;

  // Friends
  
  // Data members

  // Private methods
  void myDraw(const double& eta, const double& deta, 
	      const double& phi, const double& dphi, 
	      const double& eng, const double& emin, 
	      const double& emax, const Color_t &lcolor,
	      const char *option, const bool & log);
  
public:
  
  // Constructors
  PlotPolar() 
  {
    _name  = 0;
  }

  // Destructors
  virtual ~PlotPolar()
  {
    if ( _name ) {
      delete _name;
      _name = 0;
    }
  }

  // Operators

  // Selectors 

  // Modifiers

  // Methods

  void DrawHist(TH2F *histo=0, const char *option=0, bool dolog=false);

  double TanT(const double eta);

  void DrawGrid();

  ClassDef(PlotPolar,1)
};


#endif // PLOTPOLAR_HH

