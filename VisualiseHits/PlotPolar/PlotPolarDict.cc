// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME PlotPolardIPlotPolarDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "PlotPolar/PlotPolar.hh"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void *new_PlotPolar(void *p = nullptr);
   static void *newArray_PlotPolar(Long_t size, void *p);
   static void delete_PlotPolar(void *p);
   static void deleteArray_PlotPolar(void *p);
   static void destruct_PlotPolar(void *p);
   static void streamer_PlotPolar(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PlotPolar*)
   {
      ::PlotPolar *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PlotPolar >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("PlotPolar", ::PlotPolar::Class_Version(), "PlotPolar/PlotPolar.hh", 26,
                  typeid(::PlotPolar), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::PlotPolar::Dictionary, isa_proxy, 16,
                  sizeof(::PlotPolar) );
      instance.SetNew(&new_PlotPolar);
      instance.SetNewArray(&newArray_PlotPolar);
      instance.SetDelete(&delete_PlotPolar);
      instance.SetDeleteArray(&deleteArray_PlotPolar);
      instance.SetDestructor(&destruct_PlotPolar);
      instance.SetStreamerFunc(&streamer_PlotPolar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PlotPolar*)
   {
      return GenerateInitInstanceLocal((::PlotPolar*)nullptr);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::PlotPolar*)nullptr); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr PlotPolar::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *PlotPolar::Class_Name()
{
   return "PlotPolar";
}

//______________________________________________________________________________
const char *PlotPolar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PlotPolar*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int PlotPolar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PlotPolar*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PlotPolar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PlotPolar*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PlotPolar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PlotPolar*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void PlotPolar::Streamer(TBuffer &R__b)
{
   // Stream an object of class PlotPolar.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      TObject::Streamer(R__b);
      R__b >> _name;
      R__b.CheckByteCount(R__s, R__c, PlotPolar::IsA());
   } else {
      R__c = R__b.WriteVersion(PlotPolar::IsA(), kTRUE);
      TObject::Streamer(R__b);
      R__b << _name;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PlotPolar(void *p) {
      return  p ? new(p) ::PlotPolar : new ::PlotPolar;
   }
   static void *newArray_PlotPolar(Long_t nElements, void *p) {
      return p ? new(p) ::PlotPolar[nElements] : new ::PlotPolar[nElements];
   }
   // Wrapper around operator delete
   static void delete_PlotPolar(void *p) {
      delete ((::PlotPolar*)p);
   }
   static void deleteArray_PlotPolar(void *p) {
      delete [] ((::PlotPolar*)p);
   }
   static void destruct_PlotPolar(void *p) {
      typedef ::PlotPolar current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_PlotPolar(TBuffer &buf, void *obj) {
      ((::PlotPolar*)obj)->::PlotPolar::Streamer(buf);
   }
} // end of namespace ROOT for class ::PlotPolar

namespace {
  void TriggerDictionaryInitialization_PlotPolarDict_Impl() {
    static const char* headers[] = {
"PlotPolar/PlotPolar.hh",
nullptr
    };
    static const char* includePaths[] = {
"/home/ellis/root/include/",
"/home/ellis/VisualiseHits/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "PlotPolarDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$PlotPolar/PlotPolar.hh")))  PlotPolar;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "PlotPolarDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "PlotPolar/PlotPolar.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"PlotPolar", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("PlotPolarDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_PlotPolarDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_PlotPolarDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_PlotPolarDict() {
  TriggerDictionaryInitialization_PlotPolarDict_Impl();
}
