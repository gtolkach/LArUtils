//-----------------------------------------------------------------------
// File and Version Information:
//
//
// Description: see PlotPolar.hh
// 
// Environment:
//      Software developed for the ATLAS Detector at CERN LHC
//
// Author List:
//      Sven Menke
//
//-----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "PlotPolar.hh"

//----------------------------
// This Class's Base Header --
//----------------------------
#include "TObject.h"

//---------------
// C++ Headers --
//---------------
#include <iostream>
#include "TROOT.h"
#include "TCanvas.h"
#include "TEllipse.h"
#include "TGraph.h"
#include "TLine.h"
#include "TMath.h"
#include "TPolyLine.h"
#include "TStyle.h"
#include "TH2.h"

//---------------
// C Headers --
//---------------
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifdef linux
#include <unistd.h>
#endif

//----------------
// Constructors --
//----------------

ClassImp(PlotPolar);

//-------------
// Modifiers --
//-------------


void PlotPolar::DrawHist(TH2F *histo, const char *option, bool dolog) {
  if ( histo && histo->GetEntries()>0) {
    gPad->SetBit(TGraph::kClipFrame,true);
    TAxis * xax = histo->GetXaxis();
    TAxis * yax = histo->GetYaxis();
    double emax=histo->GetMaximum();
    double emin=histo->GetMinimum();
    Color_t lcolor=histo->GetLineColor();
    for (int ix=0;ix<histo->GetNbinsX();ix++)
      for (int iy=0;iy<histo->GetNbinsY();iy++)
	if ( histo->GetBinContent(ix+1,iy+1) != 0 ) {
	  double eng,eta,phi,deta,dphi;
	  eng = histo->GetBinContent(ix+1,iy+1);
	  eta = yax->GetBinCenter(iy+1);
	  deta = (yax->GetBinUpEdge(iy+1)-eta);
	  phi = xax->GetBinCenter(ix+1);
	  dphi = (xax->GetBinUpEdge(ix+1)-phi);
	  myDraw(eta,deta,phi,dphi,eng,emin,emax,lcolor,option,dolog);
	}
    gPad->Modified();
    gPad->Update();
  }
}

void PlotPolar::DrawGrid() {
  gPad->SetBit(TGraph::kClipFrame,true);
  //double etas[] = {3.2,2.5,1.5,0.5,0.,-.5};
  double etas[] = {1.385,2.5,3.2};
  int netas = sizeof(etas)/sizeof(etas[0]);
  int nphi = 32;
  int i;
  for(i=0;i<netas;i++) {
    //double r=5-etas[i];
    double r=TanT(etas[i]);
    TEllipse * te = new TEllipse(0,0,r);
    te->SetLineWidth(1);
    te->SetFillStyle(0);
    te->SetLineColor(1);
    te->SetLineStyle(4);
    te->Draw();
  }
  for(i=0;i<nphi;i++) {
    //double rmin=5-etas[0];
    //double rmax=5-etas[netas-1];
    double rmin=TanT(etas[0]);
    double rmax=TanT(etas[netas-1]);
    double phi=TMath::TwoPi()/nphi*i;
    double sp=sin(phi);
    double cp=cos(phi);
    TLine * tl = new TLine(rmin*cp,rmin*sp,rmax*cp,rmax*sp);
    tl->SetLineWidth(1);
    tl->SetLineColor(1);
    tl->SetLineStyle(4);
    tl->Draw();
  }
  gPad->Modified();
  gPad->Update();
}

void PlotPolar::myDraw(const double& eta, const double& deta, 
		       const double& phi, const double& dphi, 
		       const double& eng, const double& emin, 
		       const double& emax, const Color_t &lcolor,
		       const char *option,const bool &dolog) {
  bool drawLogz = dolog;
  int ncolors = gStyle->GetNumberOfColors();
  double r[2];

  int loop=1;
  r[0] = TanT(eta);
//  if ( option && ( option[0] == 'E' || option[1] == 'E' )) {
//    if (fabs(eta-deta) < 0.5 && fabs(eta+deta) < 0.5 ) {
//      r[0] = 5-eta;
//      if (eta-deta < -0.25 && eta+deta < -0.25 ) {
//	loop=2;
//	r[1] = 5-fabs(eta);
//      }
//    }
//    else
//      r[0] = 5-fabs(eta);
//  }
//  else
//    r[0] = 5-eta;

  double slphi[2] = {sin(phi+dphi),sin(phi-dphi)};
  double clphi[2] = {cos(phi+dphi),cos(phi-dphi)};

  while ( loop > 0 ) {
    //double dr=fabs(deta);
    
    double xa[5];
    double ya[5];
    //double lr[2] = {r[loop-1]-dr,r[loop-1]+dr};
    double lr[2] = {TanT(eta-deta),TanT(eta+deta)};
    for (int i=0;i<4;i++) {
      xa[i] = lr[((int)(i/2))]*clphi[(((int)((i+1)/2))%2)];
      ya[i] = lr[((int)(i/2))]*slphi[(((int)((i+1)/2))%2)];
    }
    xa[4] = xa[0];
    ya[4] = ya[0];
    TPolyLine *tl = new TPolyLine(5,xa,ya);
    tl->SetLineColor(lcolor);
    tl->SetLineWidth(1);
    tl->SetFillStyle(1001);
    if ( !option || ( option[0] != 'G' && option[1] != 'G' )) {
      int fcolor = gStyle->GetColorPalette(0);
      double mye,myemin,myemax;
      if ( drawLogz ) {
	myemin = log10(emin>0?emin:1e-3);
	myemax = log10(emax>0?emax:1);
	mye = (fabs(eng)>0?log10(fabs(eng)):-3);
      }
      else {
	mye = eng;
	myemin = emin;
	myemax = emax;
      }
      if ( mye > myemin ) {
	if ( mye >= myemax )
	  fcolor = gStyle->GetColorPalette(ncolors-1);
	else {
	  fcolor = gStyle->GetColorPalette((int)((mye - myemin)/(myemax-myemin)*(ncolors-1)));
	}
	tl->SetFillColor(fcolor);
      
	tl->Draw("f");
      }
    }
    else 
      tl->Draw("");
    loop--;
  }
}

double PlotPolar::TanT(const double eta) {
  return fabs(tan(2*atan(exp(-eta))));
}

