#!/bin/sh

LCG=$1

cmd="source /cvmfs/sft.cern.ch/lcg/views/${LCG}/x86_64-el9-gcc13-opt/setup.sh"
echo ${cmd}
eval ${cmd}

for var in PYTHONPATH PYTHONHOME LD_LIBRARY_PATH ROOTSYS C_INCLUDE_PATH CPLUS_INCLUDE_PATH CMAKE_PREFIX_PATH MANPATH PATH ROOT_INCLUDE_PATH CORAL_AUTH_PATH CORAL_DBLOOKUP_PATH TNS_ADMIN; do
    if [ -n "${var}" ]; then
	#echo "SetEnv ${var} \"${!var}\""
	echo "export ${var}=\"${!var}\""
	#echo $var yes
    else
	#echo "SetEnv ${var} \"\""
	echo "export ${var} \"\""
	#echo $var no
    fi
    
done
