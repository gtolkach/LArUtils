#!/bin/bash

isSC=0
if [[ $# -lt 1 ]]; then
    echo "Please provide the run number as an argument"
fi

runNumber=$1

if [[ $# -gt 1 ]]; then
    isSC=$2
fi

# Set up athena
#AthenaCmd=Athena,master,latest
AthenaCmd="--stable 24.0,Athena,latest"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
ascmd="source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh "
echo ${ascmd}
eval ${ascmd}

export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup/scripts
acmd="source ${AtlasSetup}/asetup.sh ${AthenaCmd}"
echo ${acmd}
eval ${acmd}

if ! which AtlCoolCopy 1>/dev/null 2>&1
then
    echo "No offline setup found!"
    exit
fi

if [ $isSC == 0 ]
then
    echo "Dumping bad channel folder content for standard cells"
    echo "Resolving current folder-level tag suffix for /LAR/BadChannelsOfl/BadChannels...."
    fulltag=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" /LAR/BadChannelsOfl/BadChannels` 
    if [ $? -ne 0 ]
    then
	exit
    fi
    upd4TagName=`echo $fulltag | grep -o "RUN2-UPD4-[0-9][0-9]"` 
    echo "Found UPD4 $upd4TagName"
    upd1TagName="RUN2-UPD1-00"
    BulkTagName="RUN2-Bulk-00"
    upd3TagName="RUN2-UPD3-00"
    outname="badChan_${runNumber}"
    SCParam=""
else   
    echo "Dumping bad channel folder content for SuperCells"
    echo "Resolving current folder-level tag suffix for /LAR/BadChannelsOfl/BadChannelsSC...."
    fulltag=`getCurrentFolderTag.py "COOLOFL_LAR/CONDBR2" /LAR/BadChannelsOfl/BadChannelsSC` 
    if [ $? -ne 0 ]
    then
	exit
    fi
    upd4TagName=`echo $fulltag | grep -o "RUN3-UPD4-[0-9][0-9]"` 
    echo "Found UPD4 $upd4TagName"
    upd1TagName="RUN3-UPD1-00"
    BulkTagName="RUN3-Bulk-00"
    upd3TagName="RUN3-UPD3-00"
    outname="badSC_${runNumber}"
    SCParam="--SC"
fi

for tag in ${upd4TagName} ${upd1TagName} ${upd3TagName} ${BulkTagName}; do
    thisoutname=${outname}_${tag}.txt
    if [[ "$tag" == *"UPD1"* ]]; then 
	database="LAR_ONL"
    else
	database="LAR_OFL"
    fi
  
    echo "Running athena to read current database content in tag ${tag} for run number ${runNumber}"
    athcmd="python -m LArBadChannelTool.LArBadChannel2Ascii -r ${runNumber} -o ${thisoutname} -t ${tag} -d ${database} ${SCParam}"
    eval ${athcmd} > oracle2ascii_${tag}.log 2>&1 

  if [ $? -ne 0 ];  then
      echo "Athena reported an error reading back sqlite file ! Please check oracle2ascii_${tag}.log!"
  fi

done
