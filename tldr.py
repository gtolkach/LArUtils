#!/bin/python3 
import sys, os
import imaplib
import email
from getpass import getpass, getuser 
import smtplib 
from email.message import EmailMessage
from argparse import ArgumentParser


def sendMail(mail, override={}):
    #smtp = smtplib.SMTP()
    #smtp.connect()
    #smtp.sendmail(mail['From'], mail['To'], mail.get_payload())
    msg = EmailMessage()
    msg.set_content(mail.get_payload())
    
    for k in ['Subject', 'From', 'To' ]:
        msg[k] = mail[k]
    for ok in override.keys():
        msg[ok] = override[ok]

    print(msg['From'], msg['To'], msg['Subject'])
    #smtp.close()


def connectIMAP(server="imap.cern.ch", returncred=False):
    username = getuser()
    print("Please provide password for",username)
    password = getpass()
    # create an IMAP4 class with SSL 
    imap = imaplib.IMAP4_SSL(server)
    # authenticate
    try:
        imap.login(username, password)
    except Exception:
        password = getpass()
        imap.login(username, password)
    if returncred:
        return imap, [username,password]
    else:
        return imap

def getMessages(folder="inbox", local=False, mailmax=20000, returncred=False, criteria={}):
    if returncred:
        imap, cred = connectIMAP(returncred=returncred)
    else:
        imap = connectIMAP()
    folders =  imap.list()[1]
    folders = [ f.decode('utf-8').replace('"','').split('/ ')[1] for f in folders ]

    if folder not in folders:
        print("Requested folder",folder,"not in mail box... using inbox")
        folder = 'inbox'
    (status, response_text) = imap.select('"{}"'.format(folder))
    message_count = int(response_text[0].decode("ascii"))
    print("Found",message_count,"messages")

    result, data = imap.uid('search', None, "ALL")
    if result != 'OK':
        print("Problem getting emails...")
        sys.exit()
    
    mails = []
    datalist = data[0].split()
    datalist.reverse()
    datalist = datalist[0:mailmax]
    for num in datalist:
        result, data = imap.uid('fetch', num, '(RFC822)')
        if result != 'OK':
            print("Problem getting mail",num)
            continue
        email_message = email.message_from_bytes(data[0][1])
        if isinstance(email_message.get_payload(), str):
            content = email_message.get_payload()
        else:
            content = str(email_message.get_payload()[0])
        
        if email_message['Subject'] is None: continue
        if not isinstance(email_message['Subject'], str): continue # not sure why it is sometimes 'Header'
        if "RE:" in email_message['Subject']: continue
        if "Re:" in email_message['Subject']: continue

        keepmail=0
        for req in criteria.keys():
            if isinstance(criteria[req], list):
                if email_message[req] is None:
                    keepmail=0
                    break
                if all(r in email_message[req] for r in criteria[req]):
                    keepmail=1
                    #print("keeping", req, email_message[req])
                else:
                    keepmail=0
                    break
            else:
                if criteria[req] in email_message[req]:
                    keepmail=1
                    #print("keeping", req, email_message[req])
                else:
                    keepmail=0
                    break
        if keepmail == 1:
            mails.append(email_message)
                    
    if not local:
        if returncred:
            return mails, cred
        else:
            return mails 
    imap.close()
    imap.logout()        


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument('-f', '--folder', dest='folder', default='inbox', help='Folder in your emails where the db expert update requests are found.')
    parser.add_argument('-s', '--subject', dest='Subject', nargs='+', default=None, help='Strings to search for in the subjects of mails')
    parser.add_argument('-fr', '--from', dest='From', nargs='+', default=None, help='Email address(es) from which the target mails are sent (match all)')
    parser.add_argument('-to', '--to', dest='To', nargs='+', default=None, help='Email address(es) to which the target mails are sent (match all)')
    parser.add_argument('--sendto', dest='sendTo', nargs='+', default=None, help='Email address to send the forwarded mail to')
    args = parser.parse_args()

    if args.Subject is None and args.From is None and args.To is None:
        print("Please provide some criteria...")

    criteria = {}
    if args.Subject is not None:
        criteria['Subject'] = args.Subject
    if args.From is not None:
        criteria['From'] = args.From
    if args.To is not None:
        criteria['To'] = args.To
    

    mails = getMessages(folder=args.folder, criteria=criteria, mailmax=1000)

    
    print("-"*30)
    
    print(len(mails), "emails found")

    if args.sendTo is not None:
        sFrom = os.environ["USER"]+" <"+os.environ["USER"]+"@cern.ch>"
        for m in mails:
            sendMail(m, override={'From':sFrom, 'To':args.sendTo})
