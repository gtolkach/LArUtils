#!/bin/python3
import glob
import uproot as U
import awkward as ak
import numpy as np
import pandas as pd
import sys
import printMapping

#chansToCheck = [962594304,939524096]
chansToCheck = [972559360, 952110080, 960498176, 958934528, 962601984, 943249408, 961093120, 942172160, 971052544, 967384576, 967385600, 967386624, 969481216, 946976256, 969541632, 972678656, 972667904, 948007936, 948005888, 948009984, 948012032, 944900608, 971115008, 951175168, 952219648, 967444992, 967967744, 967968256, 953291264, 953290752, 953290240, 953289728, 952772608, 954874880, 963776000, 982516736, 982517248, 1000885760, 999350272, 998040576, 998036992, 994889216, 996212224, 977835520, 977827328, 975234560, 973623296, 978075136, 975242752, 973623808, 973625344, 973625856, 973627392, 990916608, 973627904, 990917120, 973629440, 973629952, 973631488, 973632000, 973633536, 973634048, 973635584, 973636096, 973637632, 973638144, 973639680, 973640192, 973641728, 973642240, 973643776, 973644288, 973645824, 973646336, 991169536, 991170048, 997785088, 997773824, 995764224, 995762176, 994909696, 994628096, 1002580480, 990965760, 994873344, 990958080, 977886208, 993651712, 993645568, 993647616, 974195712, 977414656, 977415680, 995721728, 984183296, 984189952, 984382976, 994856960, 989997056, 998888448, 998888960, 981051904, 994692096, 976893952, 976894464, 998289408, 1001392128, 995101184, 991509504, 991441920, 978365440, 978419200, 974682624, 1001397248, 981504512, 981524992]


want = ["SC_ONL_ID","ONL_ID","BadChannels_UPD1", "BadSC"]
have = ["SC_ONL_ID"]
have_val = [ ",".join([str(ch) for ch in chansToCheck])]
inputs={"want":want,"showdecimal":True,"have":have,"have_val":have_val, "run":473617}
print("querying printMapping for",inputs)
pmout = printMapping.query(**inputs)
pmdf = pd.DataFrame(pmout, columns = want)
pmdf = pmdf.astype({'SC_ONL_ID':'int', 'ONL_ID':'int'})
checkPA = True
checkRamp = True
checkCali = True
checkPhys = True

topdir = "/eos/home-l/lardaq/P1Results"
indir_weekly = "LATOMERun_BarrelEndcapWeekly_240416-004831"
indir_PA = "LATOMERun_PulseAll_473633_240417-075313"

infiles_ramp = glob.glob(f"{topdir}/{indir_weekly}/LAr*Ramp*.root")
#print(infiles_ramp)

infiles_caliwave = glob.glob(f"{topdir}/{indir_weekly}/LAr*CaliWave*.root")
#print(infiles_caliwave)

infiles_physwave = glob.glob(f"{topdir}/{indir_weekly}/LAr*PhysWave*.root")
#print(infiles_physwave)

infile_PA = glob.glob(f"{topdir}/{indir_PA}/LArD*.root")[0]
#print(infile_PA)

cut = ' | '.join([ f"( channelId=={ch} )" for ch in chansToCheck ])
print("cut:",cut)

PAtree = "LARDIGITS"
Ramptree = "RAMPS"


ncells = { ch:len(pmdf[pmdf.SC_ONL_ID==ch].values) for ch in chansToCheck }
PA_amplitude_dict = { ch:[0,0] for ch in chansToCheck }
Ramp_amplitude_dict = {ch:{} for ch in chansToCheck }
Cali_amplitude_dict = {ch:None for ch in chansToCheck }
PA_DAC_dict = {ch:None for ch in chansToCheck }
Cali_DAC_dict = {ch:None for ch in chansToCheck }
Phys_amplitude_dict = {ch:None for ch in chansToCheck }
if checkPA:
    for arrays in U.iterate([infile_PA], expressions=["channelId","samples"],cut=cut, step_size="250 MB"):
        for ch in chansToCheck:
            thisChunk = arrays[arrays.channelId == ch]
            firsts = thisChunk.samples[:,0]
            maxes = ak.max(thisChunk.samples, axis=1)
            #print(firsts, maxes, thisChunk.samples[0], len(thisChunk.samples[0]))
            maxmin = maxes-firsts
            PA_amplitude_dict[ch][0] += sum(maxmin)
            PA_amplitude_dict[ch][1] += len(maxmin)
            #print("Adding sum of amplitudes for channel",ch,":",sum(maxmin), "over",len(maxmin),"entries - average =",sum(maxmin)/len(maxmin))
            # Checking 
            #for i in range(0, len(firsts)):
            #    if thisChunk.samples[i][0] != firsts[i]:
            #        print("-firsts-",thisChunk.samples[i][0], firsts[i] )
            #        print(chi.samples)
            #        #sys.exit()
            #    if max(thisChunk.samples[i]) != maxes[i]:
            #        print("-maxes-", max(thisChunk.samples[i]), maxes[i])
            #        print(chi.samples)
                    #sys.exit()

    PA_amplitude_dict = { ch:PA_amplitude_dict[ch][0]/PA_amplitude_dict[ch][1] for ch in PA_amplitude_dict.keys() }


DAC = 2000

if checkCali:
    for arrays,report in U.iterate([f"{f}:CALIWAVE" for f in infiles_caliwave], expressions=["channelId","MaxAmp","DAC"],cut=cut, step_size="250 MB", report=True):
        print("Reading chunk from:", report.file_path)
        for ch in chansToCheck:
            thisChunk = arrays[arrays.channelId == ch]
            if len(thisChunk) == 0: continue
            Cali_amplitude_dict[ch] = thisChunk.MaxAmp[0] 
            Cali_DAC_dict[ch] = thisChunk.DAC[0]
            PA_DAC_dict[ch] = DAC*ncells[ch]
            
if checkRamp:
    for arrays,report in U.iterate([f"{f}:RAMPS" for f in infiles_ramp], expressions=["channelId","X","ADC","DAC"],cut=cut, step_size="250 MB", report=True):
        print("Reading chunk from:", report.file_path)
        for ch in chansToCheck:
            thisChunk = arrays[arrays.channelId == ch]
            if len(thisChunk) == 0: continue
            if len(thisChunk.ADC[0]) == 0: continue
            thisADC = ( PA_DAC_dict[ch] - thisChunk.X[0][0]) / thisChunk.X[0][1] 
            if ch in PA_amplitude_dict.keys():
                thisAmp = PA_amplitude_dict[ch]
            else:
                thisAmp = None
            Ramp_amplitude_dict[ch][PA_DAC_dict[ch]] = round(thisADC,2)

            thisADC_caliDAC = ( Cali_DAC_dict[ch] - thisChunk.X[0][0]) / thisChunk.X[0][1] 
            Ramp_amplitude_dict[ch][Cali_DAC_dict[ch]] = round(thisADC_caliDAC,2)


if checkPhys:
    for arrays,report in U.iterate([f"{f}:PHYSWAVE" for f in infiles_physwave], expressions=["channelId","Amplitude"],cut=cut, step_size="250 MB", report=True):
        print("Reading chunk from:", report.file_path)
        for ch in chansToCheck:
            thisChunk = arrays[arrays.channelId == ch]
            if len(thisChunk) == 0: continue
            Phys_amplitude_dict[ch] = ak.max(thisChunk.Amplitude[0])

nch = 0
for ch in chansToCheck:
    badSCstatus = list(set(pmdf[pmdf.SC_ONL_ID==ch].BadSC.values))[0]
    badCellstatus = pmdf[pmdf.SC_ONL_ID==ch].BadChannels_UPD1.values

    if len([s for s in badCellstatus if s == 'deadCalib']) == ncells[ch]:
        print("*"*10,ch,"has deadCalib for all constituent cells",badSCstatus)
        continue
    if 'OffAmplitude' in badSCstatus:
        print("*"*10,ch,"is already flagged",badSCstatus)
        continue
    nch += 1
    print("*"*10, ch, "*"*10)
    print(f"PulseAll DAC {PA_DAC_dict[ch]}, peak-ped = {PA_amplitude_dict[ch]:.2f}")
    PA_ramp = Ramp_amplitude_dict[ch][PA_DAC_dict[ch]]
    PA_diff = ((PA_amplitude_dict[ch] - PA_ramp) / PA_amplitude_dict[ch])*100
    print(f"Ramp ADC at PA DAC = {PA_ramp} ({round(PA_diff,2)}% diff.)")
    Cali_ramp = Ramp_amplitude_dict[ch][Cali_DAC_dict[ch]]
    Cali_diff = ((Cali_amplitude_dict[ch] - Cali_ramp) / Cali_amplitude_dict[ch])*100
    print(f"Caliwave DAC {Cali_DAC_dict[ch]}, MaxAmp = {Cali_amplitude_dict[ch]:.2f}")
    print(f"Ramp ADC at Cali DAC = {Cali_ramp} ({round(Cali_diff,2)}% diff.)")
    print(f"PhysWaveMaxAmp = {Phys_amplitude_dict[ch]:.2f}")
    print("---- constituent cells ----")
    print(pmdf[pmdf.SC_ONL_ID==ch])
print("*"*20)
print(f"{nch} channels without OffAmplitude SC flag")
