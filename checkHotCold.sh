#!/bin/sh
runNo=$1
eta=$2
phi=$3
if [[ $# -gt 3 ]]; then
    delta=$4
else
    delta=0.15
fi
if [[ $# -gt 4 ]]; then
    stream=$5
else
    stream=CosmicCalo
fi

if (( $(echo "$eta > 0" | bc -l) )); then
    side="A"
else
    side="C"
fi

if (( $(echo "$eta > 1.52" | bc -l) )) || (( $(echo "$eta < -1.52" | bc -l) )); then
    BAR=0
else
    BAR=1
fi
if (( $(echo "$eta > 3" | bc -l) )) || (( $(echo "$eta < 3" | bc -l) )); then
    FCAL=1
else
    FCAL=0
fi

echo $eta $phi $BAR


cmd="python checkCorrelInHIST.py -r ${runNo} -s ${stream} -x ${eta} -y ${phi} -d ${delta} --onlyBoxes --histoWD "
if [[ ${BAR} -eq 1 ]]; then
    cmd="${cmd} CaloTopoClusters/CalBAR/Thresh1BAROcc CaloTopoClusters/CalEMBAR/EMThresh1BAROcc"
fi
cmd="${cmd} CaloTopoClusters/CalEC${side}/Thresh1EC${side}Occ CaloTopoClusters/CalEMEC${side}/EMThresh1EC${side}Occ"
if [[ ${BAR} -eq 1 ]]; then
    cmd="${cmd} LAr/EMB${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_EMB*${side}_hiEth_CSCveto" 
    cmd="${cmd} LAr/EMB${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_EMB*${side}_5Sigma_CSCveto" 
    cmd="${cmd} LAr/EMB${side}/Detector_Status/DatabaseNoiseVsEtaPhi_EMB*${side}"
    cmd="${cmd} LAr/EMB${side}/Detector_Status/LArHVCorrectionEMB${side}"
    cmd="${cmd} LAr/EMB${side}/Detector_Status/LArAffectedRegionsEMB${side}"
    cmd="${cmd} LAr/EMB${side}/Detector_Status/LArAffectedRegionsEMB${side}PS"
fi
cmd="${cmd} LAr/EMEC${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_EMEC*${side}_hiEth_CSCveto" 
cmd="${cmd} LAr/EMEC${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_EMEC*${side}_5Sigma_CSCveto" 
cmd="${cmd} LAr/EMEC${side}/Detector_Status/DatabaseNoiseVsEtaPhi_EMEC*${side}"
cmd="${cmd} LAr/EMEC${side}/Detector_Status/LArHVCorrectionEMEC${side}"
cmd="${cmd} LAr/EMEC${side}/Detector_Status/LArAffectedRegionsEMEC${side}"
cmd="${cmd} LAr/EMEC${side}/Detector_Status/LArAffectedRegionsEMEC${side}PS"
cmd="${cmd} LAr/HEC${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_HEC*${side}_hiEth_CSCveto" 
cmd="${cmd} LAr/HEC${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_HEC*${side}_5Sigma_CSCveto" 
cmd="${cmd} LAr/HEC${side}/Detector_Status/DatabaseNoiseVsEtaPhi_HEC*${side}"
cmd="${cmd} LAr/HEC${side}/Detector_Status/LArHVCorrectionHEC${side}"
cmd="${cmd} LAr/HEC${side}/Detector_Status/LArAffectedRegionsHEC${side}0"
cmd="${cmd} LAr/HEC${side}/Detector_Status/LArAffectedRegionsHEC${side}1"
cmd="${cmd} LAr/HEC${side}/Detector_Status/LArAffectedRegionsHEC${side}2"
cmd="${cmd} LAr/HEC${side}/Detector_Status/LArAffectedRegionsHEC${side}3"
if [[ ${FCAL} -eq 1 ]]; then
    cmd="${cmd} LAr/FCAL${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_FCAL*${side}_hiEth_CSCveto" 
    cmd="${cmd} LAr/FCAL${side}/Occupancy-Noise/Single_Cells/*/CellOccupancyVsEtaPhi_FCAL*${side}_5Sigma_CSCveto" 
    cmd="${cmd} LAr/FCAL${side}/Detector_Status/DatabaseNoiseVsEtaPhi_FCAL*${side}"
    cmd="${cmd} LAr/FCAL${side}/Detector_Status/LArHVCorrectionFCAL${side}"
    #cmd="${cmd} LAr/FCAL${side}/Detector_Status/LArAffectedRegionsFCAL${side}"
fi
cmd="${cmd} TileCal/Cell/AnyPhysTrig/TileCellEneEtaPhi_Samp*_AnyPhysTrig"

echo $cmd
