#!/bin/python3
import sqlite3
import sys,os
import array
import numpy as np

printMapping = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/printMapping.py"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def check_payload(payload_name, cursor, folderName=None):
    try:
        cursor.execute('SELECT * FROM '+payload_name)
    except sqlite3.OperationalError:
        # print(payload_name,"is not a table in the input db")
        return None
    colnames = list(map(lambda x: x[0], cursor.description))
    #print("Columns:",", ".join(colnames))

    npayl=0

    payldict = {}

    for payl in cursor.fetchall():
        for col in colnames:
            data = payl[colnames.index(col)]
            if isinstance(data,bytes):
                vartype='f'
                if "onlineid" in col.lower() or "offlineid" in col.lower():
                    vartype='i'
                #sys.exit()
                bytearr = array.array(vartype,data)
                bytearr = np.array(bytearr, dtype=vartype)
                factor_cells = len(bytearr)/ 195072.
                factor_SC = len(bytearr)/34048.
                factor_LATOME = len(bytearr)/116.
                if sum([factor_cells.is_integer(), factor_SC.is_integer(), factor_LATOME.is_integer()]) > 1:
                    print("NONONONONO something could go wrong here - is this a cell, SC or LATOME variable? Maybe we should assert that factor == 1")
                factor = None
                if factor_cells.is_integer():
                    factor = factor_cells
                if factor_SC.is_integer(): 
                    factor = factor_SC
                elif factor_LATOME.is_integer(): 
                    factor = factor_LATOME
                else:
                    print("ERROR: not sure what this blob represents, since  it is not divisible by #SC or #cell or #LATOME")
                    sys.exit()
                if factor!=1: 
                    #if not factor.is_integer(): # and not factor_SC.is_integer() and not factor_LATOME.is_integer():
                    #    print("ERROR: weird number of entries for",col,"("+str(len(bytearr))+")","... exiting")
                    #    sys.exit()
                    #else:
                
                    #print(col,"is an array of vectors with size",factor)
                    factor=int(factor)
                    bytearr = np.reshape(bytearr,(len(bytearr)//factor,factor))
                if npayl==0: 
                    #print("**",col, "has", len(bytearr),"BLOB entries. Here are the first 10:\n", bytearr[0:10])
                    if "OFC" in col:
                        if folderName is not None:
                            if "OFCCali" in folderName:
                                payldict[col+"_Cali"] = bytearr
                            else:
                                payldict[col+"_Phys"] = bytearr
                    else:
                        payldict[col] = bytearr
                    #print("ZEROS:", np.count_nonzero(bytearr == 0))
                    #print("ONES:", np.count_nonzero(bytearr == 1))
                    #print("THOUSANDS:", np.count_nonzero(bytearr == 1000))
                    #print("p.s. size", factor, len(data) / (len(bytearr)/factor))
            #else:
            #    if npayl==0:
            #        print(col,":",data)
        npayl+=1
    #if npayl != 1 and npayl != 0:
    #    print(npayl, "entries in the payload")
    return payldict

def getBlobContents(offlid,verbose=True):
    if len(sys.argv)<1:
        print("Please provide the path to a sqlite file as an input argument")
        sys.exit()
    infile = sys.argv[1]
    if not os.path.isfile(infile):
        print(infile,"is not a valid path")
        sys.exit()
    dbname = "CONDBR2"
    # Create a SQL connection to our SQLite database
    con = create_connection(infile)
    
    cursor = con.cursor()

    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    # The result of a "cursor.execute" can be iterated over by row

    payl = {}

    for table in cursor.fetchall():
        table_name = table[0]
        # print("="*20, table_name)
        cursor.execute('SELECT * FROM '+table_name)
        names = list(map(lambda x: x[0], cursor.description))
        #print("Names:",", ".join(names))
        for row in cursor.fetchall():
            #if row[names.index("NODE_FULLPATH")] == '/LAR/ElecCalibFlat/Pedestal':
            #    print(row)
            for n in names:
                pl_name = row[names.index(n)]
                if not isinstance(pl_name,str): continue
                if dbname not in pl_name: continue
                #print("*"*20)
                #print("*"*20)
                #print(pl_name)
                folderName = None
                for r in list(row):
                    if isinstance(r,str) and "/LAR/" in r:
                        folderName = r
                thispayl = check_payload(pl_name, cursor, folderName)
                if thispayl is not None and len(thispayl.keys())!=0:
                    payl.update(thispayl)
                    
    print("PAYL:", [ str(k)+":"+str(len(payl[k])) for k in payl.keys() ])
    

    def printVals(index, lo, hi=None):
        if index not in payl.keys():
            return 
        if hi is None:
            print(index,":",payl[index][lo], "max:",max(payl[index]), "min:",min(payl[index]))
        else:
            print(index,":",payl[index][lo:hi], "max:",payl[index].max(), "min:",payl[index].min())

    lo=0
    hi=10
    if verbose:
        if "OnlineIDs" in payl.keys():
            onlid = [ l for l in payl["OnlineIDs"].tolist() if l != 0 ]
            payl["OnlineIDs"] = [[y for y in x if y != 0] for x in onlid]
        if "OnlineHashToOfflineId" in payl.keys():
            payl["OnlineHashToOfflineId"] =  payl["OnlineHashToOfflineId"].tolist()

        if offlid is not None and "OnlineHashToOfflineId" not in payl.keys():
            print("CANNOT PRINT INFO FOR DESIRED OFFLINE ID, AS THE HW ADDRESS INFO IS NOT AVAILABLE IN THIS DB")
            sys.exit()
        
        ind = None
        if offlid is not None:
            if int(offlid) not in payl["OnlineHashToOfflineId"]:
                print("CANNOT FIND REQUESTED OFFLINE ID IN LIST OF AVAILABLE CHANNELS"+" ("+offlid+")")
                print(payl["OnlineHashToOfflineId"])
                sys.exit()
            else:
                ind = payl["OnlineHashToOfflineId"].index(int(offlid))
            
        for k in payl.keys():
            if ind is not None:
                printVals(k, ind)
            else:
                printVals(k, lo, hi)

    con.close()

    return payl 

if __name__=="__main__":
    if sys.version_info.major == 2:
        print("Please run me with python3, thank you & goodbye")
        sys.exit()
    offlid=None
    if len(sys.argv)>2:
        offlid=sys.argv[2]
        print("Printing for offl ID",offlid)

    getBlobContents(offlid)
