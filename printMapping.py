#!/bin/python3
# Read values from LArID translator
# Author: Ellis Kay <ellis.kay@cern.ch>
# Date: 14/12/2020
# Using script from Clement Camincher <ccaminch@cern.ch>
################################################################################

import sqlite3
import sys,os
import numpy as np
import itertools
import argparse
import importlib
import ast, json
import xmlrpc.client 

def get_latest_run():
    dqmsite = "atlasdqm.cern.ch"
    dqmpassfile = "/afs/cern.ch/user/l/larmon/public/atlasdqmpass.txt"
    if not os.path.isfile(dqmpassfile):
        return 555555
    dqmpass = None
    dqmapi = None
    with open(dqmpassfile, "r") as f:
        dqmpass = f.readlines()[0].strip()
        if ":" not in dqmpass:
            print("Problem reading dqmpass")
            sys.exit()
        try:
            dqmapi = None #xmlrpc.client.ServerProxy("https://"+dqmpass+"@"+dqmsite)
        except:
            print("Failed to connect to atlasdqm, therefore giving a fake run number 555555")
            return 555555
    if dqmapi is not None:
        return dqmapi.get_latest_run()
    else:
        return 555555
# Paths to the sqlite files
db = None
baddb = None
baddb_sc = None
if os.path.isdir("/det/lar/project/"):
    db = '/det/lar/project/databases/LArId.db'
    baddb = '/det/lar/project/databases/LArBad.db'
elif os.path.isdir("/afs/cern.ch/user/l/larmon"):
    db = '/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArId.db'
    baddb = '/afs/cern.ch/user/l/larmon/public/prod/LArIdtranslator/LArBad.db'
    baddb_sc = '/eos/home-l/larmon/public/SCBadChan/LArBadSC.db'
    

def getRoDName(rodname):
    rodprefix  = rodname[:rodname.rindex('_')]
    subdet  = rodprefix[rodprefix.index('_')+1:]
    subrod  = subdet[-2:]

    if ("HEC" in rodname):
      rodcname = "RODC_EMEC"+subrod
      # HEC is always controlled by EMEC[A,C]3 RODC.
      # The subrod number should always be 3.
      rodcname = rodcname[:-1] + '3'
    elif "FCAL" in rodname:
      rodcname = "RODC_HECFCAL"+subrod
    else:
      rodcname = "RODC_" + subdet
    return rodcname


def cellInfo(cell):
    """Specific function for the actonSSwinRun. Only one cell should be provided"""
    optionquery = {}
    optionquery["have"]='ONL_ID'
    optionquery["have_val"]=cell
    optionquery["want"]="ROD,FEB,TT_COOL_ID,FT,SL,CH"
    results=query(**optionquery)

    #Need to check if resut is in the good format
    for line in results:
      line[0]=getRoDName(line[0])
    return results[0]


from importlib.util import find_spec
#if importlib.util.find_spec('larIdTranslatorSqlite') is None:
if find_spec('larIdTranslatorSqlite') is None:
    if os.path.isdir("/det/lar/project"):
        sys.path.append("/det/lar/project/scripts/misc/")
    elif os.path.isdir("/afs/cern.ch/user/l/lardaq"):
        sys.path.append("/afs/cern.ch/user/l/lardaq/public/detlar/scripts/misc/")
    else:
        print("ERROR: cannot locate directory containing larIdTranslatorSqlite tools")
        sys.exit()
from larIdTranslatorSqlite import *


def indexBadChan(ONL_ID, badChanDict, UPD=None):
    if UPD is None:
        theDict1 = badChanDict["UPD1"]["BadChannels"]
        theDict2 = badChanDict["UPD4"]["BadChannels"]
        theStr = []
        if int(ONL_ID) in theDict1.keys():
            theStr.append( theDict1[int(ONL_ID)] )
        if int(ONL_ID) in theDict2.keys():
            theStr.append(theDict2[int(ONL_ID)])
        if len(theStr) == 0:
            return "-"
        else:
            theStr = ",".join(theStr)
            # remove duplicates
            theStr = list(set(theStr.split(",")))
            return ",".join(theStr)
    else:
        theDict = badChanDict["UPD"+str(UPD)]["BadChannels"]
        if int(ONL_ID) in theDict.keys():
            return theDict[int(ONL_ID)]
        else:
            return "-"
    

# TO do, add inversion
def LATOMEsourceID(LATOME_NAME):
    #https://atlasop.cern.ch/twiki/pub/LAr/LArHardwareMemos/LATOMESourceIDs.pdf
    #LATOME_EMBC_EMECC_5
    if "EMBA_EMECA" in LATOME_NAME:
        sid="0x49"
    elif "EMBC_EMECC" in LATOME_NAME:
        sid="0x4a"
    elif "EMECA_HECA" in LATOME_NAME:
        sid="0x4b"
    elif "EMECC_HECC" in LATOME_NAME:
        sid="0x4c"
    elif "FCALA" in LATOME_NAME:
        sid="0x47"
    elif "FCALC" in LATOME_NAME:
        sid="0x48"
    elif "HECA" in LATOME_NAME:
        sid="0x45"
    elif "HECC" in LATOME_NAME:
        sid="0x46"
    elif "EMECA" in LATOME_NAME:
        sid="0x43"
    elif "EMECC" in LATOME_NAME:
        sid="0x44"
    elif "EMBA" in LATOME_NAME:
        sid="0x41"
    elif "EMBC" in LATOME_NAME:
        sid="0x42"
    else:
        print("WARNING: cannot get LATOME source ID from", LATOME_NAME)
        return "1111"
    sid+="100"
    phival = int(LATOME_NAME.split("_")[-1])-1
    sid+= hex(phival).split("0x")[1]
    
    return sid



def convertNames(have, have_val, fancy=False):
    have = have
    have_val = have_val
    def getInd(lst,k):
        if any(l.lower() == k.lower() for l in lst):
            return [index for index, element in enumerate(lst) if element.lower() == k.lower() ]
        else:
            return None
    DET  = {"EMB":"0", "EMEC":"1", "HEC":"2", "FCAL":"3"}
    AC = {"A":"1","C":"-1"}
    PARTS = [ l[0]+l[1] for l in list(itertools.product(DET.keys(),AC.keys()))]
    for d in DET.keys():
        indexes = getInd(have_val,d)
        if indexes is not None:
            for ind in indexes:
                if ind is not None:
                    have_val[ind] = DET[d]
    #print(have, have_val)
    for ac in AC.keys():
        indexes = getInd(have_val,ac)
        if indexes is not None:
            for ind in indexes:            
                if ind is not None and have[ind]=="AC":
                    have_val[ind] = AC[ac]

    for p in PARTS:
        indexes = getInd(have_val,p)
        if indexes is not None:
            for ind in indexes:
                if ind is not None:
                    ac = p[-1]
                    d = p[:-1]
                    if "DET" in have:
                        have_val[ind] = DET[d]
                        have.append("AC")
                        have_val.append(AC[ac])
                    elif "AC" in have:
                        have_val[ind] = AC[ac]
                        have.append("DET")
                        have_val.append(DET[d])
    if fancy:
        if have != have: print("Input names were: "+(", ").join(have)+" now: "+(",").join(have))
        if have_val != have_val: print("Input values were: "+(", ").join(have_val)+" now: "+(",").join(have_val))
    #print(have,have_val)
    return have, have_val

def getCols(db, folders=None):
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
    names = []
    if folders is None:
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        #folders = [ f[0].encode('ascii','ignore') for f in cursor.fetchall()]
        folders = [ f[0] for f in cursor.fetchall()]
    for f in folders:
        cursor.execute('select * from '+f )
    names.extend( [ l for l in list(map(lambda x: x[0], cursor.description)) if l not in names ] )
    return names

# Format the names of the queried fields
def displayResults(listvalues,args):
    if args.printList == True and len(args.want) == 1:
        print(", ".join(list(set([l[0] for l in listvalues]))))
        return
    divider="\33[92m| \33[0m"    
    #Build header
    if args.fancy==True:
        if args.counter:
            linename=divider+"LINE "+divider
            linedash=divider+"---- "+divider
        else:
            linename=divider
            linedash=divider
        
        for name in args.want:
            linename=linename+name
            maxlen = max([ len(lv[args.want.index(name)]) for lv in listvalues])
            maxlen = max( maxlen, len(name) ) # in case title is longer
            for l in range(maxlen):
                linedash=linedash+"-"
            for l in range(maxlen - len(name)):
                linename=linename+" "
            linedash=linedash+" "+divider
            linename=linename+" "+divider
        print(linename)
        print(linedash)
    #Body of the printing
    numberlines=0
    for line in listvalues:
        if args.fancy:
            printline=divider
        else:
            printline=""
        if args.counter:
            printline+=str(numberlines).ljust(4)
            if args.fancy: printline+=" "+divider
        for value in line :
            if args.fancy:
                maxlen = max([ len(lv[line.index(value)]) for lv in listvalues])
                maxlen = max( maxlen, len(args.want[line.index(value)]) ) # in case title is longer
                try:
                    printline=printline+value+" "*(maxlen-len(value))+" "+divider
                except Exception as e:
                    print(e)
                    print(type(printline), type(value), value)
            else :
                try:
                    printline=printline+value+" "
                except Exception as e:
                    print(e)
                    print(type(printline), type(value), value)
        print(printline)
        numberlines+=1
    if args.total == True:
        print("-"*10)
        print("Total number of results:", numberlines)
    #Print the trailer
    if  args.fancy==True :
        print(linedash)
        print(linename)

def convertBadChanEnum(isSC=False):
    infile = open("/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.42/InstallArea/x86_64-centos7-gcc11-opt/src/LArCalorimeter/LArRecConditions/LArRecConditions/LArBadChannel.h", "r")
    start = 0
    start2 = 0
    vals = {}
    for line in infile:
        if not isSC and "LArBadChannelEnum" in line: start=1
        if isSC and "LArBadChannelSCEnum" in line: start = 1
        if start != 1: continue
        if "ProblemType" in line: start2 = 1
        if start2 != 1: continue
        if "}" in line: break
        if "=" in line: 
            line = line.strip("\n").replace(",","").strip()
            
            bctype = line.split("=")[0].strip()
            bcnum = int(line.split("=")[1].strip())
            vals[bctype] = bcnum

    return vals

def statusBad(badNum, bcEnum):
    status = []
    for badword in bcEnum.keys():
        num = bcEnum[badword]
        mask = 1 << num
        if badNum&mask != 0x0:
            status.append(badword.replace("Bit",""))
    return status

def getBad(run, baddbpath=baddb, isSC=False):
    if baddbpath is None:
        return None
    badChanEnum = convertBadChanEnum(isSC)
    conn = sqlite3.connect(baddbpath)
    cursor = conn.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = [ t[0] for t in cursor.fetchall() ]
    cols = getCols(baddbpath)
    B = {}
    if isSC:
        tables = ["BadChannelsSC"]
        UPDs = ['1']
    else:
        tables = ['BadChannels','MissingFEBs']
        UPDs = ['1','4']
    for u,upd in enumerate(UPDs):
        for b,bad in enumerate(tables):
            cursor.execute('select status from %s where %d>=since and %d<=until'%(bad+'UPD'+upd,run,run))
            if "SC" in bad: bad = bad.replace("SC","")
            for d in cursor.fetchall(): 
                data = d[0].decode('utf-8')
                data = data.replace("L","")
                data = ast.literal_eval(str(data))
                #print(u, bad, len(data))
                if "UPD"+upd not in B.keys():
                    B["UPD"+upd] = {}
                if bad not in B["UPD"+upd].keys():
                    B["UPD"+upd][bad] = {}
                B["UPD"+upd][bad].update(data)
            if bad == 'BadChannels':
                B['UPD'+upd][bad] = { key : ", ".join(statusBad(value, badChanEnum)) for key,value in B['UPD'+upd][bad].items()}
            elif bad == 'MissingFEBs':
                #print("OIOI", B['UPD'+upd][bad])
                B['UPD'+upd][bad] = { key : ", ".join(statusBad(value, badChanEnum)) for key,value in B['UPD'+upd][bad].items()}
                #print("OIOI", B['UPD'+upd][bad])
    # sys.exit()
    conn.close()
    #print(B)
    return B #, cols
        
def query( want=None, have=None, have_val=None, dbpath=db, fancy=False, printSQL=False, showdecimal=False, **kwargs):
    if not os.path.isfile(dbpath):
        print("ERROR: the provided dbpath =",dbpath,"does not exist! Cannot go on")
        sys.exit()

    if want is None:
        print("ERROR: you must provide a 'want' variable, i.e. the value that you want to obtain from the query!")
        sys.exit()
    if not isinstance(want, list): want = [want]
    want = [str(w) for w in want]
    if have is not None:
        if not isinstance(have, list): have = [have]
        have = [str(h) for h in have]
    if have_val is not None:
        if not isinstance(have_val, list): have = [have_val]
        have_val = [str(h) for h in have_val]
        
    wildcards = None
    if "wildcard" in kwargs.keys():
        wildcards = kwargs["wildcard"]

    HEXvals = [ "OFF_ID","ONL_ID","TT_COOL_ID","SC_OFFL_ID","SC_ONL_ID","FEB_ID","SPAC","TtcRx_Address","TBB_CH","CALIB_ID","FEB_SPAC","LATOME_SOURCE_ID"]

    allowed = getCols(dbpath)

    #badlist = getBad(args)
    #print(badlist)
    #sys.exit()

    allowed.append("LATOME_SOURCE_ID")
    allowed.append("DETNAME")
    allowed.append("BadChannels_UPD1")
    allowed.append("BadChannels_UPD4")
    allowed.append("BadChannels")
    allowed.append("BadSC")

    def notAllowed(req):       
        if any( r not in allowed for r in req ):
            odd = [r for r in req if r not in allowed]
            print("ERROR: Requested item(s) not available in database:",(", ").join(odd))
            print("Available items are: "+(", ").join(allowed))
            sys.exit()

    if have is not None: notAllowed(have)
    if want is not None: notAllowed(want)
    
    badChan = None
    badSC = None
    if any(bc in want for bc in ["BadChannels", "BadChannels_UPD1", "BadChannels_UPD4"]):
           badChan = getBad(kwargs['run'])
    if any(bc in want for bc in ["BadSC"]) and baddb_sc is not None:
        badSC = getBad(kwargs['run'], baddbpath=baddb_sc, isSC=True)
        
    if have is not None:
        if len(have)!=len(have_val):
            print( "ERROR: Lists of provided variables (names and values) have different lengths!")
            sys.exit()

    LArID = LArIdTranslatorSqlite()
    LArID.dbfile = dbpath
    LArID.quiet = not fancy

    want_amended = [ w for w in want ]
    
    if "LATOME_SOURCE_ID" in want:
        want_amended[want.index("LATOME_SOURCE_ID")] = "LATOME_NAME"
    for bcStr in ["BadChannels", "BadChannels_UPD1", "BadChannels_UPD4"]:
        if bcStr in want:
            want_amended[want.index(bcStr)] = "ONL_ID"
    if "BadSC" in want and badSC is not None:
        want_amended[want.index("BadSC")] = "SC_ONL_ID"
    if "DETNAME" in want:
        want_amended[want.index("DETNAME")] = "CALIB"

    if  want is None: #in print all mode
        LArID.setfields(",".join(have))
    else : # in print all given the conditions provided
        LArID.setfields(",".join(want_amended))

    conditions="where ETA > -9000.0"

    # convert EMB, EMEC, HEC, FCAL
    # check have and have_val are same length
    # if have is 0, LArID.setcommands("select distinct","")
    if have is not None and have_val is not None:
        have = have
        have_val = have_val

        # if inner/outer wheel were requested, process this
        if "DET" in have:
            if "OW" in have_val[have.index("DET")]:
                conditions = "where ABS(ETA) between 1.375 and 2.5"
                have_val = have_val.replace("OW","")
            elif "IW" in have_val[have.index("DET")]:
                conditions = "where ABS(ETA) between 2.5 and 3.5"
                have_val = have_val.replace("IW","")


        newhave = []
        newhave_val = []
        haveind=0
        for hv in have_val:
            #haveind = have_val.index(hv)
            havename = have[haveind]
            if "," in hv:
                newhave_val.extend(hv.split(","))
                newhave.extend([havename]*len(hv.split(",")))
                #del have_val[haveind]
                #del have[haveind]
                #have_val.extend(ors[havename])
                #have.extend([havename]*len(ors[havename]))
            else:
                newhave.append(havename)
                newhave_val.append(hv)
            haveind+=1
        have = newhave
        have_val = newhave_val
        have, have_val = convertNames(have, have_val, fancy)


        # find duplicates and add to 'ors' list
        ors = {}
        def indices(lst, item):
            return [i for i, x in enumerate(lst) if x == item]
        for h in list(set(have)):
            ind= indices(have,h)
            if len(ind)>1:
                ors[h] = [ have_val[i] for i in ind ]

        conditions += " and ("

        def convStr(table, nm,vl):
            startstr=table+"."+nm+" like "
            ops = ["<",">","="]
            if any(op in vl for op in ops):
                startstr = table+"."+nm+" "+"".join(s for s in vl if s in ops)
                if "&" not in vl:
                    return startstr+" "+"".join(s for s in vl if s not in ops)
                else:
                    expr = vl.split("&")
                    newexp = []
                    for exp in expr:
                        startstr = table+"."+nm+" "+"".join(s for s in exp if s in ops)
                        newexp.append( startstr+" "+"".join(s for s in exp if s not in ops))
                    return "( "+" AND ".join(newexp)+" )"

            if vl.startswith("0x"):
                return startstr+" '%"+str(int(vl,16)) +"%'"
            #elif any(valtype == nm for valtype in ["AC","FT","SL","CH","LATOME_NAME"]):
            elif wildcards is not None and any(valtype == nm for valtype in wildcards):
                return startstr+" '%"+vl+"%'"
            else:
                return startstr+" '"+vl+"'"

        ordone = []
        for hv in range(0,len(have_val)):
            # HEX entries  - replace with if in hexlist?
            if have[hv] in ors.keys():
                if have[hv] not in ordone:
                    thisor = [ convStr(LArID.tablename, have[hv],hvv) for hvv in ors[have[hv]] ]
                    conditions+="("+ (" or ").join(thisor)+") and "
                    ordone.append(have[hv])
            else:
                conditions+=convStr(LArID.tablename, have[hv],have_val[hv])+" and "
                
        if conditions.endswith("and "):
            conditions=conditions[:-4]
        conditions +=")"

    if printSQL is True:
        print("*"*30)
        print(conditions)
        print("*"*30)
    LArID.setcommands("select distinct",conditions)
    rId = LArID.fetch(printr=False)
    #Build output lines
    output = []
    Sorted = sorted(rId)

    def print_in_format(instr,refname='str',thisind=0):
        if "LATOME_SOURCE_ID" in want and refname == "LATOME_NAME" and thisind==want.index("LATOME_SOURCE_ID"):
            instr = LATOMEsourceID(instr)
            if showdecimal:
                return str(int(instr,16))
            else:
                return instr
        if "BadChannels_UPD1" in want and refname == "ONL_ID" and thisind==want.index("BadChannels_UPD1"):
            instr = indexBadChan(instr, badChan, UPD=1) 
            return instr
        if "BadChannels_UPD4" in want and refname == "ONL_ID" and thisind==want.index("BadChannels_UPD4"):
            instr = indexBadChan(instr, badChan, UPD=4) 
            return instr
        if "BadChannels" in want and refname == "ONL_ID" and thisind==want.index("BadChannels"):
            instr = indexBadChan(instr, badChan, UPD=None) 
            return instr
        if "BadSC" in want and refname == "SC_ONL_ID" and thisind==want.index("BadSC"):
            instr = indexBadChan(instr, badSC, UPD=1)
            return instr
        if "DETNAME" in want and refname == "CALIB" and thisind==want.index("DETNAME"):
            instr = instr.split("_")[1][:-1]
            return instr
        if ("ETA" in want and thisind==want.index("ETA")) \
           or ("PHI" in want and thisind==want.index("PHI")) \
           or ("Z" in want and thisind==want.index("Z")) \
           or ("R" in want and thisind==want.index("R")):
            return str(format(instr,'.4f'))
        if refname in HEXvals:
            if showdecimal:
                # return str(hex(instr))+" ("+str(instr)+")"
                return str(instr)
            else:
                return str(hex(instr))
        #elif isint(reftype): # do we need this?
        #    return str(int(instr))
        #elif isfloat(reftype): # do we need this?
        #    return str(float(instr))
        else:
            return str(instr)

    for l in Sorted:
        output.append( [ print_in_format(*insref, i) for i, insref in enumerate(zip(l,want_amended)) ] )
    #print(Sorted)
    
    del LArID
    if rId==1:
        print("ERROR: Something went wrong... no results")
        return 1
    else:
        if fancy:
            print("DONE")
        return output

    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    #from argparse import ArgumentParser
    parser.add_argument('-have', '-t', nargs='+', type=str, dest='have', required=False, help='Supplied variable name(s), e.g. FEB FTNAME CH',default=None) 
    parser.add_argument('-have_val', '-i', nargs='+', type=str, dest='have_val', required=False, help='Supplied variable value(s), which must match the list of variable names, e.g. FEB_EMBC4_15R_B1 H15R 116. You may also provide a comma separated list if you want different possibilities of this variable, e.g. FEB_EMBC4_15R_B1 H14R,H15R 116',default=None)
    parser.add_argument('-want', '-w', nargs='+', type=str, dest='want', help='Variable(s) which you want to retrieve from the database, e.g. ONL_ID ETA PHI') # add example
    parser.add_argument('-r', '-run', dest='run', type=int, default=get_latest_run(), help='Run number - used for bad channels. Default is latest run.')
    parser.add_argument('-dbpath', '-db',dest='dbpath', type=str, required=not(db is not None),default=db, help='Path to LArId database to read from')
    parser.add_argument('-baddbpath', '-baddb',dest='baddbpath', type=str, required=False,default=baddb, help='Path to LArBad database to read from')

    parser.add_argument('-fancy', '-f',dest='fancy',action='store_true',help='Easily readable printouts')
    parser.add_argument('-counter', '-n',dest='counter',action='store_true',help='Add line number count to output')
    parser.add_argument('-total', '-tot',dest='total',action='store_true',help='Add line giving total number of results')

    parser.add_argument('-showdecimal', '-dec',dest='showdecimal',action='store_true',help='Show the decimal value instead of hex')
    parser.add_argument('-printSQL', '-sql',dest='printSQL',action='store_true',help='Print out the SQL command which is used to query the db')

    parser.add_argument('-wildcard', dest='wildcard', nargs='+', help="Query the provided variables as wildcards, i.e. don't require an exact match for the provided string")
    # -v
    parser.add_argument('-printList', dest='printList', action='store_true', help='Print the results in a single line, separated by comma')

    args   = parser.parse_args()
    if args.run is None:
        args.run = get_latest_run()

    listvalues = query(**vars(args))


    displayResults(listvalues,args)

