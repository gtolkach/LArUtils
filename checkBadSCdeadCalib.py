import printMapping
import pandas as pd

want = ["SC_ONL_ID","ONL_ID","BadChannels_UPD1", "BadSC", "CALIB", "CL"]
inputs={"want":want,"showdecimal":True,"run":473617}
print("querying printMapping for",inputs)
pmout = printMapping.query(**inputs)
pmdf = pd.DataFrame(pmout, columns = want)
pmdf = pmdf.astype({'SC_ONL_ID':'int', 'ONL_ID':'int'})

boardline = []

for SC in list(set(pmdf.SC_ONL_ID.values)):
    ncells = len(pmdf[pmdf.SC_ONL_ID==SC].values)
    badSCstatus = list(set(pmdf[pmdf.SC_ONL_ID==SC].BadSC.values))[0]
    badCellstatus = pmdf[pmdf.SC_ONL_ID==SC].BadChannels_UPD1.values

    if len([s for s in badCellstatus if s == 'deadCalib']) >= ncells/2:
        print("*"*10,SC,f"has deadCalib for {len([s for s in badCellstatus if s == 'deadCalib'])}/{ncells} constituent cells - status in bad SC DB is:",badSCstatus)
        #print(pmdf[pmdf.SC_ONL_ID==SC])
        this_pmdf = pmdf[pmdf.SC_ONL_ID==SC]
        dead = this_pmdf[this_pmdf.BadChannels_UPD1.str.contains("deadCalib" )]

        boards = dead[dead.SC_ONL_ID==SC].CALIB.values
        lines = dead[dead.SC_ONL_ID==SC].CL.values
        for board in boards:
            for line in lines:
                if " " in line:
                    subline = "_".join(line.split(" "))
                    boardline.append(f"{board}_{subline}")
                        
                else:
                    boardline.append(f"{board}_{line}")
                #print(f"{board}_{line}")
boardline = sorted(list(set(boardline)))
print(f"{len(boardline)} calibration boards or lines responsible for deadCalib in SCs:")
for bl in boardline:
    print(bl)
