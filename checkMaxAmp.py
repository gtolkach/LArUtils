#!/bin/python3

import pandas as pd
import uproot as U
import printMapping

infile = U.open("/eos/home-l/lardaq/P1Results/LATOMERun_BarrelEndcapWeekly_230417-172557/LArCaliWave_00449538.root")

want = ["SC_ONL_ID","DETNAME","FEB","QUADRANT", "CALIB", "LATOME_NAME", "LATOME_FIBRE", "LTDB"]
inputs={"want":want,"showdecimal":True}
print("querying printMapping for",want)
pmout = printMapping.query(**inputs)
pmdf = pd.DataFrame(pmout, columns = want)

intree = infile["CALIWAVE"]

df = intree.arrays(["channelId","detector","pos_neg","MaxAmp","TmaxAmp", "eta", "phi", "layer", "badChan"], library="pd")

df = df.astype({"channelId": int})
pmdf = pmdf.astype({"SC_ONL_ID": int})
df = df.set_index("channelId")
pmdf = pmdf.set_index("SC_ONL_ID")

df = df[df.badChan==0]
df = pd.merge(df, pmdf, left_index=True, right_index=True)
#minT = df[ df['TmaxAmp'] == df['TmaxAmp'].min() ]
#print(df.nsmallest(20, "TmaxAmp"))
#print(df.nlargest(20, "TmaxAmp"))
pd.set_option('display.max_rows', None) 

#print(df[((df.TmaxAmp < 60) | (df.TmaxAmp > 80)) & (~df.LATOME_NAME.str.contains("FCAL"))][["TmaxAmp","LATOME_NAME","badChan"]])
print(list(set(df[df.TmaxAmp < 65]["LATOME_NAME"].values.tolist())))

print(df[(df.TmaxAmp < 55)  & (~df.LATOME_NAME.str.contains("FCAL"))][["TmaxAmp","LATOME_NAME","badChan"]])

print(df[(df.TmaxAmp > 90)  & (~df.LATOME_NAME.str.contains("FCAL"))][["TmaxAmp","LATOME_NAME","badChan"]])

print("<55")
print((" || ").join(["channelId == "+str(s) for s in list(set(df[(df.TmaxAmp < 55) & (~df.LATOME_NAME.str.contains("FCAL"))].index))]))

print(">90")
print((" || ").join(["channelId == "+str(s) for s in list(set(df[(df.TmaxAmp > 90) & (~df.LATOME_NAME.str.contains("FCAL"))].index))]))
