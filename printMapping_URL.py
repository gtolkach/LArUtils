import requests
import pandas as pd
from requests.structures import CaseInsensitiveDict
import urllib3
urllib3.disable_warnings()

        
    
def getDetName(benc, pn, ft):
    if pn == 0 :
        side = "C"
    elif pn == 1:
        side = "A"
    if benc == 0: 
        part = "EMB"
    elif benc == 1:
        part = "EMEC"
        if ft == 3 or ft == 10 or ft == 16 or ft == 22:
            part = "HEC"
        elif ft == 6:
            part = "FCAL"
    return part+side

def getDetNameFromFEB(onId):
    if "x" in str(onId):
        onId = int(onId, 16)
    else:
        onId = int(onId)
    pn = int( ((onId)>>24)&0x1 )
    benc = int( ((onId)>> 25)&0x1 )
    ft = int( ((onId)>>19)&0x1F )
    sl = int( (((onId)>>15)&0xF)+1 )
    detname = getDetName(benc, pn, ft)
    return detname

class FEB():
    def __init__(self,onId):
        if "x" in str(onId):
            self.onId = int(onId, 16)
        else:
            self.onId = int(onId)
        self.pn = int( ((onId)>>24)&0x1 )
        self.benc = int( ((onId)>> 25)&0x1 )
        self.ft = int( ((onId)>>19)&0x1F )
        self.sl = int( (((onId)>>15)&0xF)+1 )
        self.part, self.side = self.getDetName(self.benc, self.pn, self.ft)
        

pmdfcols = ['ONL_ID','CH','SAM','HVLINES','HVCORR','FEB_ID','OFF_ID','CL','TT_COOL_ID']

url = f"https://atlas-larmon.cern.ch/LArIdTranslatorBackend/api/data?columns={','.join(pmdfcols)}"
print(url)

headers = CaseInsensitiveDict()
headers["Accept"] = "application/json"

resp = requests.get(url, headers=headers, verify=False)

print(type(resp.content))
#df = pd.read_json(resp.content)
j = resp.json()
try:
    

    df = pd.DataFrame.from_dict(j)
    
    print(df)
except:
    print("DF didn't work")
    print(j)
    sys.exit()

#febs = df["FEB_ID"].values
#print(febs)
#for feb in febs:
#    test = FEB(feb)
#    print(test.part)
print(list(df.columns.values))

df['DETNAME'] = df.FEB_ID.apply(getDetNameFromFEB)
print(df)
chans = list([int(d) for d in df.ONL_ID.values])
if 947419136 not in chans: print("not there")

print(len(chans))
