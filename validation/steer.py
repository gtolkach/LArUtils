import os, sys
import datetime as dt
import platform
import PulseAll, Pedestal, CaliWave, Ramp
import plotter
sys.path.append("..")
import printMapping
import pandas as pd

def defineThresholds():
    thresholds = {}
    thresholds["Pedestal"] = {}
    thresholds["Pedestal"]["ped"] = [750, 1100]
    thresholds["Pedestal"]["rms"] = [0.2, 1.0]
    thresholds["CaliWave"] = {}
    thresholds["CaliWave"]["MaxAmp"] = [100,3000]
    thresholds["CaliWave"]["TmaxAmp"] = [40,140]
    thresholds["OFCCali"] = None
    thresholds["Ramp"] = None
    for runtype in thresholds.keys():
        if thresholds[runtype] is None: continue
        print("Thresholds for",runtype,"are:")
        for k in thresholds[runtype].keys():            
            print(k,":",thresholds[runtype][k])
        
    return thresholds

def getSCinfo():
    PrintMappingDir = "/afs/cern.ch/user/e/ekay/LAr/LArUtils/"
    query = [ "SC_ONL_ID", "SCNAME", "FT", "SL", "SAM", "SCETA", "SCPHI", "FTNAME", "LTDB", "SCNAME", "QUADRANT", "LATOME_NAME", "DET", "AC" ]
    criteria = {"want":query, "showdecimal":True}
    pmdf = pd.DataFrame(printMapping.query(**criteria), columns=query)
    for intval in ["SC_ONL_ID", "FT", "SL", "DET", "AC" ]:
        pmdf = pmdf.astype({intval: int})
    for floatval in ["SCETA", "SCPHI"]:
        pmdf = pmdf.astype({floatval: float})
    pmdf['SAM'] = [[int(idx) for idx in x.split(",")] for x in pmdf['SAM']]
    
    
    #pmdf = pmdf.set_index(onlid)

    return pmdf

if __name__ == "__main__":
    started = dt.datetime.now()
    print("Started at "+str(started) )
    ver = platform.python_version()

    if len(sys.argv) < 2:
        print("Pass the input directory as an argument")
        sys.exit()
    indir = sys.argv[1]
    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots"

    plotter.chmkDir(outdir)

    thresholds = defineThresholds()


    infiles = [ f for f in os.listdir(indir) if f.endswith(".root") ]

    peds = sorted(list([ f for f in infiles if "LArPedAutoCorr" in f and "RootHistos" not in f ]))

    caliwaves = sorted(list([ f for f in infiles if "LArCaliWave" in f ]))

    ofccalis = sorted(list([ f for f in infiles if "LArOFCCali" in f ]))

    ramps = sorted(list([ f for f in infiles if "LArRamp" in f ]))

    print("Getting SC info from LArId.db")
    SCinfo = getSCinfo()
    print( "Process took",str(dt.datetime.now()-started) )

    for pedestal in peds:
        run = pedestal.split("_")[-1].split(".root")[0]
        #Pedestal.process(indir+"/"+pedestal, run, SCinfo, outdir, thresholds["Pedestal"])
    for caliwave in caliwaves:
        run = caliwave.split("_")[-1].split(".root")[0]
        CaliWave.process(indir+"/"+caliwave, run, SCinfo, outdir, thresholds["CaliWave"])
    for ramp in ramps:
        run = ramp.split("_")[-1].split(".root")[0]
        #Ramp.process(indir+"/"+ramp, run, SCinfo, outdir, thresholds["Ramp"])
