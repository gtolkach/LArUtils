import sys, os, errno
import uproot4 as U
import numpy as np
import numba as nb
import pandas as pd 
import glob
from argparse import ArgumentParser
from BadChan import statusBad, convertBadChanEnum
bcEnum = convertBadChanEnum(True)

from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings

warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

ntupMap = {} # Mapping ntuple names to the run type
ntupMap["LArCaliWave"] = "delay"
ntupMap["LArOFCCali"] = "delay"
ntupMap["LArRamp"] = "ramp"
ntupMap["LArOFCPhys*_4samples"] = "ramp"
ntupMap["LArOFCPhys*All4samples_Picked"] = "ramp"
ntupMap["LArParams"] = "ramp"
ntupMap["LArPhysAutoCorr"] = "ramp"
ntupMap["LArPhysWave_RTM"] = "ramp"
ntupMap["LArPedAutoCorr"] = "pedestal"

treeVars = {} # Trees to check, with the list of their branches which should be compared
treeVars["PEDESTALS"] = ["ped", "rms"]
treeVars["CALIWAVE"] = ["TmaxAmp", "MaxAmp"] # oioioi add integral??
treeVars["RAMPS"] = ["X"]
treeVars["OFC"] = ["OFCa", "OFCb"] # OFC_1ns, OFC_mu, OFC_1ns_mu
treeVars["MPMC"] = ["mphysovermcal"]

def parseLogs(campaign, logdir):
    inlog = logdir+"/"+campaign+".log"
    runs = {}
    if not os.path.isfile(inlog):
        print(f"Can't find takeCalib log for {campaign} in {logdir}")
        # oioi do something
        sys.exit()
    for line in open(inlog,"r"):
        if not line.startswith("Run"): continue
        run = int("".join(list(filter(str.isdigit, line.split(":")[0]))))
        calib = line.split(";")[4].strip(" ")
        pattern = line.split(";")[3].strip(" ")
        runs[run] = [calib,pattern]

    return runs

def findROOTFile(indir, runNo, startsWith=None, otherWC=None):
    if startsWith is not None:
        criteria = f"{startsWith}*{runNo}*.root"
    else:
        criteria = f"*{runNo}*.root"
    ls = glob.glob(f"{indir}/{criteria}")
    if otherWC is not None:
        ls = [ l for l in ls if otherWC in l ]
        criteria += f" with *{otherWC}*"
    if len(ls) == 0:
        print(f"Couldn't find any root files in {indir} for wildcard {criteria}")
        return None
    elif len(ls) > 1:
        print(f"Found too many ({len(ls)}) root files in {indir} for wildcard {criteria}")
        return None
    return ls[0]

def getTreesStats(filePath):
    infile = U.open(filePath)
    treenames = [ k.strip(";1") for k in infile.keys() ]
    trees = {}
    trees_entries = {}
    trees_branches = {}
    for tree in treenames:
        trees[tree] = infile[tree]
        trees_entries[tree] = trees[tree].num_entries
        trees_branches[tree] = trees[tree].keys()
    return trees, trees_entries, trees_branches

@nb.jit()
def compX(tree1Ch, tree2Ch, tree1Br, tree2Br, Xno):
    channelsNew = np.unique(tree1Ch)
    channelsRef = np.unique(tree2Ch)
    diffs = dict()
    warned = 0
    for ch in channelsNew:
        if ch not in channelsRef: continue
        events1 = np.where( tree1Ch == ch )[0]  # Need the 0 because it returns a tuple
        events2 = np.where( tree2Ch == ch )[0]  # Need the 0 because it returns a tuple
        if ( sorted(list(events1)) != sorted(list(events2)) ) and warned == 0:
            print(f"Cannot match events for the two trees {len(tree1Br)} {len(tree2Br)}")
            warned = 1
        events = np.intersect1d(events1, events2)
        diffs[ch] = np.zeros( len(events), dtype='d')
        for ievent in range(events.shape[0]): 
            event = events[ievent]
            if event > len(tree2Br): continue
            arr1 = tree1Br[event]
            arr2 = tree2Br[event]
            if len(arr1) == 0:
                val1 = 0
            else:
                val1 = arr1[Xno]
            if len(arr2) == 0:
                val2 = 0
            else:
                val2 = arr2[Xno]
            if val2 == 0 and val1 == 0:
                diff = 0
            elif val2 == 0:
                diff = 100
            else:
                diff = ((val2-val1)/val2)*100
            diffs[ch][ievent] = diff
    return diffs


@nb.jit()
def comp1D(tree1Ch, tree2Ch, tree1Br, tree2Br):
    channelsNew = np.unique(tree1Ch)
    channelsRef = np.unique(tree2Ch)
    diffs = dict()
    warned = 0
    for ch in channelsNew:
        if ch not in channelsRef: continue
        events1 = np.where( tree1Ch == ch )[0]  # Need the 0 because it returns a tuple
        events2 = np.where( tree2Ch == ch )[0]  # Need the 0 because it returns a tuple
        if ( sorted(list(events1)) != sorted(list(events2)) ) and warned == 0:
            print(f"Cannot match events for the two trees {len(tree1Br)} {len(tree2Br)}")
            warned = 1
        events = np.intersect1d(events1, events2)
        diffs[ch] = np.zeros( len(events), dtype='d')
        for ievent in range(events.shape[0]): 
            event = events[ievent]
            if event > len(tree2Br):
                diff = 100
            else:
                val1 = tree1Br[event]
                val2 = tree2Br[event]
                if val2 == 0 and val1 == 0:
                    diff = 0
                elif val2 == 0:
                    diff = 100
                else:
                    diff = ((val2-val1)/val2)*100
            diffs[ch][ievent] = diff

    return diffs

@nb.jit()
def comp2D(tree1Ch, tree2Ch, tree1Br, tree2Br):
    channelsNew = np.unique(tree1Ch)
    channelsRef = np.unique(tree2Ch)
    diffs = dict()
    warned = 0
    for ch in channelsNew:
        events1 = np.where( tree1Ch == ch )[0]  # Need the 0 because it returns a tuple
        events2 = np.where( tree2Ch == ch )[0]  # Need the 0 because it returns a tuple
        if ( sorted(list(events1)) != sorted(list(events2)) ) and warned == 0:
            print(f"Cannot match events for the two trees {len(tree1Br)} {len(tree2Br)}")
            warned = 1
        events = np.intersect1d(events1, events2)
        diffs[ch] = np.zeros( len(events), dtype='d')
        for ievent in range(events.shape[0]): 
            event = events[ievent]
            if event > len(tree2Br): continue
            for ind in range(0, len(tree1Br[event])):
                val1 = tree1Br[event][ind]
                val2 = tree2Br[event][ind]
                if val2 == 0 and val1 == 0:
                    diff = 0
                elif val2 == 0:
                    diff = 100
                else:
                    diff = ((val2-val1)/val2)*100
                diffs[ch][ievent] = diff
    return diffs

@nb.jit
def getBadChan(chidBranch, badBranch):
    channels = np.unique(chidBranch)
    badCh = dict()
    for ch in channels:
        events = np.where( chidBranch == ch )[0]  # Need the 0 because it returns a tuple
        badCh[ch] = badBranch[events[0]]
    return badCh


def compareTree(newTree, refTree):
    #print(newTree, refTree)
    if newTree.name not in treeVars.keys():
        return None
    newTreeArr = newTree.arrays(library="np")
    refTreeArr = refTree.arrays(library="np")
    channelsNew = list(dict.fromkeys(newTreeArr["channelId"]))
    channelsRef = list(dict.fromkeys(refTreeArr["channelId"]))
    if "badChan" in newTree.keys():
        badChan = getBadChan(newTreeArr["channelId"], newTreeArr["badChan"])
        badChan = { k : ", ".join(statusBad(v, bcEnum)) for k, v in badChan.items() }
    else:
        print(f"No badChan branch in {newTree.name}")
        sys.exit()

    diff_df = pd.DataFrame.from_dict(badChan, orient='index', columns=['badChan'])

    ## OIOIOIOIOI NEED TO ADD SKIPPING OF MASKEDOSUM    

    if len(channelsNew) !=  len(channelsRef):
        print("DIFFERENT NUMBER OF CHANNELS FOUND IN THE TWO FILES")
        print(f"New: {len(channelsNew)}")
        print(f"Ref: {len(channelsRef)}")
    percentDiffs = {}
    for branch in treeVars[newTree.name]:
        newch = newTreeArr["channelId"]
        refch = refTreeArr["channelId"]
        newbr = newTreeArr[branch]
        refbr = refTreeArr[branch]
        
        title = newTree.name+"_"+branch
        
        if branch == "X": # OIOI SPECIAL CASE FOR RAMP SLOPE AND INTERCEPT
            percentDiffs[newTree.name+"_X0"] = compX(newch, refch, newbr, refbr, 0)
            percentDiffs[newTree.name+"_X1"] = compX(newch, refch, newbr, refbr, 1)
        elif len(newbr.shape) == 1:
            percentDiffs[title] = comp1D(newch, refch, newbr, refbr)
        else:
            percentDiffs[title] = comp2D(newch, refch, newbr, refbr)
    
    for branch in percentDiffs.keys():
        diff_df[branch] = diff_df.index.map(percentDiffs[branch])
    print("****", diff_df)





    
def compareFiles(newFile, refFile):
    '''Compare the two provided root files. Check trees, number of entries, branches, then pass the matched trees to the tree comparison function.'''
    #print(newFile, refFile)
    trees_new, treeStats_new, treeBr_new = getTreesStats(newFile)
    trees_ref, treeStats_ref, treeBr_ref = getTreesStats(refFile)
    if treeStats_new.keys() != treeStats_ref.keys():
        print("See different trees in the new and reference ntuples.")
        print(f"New: {treeStats_new}")
        print(f"Ref: {treeStats_ref}")
        return
    for tree in treeStats_new.keys():
        if len(treeBr_new[tree]) != len(treeBr_ref[tree]):
            print(f"Different number of branches in {tree} for new and reference ntuples")
            print(f"New: {len(treeBr_new[tree])}")
            print(f"Ref: {len(treeBr_ref[tree])}")
            return
        compareTree(trees_new[tree], trees_ref[tree])



def main(args):
    newRuns = parseLogs(args.newSet, args.logDir)
    refRuns = parseLogs(args.refSet, args.logDir)

    for run in newRuns.keys():
        try:
            ref = list(refRuns.keys())[list(refRuns.values()).index(newRuns[run])]
        except Exception as e:
            print(f"Can't find matching run in reference set for new run {run} ({', '.join(newRuns[run])})")
            print(refRuns)
            continue
        print(f"** Comparing runs {run} and {ref} ({', '.join(newRuns[run])}) **")

        runType = newRuns[run][0]
        possNtup = [ k for k in ntupMap.keys() if ntupMap[k].lower() == runType.lower() ]
        for ntup in possNtup:
            if "*" in ntup:
                startsWith = ntup.split("*")[0]
                otherWC = ntup.split("*")[1]
            else:
                startsWith = ntup
                otherWC = None
            infileNew = findROOTFile(args.inputDirNew, run, startsWith, otherWC)
            infileRef = findROOTFile(args.inputDirRef, ref, startsWith, otherWC)
            print(f"{startsWith} {run}: {infileNew}")
            print(f"{startsWith} {ref}: {infileRef}")

            compareFiles(infileNew, infileRef)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(dest='newSet', type=str, help='Name of new campaign (directory name) which you would like to compare, e.g. LATOMERun_BarrelEndcapWeekly_230912-094242')
    parser.add_argument('-r', '--refSet', dest='refSet', default='LATOMERun_BarrelEndcapWeekly_230425-153106', type=str, help='Name of reference campaign (directory name) which you would like to compare the new set to (default: %(default)s')

    parser.add_argument('-in', '--inputDirNew',  dest='inputDirNew', default='/eos/home-l/lardaq/P1Results', help='The directory where the requested new calibration set can be found (default: %(default)s')

    parser.add_argument('-ir', '--inputDirRef',  dest='inputDirRef', default='/eos/home-l/lardaq/P1Results', help='The directory where the requested reference calibration set can be found (default: %(default)s')

    parser.add_argument('-o', '--outputDir', dest='outputDir', type=str, default='.', help='The output directory where plots and logs should be written')

    parser.add_argument('-l', '--logDir', dest='logDir', type=str, default='/afs/cern.ch/user/l/lardaq/public/hfec-calibration-logs', help='The directory where the run logs can be found (default: %(default)s')
    args = parser.parse_args()

    args.inputDirNew = args.inputDirNew+"/"+args.newSet
    if not os.path.isdir(args.inputDirNew):
        print(f"Cannot find requested input directory {args.inputDirNew}. Please check the 'newSet' and 'inputDirNew' arguments and try again.")
        sys.exit()

    args.inputDirRef = args.inputDirRef+"/"+args.refSet
    if not os.path.isdir(args.inputDirRef):
        print(f"Cannot find requested input directory {args.inputDirRef}. Please check the 'refSet' and 'inputDirRef' arguments and try again.")
        sys.exit()

    args.outputDir += "/"+args.newSet

    try:
        os.makedirs(args.outputDir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
    main(args)
