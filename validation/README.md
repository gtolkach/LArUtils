Preliminary python-based validation suite

Concept is : root files are read in uproot to create something like dataframes, plots can be made from individual data frames and by comparisons between the two

```source setupDD.sh```

```steer.py``` reads info from the LArIDtranslator and dumps this into a dictionary which can be read by all jobs. It also defined thresholds which can be used for plots of individual runs. 



Need to add a method of introducing a reference set and then doing comparison.