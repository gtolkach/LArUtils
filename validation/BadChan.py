
def statusBad(badNum, bcEnum):
    status = []
    for badword in bcEnum.keys():
        num = bcEnum[badword]
        mask = 1 << num
        if badNum&mask != 0x0:
            status.append(badword.replace("Bit",""))
    return status

def convertBadChanEnum(isSC=False):
    infile = open("/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.42/InstallArea/x86_64-centos7-gcc11-opt/src/LArCalorimeter/LArRecConditions/LArRecConditions/LArBadChannel.h", "r")
    start = 0
    start2 = 0
    vals = {}
    nline = 0
    for line in infile:
        if not isSC and "LArBadChannelEnum" in line: start=1
        if isSC and "LArBadChannelSCEnum" in line: start = 1
        if start != 1: continue
        if "ProblemType" in line: start2 = 1
        if start2 != 1: continue
        if "}" in line: break
        if "=" in line: 
            line = line.strip("\n").replace(",","").strip()
            
            bctype = line.split("=")[0].strip()
            bcnum = int(line.split("=")[1].strip())
            vals[bctype] = bcnum
        nline+=1
    return vals
