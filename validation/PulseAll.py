import sys
import uproot4 as U
import numpy as np 
import plotter
import datetime as dt 

sourceIDs = [ "0x411000", "0x411009", "0x41100a", "0x41100b", "0x41100c", "0x41100d", "0x41100e", "0x41100f", "0x411001", "0x411002", "0x411003", "0x411004", "0x411005", "0x411006", "0x411007", "0x411008", "0x491000", "0x491009", "0x49100a", "0x49100b", "0x49100c", "0x49100d", "0x49100e", "0x49100f", "0x491001", "0x491002", "0x491003", "0x491004", "0x491005", "0x491006", "0x491007", "0x491008", "0x421000", "0x421009", "0x42100a", "0x42100b", "0x42100c", "0x42100d", "0x42100e", "0x42100f", "0x421001", "0x421002", "0x421003", "0x421004", "0x421005", "0x421006", "0x421007", "0x421008", "0x4a1000", "0x4a1009", "0x4a100a", "0x4a100b", "0x4a100c", "0x4a100d", "0x4a100e", "0x4a100f", "0x4a1001", "0x4a1002", "0x4a1003", "0x4a1004", "0x4a1005", "0x4a1006", "0x4a1007", "0x4a1008", "0x431000", "0x431009", "0x43100a", "0x43100b", "0x43100c", "0x43100d", "0x43100e", "0x43100f", "0x431001", "0x431002", "0x431003", "0x431004", "0x431005", "0x431006", "0x431007", "0x431008", "0x4b1000", "0x4b1001", "0x4b1002", "0x4b1003", "0x4b1004", "0x4b1005", "0x4b1006", "0x4b1007", "0x441000", "0x441009", "0x44100a", "0x44100b", "0x44100c", "0x44100d", "0x44100e", "0x44100f", "0x441001", "0x441002", "0x441003", "0x441004", "0x441005", "0x441006", "0x441007", "0x441008", "0x4c1000", "0x4c1001", "0x4c1002", "0x4c1003", "0x4c1004", "0x4c1005", "0x4c1006", "0x4c1007", "0x471000", "0x481000" ] # "0x4710016", "0x4810016" ]


def process(infile, SCinfo, outdir="."):
    started = dt.datetime.now()
    run = infile.split("/")[-1].split("_")[1].split(".root")[0]
    tree = U.open(infile)["LARDIGITS"]
    print("making arrays")
    tree = tree.arrays(["samples","latomeSourceId","channelId","latomeChannel","eta","phi","layer","bcidVec","bcidLATOMEHEAD","detector","pos_neg","barrel_ec"], library="np")

    print("Available branches:",tree.keys())
    stats = plotter.getStats(tree)

    detHists = {}
    hkey2 = "ALL"
    #if hkey2 in detHists.keys(): continue
    detHists[hkey2] = {}
    detHists[hkey2]["coverage"] = plotter.InitEtaPhiHist("ALL", 0, "ALL", "Coverage", "Coverage", None, None)                
    channels = list(dict.fromkeys(tree["channelId"]))

    latomes = list(dict.fromkeys(tree["latomeSourceId"]))
    print(len(channels), "channels and",len(latomes), "LATOMEs")
    print("LATOMEs are:",latomes)
    for lay in stats["poss_layer"]:
        events = np.where( tree["layer"] == lay )[0]
        eta = np.array([ tree["eta"][ev] for ev in events ], dtype='d')
        phi = np.array([ tree["phi"][ev] for ev in events ], dtype='d')
        w = np.ones_like(eta)
        
        detHists["ALL"]["coverage"].FillN(len(eta), eta, phi, w)
    
    print("Plotting hist")
    details = [ str(len(channels))+" channels, "+str(len(latomes))+" LATOMEs" ]

    #ids = [ int(s, 16) for s in sourceIDs ]
    hexlatomes = [ hex(l) for l in latomes ]
    if len(latomes) != 116:
        missing = [ i for i in sourceIDs if i not in hexlatomes ]
        extra = [ l for l in hexlatomes if l not in sourceIDs ]
        print(len(missing), "missing LATOMEs")
        print(missing)
        print(outdir+"/missingLATOMEs.txt")
        with open(outdir+"/missingLATOMEs.txt", "w") as f:
            f.write("LATOMEs which were not found in the ntuple \n")
            f.write("-"*40+"\n")
            for m in missing:
                f.write(m+"\n")

        if len(missing) > 5:
            subl = [missing[x:x+5] for x in range(0, len(missing), 5)]            
            details.append( "Missing LATOMEs: "+(", ").join(subl[0]) )
            #for s in subl[1:6]:
            #details.append(17*" "+(", ").join(s))
            details.append(17*" "+(", ").join(subl[1]))
            if len(missing) > 10:
                details.append("Full list in missingLATOMEs.txt")
        else:
            details.append( "Missing LATOMEs: "+(", ").join(missing) )
          
    plotter.plotDetHists(detHists, outdir, details=details)

    finished = dt.datetime.now()
    timeforjob = finished - started
    print( "Done - Time taken for job:", str(timeforjob) )


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Pass the input file as an argument")
        sys.exit()
    infile = sys.argv[1]
    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots_"+infile.split("/")[-2]

    plotter.chmkDir(outdir)


    process(infile, outdir)
