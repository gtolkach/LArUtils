import sys, os
import uproot4 as U
import datetime as dt
import plotter
import numpy as np
import ROOT as R

# mean amplitude - check if negative?
from BadChan import convertBadChanEnum, statusBad
enum = convertBadChanEnum(True)

def process(infilename, run, SCinfo, outdir=".", thresholds=None):

    run = infilename.split("/")[-1].split("_")[1].split(".root")[0]
    infile = U.open(infilename)
    print(infilename)
    treename = "CALIWAVE"
    posstrees = [ t for t in infile.keys() if t == treename or t == treename+";1" ]
    if len(posstrees) == 0:
        print("ERROR: tree", treename, "is not in the file",infilename)
        sys.exit()
    elif len(posstrees) > 1:
        print("WARNING: tree", treename, "may have a duplicate:", posstrees)
    tree = infile[posstrees[0]] 
    tree = tree.arrays(library="np")

    print("Available branches:",tree.keys())
    
    stats = plotter.getStats(tree)
    print(stats.keys())

    if thresholds is None:
        thresholds = {}
    detHists={}
    outliers = {}
    maxval = {}
    minval = {}
    chanGraphs = {}

    if thresholds is not None:
        for key in thresholds.keys():
            outliers[key] = {}

    for det in stats["poss_detector"]:
        if det == 2: continue  # same plot for IW and OW EMEC
        for side in stats["poss_pos_neg"]:
            for lay in stats["poss_layer"]:
                hkey = str(det)+"_"+str(side)+"_"+str(lay)
                if hkey in chanGraphs.keys(): continue
                chanGraphs[hkey] = {}
                chanGraphs[hkey]["Time_Amplitude"] = {}

    channels = list(dict.fromkeys(tree["channelId"]))
    print(len(channels), "channels")
    nwarn=0
    dets = []

    ch = -1

    badChan = {}

    for chanID in channels:
        ch += 1
        
        events = np.where( tree["channelId"] == chanID )[0]  # Need the 0 because it returns a tuple

        if "badChan" in tree.keys():
            badChan[chanID] = [ statusBad(tree["badChan"][ev], enum) for ev in events ][0]
        else:
            badChan[chanID] = []

        eta = [ tree["eta"][ev] for ev in events ][0]
        phi = [ tree["phi"][ev] for ev in events ][0]

        maxamp = [ tree["MaxAmp"][ev] for ev in events ][0]
        tmaxamp = [ tree["TmaxAmp"][ev] for ev in events ][0]
        layer = int([ tree["layer"][ev] for ev in events ][0])
        detector = int([ tree["detector"][ev] for ev in events ][0])
        side = str([ tree["pos_neg"][ev] for ev in events ][0])

        plotter.checkSCinfo(layer, "SAM", chanID, SCinfo, False)  # nwarn<10
        plotter.checkSCinfo(detector, "DET", chanID, SCinfo, False)  # oioi make this return 1 or 0 for warning

        #print(len(events))       
        thisdet = detector
        if detector not in dets: dets.append(detector)
        if thisdet == "2": thisdet = "1"
        hkey = str(thisdet)+"_"+str(side)+"_"+str(layer)
        for val in outliers.keys():
            if val in tree.keys():
                thisval = [ tree[val][ev] for ev in events ][0]
                if thisval < thresholds[val][0] or thisval > thresholds[val][1]:
                    if hkey not in outliers[val].keys():
                        outliers[val][hkey] = []
                    outliers[val][hkey].append([thisval,chanID])


        if hkey not in chanGraphs.keys(): continue 
        # Loop over the variables and fill the histograms
        for var in chanGraphs[hkey].keys():
            val1 = None
            val2 = None
            grvals = var.split("_")
            val1 = [tree[grvals[0]][ev] for ev in events]
            if len(val1) != 1:
                print("ERROR: weird length for values of",grvals[0])
                sys.exit()
            val1 = val1[0]
            if len(grvals)==2:
                val2 = [tree[grvals[1]][ev] for ev in events]
                if len(val2) != 1:
                    print("ERROR: weird length for values of",grvals[1])
                    sys.exit()
                val2 = val2[0]
                chanGraphs[hkey][var][chanID] = plotter.InitGr(thisdet, side, layer, chanID, hkey.replace("_", " "), grvals[0], grvals[1], val1, val2)
            else:
                chanGraphs[hkey][var][chanID] = plotter.InitGr(thisdet, side, layer, chanID, hkey.replace("_", " "), grvals[0], "Entries", val1, val2)
            if val2 is None:
                chanGraphs[hkey][var][chanID].SetPoint(chanGraphs[hkey][var].GetN(),
                                              np.arange(len(val1)), val1 )       

    for hkey in chanGraphs.keys():
        for var in chanGraphs[hkey].keys():
            if len(list(chanGraphs[hkey][var].keys())) == 0:
                continue
            detCanv = R.TCanvas()
            detCanv.objs = []
            R.gPad.SetBorderSize(0)
            R.gPad.SetBottomMargin(.3)
            R.gPad.SetLeftMargin(.08)
            theseoutliers = []
            for val in outliers.keys():
                if hkey in outliers[val].keys():
                    theseoutliers.append(str(len(outliers[val][hkey]))+" outlier(s) in "+val)
                

            gmax = max([ chanGraphs[hkey][var][ch].GetHistogram().GetMaximum() for ch in chanGraphs[hkey][var].keys() ])

            gmin = min([ chanGraphs[hkey][var][ch].GetHistogram().GetMinimum() for ch in chanGraphs[hkey][var].keys() ])
            for ch in chanGraphs[hkey][var].keys():
                if list(chanGraphs[hkey][var].keys()).index(ch) == 0:
                    chanGraphs[hkey][var][ch].GetHistogram().SetMaximum(gmax)
                    chanGraphs[hkey][var][ch].GetHistogram().SetMinimum(gmin)
                    chanGraphs[hkey][var][ch].Draw("APl")
                else:
                    chanGraphs[hkey][var][ch].Draw("same,Pl")
                detCanv.objs.append(chanGraphs[hkey][var][ch])
            if len(theseoutliers) > 0:
                l = R.TLatex()
                l.SetNDC()
                l.SetTextFont(42)
                l.SetTextColor(R.kBlack)
                l.SetTextSize(0.04)
                stringy=0.2
                for ol in theseoutliers:
                    stringy-=0.05
                    l.DrawLatex(0.01, stringy, ol)
                    detCanv.objs.append(l)

            detCanv.Print(outdir+"/detPlots_"+run+"_"+hkey+"_"+var+".png")
    






if __name__ == "__main__":
    started = dt.datetime.now()
    print("Started CaliWave job at "+str(started) )
    
    
    if len(sys.argv) < 2:
        print("Pass the input file as an argument")
        sys.exit()
    infile = sys.argv[1]
    if len(sys.argv) > 2:
        outdir  = sys.argv[2]
    else:
        outdir = "plots_"+infile.split("/")[-1]
    plotter.chmkDir(outdir)

    from steer import getSCinfo

    process(infile, getSCinfo, outdir)
