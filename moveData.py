import os, time, sys
import subprocess
from argparse import ArgumentParser
import shutil
from math import log

def check_directory_permissions(directory_path, verbose=False):
    read =  os.access(directory_path, os.R_OK)
    write = os.access(directory_path, os.W_OK)
    execute = os.access(directory_path, os.X_OK)
    if verbose:
        if read:
            print(f"Read permissions are granted for the directory: {directory_path}")
        else:
            print(f"Read permissions are not granted for the directory: {directory_path}")

        if write:
            print(f"Write permissions are granted for the directory: {directory_path}")
        else:
            print(f"Write permissions are not granted for the directory: {directory_path}")

        if execute:
            print(f"Execute permissions are granted for the directory: {directory_path}")
        else:
            print(f"Execute permissions are not granted for the directory: {directory_path}")
    return read, write, execute


def pretty_size(n,pow=0,b=1024,u='B',pre=['']+[p+'i'for p in'KMGTPEZY']):
    ''' From https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size '''
    pow,n=min(int(log(max(n*b**pow,1),b)),len(pre)-1),n*b**pow
    return "%%.%if %%s%%s"%abs(pow%(-pow-1))%(n/b**float(pow),pre[pow],u)


class dataDir():
    def __init__(self, path):
        if not os.path.isdir(path):
            print(f"ERROR: the requested dataDir path ({path}) is not a directory")
            sys.exit()
        if "/home-" in path:
            initial = path.split("/home-")[1].split("/")[0]
            path = path.replace("home-"+initial,"user/"+initial)
        if "/user/" in path:
            eosproject = "root://eosuser.cern.ch"
        elif "/atlas/" in path:
            eosproject = "root://eosatlas.cern.ch"
        else:
            print("Cannot decipher eos project from path",path)
            sys.exit()
        self.path = path
        self.eosproject = eosproject
        self.read, self.write, self.execute = check_directory_permissions(path)
        cmd = "eos "+self.eosproject+" quota -m "+self.path
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        alloutput = p.communicate()[0].decode('ascii').strip('\n')
        output_split = alloutput.split("\n")
        groupdata = [ o for o in output_split if "gid=" in o][0]
        output = groupdata.split(" ")
        percents = [ float(p.split("percentageusedbytes=")[1]) for p in output if p.startswith("percentageusedbytes")]
        self.percentUsed = max(percents)
        self.percentFree = 100 - self.percentUsed

    def __str__(self):
        return self.path

    def quota(self):
        print(f"{self.percentUsed}% of space on {self.path} is used")    

    def fileList(self, olderThanDays=None, fileOrDir=0):
        # fileOrDir 0 -> both, 1 -> files, 2 -> dirs
        now = time.time()
        files = []
        paths = [ self.path+"/"+l for l in  os.listdir(self.path) ]
        if fileOrDir == 1:
            paths = [ p for p in paths if os.path.isfile(p) ]
            printstr = "files"
        elif fileOrDir == 2: 
            paths = [ p for p in paths if os.path.isdir(p) ]
            printstr = "directories"
        else:
            printstr = "files & directories"
        if olderThanDays is not None:
            printstr += f" older than {olderThanDays} days old"
        for fullpath in paths:
            if olderThanDays is None:
                files.append(fullpath)
            else:
                filestamp = os.stat(fullpath).st_mtime
                n_days_ago = now - olderThanDays * 86400
                if filestamp < n_days_ago:
                    files.append(fullpath)
        print(len(files),printstr)
        return files


def main(args):
    sourceDir = dataDir(args.inputDir)


    sourceDir.quota()

    if args.sourceQuotaLim is not None:
        if sourceDir.percentUsed < args.sourceQuotaLim:
            print(f"The source directory is only {sourceDir.percentUsed}% full - this is less than the specified {args.sourceQuotaLim}%, therefore no action will be taken to move files")
            sys.exit()
        else:
            print(f"The source directory is {sourceDir.percentUsed}% full - this is more than the specified {args.sourceQuotaLim}%, therefore will try to proceed with migration")
    if not os.path.isdir(args.outputDir):
        print("WARNING: requested output directory does not exist - will try to make one")
        try:
            os.makedirs(args.outputDir)
        except Exception as e:
            print(e)
            sys.exit()
    destDir = dataDir(args.outputDir)
    destDir.quota()

    fileOrDir=0
    fStr = "files & directories"
    if args.filesOnly is True:
        fileOrDir=1
        fStr = "files"
    if args.dirsOnly is True:
        fileOrDir=2
        fStr = "directories"
    if args.filesOnly is True and args.dirsOnly is True:
        print("Both the --filesOnly and --dirsOnly options have been selected - this is contradictory, please choose one or the other (or neither)")
        sys.exit()
    nStr = ""
    if args.nDaysOld is not None:
        nStr = f"which are over {args.nDaysOld} days old"
    print(f"Getting list of {fStr} from within {sourceDir} {nStr}")
    filesToMove = sourceDir.fileList( args.nDaysOld, fileOrDir )
    
    brokenFile = args.outputDir+"/BROKEN"
    if os.path.isfile(brokenFile):
        print(f"The file {brokenFile} exists in the target directory! This indicates that there were problems with previous migration jobs. Please look at the contents of this file and address the problem(s). Once everything is addressed, delete this file so that the jobs can continue")
        sys.exit()
    
    if sourceDir.write is False or destDir.write is False:
        print("**** This account is not permitted to write to one or both of the folders involved")
        print(f"Write to {sourceDir}? {sourceDir.write}")
        print(f"Write to {destDir}? {destDir.write}")
        #sys.exit()
    totalSpace = 0
    for ftm in filesToMove:
        size = float(subprocess.check_output(['du','-sh', '-b', ftm]).split()[0].decode('utf-8'))
        print(f"{ftm}: {pretty_size(size)}")
        ftm = ftm.rstrip("/") # remove trailing slashes
        dest_path = args.outputDir+"/"+ftm.split(args.inputDir+"/")[1]
        problem = 0
        #try:
        #    shutil.move(ftm, dest_path)
        #except shutil.Error as e:
        #    print(f'ERROR while attempting mv {ftm} {dest_path}. Message is: {e}')
        #newsize = float(subprocess.check_output(['du','-sh', '-b', dest_path]).split()[0].decode('utf-8'))
        # newsize == size
        # rmtree
        # make BROKEN file
        problem=1
        if problem != 0:
            with open(brokenFile, 'w') as f:
                f.write('Create a new text file!')
            print(f"Problem with jobs! Please see {brokenFile} for details and address the issue(s)")
            sys.exit()
        totalSpace += size
        print(f"Processed {pretty_size(totalSpace)} thus far")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--inputDir', dest='inputDir', required=True, type=str, help='The input directory from which files must be migrated.')
    parser.add_argument('-o', '--outputDir', dest='outputDir', required=True, type=str, help='The output directory to which files must be migrated.')
    parser.add_argument('-n', '--nDaysOld', dest='nDaysOld', default=None, type=int, help='A minimum age, in days, of the files that should be eligible for migration. Optional.')
    parser.add_argument('-q', '--sourceQuotaLim', dest='sourceQuotaLim', default=None, type=float, help='A maximum quota for the source directory, above which we should perform the migration - in % of used disk')
    parser.add_argument('--filesOnly', dest='filesOnly',action='store_true', help='Only copy files - no directories')
    parser.add_argument('--dirsOnly', dest='dirsOnly',action='store_true', help='Only copy directories - no files from the top directory')
    
    args = parser.parse_args()

    main(args)
