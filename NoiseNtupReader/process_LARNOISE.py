import os, sys, glob, time
import pandas as pd
import numpy as np
import ROOT as R
import argparse
R.gROOT.SetBatch(True)

import uproot as U

def convertDet(det):
    dets = {0: "EMB", 1: "EMEC OW", 2: "EMEC IW", 3: "HEC", 4: "FCAL"}
    return dets[det]

def main(file_wildcard, treeName='CollectionTree'):
    rfiles = glob.glob(file_wildcard)

    totlen = 0
    dfs = []
    i = 0
    for arrays,report in U.iterate({r:treeName for r in rfiles}, report=True, step_size="1 GB",allow_missing=True,library="pd"):
        print("Reading chunk from:", report.file_path)
        arrlen = len(arrays.index.values)
        print(arrlen,"entries")
        totlen += arrlen
        dfs.append(arrays)
        if i == 0 : print(f"Branches are: {arrays.columns.values}")
        i+=1
    print(totlen, "entries total")
    if totlen == 0:
        print("Didn't get any entries from root files... are the root files ok?")
        sys.exit()

    #print("Merging")
    #df = pd.concat(dfs, ignore_index=True)
    #print("merged")
    #cats = sorted(list(df.algo.unique()))
    #
    # print(cats)
    # df = df.sort_values(by=['time_ns'])
    #print(df.columns)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('run', type=int, help='Run number - used for bad channels. Default is latest run.')
    parser.add_argument('-a', '--amiTag', dest='amiTag', type=str, default='f', help='AMI tag for the data. Can be simply "x" (for express processed) or "f" (for bulk processed". Default %(default)s')
    parser.add_argument('-t', '--tree', dest='tree', type=str, default='CollectionTree', help='Name of the tree which should be read from the file. Default %(default)s')
    args = parser.parse_args()


    stream = "physics_CosmicCalo"
    run = str(args.run).zfill(8)

    rootpath = "/eos/atlas/atlascerngroupdisk/det-larg/Tier0/perm/*/"+stream+"/"+run+"/*."+run+"."+stream+".merge.NTUP_LARNOISE."+args.amiTag+"*/*."+run+"."+stream+".merge.NTUP_LARNOISE."+args.amiTag+"*"
    print("Wildcard:",rootpath)
    rootfiles = glob.glob(rootpath)
    if len(rootfiles) == 0:
        print("Didn't find any ntuples for this wildcard... please check")
        sys.exit()
    print(f"{len(rootfiles)} ROOT files found")
    amis = sorted(list(set(([ r.split(".")[-3] for r in rootfiles]))))
    if len(amis) > 1:
        print("Picking up multiple AMI tags for the same run. Please run with a more specific --amiTag option - choose one of the following:")
        print(", ".join(amis))
        sys.exit()
    main(rootpath, args.tree)
