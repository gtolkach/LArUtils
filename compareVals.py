import uproot as U
from argparse import ArgumentParser
import printMapping
import pandas as pd

def processROOT(args, inf):
    """ Make df when the input is a root file """
    expr = None
    if args.branches is not None:
        expr = ["channelId"]
        expr.extend(args.branches)
    df = {}
    inFile = U.open(inf)
    treenames = inFile.keys()
    treenames = [ t.split(";")[0] if ";" in t else t for t in treenames ]
    print("AAAA",treenames)
    if args.trees is not None:
        treenames = [ t for t in treenames if t in args.trees ]

    for tree in treenames:
        df[tree] = inFile[tree].arrays(expressions=expr, library="pd")
        if args.channels is not None:
            df[tree] = df[tree].loc[(df[tree]['channelId'].isin(args.channels))]

    inFile.close()

    return df

def main(args):
    dfs = {}

    for inf in args.inputFiles:
        if inf.endswith(".root"):
            dfs[inf] = processROOT(args,inf)

    for f in dfs.keys():
        print("**",f,"**")
        for t in dfs[f].keys():
            print("--", t, "--")
            print(dfs[f][t])

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-i', '--inputFile', dest='inputFiles', help='Input files to compare', type=str, nargs='+', required=True)
    parser.add_argument('-t', '--trees', dest='trees', help='Trees that we care about from the files. If not provided, will compare all trees', type=str, nargs='+', default=None)
    parser.add_argument('-b', '--branches', dest='branches', help='Branches that we care about from the files. If not provided, will compare all branches', type=str, nargs='+', default=None)
    parser.add_argument('-c', '--channels', dest='channels', help='Channels that we want to compare. If not provided, will compare all channels', type=int, nargs='+', default=None)
    args = parser.parse_args()
    main(args)
