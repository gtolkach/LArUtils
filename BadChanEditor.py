#!/bin/python
import sys,os
import argparse


def getFEBinfoFromOnId( onId ):
    onId = int(onId, 16)
    pn = int( ((onId)>>24)&0x1 )
    benc = int( ((onId)>> 25)&0x1 )
    ft = int( ((onId)>>19)&0x1F )
    sl = int( (((onId)>>15)&0xF)+1 )
    chan = int( ((onId)>>8)&0x7F )
    #return getFebStr(benc, pn, ft, sl)
    return benc, pn, ft, sl, chan


def febOnIDChans(febOnId):
    """Get online IDs for channels from FEB online ID"""
    onIds = []
    febOnId = int(febOnId, 16)
    for ch in range(1, 128):
        onId = febOnId+(ch<<8)
        # print(hex(febOnId), ch, onId, hex(onId))
        onIds.append(hex(onId))
    return onIds

def bcstr_todict(bci, asdict=False):
    """Convert bad channel string to dictionary"""
    b = bci.split()
    benc = int(b[0])
    pn = int(b[1])
    ft = int(b[2])
    slot = int(b[3])
    channel = int(b[4])
    calib = int(b[5])
    status = [b[6]]
    bnum=7
    statdone=False
    while statdone is False:
        if bnum == len(b):
            statdone = None
        elif b[bnum] == "#":
            statdone = True
            bnum+=1
        else:
            status.append(b[bnum])
            bnum+=1
    if statdone is not None:
        onlID = b[bnum]
        if "->" in bci:
            bnum+=2 # ->
            offID = b[bnum]
        else:
            offID = None
    else:
        onlID = None
        offID = None
    if asdict is False:
        return benc,pn,ft,slot,channel,calib,status,onlID,offID
    else:
        return {"benc":benc,"pn":pn,"ft":ft,"slot":slot,"channel":channel,"calib":calib,"status":status,"onlID":onlID,"offID":offID}

    
def makeStr(benc, pn, ft, sl, chan, status, onid=None, offid=None):
    """Create string in correct format using bad channel info"""
    thisstr = str(benc)+" "+str(pn)+" "+str(ft)+" "+str(sl)+" "+str(chan)+" 0 "+(" ").join(status)
    if onid is not None and offid is not None:
        thisstr+="  # "+str(onid)+" -> "+str(offid)
    return thisstr+"\n"

def makeStrFromDict(thisdict):
    """Create string in correct format using bad channel info"""
    thisstr = str(thisdict["benc"])+" "+str(thisdict["pn"])+" "+str(thisdict["ft"])+" "+str(thisdict["slot"])+" "+str(thisdict["channel"])+" 0 "+(" ").join(thisdict["status"])
    if thisdict["onlID"] is not None and thisdict["offID"] is not None:
        thisstr+="  # "+str(thisdict["onlID"])+" -> "+str(thisdict["offID"])
    return thisstr+"\n"


def fillList(infile, fillDict=False, flip=False):
    if fillDict:
        lines={}
    else:
        lines=[]
    with open(infile,"r") as f:
        for line in f:
            if fillDict:
                l = line.split()
                if len(l)<=1:
                    print("can't turn",l,"into a dict")
                    sys.exit()
                elif len(l)>2:
                    print(l,"is too long to turn to a dict (?)")
                    continue
                if flip:
                    lines[l[1]] = l[0]
                else:
                    lines[l[0]] = l[1]
            else:
                lines.append(line)
    return lines


def implementFEBSwaps(badChanInfo):
    swaps_old_new = fillList("swaps_onlineID.txt", True, True)
    print(len(swaps_old_new), "swapped febs")
    swaps_old_new_ch = {}
    # Convert swapped febs to swapped chans
    for sw in range(0, len(swaps_old_new)):
        oldchans = febOnIDChans(list(swaps_old_new.keys())[sw])
        newchans = febOnIDChans(list(swaps_old_new.values())[sw])
        these_swaps_ch = dict(zip(oldchans, newchans))
        swaps_old_new_ch.update(these_swaps_ch)
    swaps_old_new.update(swaps_old_new_ch)
    print("~",len(swaps_old_new),"swapped channels")
    # Get mapping of online to offline IDs
    onID_offID_map = fillList("OffID_OnlID_ALL.txt", True, True)

    unswapped_status = ["deadCalib", "deadPhys"]
    swapcount=0
    # open new file to write out?

    outlines = []
    outlines_annotated = []
    outliers = [ [0,1,9,1,42], [0,1,4,12,12], [0,1,25,10,48], [0,1,20,8,116] ] # issues with bad channel category, need to be investigated
    outliers.append( [0,1,25,10,48] )  # added to list after installation
    outliers.append( [0,1,4,2,95] ) # added to list after installation
    for bci in badChanInfo:
        if any(us in bci for us in unswapped_status):
            outlines.append(bci)
            outlines_annotated.append(bci)
            #outfile.write(bci)
            continue
        b = bci.split()
        benc,pn,ft,slot,channel,calib,status,onlid,offid = bcstr_todict(bci)
        if onlid not in swaps_old_new.keys():
            outlines.append(bci)
            outlines_annotated.append(bci)
            #outfile.write(bci)
            continue
        if [benc,pn,ft,slot,channel] in outliers: # one of the weird channels that may need to be re-classified
            outlines.append(bci)
            outlines_annotated.append(bci)
            continue
        swapcount+=1

        newonlid = swaps_old_new[onlid]
        newoffid = onID_offID_map[newonlid]
        newbenc, newpn, newft, newsl, newchan = getFEBinfoFromOnId( newonlid )
        oldbenc, oldpn, oldft, oldsl, oldchan = getFEBinfoFromOnId( onlid )
        thisline = makeStr(newbenc, newpn, newft, newsl, newchan, status, newonlid, newoffid)
        oldline = " WAS "+makeStr(oldbenc, oldpn, oldft, oldsl, oldchan, status, onlid, offid)        
        outlines.append(thisline)
        outlines_annotated.append(thisline.strip("\n")+oldline)
    print(swapcount,"entries were swapped")
    return outlines, outlines_annotated



class BadChannelsList():
    def __init__(self,infile):
        self.infile = infile
        self.lines = fillList(infile)

    def addOrRemoveLine(self,linestr,remove=False):
        thisbenc,thispn,thisft,thissl,thischan,thiscal,thisstat,thisonlid,thisoffid = bcstr_todict(linestr)    
        for bci in self.lines:
            benc,pn,ft,sl,chan,cal,stat,onlid,offid = bcstr_todict(bci)            
            if thisbenc==benc and thispn==pn and thisft==ft and thissl==sl and thischan==chan:
                if remove is False:
                    print("addOrRemoveLine: requested addition is already in file! Skipping it")
                    return
                else:
                    print("Removing line",bci.strip("\n"))
                    self.lines.remove(bci)
                    return
            else:
                continue
        if remove:
            print("addOrRemoveLine: requested removal is not present in the provided bad channels file")
            return
        else:
            thisline=makeStr(thisbenc, thispn, thisft, thissl, thischan, thisstat, thisonlid, thisoffid)
            print("Adding line",thisline.strip("\n"))
            self.lines.append(thisline)
            return


    def switchScanLines(self, tofind, torepl=None):
        lines_annotated = self.lines.copy()
        nswitched = 0
        nfound = 0
        tofind = {tf.split("=")[0].lower().strip():tf.split("=")[1].strip() for tf in tofind}
        def convdict(thisdict):
            if "status" in thisdict.keys():
                thisdict["status"]=thisdict["status"].split(",")
            for k in ["benc","pn","ft","slot","channel","calib"]:
                if k in thisdict.keys():
                    thisdict[k]=int(thisdict[k])
        convdict(tofind)
        if torepl is not None:
            torepl = {tr.split("=")[0].lower().strip():tr.split("=")[1].strip() for tr in torepl}
            convdict(torepl)
        for bci in self.lines:
            linedict= bcstr_todict(bci,True)
            if all(item in linedict.items() for item in tofind.items()):
                if torepl is None:
                    print(bci.strip("\n"))
                    nfound+=1
                    continue
                
                oldline = " WAS "+makeStrFromDict(linedict)
                print("switchScanLines: found a match",bci.strip("\n"))
                for key,value in torepl.items():
                    print("switchScanLines: replacing",key,"=",linedict[key],"with",value)
                    linedict[key] = value
                nswitched+=1
                thisline=makeStrFromDict(linedict)
                self.lines.remove(bci)
                self.lines.append(thisline)
                lines_annotated.remove(bci)
                lines_annotated.append(thisline.strip("\n")+oldline)

        if torepl is not None and nswitched != 0:
            print("switchScanLines:",nswitched,"lines changed")
            return lines_annotated
        else:
            if torepl is None:
                print("switchScanLines: found",nfound,"matching entry/entries")
            else:
                print("switchScanLines: NO MATCHING LINES FOUND FOR SWAPPING")
            return None
def main():
    print("Reading in bad channels from",args.infile)
    bcl = BadChannelsList(args.infile)
    print(len(bcl.lines), "bad channels")
    outlines=None
    outlines_annotated=None
    if args.doSwaps:
        outlines, outlines_annotated = implementFEBSwaps(bcl.lines)
        
    if args.addLine is not None:
        bcl.addOrRemoveLine(args.addLine)
        print(len(bcl.lines),"bad channels after addition")
    if args.rmLine is not None:
        bcl.addOrRemoveLine(args.rmLine, True)
        print(len(bcl.lines),"bad channels after removal")
    if args.switchEntries is not None:
        tofind = args.switchEntries.split("->")[0].split("&&")
        torepl = args.switchEntries.split("->")[1].split("&&")
        outlines_annotated=bcl.switchScanLines(tofind,torepl)
    if args.scanEntries is not None:
        tofind = args.scanEntries.split("&&")
        print("---- matching entries ----")
        bcl.switchScanLines(tofind)
        print("--------------------------")
    if outlines is None:
        outlines = bcl.lines
   
    def listToFile(lines, f):
        lines.sort()
        for l in lines:
            f.write(l)
        f.close()
    outfile = open(args.outfile,"w+")           
    listToFile(outlines,outfile)
    if outlines_annotated is not None:
        outfile_annotated = open(args.outfile.replace(".txt","_annotated.txt"),"w+")
        listToFile(outlines_annotated,outfile_annotated)
    print(len(outlines),"lines written out to file")
        
if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Create a new bad channels file based on swapped FEBs or some new info')
    parser.add_argument('-i', dest='infile', default='bad_chan.txt', help='input bad channels file')
    parser.add_argument('-o', dest='outfile',  default='Newbadchan.txt', help='output bad channels file')
    parser.add_argument('-doSwaps', dest='doSwaps', default=False, action='store_true', help='implement swapped FEBs lists')
    parser.add_argument('-add', dest='addLine', default=None, help='line to add to the bad channels list')
    parser.add_argument('-rem', dest='rmLine', default=None, help='line to remove from the bad channels list')
    parser.add_argument('-switch', dest='switchEntries', default=None, help='string containing description of entries to switch, e.g. FT=4&&slot=1&&status=deadCalib->status=deadPhys')
    parser.add_argument('-scan', dest='scanEntries', default=None, help='string containing entries to scan, e.g. FT=4&&status=deadCalib')
    global args
    args  = parser.parse_args()
        
    main()
